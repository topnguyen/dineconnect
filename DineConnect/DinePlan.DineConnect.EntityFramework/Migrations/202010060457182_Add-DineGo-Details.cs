namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddDineGoDetails : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.ProgramSettings", newName: "ProgramSettingValues");
            RenameTable(name: "dbo.ProductComboJoins", newName: "ProductComboGroupDetails");
            DropForeignKey("dbo.DinePlanTaxLocations", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.DinePlanTaxLocations", "TransactionTypeId", "dbo.TransactionTypes");
            DropIndex("dbo.DinePlanTaxLocations", new[] { "LocationRefId" });
            DropIndex("dbo.DinePlanTaxLocations", new[] { "TransactionTypeId" });
            CreateTable(
                "dbo.DineGoBrands",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Label = c.String(),
                        LocationId = c.Int(nullable: false),
                        ScreenMenuId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DineGoBrand_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DineGoBrand_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .ForeignKey("dbo.ScreenMenus", t => t.ScreenMenuId, cascadeDelete: true)
                .Index(t => t.LocationId)
                .Index(t => t.ScreenMenuId);
            
            CreateTable(
                "dbo.DineGoDepartments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Label = c.String(),
                        DepartmentId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DineGoDepartment_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DineGoDepartment_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Departments", t => t.DepartmentId, cascadeDelete: true)
                .Index(t => t.DepartmentId);
            
            CreateTable(
                "dbo.DineGoCharges",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Label = c.String(),
                        IsPercent = c.Boolean(nullable: false),
                        TaxIncluded = c.Boolean(nullable: false),
                        ChargeValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransactionTypeId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DineGoCharge_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DineGoCharge_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TransactionTypes", t => t.TransactionTypeId, cascadeDelete: true)
                .Index(t => t.TransactionTypeId);
            
            CreateTable(
                "dbo.DineGoDevices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        BannerImage = c.String(),
                        AdsImage = c.String(),
                        IdleTime = c.Int(nullable: false),
                        ThemeSettings = c.String(),
                        OrderLink = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DineGoDevice_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DineGoDevice_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DineGoPaymentTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Label = c.String(),
                        PaymentTypeId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DineGoPaymentType_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DineGoPaymentType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PaymentTypes", t => t.PaymentTypeId, cascadeDelete: true)
                .Index(t => t.PaymentTypeId);
            
            CreateTable(
                "dbo.PrintConfigurations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        TenantId = c.Int(nullable: false),
                        PrintConfigurationType = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PrintConfiguration_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PrintConfiguration_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DineGoDepartmentChargeDetails",
                c => new
                    {
                        DineGoDepartmentId = c.Int(nullable: false),
                        DineGoChargeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.DineGoDepartmentId, t.DineGoChargeId })
                .ForeignKey("dbo.DineGoDepartments", t => t.DineGoDepartmentId, cascadeDelete: true)
                .ForeignKey("dbo.DineGoCharges", t => t.DineGoChargeId, cascadeDelete: true)
                .Index(t => t.DineGoDepartmentId)
                .Index(t => t.DineGoChargeId);
            
            CreateTable(
                "dbo.DineGoBrandDepartmentDetails",
                c => new
                    {
                        DineGoBrandId = c.Int(nullable: false),
                        DineGoDepartmentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.DineGoBrandId, t.DineGoDepartmentId })
                .ForeignKey("dbo.DineGoBrands", t => t.DineGoBrandId, cascadeDelete: true)
                .ForeignKey("dbo.DineGoDepartments", t => t.DineGoDepartmentId, cascadeDelete: true)
                .Index(t => t.DineGoBrandId)
                .Index(t => t.DineGoDepartmentId);
            
            CreateTable(
                "dbo.DineGoDeviceBrandDetails",
                c => new
                    {
                        DineGoDeviceId = c.Int(nullable: false),
                        DineGoBrandId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.DineGoDeviceId, t.DineGoBrandId })
                .ForeignKey("dbo.DineGoDevices", t => t.DineGoDeviceId, cascadeDelete: true)
                .ForeignKey("dbo.DineGoBrands", t => t.DineGoBrandId, cascadeDelete: true)
                .Index(t => t.DineGoDeviceId)
                .Index(t => t.DineGoBrandId);
            
            CreateTable(
                "dbo.DineGoDevicePaymentTypeDetails",
                c => new
                    {
                        DineGoDeviceId = c.Int(nullable: false),
                        DineGoPaymentTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.DineGoDeviceId, t.DineGoPaymentTypeId })
                .ForeignKey("dbo.DineGoDevices", t => t.DineGoDeviceId, cascadeDelete: true)
                .ForeignKey("dbo.DineGoPaymentTypes", t => t.DineGoPaymentTypeId, cascadeDelete: true)
                .Index(t => t.DineGoDeviceId)
                .Index(t => t.DineGoPaymentTypeId);
            
            AlterTableAnnotations(
                "dbo.DinePlanTaxLocations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationRefId = c.Int(nullable: false),
                        DinePlanTaxRefId = c.Int(nullable: false),
                        TaxPercentage = c.Decimal(nullable: false, precision: 18, scale: 3),
                        TransactionTypeId = c.Int(),
                        Tag = c.String(maxLength: 50),
                        Locations = c.String(),
                        NonLocations = c.String(),
                        Group = c.Boolean(nullable: false),
                        LocationTag = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_DinePlanTaxLocation_MustHaveTenant",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.ProductComboGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SortOrder = c.Int(nullable: false),
                        Minimum = c.Int(nullable: false),
                        Maximum = c.Int(nullable: false),
                        ProductComboId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ProductComboGroup_MustHaveTenant",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                    { 
                        "DynamicFilter_ProductComboGroup_SoftDelete",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.ProgramSettingValues",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProgramSettingTemplateId = c.Int(nullable: false),
                        ActualValue = c.String(),
                        Locations = c.String(),
                        NonLocations = c.String(),
                        Group = c.Boolean(nullable: false),
                        LocationTag = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ProgramSetting_MustHaveTenant",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                    { 
                        "DynamicFilter_ProgramSetting_SoftDelete",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                    { 
                        "DynamicFilter_ProgramSettingValue_MustHaveTenant",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                    { 
                        "DynamicFilter_ProgramSettingValue_SoftDelete",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.ProgramSettingTemplates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DefaultValue = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ProgramSettingTemplate_MustHaveTenant",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AddColumn("dbo.Locations", "ReferenceCode", c => c.String());
            AddColumn("dbo.DinePlanTaxes", "TransactionTypeId", c => c.Int());
            AddColumn("dbo.UpMenuItems", "MaxQty", c => c.Int(nullable: false));
            AddColumn("dbo.DinePlanTaxLocations", "Locations", c => c.String());
            AddColumn("dbo.DinePlanTaxLocations", "NonLocations", c => c.String());
            AddColumn("dbo.DinePlanTaxLocations", "Group", c => c.Boolean(nullable: false));
            AddColumn("dbo.DinePlanTaxLocations", "LocationTag", c => c.Boolean(nullable: false));
            AddColumn("dbo.DinePlanTaxLocations", "TenantId", c => c.Int(nullable: false));
            AddColumn("dbo.DinePlanTaxMappings", "DinePlanTaxLocationId", c => c.Int(nullable: false));
            Sql("Update DinePlanTaxMappings set DinePlanTaxLocationId = DinePlanTaxLocationRefId");

            AddColumn("dbo.PaymentTypes", "AutoPayment", c => c.Boolean(nullable: false));
            AddColumn("dbo.Printers", "PrintConfigurationId", c => c.Int(nullable: false));
            AddColumn("dbo.PrintJobs", "PrintConfigurationId", c => c.Int(nullable: false));
            AddColumn("dbo.PrintTemplates", "PrintConfigurationId", c => c.Int(nullable: false));
            AddColumn("dbo.PrintTemplateConditions", "PrintConfigurationId", c => c.Int(nullable: false));
            AddColumn("dbo.ProductComboGroups", "TenantId", c => c.Int(nullable: false));
            Sql("UPDATE ProductComboGroups SET TenantId = t2.TenantId FROM ProductComboGroupDetails T1 INNER JOIN ProductComboes t3 on t1.ProductComboId = t3.Id INNER JOIN MenuItems T2 on t2.Id = t3.MenuItemId");

            AddColumn("dbo.ProductComboGroups", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.ProductComboGroups", "DeleterUserId", c => c.Long());
            AddColumn("dbo.ProductComboGroups", "DeletionTime", c => c.DateTime());
            AddColumn("dbo.ProductComboGroups", "LastModificationTime", c => c.DateTime());
            AddColumn("dbo.ProductComboGroups", "LastModifierUserId", c => c.Long());
            AddColumn("dbo.ProgramSettingTemplates", "TenantId", c => c.Int(nullable: false));
            AlterColumn("dbo.GiftVoucherTypes", "Locations", c => c.String());
            AlterColumn("dbo.GiftVoucherTypes", "NonLocations", c => c.String());
            AlterColumn("dbo.MenuItems", "Locations", c => c.String());
            AlterColumn("dbo.MenuItems", "NonLocations", c => c.String());
            AlterColumn("dbo.PriceTags", "Locations", c => c.String());
            AlterColumn("dbo.PriceTags", "NonLocations", c => c.String());
            AlterColumn("dbo.DinePlanUsers", "Locations", c => c.String());
            AlterColumn("dbo.DinePlanUsers", "NonLocations", c => c.String());
            AlterColumn("dbo.Departments", "Locations", c => c.String());
            AlterColumn("dbo.Departments", "NonLocations", c => c.String());
            AlterColumn("dbo.Calculations", "Locations", c => c.String());
            AlterColumn("dbo.Calculations", "NonLocations", c => c.String());
            AlterColumn("dbo.ConnectCardTypes", "Locations", c => c.String());
            AlterColumn("dbo.ConnectCardTypes", "NonLocations", c => c.String());
            AlterColumn("dbo.ConnectTableGroups", "Locations", c => c.String());
            AlterColumn("dbo.ConnectTableGroups", "NonLocations", c => c.String());
            AlterColumn("dbo.Promotions", "Locations", c => c.String());
            AlterColumn("dbo.Promotions", "NonLocations", c => c.String());
            AlterColumn("dbo.PaymentTypes", "Locations", c => c.String());
            AlterColumn("dbo.PaymentTypes", "NonLocations", c => c.String());
            AlterColumn("dbo.FullTaxMembers", "Locations", c => c.String());
            AlterColumn("dbo.FullTaxMembers", "NonLocations", c => c.String());
            AlterColumn("dbo.FullTaxInvoices", "Locations", c => c.String());
            AlterColumn("dbo.FullTaxInvoices", "NonLocations", c => c.String());
            AlterColumn("dbo.FutureDateInformations", "Locations", c => c.String());
            AlterColumn("dbo.FutureDateInformations", "NonLocations", c => c.String());
            AlterColumn("dbo.ImportSettings", "Locations", c => c.String());
            AlterColumn("dbo.ImportSettings", "NonLocations", c => c.String());
            AlterColumn("dbo.OrderTagGroups", "Locations", c => c.String());
            AlterColumn("dbo.OrderTagGroups", "NonLocations", c => c.String());
            AlterColumn("dbo.PlanReasons", "Locations", c => c.String());
            AlterColumn("dbo.PlanReasons", "NonLocations", c => c.String());
            AlterColumn("dbo.Printers", "Locations", c => c.String());
            AlterColumn("dbo.Printers", "NonLocations", c => c.String());
            AlterColumn("dbo.PrintJobs", "Locations", c => c.String());
            AlterColumn("dbo.PrintJobs", "NonLocations", c => c.String());
            AlterColumn("dbo.PrintTemplates", "Locations", c => c.String());
            AlterColumn("dbo.PrintTemplates", "NonLocations", c => c.String());
            AlterColumn("dbo.PrintTemplateConditions", "Locations", c => c.String());
            AlterColumn("dbo.PrintTemplateConditions", "NonLocations", c => c.String());
            AlterColumn("dbo.ProgramSettingValues", "Locations", c => c.String());
            AlterColumn("dbo.ProgramSettingValues", "NonLocations", c => c.String());
            AlterColumn("dbo.ScreenMenus", "Locations", c => c.String());
            AlterColumn("dbo.ScreenMenus", "NonLocations", c => c.String());
            AlterColumn("dbo.Terminals", "Locations", c => c.String());
            AlterColumn("dbo.Terminals", "NonLocations", c => c.String());
            AlterColumn("dbo.TicketTagGroups", "Locations", c => c.String());
            AlterColumn("dbo.TicketTagGroups", "NonLocations", c => c.String());
            AlterColumn("dbo.TickMessages", "Locations", c => c.String());
            AlterColumn("dbo.TickMessages", "NonLocations", c => c.String());
            AlterColumn("dbo.TillAccounts", "Locations", c => c.String());
            AlterColumn("dbo.TillAccounts", "NonLocations", c => c.String());
            CreateIndex("dbo.DinePlanTaxes", "TransactionTypeId");
            CreateIndex("dbo.DinePlanTaxLocations", "DinePlanTaxRefId");
            CreateIndex("dbo.DinePlanTaxMappings", "DinePlanTaxLocationId");
            AddForeignKey("dbo.DinePlanTaxes", "TransactionTypeId", "dbo.TransactionTypes", "Id");
            AddForeignKey("dbo.DinePlanTaxLocations", "DinePlanTaxRefId", "dbo.DinePlanTaxes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DinePlanTaxMappings", "DinePlanTaxLocationId", "dbo.DinePlanTaxLocations", "Id", cascadeDelete: true);
            DropColumn("dbo.Locations", "Currency");
            DropColumn("dbo.Locations", "Settings");
            DropColumn("dbo.Printers", "Name");
            DropColumn("dbo.PrintJobs", "Name");
            DropColumn("dbo.PrintTemplates", "Name");
            DropColumn("dbo.PrintTemplates", "PrintTemplateType");
            DropColumn("dbo.PrintTemplateConditions", "Name");
            DropColumn("dbo.ProgramSettingTemplates", "ProgramSettingType");
            DropColumn("dbo.ProgramSettingTemplates", "PossibleValues");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProgramSettingTemplates", "PossibleValues", c => c.String());
            AddColumn("dbo.ProgramSettingTemplates", "ProgramSettingType", c => c.Int(nullable: false));
            AddColumn("dbo.PrintTemplateConditions", "Name", c => c.String());
            AddColumn("dbo.PrintTemplates", "PrintTemplateType", c => c.Int(nullable: false));
            AddColumn("dbo.PrintTemplates", "Name", c => c.String());
            AddColumn("dbo.PrintJobs", "Name", c => c.String());
            AddColumn("dbo.Printers", "Name", c => c.String());
            AddColumn("dbo.Locations", "Settings", c => c.String());
            AddColumn("dbo.Locations", "Currency", c => c.String());
            DropForeignKey("dbo.DinePlanTaxMappings", "DinePlanTaxLocationId", "dbo.DinePlanTaxLocations");
            DropForeignKey("dbo.DinePlanTaxLocations", "DinePlanTaxRefId", "dbo.DinePlanTaxes");
            DropForeignKey("dbo.DineGoBrands", "ScreenMenuId", "dbo.ScreenMenus");
            DropForeignKey("dbo.DineGoBrands", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.DineGoDevicePaymentTypeDetails", "DineGoPaymentTypeId", "dbo.DineGoPaymentTypes");
            DropForeignKey("dbo.DineGoDevicePaymentTypeDetails", "DineGoDeviceId", "dbo.DineGoDevices");
            DropForeignKey("dbo.DineGoPaymentTypes", "PaymentTypeId", "dbo.PaymentTypes");
            DropForeignKey("dbo.DineGoDeviceBrandDetails", "DineGoBrandId", "dbo.DineGoBrands");
            DropForeignKey("dbo.DineGoDeviceBrandDetails", "DineGoDeviceId", "dbo.DineGoDevices");
            DropForeignKey("dbo.DineGoBrandDepartmentDetails", "DineGoDepartmentId", "dbo.DineGoDepartments");
            DropForeignKey("dbo.DineGoBrandDepartmentDetails", "DineGoBrandId", "dbo.DineGoBrands");
            DropForeignKey("dbo.DineGoDepartmentChargeDetails", "DineGoChargeId", "dbo.DineGoCharges");
            DropForeignKey("dbo.DineGoDepartmentChargeDetails", "DineGoDepartmentId", "dbo.DineGoDepartments");
            DropForeignKey("dbo.DineGoCharges", "TransactionTypeId", "dbo.TransactionTypes");
            DropForeignKey("dbo.DineGoDepartments", "DepartmentId", "dbo.Departments");
            DropForeignKey("dbo.DinePlanTaxes", "TransactionTypeId", "dbo.TransactionTypes");
            DropIndex("dbo.DineGoDevicePaymentTypeDetails", new[] { "DineGoPaymentTypeId" });
            DropIndex("dbo.DineGoDevicePaymentTypeDetails", new[] { "DineGoDeviceId" });
            DropIndex("dbo.DineGoDeviceBrandDetails", new[] { "DineGoBrandId" });
            DropIndex("dbo.DineGoDeviceBrandDetails", new[] { "DineGoDeviceId" });
            DropIndex("dbo.DineGoBrandDepartmentDetails", new[] { "DineGoDepartmentId" });
            DropIndex("dbo.DineGoBrandDepartmentDetails", new[] { "DineGoBrandId" });
            DropIndex("dbo.DineGoDepartmentChargeDetails", new[] { "DineGoChargeId" });
            DropIndex("dbo.DineGoDepartmentChargeDetails", new[] { "DineGoDepartmentId" });
            DropIndex("dbo.DinePlanTaxMappings", new[] { "DinePlanTaxLocationId" });
            DropIndex("dbo.DinePlanTaxLocations", new[] { "DinePlanTaxRefId" });
            DropIndex("dbo.DineGoPaymentTypes", new[] { "PaymentTypeId" });
            DropIndex("dbo.DineGoCharges", new[] { "TransactionTypeId" });
            DropIndex("dbo.DineGoDepartments", new[] { "DepartmentId" });
            DropIndex("dbo.DineGoBrands", new[] { "ScreenMenuId" });
            DropIndex("dbo.DineGoBrands", new[] { "LocationId" });
            DropIndex("dbo.DinePlanTaxes", new[] { "TransactionTypeId" });
            AlterColumn("dbo.TillAccounts", "NonLocations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.TillAccounts", "Locations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.TickMessages", "NonLocations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.TickMessages", "Locations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.TicketTagGroups", "NonLocations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.TicketTagGroups", "Locations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.Terminals", "NonLocations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.Terminals", "Locations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.ScreenMenus", "NonLocations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.ScreenMenus", "Locations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.ProgramSettingValues", "NonLocations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.ProgramSettingValues", "Locations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.PrintTemplateConditions", "NonLocations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.PrintTemplateConditions", "Locations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.PrintTemplates", "NonLocations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.PrintTemplates", "Locations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.PrintJobs", "NonLocations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.PrintJobs", "Locations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.Printers", "NonLocations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.Printers", "Locations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.PlanReasons", "NonLocations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.PlanReasons", "Locations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.OrderTagGroups", "NonLocations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.OrderTagGroups", "Locations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.ImportSettings", "NonLocations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.ImportSettings", "Locations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.FutureDateInformations", "NonLocations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.FutureDateInformations", "Locations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.FullTaxInvoices", "NonLocations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.FullTaxInvoices", "Locations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.FullTaxMembers", "NonLocations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.FullTaxMembers", "Locations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.PaymentTypes", "NonLocations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.PaymentTypes", "Locations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.Promotions", "NonLocations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.Promotions", "Locations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.ConnectTableGroups", "NonLocations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.ConnectTableGroups", "Locations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.ConnectCardTypes", "NonLocations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.ConnectCardTypes", "Locations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.Calculations", "NonLocations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.Calculations", "Locations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.Departments", "NonLocations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.Departments", "Locations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.DinePlanUsers", "NonLocations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.DinePlanUsers", "Locations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.PriceTags", "NonLocations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.PriceTags", "Locations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.MenuItems", "NonLocations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.MenuItems", "Locations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.GiftVoucherTypes", "NonLocations", c => c.String(maxLength: 1000));
            AlterColumn("dbo.GiftVoucherTypes", "Locations", c => c.String(maxLength: 1000));
            DropColumn("dbo.ProgramSettingTemplates", "TenantId");
            DropColumn("dbo.ProductComboGroups", "LastModifierUserId");
            DropColumn("dbo.ProductComboGroups", "LastModificationTime");
            DropColumn("dbo.ProductComboGroups", "DeletionTime");
            DropColumn("dbo.ProductComboGroups", "DeleterUserId");
            DropColumn("dbo.ProductComboGroups", "IsDeleted");
            DropColumn("dbo.ProductComboGroups", "TenantId");
            DropColumn("dbo.PrintTemplateConditions", "PrintConfigurationId");
            DropColumn("dbo.PrintTemplates", "PrintConfigurationId");
            DropColumn("dbo.PrintJobs", "PrintConfigurationId");
            DropColumn("dbo.Printers", "PrintConfigurationId");
            DropColumn("dbo.PaymentTypes", "AutoPayment");
            DropColumn("dbo.DinePlanTaxMappings", "DinePlanTaxLocationId");
            DropColumn("dbo.DinePlanTaxLocations", "TenantId");
            DropColumn("dbo.DinePlanTaxLocations", "LocationTag");
            DropColumn("dbo.DinePlanTaxLocations", "Group");
            DropColumn("dbo.DinePlanTaxLocations", "NonLocations");
            DropColumn("dbo.DinePlanTaxLocations", "Locations");
            DropColumn("dbo.UpMenuItems", "MaxQty");
            DropColumn("dbo.DinePlanTaxes", "TransactionTypeId");
            DropColumn("dbo.Locations", "ReferenceCode");
            AlterTableAnnotations(
                "dbo.ProgramSettingTemplates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DefaultValue = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ProgramSettingTemplate_MustHaveTenant",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.ProgramSettingValues",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProgramSettingTemplateId = c.Int(nullable: false),
                        ActualValue = c.String(),
                        Locations = c.String(),
                        NonLocations = c.String(),
                        Group = c.Boolean(nullable: false),
                        LocationTag = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ProgramSetting_MustHaveTenant",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                    { 
                        "DynamicFilter_ProgramSetting_SoftDelete",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                    { 
                        "DynamicFilter_ProgramSettingValue_MustHaveTenant",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                    { 
                        "DynamicFilter_ProgramSettingValue_SoftDelete",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.ProductComboGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SortOrder = c.Int(nullable: false),
                        Minimum = c.Int(nullable: false),
                        Maximum = c.Int(nullable: false),
                        ProductComboId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ProductComboGroup_MustHaveTenant",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                    { 
                        "DynamicFilter_ProductComboGroup_SoftDelete",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.DinePlanTaxLocations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationRefId = c.Int(nullable: false),
                        DinePlanTaxRefId = c.Int(nullable: false),
                        TaxPercentage = c.Decimal(nullable: false, precision: 18, scale: 3),
                        TransactionTypeId = c.Int(),
                        Tag = c.String(maxLength: 50),
                        Locations = c.String(),
                        NonLocations = c.String(),
                        Group = c.Boolean(nullable: false),
                        LocationTag = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_DinePlanTaxLocation_MustHaveTenant",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            DropTable("dbo.DineGoDevicePaymentTypeDetails");
            DropTable("dbo.DineGoDeviceBrandDetails");
            DropTable("dbo.DineGoBrandDepartmentDetails");
            DropTable("dbo.DineGoDepartmentChargeDetails");
            DropTable("dbo.PrintConfigurations",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PrintConfiguration_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PrintConfiguration_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DineGoPaymentTypes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DineGoPaymentType_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DineGoPaymentType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DineGoDevices",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DineGoDevice_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DineGoDevice_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DineGoCharges",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DineGoCharge_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DineGoCharge_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DineGoDepartments",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DineGoDepartment_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DineGoDepartment_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DineGoBrands",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DineGoBrand_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DineGoBrand_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            CreateIndex("dbo.DinePlanTaxLocations", "TransactionTypeId");
            CreateIndex("dbo.DinePlanTaxLocations", "LocationRefId");
            AddForeignKey("dbo.DinePlanTaxLocations", "TransactionTypeId", "dbo.TransactionTypes", "Id");
            AddForeignKey("dbo.DinePlanTaxLocations", "LocationRefId", "dbo.Locations", "Id", cascadeDelete: true);
            RenameTable(name: "dbo.ProductComboGroupDetails", newName: "ProductComboJoins");
            RenameTable(name: "dbo.ProgramSettingValues", newName: "ProgramSettings");
        }
    }
}
