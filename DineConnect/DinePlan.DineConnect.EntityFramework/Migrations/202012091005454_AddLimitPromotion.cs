namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLimitPromotion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Promotions", "Limi1PromotionPerItem", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Promotions", "Limi1PromotionPerItem");
        }
    }
}
