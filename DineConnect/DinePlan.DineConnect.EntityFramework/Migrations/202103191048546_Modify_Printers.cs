namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Printers : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.ConnectMembers", "MemberCode");
            AddColumn("dbo.PrintJobs", "AliasName", c => c.String());
            AddColumn("dbo.Printers", "AliasName", c => c.String());
            AddColumn("dbo.PrintTemplateConditions", "AliasName", c => c.String());
            AddColumn("dbo.PrintTemplates", "AliasName", c => c.String());
            AlterColumn("dbo.ConnectMembers", "MemberCode", c => c.String(maxLength: 50));
            CreateIndex("dbo.ConnectMembers", "MemberCode", unique: true, name: "MemberCode");
        }
        
        public override void Down()
        {
            DropIndex("dbo.ConnectMembers", "MemberCode");
            AlterColumn("dbo.ConnectMembers", "MemberCode", c => c.String(maxLength: 30));
            DropColumn("dbo.PrintTemplates", "AliasName");
            DropColumn("dbo.PrintTemplateConditions", "AliasName");
            DropColumn("dbo.Printers", "AliasName");
            DropColumn("dbo.PrintJobs", "AliasName");
            CreateIndex("dbo.ConnectMembers", "MemberCode", unique: true, name: "MemberCode");
        }
    }
}
