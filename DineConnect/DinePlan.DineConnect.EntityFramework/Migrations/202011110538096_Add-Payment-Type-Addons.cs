namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPaymentTypeAddons : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PaymentTypes", "AddOns", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PaymentTypes", "AddOns");
        }
    }
}
