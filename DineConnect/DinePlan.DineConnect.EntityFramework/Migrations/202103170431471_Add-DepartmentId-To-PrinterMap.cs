namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDepartmentIdToPrinterMap : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PrinterMaps", "DepartmentId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PrinterMaps", "DepartmentId");
        }
    }
}
