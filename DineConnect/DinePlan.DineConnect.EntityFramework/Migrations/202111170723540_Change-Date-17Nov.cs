namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeDate17Nov : DbMigration
    {
        public override void Up()
        {
            //DropForeignKey("dbo.DelAggLocations", "DelAggLocationGroupId", "dbo.DelAggLocationGroups");
            //DropIndex("dbo.DelAggLocations", new[] { "DelAggLocationGroupId" });
            //AddColumn("dbo.Locations", "QuickDineInActive", c => c.Boolean(nullable: false));
            //AddColumn("dbo.Locations", "RestrictSync", c => c.Boolean(nullable: false));
            //AddColumn("dbo.DelAggLocations", "Name", c => c.String());
            //AlterColumn("dbo.DelAggLocations", "DelAggLocationGroupId", c => c.Int());
            //CreateIndex("dbo.DelAggLocations", "DelAggLocationGroupId");
            //AddForeignKey("dbo.DelAggLocations", "DelAggLocationGroupId", "dbo.DelAggLocationGroups", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DelAggLocations", "DelAggLocationGroupId", "dbo.DelAggLocationGroups");
            DropIndex("dbo.DelAggLocations", new[] { "DelAggLocationGroupId" });
            AlterColumn("dbo.DelAggLocations", "DelAggLocationGroupId", c => c.Int(nullable: false));
            DropColumn("dbo.DelAggLocations", "Name");
            DropColumn("dbo.Locations", "RestrictSync");
            DropColumn("dbo.Locations", "QuickDineInActive");
            CreateIndex("dbo.DelAggLocations", "DelAggLocationGroupId");
            AddForeignKey("dbo.DelAggLocations", "DelAggLocationGroupId", "dbo.DelAggLocationGroups", "Id", cascadeDelete: true);
        }
    }
}
