namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTicketUnique : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Tickets", new[] { "LocationId" });
            AlterColumn("dbo.Tickets", "TicketNumber", c => c.String(maxLength: 15));
            CreateIndex("dbo.Tickets", new[] { "LocationId", "TicketNumber" }, unique: true, name: "IX_FirstAndSecond");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Tickets", "IX_FirstAndSecond");
            AlterColumn("dbo.Tickets", "TicketNumber", c => c.String());
            CreateIndex("dbo.Tickets", "LocationId");
        }
    }
}
