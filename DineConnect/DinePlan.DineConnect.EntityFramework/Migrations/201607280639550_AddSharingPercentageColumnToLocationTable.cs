namespace DinePlan.DineConnect.Migrations

{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSharingPercentageColumnToLocationTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Locations", "SharingPercentage", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Locations", "SharingPercentage");
        }
    }
}
