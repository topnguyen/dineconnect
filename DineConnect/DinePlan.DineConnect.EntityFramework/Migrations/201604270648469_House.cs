namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class House : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdjustmentDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AdjustmentRefIf = c.Int(nullable: false),
                        Sno = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        AdjustmentQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AdjustmentMode = c.String(nullable: false),
                        AdjustmentApprovedRemarks = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Adjustments", t => t.AdjustmentRefIf)
                .Index(t => t.AdjustmentRefIf);
            
            CreateTable(
                "dbo.Adjustments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationRefId = c.Int(nullable: false),
                        AdjustmentDate = c.DateTime(nullable: false),
                        TokenRefNumber = c.Int(nullable: false),
                        AdjustmentRemarks = c.String(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Adjustment_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Adjustment_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationRefId)
                .Index(t => t.LocationRefId);
            
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 30),
                        Name = c.String(nullable: false, maxLength: 60),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        City = c.String(),
                        State = c.String(),
                        Country = c.String(),
                        PostalCode = c.String(),
                        PhoneNumber = c.String(),
                        FaxNumber = c.String(),
                        UserSerialNumber = c.String(),
                        GovtApprovalId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Company_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Company_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Code, unique: true, name: "Code")
                .Index(t => t.Name, unique: true, name: "Name");
            
            CreateTable(
                "dbo.CustomerMaterials",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CustomerRefId = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        MaterialPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LastQuoteRefNo = c.String(),
                        LastQuoteDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CustomerMaterial_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customers", t => t.CustomerRefId)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .Index(t => t.CustomerRefId)
                .Index(t => t.MaterialRefId);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CustomerName = c.String(nullable: false, maxLength: 60),
                        CustomerPriceTagCode = c.String(nullable: false,defaultValue:"DEFAULT"),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        City = c.String(),
                        State = c.String(),
                        Country = c.String(),
                        ZipCode = c.String(),
                        PhoneNumber = c.String(),
                        DefaultCreditDays = c.Int(nullable: false),
                        OrderPlacedThrough = c.String(),
                        Email = c.String(),
                        FaxNumber = c.String(),
                        Website = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Customer_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Customer_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.CustomerName, unique: true, name: "CustomerName");
            
            CreateTable(
                "dbo.Materials",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MaterialTypeId = c.Int(nullable: false,defaultValue:0),
                        MaterialName = c.String(nullable: false, maxLength: 50),
                        MaterialPetName = c.String(nullable: false, maxLength: 30),
                        MaterialGroupCategoryRefId = c.Int(nullable: false),
                        DefaultUnitId = c.Int(nullable: false),
                        UserSerialNumber = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsFractional = c.Boolean(nullable: false),
                        IsBranded = c.Boolean(nullable: false),
                        IsQuoteNeededForPurchase = c.Boolean(nullable: false),
                        GeneralLifeDays = c.Int(nullable: false),
                        OwnPreparation = c.Boolean(nullable: false),
                        IsNeedtoKeptinFreezer = c.Boolean(nullable: false),
                        MRPPriceExists = c.Boolean(nullable: false),
                        PosReferencecodeifany = c.Int(nullable: false),
                        IsMfgDateExists = c.Boolean(nullable: false),
                        WipeOutStockOnClosingDay = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Material_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Material_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MaterialGroupCategories", t => t.MaterialGroupCategoryRefId)
                .ForeignKey("dbo.Units", t => t.DefaultUnitId)
                .Index(t => t.MaterialName, unique: true, name: "MaterialName")
                .Index(t => t.MaterialPetName, unique: true, name: "MaterialPetName")
                .Index(t => t.MaterialGroupCategoryRefId)
                .Index(t => t.DefaultUnitId);
            
            CreateTable(
                "dbo.MaterialGroupCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MaterialGroupCategoryName = c.String(nullable: false),
                        MaterialGroupRefId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MaterialGroupCategory_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MaterialGroups", t => t.MaterialGroupRefId)
                .Index(t => t.MaterialGroupRefId);
            
            CreateTable(
                "dbo.MaterialGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MaterialGroupName = c.String(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MaterialGroup_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_MaterialGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MaterialLedgers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationRefId = c.Int(nullable: false),
                        LedgerDate = c.DateTime(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        OpenBalance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Received = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Issued = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransferIn = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransferOut = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Damaged = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SupplierReturn = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ExcessReceived = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Shortage = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Return = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ClBalance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MaterialLedger_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationRefId)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .Index(t => t.LocationRefId)
                .Index(t => t.MaterialRefId);
            
            CreateTable(
                "dbo.MaterialLocationWiseStocks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationRefId = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        CurrentInHand = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MinimumStock = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MaximumStock = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ReOrderLevel = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsOrderPlaced = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MaterialLocationWiseStock_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationRefId)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .Index(t => t.LocationRefId)
                .Index(t => t.MaterialRefId);
            
            CreateTable(
                "dbo.Units",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Unit_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Unit_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UnitConversions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BaseUnitId = c.Int(nullable: false),
                        RefUnitId = c.Int(nullable: false),
                        Conversion = c.Decimal(nullable: false, precision: 18, scale: 6),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UnitConversion_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Units", t => t.BaseUnitId)
                .Index(t => t.BaseUnitId);
            
            CreateTable(
                "dbo.CustomerTagDefinitions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TagCode = c.String(nullable: false, maxLength: 30),
                        TagName = c.String(nullable: false, maxLength: 30),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CustomerTagDefinition_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.TagCode, unique: true, name: "TagCode")
                .Index(t => t.TagName, unique: true, name: "CustomerTagName");
            
            CreateTable(
                "dbo.CustomerTagWiseMaterialPrices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MaterialRefId = c.Int(nullable: false),
                        TagCode = c.String(nullable: false),
                        MaterialPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CustomerTagMaterialPrice_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .Index(t => t.MaterialRefId);
            
            CreateTable(
                "dbo.HouseReports",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ReportKey = c.String(maxLength: 30),
                        Description = c.String(),
                        ReportRole = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.ReportKey, unique: true, name: "ReportKey");
            
            CreateTable(
                "dbo.InterTransferDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InterTransferRefId = c.Int(nullable: false),
                        Sno = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        RequestQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IssueQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CurrentStatus = c.String(nullable: false, maxLength: 1),
                        RequestRemarks = c.String(),
                        ApprovedRemarks = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InterTransferDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.InterTransfers", t => t.InterTransferRefId)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .Index(t => t.InterTransferRefId)
                .Index(t => t.MaterialRefId);
            
            CreateTable(
                "dbo.InterTransfers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationRefId = c.Int(nullable: false),
                        RequestLocationRefId = c.Int(nullable: false),
                        RequestDate = c.DateTime(nullable: false),
                        CurrentStatus = c.String(nullable: false, maxLength: 1),
                        CompletedTime = c.DateTime(),
                        ReceivedTime = c.DateTime(),
                        ApprovedPersonId = c.Int(),
                        ReceivedPersonId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InterTransfer_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InterTransfer_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.InterTransferReceivedDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InterTransferRefId = c.Int(nullable: false),
                        Sno = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        ReceivedQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CurrentStatus = c.String(nullable: false, maxLength: 1),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InterTransferReceivedDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.InterTransfers", t => t.InterTransferRefId)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .Index(t => t.InterTransferRefId)
                .Index(t => t.MaterialRefId);
            
            CreateTable(
                "dbo.InvoiceDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InvoiceRefId = c.Int(nullable: false),
                        Sno = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        TotalQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DiscountFlag = c.String(nullable: false, maxLength: 1),
                        DiscountPercentage = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DiscountAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TaxAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NetAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Remarks = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InvoiceDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InvoiceDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Invoices", t => t.InvoiceRefId)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .Index(t => t.InvoiceRefId)
                .Index(t => t.MaterialRefId);
            
            CreateTable(
                "dbo.Invoices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationRefId = c.Int(nullable: false),
                        InvoiceDate = c.DateTime(nullable: false),
                        InvoiceMode = c.String(nullable: false),
                        AccountDate = c.DateTime(),
                        SupplierRefId = c.Int(nullable: false),
                        InvoiceNumber = c.String(nullable: false),
                        DcFromDate = c.DateTime(nullable: false),
                        DcToDate = c.DateTime(nullable: false),
                        TotalDiscountAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalShipmentCharges = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RoundedAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        InvoiceAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Invoice_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Invoice_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationRefId)
                .Index(t => t.LocationRefId);
            
            CreateTable(
                "dbo.InvoiceDirectCreditLink",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InvoiceRefId = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        InwardDirectCreditRefId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InvoiceDirectCreditLink_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InvoiceDirectCreditLink_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Invoices", t => t.InvoiceRefId)
                .ForeignKey("dbo.InwardDirectCredits", t => t.InwardDirectCreditRefId)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .Index(t => t.InvoiceRefId)
                .Index(t => t.MaterialRefId)
                .Index(t => t.InwardDirectCreditRefId);
            
            CreateTable(
                "dbo.InwardDirectCredits",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DcDate = c.DateTime(nullable: false),
                        LocationRefId = c.Int(nullable: false),
                        SupplierRefId = c.Int(nullable: false),
                        DcNumberGivenBySupplier = c.String(nullable: false),
                        IsDcCompleted = c.Boolean(nullable: false),
                        PoReferenceCode = c.String(),
                        PoRefId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InwardDirectCredit_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InwardDirectCredit_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationRefId)
                .ForeignKey("dbo.PurchaseOrders", t => t.PoRefId)
                .ForeignKey("dbo.Suppliers", t => t.SupplierRefId)
                .Index(t => t.LocationRefId)
                .Index(t => t.SupplierRefId)
                .Index(t => t.PoRefId);
            
            CreateTable(
                "dbo.PurchaseOrders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PoDate = c.DateTime(nullable: false),
                        PoReferenceCode = c.String(nullable: false),
                        ShippingLocationId = c.Int(nullable: false),
                        QuoteReference = c.String(),
                        SupplierRefId = c.Int(nullable: false),
                        PoNetAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Remarks = c.String(),
                        LocationRefId = c.Int(nullable: false),
                        DeliveryDateExpected = c.DateTime(nullable: false),
                        CreditDays = c.Int(nullable: false),
                        ApprovedTime = c.DateTime(),
                        PoCurrentStatus = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PurchaseOrder_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PurchaseOrder_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.ShippingLocationId)
                .ForeignKey("dbo.Locations", t => t.LocationRefId)
                .ForeignKey("dbo.Suppliers", t => t.SupplierRefId)
                .Index(t => t.ShippingLocationId)
                .Index(t => t.SupplierRefId)
                .Index(t => t.LocationRefId);
            
            CreateTable(
                "dbo.Suppliers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SupplierName = c.String(nullable: false, maxLength: 60),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        Address3 = c.String(),
                        City = c.String(),
                        State = c.String(),
                        Country = c.String(),
                        ZipCode = c.String(),
                        PhoneNumber1 = c.String(),
                        DefaultCreditDays = c.Int(nullable: false),
                        OrderPlacedThrough = c.String(),
                        Email = c.String(),
                        FaxNumber = c.String(),
                        Website = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Supplier_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Supplier_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.SupplierName, unique: true, name: "SupplierName");
            
            CreateTable(
                "dbo.SupplierMaterials",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SupplierRefId = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        MaterialPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LastQuoteRefNo = c.String(),
                        LastQuoteDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SupplierMaterial_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .ForeignKey("dbo.Suppliers", t => t.SupplierRefId)
                .Index(t => t.SupplierRefId)
                .Index(t => t.MaterialRefId);
            
            CreateTable(
                "dbo.InvoiceTaxDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InvoiceRefId = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        TaxRefId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InvoiceTaxDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InvoiceTaxDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Invoices", t => t.InvoiceRefId)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .ForeignKey("dbo.Taxes", t => t.TaxRefId)
                .Index(t => t.InvoiceRefId)
                .Index(t => t.MaterialRefId)
                .Index(t => t.TaxRefId);
            
            CreateTable(
                "dbo.Taxes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TaxName = c.String(nullable: false, maxLength: 50),
                        TaxClassification = c.String(nullable: false, maxLength: 2),
                        Percentage = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RebateIfAny = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TaxCalculationMethod = c.String(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Tax_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Tax_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.TaxName, unique: true, name: "TaxName");
            
            CreateTable(
                "dbo.InwardDirectCreditDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DcRefId = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        DcReceivedQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DcConvertedQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnitRefId = c.Int(nullable: false),
                        MaterialReceivedStatus = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.InwardDirectCredits", t => t.DcRefId)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .Index(t => t.DcRefId)
                .Index(t => t.MaterialRefId);
            
            CreateTable(
                "dbo.IssueDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IssueRefId = c.Int(nullable: false),
                        Sno = c.Int(nullable: false),
                        RecipeRefId = c.Int(),
                        MaterialRefId = c.Int(nullable: false),
                        RequestQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IssueQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_IssueDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Issues", t => t.IssueRefId)
                .ForeignKey("dbo.Recipes", t => t.RecipeRefId)
                .Index(t => t.IssueRefId)
                .Index(t => t.RecipeRefId);
            
            CreateTable(
                "dbo.Issues",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationRefId = c.Int(nullable: false),
                        RequestSlipNumber = c.String(),
                        RequestTime = c.DateTime(nullable: false),
                        IssueTime = c.DateTime(nullable: false),
                        TokenNumber = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Issue_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Issue_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationRefId)
                .Index(t => t.LocationRefId);
            
            CreateTable(
                "dbo.Recipes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RecipeName = c.String(nullable: false, maxLength: 50),
                        RecipeGroupRefId = c.Int(nullable: false),
                        RecipeShortName = c.String(nullable: false, maxLength: 50),
                        PrdBatchQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PrdUnitId = c.Int(nullable: false),
                        SelUnitId = c.Int(nullable: false),
                        LifeTimeInMinutes = c.Int(nullable: false),
                        AlarmFlag = c.Boolean(nullable: false),
                        AlarmTimeInMinutes = c.Int(nullable: false),
                        FixedCost = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Recipe_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RecipeGroups", t => t.RecipeGroupRefId)
                .ForeignKey("dbo.Units", t => t.PrdUnitId)
                .ForeignKey("dbo.Units", t => t.SelUnitId)
                .Index(t => t.RecipeName, unique: true, name: "RecipeName")
                .Index(t => t.RecipeGroupRefId)
                .Index(t => t.RecipeShortName, unique: true, name: "RecipeShortName")
                .Index(t => t.PrdUnitId)
                .Index(t => t.SelUnitId);
            
            CreateTable(
                "dbo.RecipeGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RecipeGroupName = c.String(nullable: false, maxLength: 30),
                        RecipeGroupShortName = c.String(nullable: false, maxLength: 30),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_RecipeGroup_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_RecipeGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.RecipeGroupName, unique: true, name: "RecipeGroupName")
                .Index(t => t.RecipeGroupShortName, unique: true, name: "RecipeGroupShortName");
            
            CreateTable(
                "dbo.LocationContacts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationRefId = c.Int(nullable: false),
                        ContactPersonName = c.String(nullable: false),
                        Designation = c.String(),
                        PhoneNumber = c.String(),
                        EmpRefCode = c.String(),
                        Email = c.String(),
                        Priority = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LocationContact_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationRefId)
                .Index(t => t.LocationRefId);
            
            CreateTable(
                "dbo.ManualReasons",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ManualReasonName = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ManualReason_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ManualReason_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MaterialBrandLinks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MaterialRefId = c.Int(nullable: false),
                        BrandRefId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MaterialBrandsLink_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Brands", t => t.BrandRefId)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .Index(t => t.MaterialRefId)
                .Index(t => t.BrandRefId);
            
            CreateTable(
                "dbo.MaterialIngredients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RecipeRefId = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        MaterialUsedQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnitRefId = c.Int(nullable: false),
                        VariationflagForProduction = c.Boolean(nullable: false),
                        UserSerialNumber = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MaterialIngredient_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .ForeignKey("dbo.Units", t => t.UnitRefId)
                .Index(t => t.MaterialRefId)
                .Index(t => t.UnitRefId);
            
            CreateTable(
                "dbo.MaterialMenuMappings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PosMenuPortionRefId = c.Int(nullable: false),
                        UserSerialNumber = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MaterialRefId = c.Int(nullable: false),
                        PortionQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PortionUnitId = c.Int(nullable: false),
                        WastageExpected = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MaterialMenuMapping_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Recipes", t => t.MaterialRefId)
                .ForeignKey("dbo.Units", t => t.PortionUnitId)
                .Index(t => t.MaterialRefId)
                .Index(t => t.PortionUnitId);
            
            CreateTable(
                "dbo.MaterialUnitLinks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MaterialRefId = c.Int(nullable: false),
                        UnitRefId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MaterialUnitsLink_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .ForeignKey("dbo.Units", t => t.UnitRefId)
                .Index(t => t.MaterialRefId)
                .Index(t => t.UnitRefId);
            
            CreateTable(
                "dbo.ProductRecipeLinks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PosRefId = c.Int(nullable: false),
                        UserSerialNumber = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RecipeRefId = c.Int(nullable: false),
                        PortionQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PortionUnitId = c.Int(nullable: false),
                        RecipeType = c.String(nullable: false),
                        WastageExpected = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ProductRecipesLink_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Recipes", t => t.RecipeRefId)
                .ForeignKey("dbo.Units", t => t.PortionUnitId)
                .Index(t => t.RecipeRefId)
                .Index(t => t.PortionUnitId);
            
            CreateTable(
                "dbo.PurchaseOrderDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PoRefId = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        QtyOrdered = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DiscountOption = c.String(nullable: false, maxLength: 1),
                        DiscountValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TaxAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NetAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PoStatus = c.Boolean(nullable: false),
                        Remarks = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PurchaseOrderDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PurchaseOrderDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .ForeignKey("dbo.PurchaseOrders", t => t.PoRefId)
                .Index(t => t.PoRefId)
                .Index(t => t.MaterialRefId);
            
            CreateTable(
                "dbo.PurchaseOrderTaxDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PoRefId = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        TaxRefId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PurchaseOrderTaxDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PurchaseOrderTaxDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .ForeignKey("dbo.PurchaseOrders", t => t.PoRefId)
                .Index(t => t.PoRefId)
                .Index(t => t.MaterialRefId);
            
            CreateTable(
                "dbo.RecipeIngredients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RecipeRefId = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        MaterialUsedQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnitRefId = c.Int(nullable: false),
                        VariationflagForProduction = c.Boolean(nullable: false),
                        UserSerialNumber = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_RecipeIngredient_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .ForeignKey("dbo.Recipes", t => t.RecipeRefId)
                .ForeignKey("dbo.Units", t => t.UnitRefId)
                .Index(t => t.RecipeRefId)
                .Index(t => t.MaterialRefId)
                .Index(t => t.UnitRefId);
            
            CreateTable(
                "dbo.ReturnDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ReturnRefId = c.Int(nullable: false),
                        Sno = c.Int(nullable: false),
                        RecipeRefId = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        ReturnQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Remarks = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Recipes", t => t.RecipeRefId)
                .ForeignKey("dbo.Returns", t => t.ReturnRefId)
                .Index(t => t.ReturnRefId)
                .Index(t => t.RecipeRefId);
            
            CreateTable(
                "dbo.Returns",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationRefId = c.Int(nullable: false),
                        ReturnDate = c.DateTime(nullable: false),
                        TokenRefNumber = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Return_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Return_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationRefId)
                .Index(t => t.LocationRefId);
            
            CreateTable(
                "dbo.SupplierContacts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SupplierRefId = c.Int(nullable: false),
                        ContactPersonName = c.String(nullable: false),
                        Designation = c.String(),
                        PhoneNumber = c.String(),
                        Email = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SupplierContact_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Suppliers", t => t.SupplierRefId)
                .Index(t => t.SupplierRefId);
            
            CreateTable(
                "dbo.TempInwardDirectCreditDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DcRefId = c.Long(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        DcReceivedQty = c.Single(nullable: false),
                        MaterialReceivedStatus = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TempInwardDirectCreditDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TempInwardDirectCreditDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Templates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        TemplateType = c.Int(nullable: false),
                        TemplateData = c.String(),
                        LocationRefId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Template_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Template_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "Name");
            
            CreateTable(
                "dbo.UserDefaultOrganizations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrganizationUnitId = c.Long(nullable: false),
                        UserId = c.Long(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UserDefaultOrganization_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_UserDefaultOrganization_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Locations", "CompanyRefId", c => c.Int(nullable: false));
            CreateIndex("dbo.Locations", "CompanyRefId");
            AddForeignKey("dbo.Locations", "CompanyRefId", "dbo.Companies", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SupplierContacts", "SupplierRefId", "dbo.Suppliers");
            DropForeignKey("dbo.ReturnDetails", "ReturnRefId", "dbo.Returns");
            DropForeignKey("dbo.Returns", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.ReturnDetails", "RecipeRefId", "dbo.Recipes");
            DropForeignKey("dbo.RecipeIngredients", "UnitRefId", "dbo.Units");
            DropForeignKey("dbo.RecipeIngredients", "RecipeRefId", "dbo.Recipes");
            DropForeignKey("dbo.RecipeIngredients", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.PurchaseOrderTaxDetails", "PoRefId", "dbo.PurchaseOrders");
            DropForeignKey("dbo.PurchaseOrderTaxDetails", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.PurchaseOrderDetails", "PoRefId", "dbo.PurchaseOrders");
            DropForeignKey("dbo.PurchaseOrderDetails", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.ProductRecipeLinks", "PortionUnitId", "dbo.Units");
            DropForeignKey("dbo.ProductRecipeLinks", "RecipeRefId", "dbo.Recipes");
            DropForeignKey("dbo.MaterialUnitLinks", "UnitRefId", "dbo.Units");
            DropForeignKey("dbo.MaterialUnitLinks", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.MaterialMenuMappings", "PortionUnitId", "dbo.Units");
            DropForeignKey("dbo.MaterialMenuMappings", "MaterialRefId", "dbo.Recipes");
            DropForeignKey("dbo.MaterialIngredients", "UnitRefId", "dbo.Units");
            DropForeignKey("dbo.MaterialIngredients", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.MaterialBrandLinks", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.MaterialBrandLinks", "BrandRefId", "dbo.Brands");
            DropForeignKey("dbo.LocationContacts", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.IssueDetails", "RecipeRefId", "dbo.Recipes");
            DropForeignKey("dbo.Recipes", "SelUnitId", "dbo.Units");
            DropForeignKey("dbo.Recipes", "PrdUnitId", "dbo.Units");
            DropForeignKey("dbo.Recipes", "RecipeGroupRefId", "dbo.RecipeGroups");
            DropForeignKey("dbo.IssueDetails", "IssueRefId", "dbo.Issues");
            DropForeignKey("dbo.Issues", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.InwardDirectCreditDetails", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.InwardDirectCreditDetails", "DcRefId", "dbo.InwardDirectCredits");
            DropForeignKey("dbo.InvoiceTaxDetails", "TaxRefId", "dbo.Taxes");
            DropForeignKey("dbo.InvoiceTaxDetails", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.InvoiceTaxDetails", "InvoiceRefId", "dbo.Invoices");
            DropForeignKey("dbo.InvoiceDirectCreditLink", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.InvoiceDirectCreditLink", "InwardDirectCreditRefId", "dbo.InwardDirectCredits");
            DropForeignKey("dbo.InwardDirectCredits", "SupplierRefId", "dbo.Suppliers");
            DropForeignKey("dbo.InwardDirectCredits", "PoRefId", "dbo.PurchaseOrders");
            DropForeignKey("dbo.PurchaseOrders", "SupplierRefId", "dbo.Suppliers");
            DropForeignKey("dbo.SupplierMaterials", "SupplierRefId", "dbo.Suppliers");
            DropForeignKey("dbo.SupplierMaterials", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.PurchaseOrders", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.PurchaseOrders", "ShippingLocationId", "dbo.Locations");
            DropForeignKey("dbo.InwardDirectCredits", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.InvoiceDirectCreditLink", "InvoiceRefId", "dbo.Invoices");
            DropForeignKey("dbo.InvoiceDetails", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.InvoiceDetails", "InvoiceRefId", "dbo.Invoices");
            DropForeignKey("dbo.Invoices", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.InterTransferReceivedDetails", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.InterTransferReceivedDetails", "InterTransferRefId", "dbo.InterTransfers");
            DropForeignKey("dbo.InterTransferDetails", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.InterTransferDetails", "InterTransferRefId", "dbo.InterTransfers");
            DropForeignKey("dbo.CustomerTagWiseMaterialPrices", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.CustomerMaterials", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.Materials", "DefaultUnitId", "dbo.Units");
            DropForeignKey("dbo.UnitConversions", "BaseUnitId", "dbo.Units");
            DropForeignKey("dbo.MaterialLocationWiseStocks", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.MaterialLocationWiseStocks", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.MaterialLedgers", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.MaterialLedgers", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.MaterialGroupCategories", "MaterialGroupRefId", "dbo.MaterialGroups");
            DropForeignKey("dbo.Materials", "MaterialGroupCategoryRefId", "dbo.MaterialGroupCategories");
            DropForeignKey("dbo.CustomerMaterials", "CustomerRefId", "dbo.Customers");
            DropForeignKey("dbo.AdjustmentDetails", "AdjustmentRefIf", "dbo.Adjustments");
            DropForeignKey("dbo.Adjustments", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.Locations", "CompanyRefId", "dbo.Companies");
            DropIndex("dbo.Templates", "Name");
            DropIndex("dbo.SupplierContacts", new[] { "SupplierRefId" });
            DropIndex("dbo.Returns", new[] { "LocationRefId" });
            DropIndex("dbo.ReturnDetails", new[] { "RecipeRefId" });
            DropIndex("dbo.ReturnDetails", new[] { "ReturnRefId" });
            DropIndex("dbo.RecipeIngredients", new[] { "UnitRefId" });
            DropIndex("dbo.RecipeIngredients", new[] { "MaterialRefId" });
            DropIndex("dbo.RecipeIngredients", new[] { "RecipeRefId" });
            DropIndex("dbo.PurchaseOrderTaxDetails", new[] { "MaterialRefId" });
            DropIndex("dbo.PurchaseOrderTaxDetails", new[] { "PoRefId" });
            DropIndex("dbo.PurchaseOrderDetails", new[] { "MaterialRefId" });
            DropIndex("dbo.PurchaseOrderDetails", new[] { "PoRefId" });
            DropIndex("dbo.ProductRecipeLinks", new[] { "PortionUnitId" });
            DropIndex("dbo.ProductRecipeLinks", new[] { "RecipeRefId" });
            DropIndex("dbo.MaterialUnitLinks", new[] { "UnitRefId" });
            DropIndex("dbo.MaterialUnitLinks", new[] { "MaterialRefId" });
            DropIndex("dbo.MaterialMenuMappings", new[] { "PortionUnitId" });
            DropIndex("dbo.MaterialMenuMappings", new[] { "MaterialRefId" });
            DropIndex("dbo.MaterialIngredients", new[] { "UnitRefId" });
            DropIndex("dbo.MaterialIngredients", new[] { "MaterialRefId" });
            DropIndex("dbo.MaterialBrandLinks", new[] { "BrandRefId" });
            DropIndex("dbo.MaterialBrandLinks", new[] { "MaterialRefId" });
            DropIndex("dbo.LocationContacts", new[] { "LocationRefId" });
            DropIndex("dbo.RecipeGroups", "RecipeGroupShortName");
            DropIndex("dbo.RecipeGroups", "RecipeGroupName");
            DropIndex("dbo.Recipes", new[] { "SelUnitId" });
            DropIndex("dbo.Recipes", new[] { "PrdUnitId" });
            DropIndex("dbo.Recipes", "RecipeShortName");
            DropIndex("dbo.Recipes", new[] { "RecipeGroupRefId" });
            DropIndex("dbo.Recipes", "RecipeName");
            DropIndex("dbo.Issues", new[] { "LocationRefId" });
            DropIndex("dbo.IssueDetails", new[] { "RecipeRefId" });
            DropIndex("dbo.IssueDetails", new[] { "IssueRefId" });
            DropIndex("dbo.InwardDirectCreditDetails", new[] { "MaterialRefId" });
            DropIndex("dbo.InwardDirectCreditDetails", new[] { "DcRefId" });
            DropIndex("dbo.Taxes", "TaxName");
            DropIndex("dbo.InvoiceTaxDetails", new[] { "TaxRefId" });
            DropIndex("dbo.InvoiceTaxDetails", new[] { "MaterialRefId" });
            DropIndex("dbo.InvoiceTaxDetails", new[] { "InvoiceRefId" });
            DropIndex("dbo.SupplierMaterials", new[] { "MaterialRefId" });
            DropIndex("dbo.SupplierMaterials", new[] { "SupplierRefId" });
            DropIndex("dbo.Suppliers", "SupplierName");
            DropIndex("dbo.PurchaseOrders", new[] { "LocationRefId" });
            DropIndex("dbo.PurchaseOrders", new[] { "SupplierRefId" });
            DropIndex("dbo.PurchaseOrders", new[] { "ShippingLocationId" });
            DropIndex("dbo.InwardDirectCredits", new[] { "PoRefId" });
            DropIndex("dbo.InwardDirectCredits", new[] { "SupplierRefId" });
            DropIndex("dbo.InwardDirectCredits", new[] { "LocationRefId" });
            DropIndex("dbo.InvoiceDirectCreditLink", new[] { "InwardDirectCreditRefId" });
            DropIndex("dbo.InvoiceDirectCreditLink", new[] { "MaterialRefId" });
            DropIndex("dbo.InvoiceDirectCreditLink", new[] { "InvoiceRefId" });
            DropIndex("dbo.Invoices", new[] { "LocationRefId" });
            DropIndex("dbo.InvoiceDetails", new[] { "MaterialRefId" });
            DropIndex("dbo.InvoiceDetails", new[] { "InvoiceRefId" });
            DropIndex("dbo.InterTransferReceivedDetails", new[] { "MaterialRefId" });
            DropIndex("dbo.InterTransferReceivedDetails", new[] { "InterTransferRefId" });
            DropIndex("dbo.InterTransferDetails", new[] { "MaterialRefId" });
            DropIndex("dbo.InterTransferDetails", new[] { "InterTransferRefId" });
            DropIndex("dbo.HouseReports", "ReportKey");
            DropIndex("dbo.CustomerTagWiseMaterialPrices", new[] { "MaterialRefId" });
            DropIndex("dbo.CustomerTagDefinitions", "CustomerTagName");
            DropIndex("dbo.CustomerTagDefinitions", "TagCode");
            DropIndex("dbo.UnitConversions", new[] { "BaseUnitId" });
            DropIndex("dbo.MaterialLocationWiseStocks", new[] { "MaterialRefId" });
            DropIndex("dbo.MaterialLocationWiseStocks", new[] { "LocationRefId" });
            DropIndex("dbo.MaterialLedgers", new[] { "MaterialRefId" });
            DropIndex("dbo.MaterialLedgers", new[] { "LocationRefId" });
            DropIndex("dbo.MaterialGroupCategories", new[] { "MaterialGroupRefId" });
            DropIndex("dbo.Materials", new[] { "DefaultUnitId" });
            DropIndex("dbo.Materials", new[] { "MaterialGroupCategoryRefId" });
            DropIndex("dbo.Materials", "MaterialPetName");
            DropIndex("dbo.Materials", "MaterialName");
            DropIndex("dbo.Customers", "CustomerName");
            DropIndex("dbo.CustomerMaterials", new[] { "MaterialRefId" });
            DropIndex("dbo.CustomerMaterials", new[] { "CustomerRefId" });
            DropIndex("dbo.Companies", "Name");
            DropIndex("dbo.Companies", "Code");
            DropIndex("dbo.Locations", new[] { "CompanyRefId" });
            DropIndex("dbo.Adjustments", new[] { "LocationRefId" });
            DropIndex("dbo.AdjustmentDetails", new[] { "AdjustmentRefIf" });
            DropColumn("dbo.Locations", "CompanyRefId");
            DropTable("dbo.UserDefaultOrganizations",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UserDefaultOrganization_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_UserDefaultOrganization_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Templates",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Template_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Template_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TempInwardDirectCreditDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TempInwardDirectCreditDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TempInwardDirectCreditDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.SupplierContacts",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SupplierContact_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Returns",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Return_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Return_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ReturnDetails");
            DropTable("dbo.RecipeIngredients",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_RecipeIngredient_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.PurchaseOrderTaxDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PurchaseOrderTaxDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PurchaseOrderTaxDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.PurchaseOrderDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PurchaseOrderDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PurchaseOrderDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ProductRecipeLinks",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ProductRecipesLink_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.MaterialUnitLinks",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MaterialUnitsLink_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.MaterialMenuMappings",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MaterialMenuMapping_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.MaterialIngredients",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MaterialIngredient_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.MaterialBrandLinks",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MaterialBrandsLink_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ManualReasons",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ManualReason_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ManualReason_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.LocationContacts",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LocationContact_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.RecipeGroups",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_RecipeGroup_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_RecipeGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Recipes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Recipe_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Issues",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Issue_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Issue_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.IssueDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_IssueDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.InwardDirectCreditDetails");
            DropTable("dbo.Taxes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Tax_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Tax_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.InvoiceTaxDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InvoiceTaxDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InvoiceTaxDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.SupplierMaterials",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SupplierMaterial_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Suppliers",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Supplier_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Supplier_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.PurchaseOrders",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PurchaseOrder_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PurchaseOrder_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.InwardDirectCredits",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InwardDirectCredit_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InwardDirectCredit_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.InvoiceDirectCreditLink",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InvoiceDirectCreditLink_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InvoiceDirectCreditLink_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Invoices",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Invoice_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Invoice_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.InvoiceDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InvoiceDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InvoiceDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.InterTransferReceivedDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InterTransferReceivedDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.InterTransfers",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InterTransfer_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InterTransfer_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.InterTransferDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InterTransferDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.HouseReports");
            DropTable("dbo.CustomerTagWiseMaterialPrices",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CustomerTagMaterialPrice_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.CustomerTagDefinitions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CustomerTagDefinition_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.UnitConversions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UnitConversion_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Units",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Unit_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Unit_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.MaterialLocationWiseStocks",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MaterialLocationWiseStock_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.MaterialLedgers",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MaterialLedger_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.MaterialGroups",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MaterialGroup_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_MaterialGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.MaterialGroupCategories",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MaterialGroupCategory_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Materials",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Material_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Material_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Customers",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Customer_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Customer_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.CustomerMaterials",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CustomerMaterial_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Companies",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Company_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Company_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Adjustments",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Adjustment_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Adjustment_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.AdjustmentDetails");
        }
    }
}
