namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddExternalDeliveryCompleted : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExternalDeliveryTickets", "Completed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ExternalDeliveryTickets", "Completed");
        }
    }
}
