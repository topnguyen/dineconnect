namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyMenuMappingAutoSalesDeduction : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MaterialMenuMappings", "AutoSalesDeduction", c => c.Boolean(nullable: false,defaultValue:true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MaterialMenuMappings", "AutoSalesDeduction");
        }
    }
}
