namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTerminalSerialNumber : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Terminals", "SerialNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Terminals", "SerialNumber");
        }
    }
}
