namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMemberName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DeliveryTickets", "Name", c => c.String());
            AddColumn("dbo.DeliveryTickets", "Locality", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DeliveryTickets", "Locality");
            DropColumn("dbo.DeliveryTickets", "Name");
        }
    }
}
