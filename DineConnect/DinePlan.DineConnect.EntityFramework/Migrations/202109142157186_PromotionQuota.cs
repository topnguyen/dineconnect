namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PromotionQuota : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.PromotionQuotas", "PlantResetDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PromotionQuotas", "PlantResetDate", c => c.DateTime());
        }
    }
}
