namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_IssueRecipeDetail_TransferValue_In_InterTransfer : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.IssueRecipeDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IssueRefId = c.Int(nullable: false),
                        Sno = c.Int(nullable: false),
                        RecipeRefId = c.Int(nullable: false),
                        RecipeProductionQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CompletedQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ProductionUomRefId = c.Int(nullable: false),
                        MultipleBatchProductionAllowed = c.Boolean(nullable: false),
                        CompletionFlag = c.Boolean(nullable: false),
                        Remarks = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_IssueRecipeDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Issues", t => t.IssueRefId, cascadeDelete: true)
                .ForeignKey("dbo.Materials", t => t.RecipeRefId, cascadeDelete: true)
                .Index(t => t.IssueRefId)
                .Index(t => t.RecipeRefId);
            
            AddColumn("dbo.InterTransferDetails", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.InterTransferDetails", "IssueValue", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AddColumn("dbo.InterTransfers", "TransferValue", c => c.Decimal(precision: 18, scale: 6));
            AddColumn("dbo.InterTransferReceivedDetails", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.InterTransferReceivedDetails", "ReceivedValue", c => c.Decimal(nullable: false, precision: 18, scale: 6));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.IssueRecipeDetails", "RecipeRefId", "dbo.Materials");
            DropForeignKey("dbo.IssueRecipeDetails", "IssueRefId", "dbo.Issues");
            DropIndex("dbo.IssueRecipeDetails", new[] { "RecipeRefId" });
            DropIndex("dbo.IssueRecipeDetails", new[] { "IssueRefId" });
            DropColumn("dbo.InterTransferReceivedDetails", "ReceivedValue");
            DropColumn("dbo.InterTransferReceivedDetails", "Price");
            DropColumn("dbo.InterTransfers", "TransferValue");
            DropColumn("dbo.InterTransferDetails", "IssueValue");
            DropColumn("dbo.InterTransferDetails", "Price");
            DropTable("dbo.IssueRecipeDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_IssueRecipeDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
