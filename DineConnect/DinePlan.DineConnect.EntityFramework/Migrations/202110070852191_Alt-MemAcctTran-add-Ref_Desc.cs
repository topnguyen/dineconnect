namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AltMemAcctTranaddRef_Desc : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EngageMemberAccountTransactions", "Reference", c => c.String(maxLength: 100));
            AddColumn("dbo.EngageMemberAccountTransactions", "Description", c => c.String(maxLength: 150));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EngageMemberAccountTransactions", "Description");
            DropColumn("dbo.EngageMemberAccountTransactions", "Reference");
        }
    }
}
