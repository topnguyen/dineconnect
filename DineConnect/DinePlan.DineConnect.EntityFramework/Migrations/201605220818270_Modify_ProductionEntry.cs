namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_ProductionEntry : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductionDetails", "ProductionQty", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.ProductionDetails", "OutputQty");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProductionDetails", "OutputQty", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.ProductionDetails", "ProductionQty");
        }
    }
}
