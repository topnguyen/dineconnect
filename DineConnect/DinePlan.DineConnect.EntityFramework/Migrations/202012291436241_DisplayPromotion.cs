namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DisplayPromotion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Promotions", "DisplayPromotion", c => c.Boolean(nullable: false));
            AddColumn("dbo.Promotions", "PromotionPosition", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Promotions", "PromotionPosition");
            DropColumn("dbo.Promotions", "DisplayPromotion");
        }
    }
}
