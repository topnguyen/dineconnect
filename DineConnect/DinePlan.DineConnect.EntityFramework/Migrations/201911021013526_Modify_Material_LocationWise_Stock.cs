namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Material_LocationWise_Stock : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Materials", "ConvertAsZeroStockWhenClosingStockNegative", c => c.Boolean(nullable: false));
            AddColumn("dbo.MaterialLocationWiseStocks", "ConvertAsZeroStockWhenClosingStockNegative", c => c.Boolean(nullable: false));
            DropColumn("dbo.Materials", "ConvertAsZeroStockWhenClosingStockZero");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Materials", "ConvertAsZeroStockWhenClosingStockZero", c => c.Boolean(nullable: false));
            DropColumn("dbo.MaterialLocationWiseStocks", "ConvertAsZeroStockWhenClosingStockNegative");
            DropColumn("dbo.Materials", "ConvertAsZeroStockWhenClosingStockNegative");
        }
    }
}
