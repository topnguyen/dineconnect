namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeCustomDataLength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ConnectMembers", "CustomData", c => c.String(maxLength: 2000));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ConnectMembers", "CustomData", c => c.String(maxLength: 200));
        }
    }
}
