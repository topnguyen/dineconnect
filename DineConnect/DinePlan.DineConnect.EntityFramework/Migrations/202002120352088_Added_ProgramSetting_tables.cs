namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_ProgramSetting_tables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProgramSettings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProgramSettingTemplateId = c.Int(nullable: false),
                        DefaultValue = c.String(),
                        Locations = c.String(maxLength: 1000),
                        NonLocations = c.String(maxLength: 1000),
                        Group = c.Boolean(nullable: false),
                        LocationTag = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ProgramSetting_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ProgramSetting_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ProgramSettingTemplates", t => t.ProgramSettingTemplateId, cascadeDelete: true)
                .Index(t => t.ProgramSettingTemplateId);
            
            CreateTable(
                "dbo.ProgramSettingTemplates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ProgramSettingType = c.Int(nullable: false),
                        DefaultValue = c.String(),
                        PossibleValues = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ProgramSettingTemplate_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProgramSettings", "ProgramSettingTemplateId", "dbo.ProgramSettingTemplates");
            DropIndex("dbo.ProgramSettings", new[] { "ProgramSettingTemplateId" });
            DropTable("dbo.ProgramSettingTemplates",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ProgramSettingTemplate_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ProgramSettings",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ProgramSetting_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ProgramSetting_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
