namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedLocation_HouseTransactionDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Locations", "HouseTransactionDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Locations", "HouseTransactionDate");
        }
    }
}
