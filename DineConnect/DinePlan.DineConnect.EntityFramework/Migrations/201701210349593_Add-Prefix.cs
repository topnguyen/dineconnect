namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPrefix : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderTagGroups", "Prefix", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.OrderTagGroups", "Prefix");
        }
    }
}
