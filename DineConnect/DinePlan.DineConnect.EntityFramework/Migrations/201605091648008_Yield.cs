namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Yield : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.YieldInput",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        YieldRefId = c.Int(nullable: false),
                        InputMaterialRefId = c.Int(nullable: false),
                        InputQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnitRefId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_YieldInput_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Materials", t => t.InputMaterialRefId)
                .ForeignKey("dbo.Yield", t => t.YieldRefId)
                .Index(t => t.YieldRefId)
                .Index(t => t.InputMaterialRefId);
            
            CreateTable(
                "dbo.Yield",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationRefId = c.Int(nullable: false),
                        RequestSlipNumber = c.String(),
                        IssueTime = c.DateTime(nullable: false),
                        CompletedTime = c.DateTime(),
                        TokenNumber = c.Int(nullable: false),
                        Status = c.String(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Yield_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Yield_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationRefId)
                .Index(t => t.LocationRefId);
            
            CreateTable(
                "dbo.YieldOutput",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        YieldRefId = c.Int(nullable: false),
                        OutputMaterialRefId = c.Int(nullable: false),
                        OutputQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnitRefId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_YieldOutput_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Materials", t => t.OutputMaterialRefId)
                .ForeignKey("dbo.Yield", t => t.YieldRefId)
                .Index(t => t.YieldRefId)
                .Index(t => t.OutputMaterialRefId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.YieldOutput", "YieldRefId", "dbo.Yield");
            DropForeignKey("dbo.YieldOutput", "OutputMaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.YieldInput", "YieldRefId", "dbo.Yield");
            DropForeignKey("dbo.Yield", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.YieldInput", "InputMaterialRefId", "dbo.Materials");
            DropIndex("dbo.YieldOutput", new[] { "OutputMaterialRefId" });
            DropIndex("dbo.YieldOutput", new[] { "YieldRefId" });
            DropIndex("dbo.Yield", new[] { "LocationRefId" });
            DropIndex("dbo.YieldInput", new[] { "InputMaterialRefId" });
            DropIndex("dbo.YieldInput", new[] { "YieldRefId" });
            DropTable("dbo.YieldOutput",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_YieldOutput_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Yield",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Yield_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Yield_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.YieldInput",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_YieldInput_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
