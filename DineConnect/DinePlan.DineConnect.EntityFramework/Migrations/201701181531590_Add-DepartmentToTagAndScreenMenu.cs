namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDepartmentToTagAndScreenMenu : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PriceTags", "Deparment", c => c.String());
            AddColumn("dbo.ScreenMenus", "Deparment", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ScreenMenus", "Deparment");
            DropColumn("dbo.PriceTags", "Deparment");
        }
    }
}
