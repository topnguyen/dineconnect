namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeTallySupplierToSupplierCode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Suppliers", "SupplierCode", c => c.String());
            DropColumn("dbo.Suppliers", "TallySupplierName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Suppliers", "TallySupplierName", c => c.String());
            DropColumn("dbo.Suppliers", "SupplierCode");
        }
    }
}
