
namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PaymentTypeChange : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PaymentTypes", "Hide", c => c.Boolean(nullable: false));
            AddColumn("dbo.PaymentTypes", "AccountCode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PaymentTypes", "AccountCode");
            DropColumn("dbo.PaymentTypes", "Hide");
        }
    }
}
