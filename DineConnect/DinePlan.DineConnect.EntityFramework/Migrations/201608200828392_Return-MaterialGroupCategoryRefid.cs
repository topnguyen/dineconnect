namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReturnMaterialGroupCategoryRefid : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Returns", "MaterialGroupCategoryRefId", c => c.Int(nullable: false));
            AlterColumn("dbo.Issues", "MaterialGroupCategoryRefId", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Issues", "MaterialGroupCategoryRefId", c => c.Int(nullable: false));
            DropColumn("dbo.Returns", "MaterialGroupCategoryRefId");
        }
    }
}
