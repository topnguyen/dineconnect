namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Hr_Structure_Schedule : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BioMetricExcludedEntries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeRefId = c.Int(nullable: false),
                        ExcludeFromDate = c.DateTime(nullable: false),
                        ExcludeToDate = c.DateTime(nullable: false),
                        Reason = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_BioMetricExcludedEntry_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_BioMetricExcludedEntry_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PersonalInformations", t => t.EmployeeRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.EmployeeRefId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.DcGroupMasters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DcHeadRefId = c.Int(nullable: false),
                        GroupName = c.String(),
                        PriorityLevel = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsActive = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DcGroupMaster_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DcGroupMaster_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DcHeadMasters", t => t.DcHeadRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.DcHeadRefId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.DcHeadMasters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        HeadName = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DcHeadMaster_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DcHeadMaster_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.DcGroupMasterVsSkillSets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DcGropupRefId = c.Int(nullable: false),
                        SkillSetRefId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DcGroupMasterVsSkillSet_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DcGroupMasterVsSkillSet_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DcGroupMasters", t => t.DcGropupRefId)
                .ForeignKey("dbo.SkillSets", t => t.SkillSetRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.DcGropupRefId)
                .Index(t => t.SkillSetRefId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.DcShiftMasters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationRefId = c.Int(nullable: false),
                        DutyDesc = c.String(),
                        StationRefId = c.Int(nullable: false),
                        MaximumStaffRequired = c.Int(nullable: false),
                        MinimumStaffRequired = c.Int(nullable: false),
                        PriorityLevel = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsMandatory = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        NightFlag = c.Boolean(nullable: false),
                        TimeIn = c.Int(nullable: false),
                        BreakFlag = c.Boolean(nullable: false),
                        BreakOut = c.Int(nullable: false),
                        BreakIn = c.Int(nullable: false),
                        BreakMinutes = c.Int(nullable: false),
                        TimeOut = c.Int(nullable: false),
                        ShiftId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        TimeDescription = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DcShiftMaster_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DcShiftMaster_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DcStationMasters", t => t.StationRefId)
                .ForeignKey("dbo.Locations", t => t.LocationRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.LocationRefId)
                .Index(t => t.StationRefId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.DcStationMasters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationRefId = c.Int(nullable: false),
                        StationName = c.String(),
                        DcGroupRefId = c.Int(nullable: false),
                        MaximumStaffRequired = c.Int(nullable: false),
                        MinimumStaffRequired = c.Int(nullable: false),
                        PriorityLevel = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsMandatory = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        NightFlag = c.Boolean(nullable: false),
                        DefaultTimeFlag = c.Boolean(nullable: false),
                        TimeIn = c.Int(nullable: false),
                        BreakFlag = c.Boolean(nullable: false),
                        BreakOut = c.Int(nullable: false),
                        BreakIn = c.Int(nullable: false),
                        BreakMinutes = c.Int(nullable: false),
                        TimeOut = c.Int(nullable: false),
                        TimeDescription = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DcStationMaster_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DcStationMaster_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DcGroupMasters", t => t.DcGroupRefId)
                .ForeignKey("dbo.Locations", t => t.LocationRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.LocationRefId)
                .Index(t => t.DcGroupRefId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.DutyChartDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompanyRefId = c.Int(nullable: false),
                        DutyChartRefId = c.Int(nullable: false),
                        LocationRefId = c.Int(nullable: false),
                        DutyChartDate = c.DateTime(nullable: false),
                        UserSerialNumber = c.Decimal(nullable: false, precision: 18, scale: 2),
                        EmployeeRefId = c.Int(nullable: false),
                        AllottedStatus = c.String(nullable: false),
                        SkillRefId = c.Int(nullable: false),
                        StationRefId = c.Int(),
                        ShiftRefId = c.Int(),
                        NightFlag = c.Boolean(nullable: false),
                        TimeIn = c.Int(nullable: false),
                        TimeOut = c.Int(nullable: false),
                        OtTimeIn = c.Int(nullable: false),
                        OtTimeOut = c.Int(nullable: false),
                        BreakFlag = c.Boolean(nullable: false),
                        BreakOut = c.Int(),
                        BreakIn = c.Int(),
                        BreakMinutes = c.Int(nullable: false),
                        Remarks = c.String(),
                        HalfDayFlag = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DutyChartDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DutyChartDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyRefId)
                .ForeignKey("dbo.DcShiftMasters", t => t.ShiftRefId)
                .ForeignKey("dbo.DcStationMasters", t => t.StationRefId)
                .ForeignKey("dbo.DutyCharts", t => t.DutyChartRefId)
                .ForeignKey("dbo.Locations", t => t.LocationRefId)
                .ForeignKey("dbo.PersonalInformations", t => t.EmployeeRefId)
                .ForeignKey("dbo.SkillSets", t => t.SkillRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.CompanyRefId)
                .Index(t => t.DutyChartRefId)
                .Index(t => new { t.EmployeeRefId, t.DutyChartDate, t.UserSerialNumber, t.LocationRefId }, unique: true, name: "MPK_EmployeeRefId_DutyChartDate_LocationRefId")
                .Index(t => t.SkillRefId)
                .Index(t => t.StationRefId)
                .Index(t => t.ShiftRefId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.DutyCharts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationRefId = c.Int(nullable: false),
                        DutyChartDate = c.DateTime(nullable: false),
                        Status = c.String(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DutyChart_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DutyChart_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => new { t.LocationRefId, t.DutyChartDate, t.TenantId }, unique: true, name: "DutyChartDateLocationRefIdAndTenantId");
            
            CreateTable(
                "dbo.EmployeeDefaultWorkStations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserSerialNumber = c.Decimal(nullable: false, precision: 18, scale: 2),
                        EmployeeRefId = c.Int(nullable: false),
                        SkillRefId = c.Int(nullable: false),
                        StationRefId = c.Int(),
                        ShiftRefId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeDefaultWorkStation_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeDefaultWorkStation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DcShiftMasters", t => t.ShiftRefId)
                .ForeignKey("dbo.DcStationMasters", t => t.StationRefId)
                .ForeignKey("dbo.PersonalInformations", t => t.EmployeeRefId)
                .ForeignKey("dbo.SkillSets", t => t.SkillRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.EmployeeRefId, unique: true, name: "EmployeeRefId")
                .Index(t => t.SkillRefId)
                .Index(t => t.StationRefId)
                .Index(t => t.ShiftRefId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.MonthWiseWorkDays",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MonthYear = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        NoOfDays = c.Int(nullable: false),
                        NoOfLeaves = c.Int(nullable: false),
                        NoOfGovernmentHolidays = c.Int(nullable: false),
                        NoOfWorkDays = c.Int(nullable: false),
                        SalaryCalculationDays = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MonthWiseWorkDay_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_MonthWiseWorkDay_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.TenantId);

            AddColumn("dbo.LeaveTypes", "AllowedForMarriedEmployeesOnly", c => c.Boolean(nullable: false));


        }

        public override void Down()
        {
            DropForeignKey("dbo.TickMessageReplies", "TickMessageId", "dbo.TickMessages");
            DropForeignKey("dbo.TickMessageReplies", "TickMessageId", "dbo.TicketMessages");
            DropForeignKey("dbo.TickMessageReplies", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.TicketDiscountPromotions", "PromotionId", "dbo.Promotions");
            DropForeignKey("dbo.MonthWiseWorkDays", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.EmployeeDefaultWorkStations", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.EmployeeDefaultWorkStations", "SkillRefId", "dbo.SkillSets");
            DropForeignKey("dbo.EmployeeDefaultWorkStations", "EmployeeRefId", "dbo.PersonalInformations");
            DropForeignKey("dbo.EmployeeDefaultWorkStations", "StationRefId", "dbo.DcStationMasters");
            DropForeignKey("dbo.EmployeeDefaultWorkStations", "ShiftRefId", "dbo.DcShiftMasters");
            DropForeignKey("dbo.DutyChartDetails", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.DutyChartDetails", "SkillRefId", "dbo.SkillSets");
            DropForeignKey("dbo.DutyChartDetails", "EmployeeRefId", "dbo.PersonalInformations");
            DropForeignKey("dbo.DutyChartDetails", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.DutyChartDetails", "DutyChartRefId", "dbo.DutyCharts");
            DropForeignKey("dbo.DutyCharts", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.DutyCharts", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.DutyChartDetails", "StationRefId", "dbo.DcStationMasters");
            DropForeignKey("dbo.DutyChartDetails", "ShiftRefId", "dbo.DcShiftMasters");
            DropForeignKey("dbo.DutyChartDetails", "CompanyRefId", "dbo.Companies");
            DropForeignKey("dbo.DcShiftMasters", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.DcShiftMasters", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.DcShiftMasters", "StationRefId", "dbo.DcStationMasters");
            DropForeignKey("dbo.DcStationMasters", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.DcStationMasters", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.DcStationMasters", "DcGroupRefId", "dbo.DcGroupMasters");
            DropForeignKey("dbo.DcGroupMasterVsSkillSets", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.DcGroupMasterVsSkillSets", "SkillSetRefId", "dbo.SkillSets");
            DropForeignKey("dbo.DcGroupMasterVsSkillSets", "DcGropupRefId", "dbo.DcGroupMasters");
            DropForeignKey("dbo.DcGroupMasters", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.DcGroupMasters", "DcHeadRefId", "dbo.DcHeadMasters");
            DropForeignKey("dbo.DcHeadMasters", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.BioMetricExcludedEntries", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.BioMetricExcludedEntries", "EmployeeRefId", "dbo.PersonalInformations");
            DropIndex("dbo.TickMessageReplies", new[] { "LocationId" });
            DropIndex("dbo.TickMessageReplies", new[] { "TickMessageId" });
            DropIndex("dbo.TicketDiscountPromotions", new[] { "PromotionId" });
            DropIndex("dbo.MonthWiseWorkDays", new[] { "TenantId" });
            DropIndex("dbo.EmployeeDefaultWorkStations", new[] { "TenantId" });
            DropIndex("dbo.EmployeeDefaultWorkStations", new[] { "ShiftRefId" });
            DropIndex("dbo.EmployeeDefaultWorkStations", new[] { "StationRefId" });
            DropIndex("dbo.EmployeeDefaultWorkStations", new[] { "SkillRefId" });
            DropIndex("dbo.EmployeeDefaultWorkStations", "EmployeeRefId");
            DropIndex("dbo.DutyCharts", "DutyChartDateLocationRefIdAndTenantId");
            DropIndex("dbo.DutyChartDetails", new[] { "TenantId" });
            DropIndex("dbo.DutyChartDetails", new[] { "ShiftRefId" });
            DropIndex("dbo.DutyChartDetails", new[] { "StationRefId" });
            DropIndex("dbo.DutyChartDetails", new[] { "SkillRefId" });
            DropIndex("dbo.DutyChartDetails", "MPK_EmployeeRefId_DutyChartDate_LocationRefId");
            DropIndex("dbo.DutyChartDetails", new[] { "DutyChartRefId" });
            DropIndex("dbo.DutyChartDetails", new[] { "CompanyRefId" });
            DropIndex("dbo.DcStationMasters", new[] { "TenantId" });
            DropIndex("dbo.DcStationMasters", new[] { "DcGroupRefId" });
            DropIndex("dbo.DcStationMasters", new[] { "LocationRefId" });
            DropIndex("dbo.DcShiftMasters", new[] { "TenantId" });
            DropIndex("dbo.DcShiftMasters", new[] { "StationRefId" });
            DropIndex("dbo.DcShiftMasters", new[] { "LocationRefId" });
            DropIndex("dbo.DcGroupMasterVsSkillSets", new[] { "TenantId" });
            DropIndex("dbo.DcGroupMasterVsSkillSets", new[] { "SkillSetRefId" });
            DropIndex("dbo.DcGroupMasterVsSkillSets", new[] { "DcGropupRefId" });
            DropIndex("dbo.DcHeadMasters", new[] { "TenantId" });
            DropIndex("dbo.DcGroupMasters", new[] { "TenantId" });
            DropIndex("dbo.DcGroupMasters", new[] { "DcHeadRefId" });
            DropIndex("dbo.BioMetricExcludedEntries", new[] { "TenantId" });
            DropIndex("dbo.BioMetricExcludedEntries", new[] { "EmployeeRefId" });
            DropColumn("dbo.TillAccounts", "NonLocations");
            DropColumn("dbo.TicketTagGroups", "NonLocations");
            DropColumn("dbo.ScreenMenus", "NonLocations");
            DropColumn("dbo.RestrictDates", "NonLocations");
            DropColumn("dbo.OrderTagGroups", "NonLocations");
            DropColumn("dbo.Tickets", "FullTaxInvoiceDetails");
            DropColumn("dbo.Tickets", "InvoiceNo");
            DropColumn("dbo.LeaveTypes", "AllowedForMarriedEmployeesOnly");
            DropColumn("dbo.PaymentTypes", "Group");
            DropColumn("dbo.DinePlanUsers", "NonLocations");
            DropColumn("dbo.Departments", "NonLocations");
            DropColumn("dbo.Promotions", "NonLocations");
            DropColumn("dbo.ConnectTableGroups", "NonLocations");
            DropColumn("dbo.Calculations", "NonLocations");
            DropColumn("dbo.PriceTags", "NonLocations");
            DropColumn("dbo.MenuItems", "NonLocations");
            DropColumn("dbo.GiftVoucherTypes", "NonLocations");
            DropTable("dbo.TickMessages",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TickMessage_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TickMessage_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TickMessageReplies");
            DropTable("dbo.TicketDiscountPromotions");
            DropTable("dbo.SecondDisplays",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SecondDisplay_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SecondDisplay_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ReceiptContents",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ReceiptContent_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ReceiptContent_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.MonthWiseWorkDays",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MonthWiseWorkDay_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_MonthWiseWorkDay_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EmployeeDefaultWorkStations",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeDefaultWorkStation_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeDefaultWorkStation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DutyCharts",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DutyChart_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DutyChart_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DutyChartDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DutyChartDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DutyChartDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DcStationMasters",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DcStationMaster_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DcStationMaster_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DcShiftMasters",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DcShiftMaster_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DcShiftMaster_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DcGroupMasterVsSkillSets",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DcGroupMasterVsSkillSet_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DcGroupMasterVsSkillSet_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DcHeadMasters",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DcHeadMaster_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DcHeadMaster_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DcGroupMasters",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DcGroupMaster_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DcGroupMaster_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.BioMetricExcludedEntries",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_BioMetricExcludedEntry_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_BioMetricExcludedEntry_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
