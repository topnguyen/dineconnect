namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTerminalTolerance : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Terminals", "Tolerance", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Terminals", "Tolerance");
        }
    }
}
