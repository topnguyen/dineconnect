namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Promotions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DemandPromotionExecutions", "ProductGroupId", c => c.Int(nullable: false));
            AddColumn("dbo.FixedPromotions", "ProductGroupId", c => c.Int(nullable: false));
            AddColumn("dbo.FreePromotionExecutions", "ProductGroupId", c => c.Int(nullable: false));
            AddColumn("dbo.FreeValuePromotionExecution", "ProductGroupId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FreeValuePromotionExecution", "ProductGroupId");
            DropColumn("dbo.FreePromotionExecutions", "ProductGroupId");
            DropColumn("dbo.FixedPromotions", "ProductGroupId");
            DropColumn("dbo.DemandPromotionExecutions", "ProductGroupId");
        }
    }
}
