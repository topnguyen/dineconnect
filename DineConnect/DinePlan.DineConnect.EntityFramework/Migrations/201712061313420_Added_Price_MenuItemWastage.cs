namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_Price_MenuItemWastage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MenuItemWastageDetails", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MenuItemWastageDetails", "Price");
        }
    }
}
