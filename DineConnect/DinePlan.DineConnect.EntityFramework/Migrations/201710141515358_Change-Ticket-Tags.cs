namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeTicketTags : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MaterialStockCycleLinks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MaterialRefId = c.Int(nullable: false),
                        InventoryCycleTagRefId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MaterialStockCycleLink_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_MaterialStockCycleLink_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.InventoryCycleTags", t => t.InventoryCycleTagRefId, cascadeDelete: true)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId, cascadeDelete: true)
                .Index(t => new { t.MaterialRefId, t.InventoryCycleTagRefId }, unique: true, name: "MPK_MaterialRefId_InvCycleRefId");
            
            AddColumn("dbo.MenuItems", "Files", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MaterialStockCycleLinks", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.MaterialStockCycleLinks", "InventoryCycleTagRefId", "dbo.InventoryCycleTags");
            DropIndex("dbo.MaterialStockCycleLinks", "MPK_MaterialRefId_InvCycleRefId");
            DropColumn("dbo.MenuItems", "Files");
            DropTable("dbo.MaterialStockCycleLinks",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MaterialStockCycleLink_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_MaterialStockCycleLink_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
