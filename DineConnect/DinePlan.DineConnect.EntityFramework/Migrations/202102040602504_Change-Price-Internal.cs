namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangePriceInternal : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrderTagLocationPrices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrderTagId = c.Int(nullable: false),
                        LocationId = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .ForeignKey("dbo.OrderTags", t => t.OrderTagId, cascadeDelete: true)
                .Index(t => t.OrderTagId)
                .Index(t => t.LocationId);
            
            CreateTable(
                "dbo.UpMenuItemLocationPrices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UpMenuItemId = c.Int(nullable: false),
                        LocationId = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .ForeignKey("dbo.UpMenuItems", t => t.UpMenuItemId, cascadeDelete: true)
                .Index(t => t.UpMenuItemId)
                .Index(t => t.LocationId);
            
            AlterColumn("dbo.PromotionCategories", "Tag", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UpMenuItemLocationPrices", "UpMenuItemId", "dbo.UpMenuItems");
            DropForeignKey("dbo.UpMenuItemLocationPrices", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.OrderTagLocationPrices", "OrderTagId", "dbo.OrderTags");
            DropForeignKey("dbo.OrderTagLocationPrices", "LocationId", "dbo.Locations");
            DropIndex("dbo.UpMenuItemLocationPrices", new[] { "LocationId" });
            DropIndex("dbo.UpMenuItemLocationPrices", new[] { "UpMenuItemId" });
            DropIndex("dbo.OrderTagLocationPrices", new[] { "LocationId" });
            DropIndex("dbo.OrderTagLocationPrices", new[] { "OrderTagId" });
            AlterColumn("dbo.PromotionCategories", "Tag", c => c.String());
            DropTable("dbo.UpMenuItemLocationPrices");
            DropTable("dbo.OrderTagLocationPrices");
        }
    }
}
