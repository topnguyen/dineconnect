namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyInvoice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InvoiceDirectCreditLink", "Sno", c => c.Int(nullable: false));
            AddColumn("dbo.InvoiceTaxDetails", "Sno", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.InvoiceTaxDetails", "Sno");
            DropColumn("dbo.InvoiceDirectCreditLink", "Sno");
        }
    }
}
