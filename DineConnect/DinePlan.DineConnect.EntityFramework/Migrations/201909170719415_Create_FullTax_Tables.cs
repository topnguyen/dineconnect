namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Create_FullTax_Tables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FullTaxAddresses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FullMemberId = c.Int(nullable: false),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        City = c.String(),
                        State = c.String(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FullTaxMembers", t => t.FullMemberId, cascadeDelete: true)
                .Index(t => t.FullMemberId);
            
            CreateTable(
                "dbo.FullTaxMembers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TaxId = c.String(maxLength: 20),
                        BranchId = c.String(maxLength: 20),
                        Name = c.String(),
                        Phone = c.String(),
                        Locations = c.String(),
                        Group = c.Boolean(nullable: false),
                        NonLocations = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_FullTaxMember_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_FullTaxMember_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.TaxId, t.BranchId }, unique: true, name: "IX_FullTaxFirstAndSecond");
            
            CreateTable(
                "dbo.FullTaxInvoices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FullMemberId = c.Int(nullable: false),
                        LocationId = c.Int(nullable: false),
                        TicketId = c.Int(nullable: false),
                        FullInvoiceNumber = c.String(),
                        Status = c.String(),
                        CopyCount = c.Int(nullable: false),
                        User = c.String(),
                        PrintTime = c.DateTime(nullable: false),
                        PreviousInvoiceNumber = c.String(),
                        Locations = c.String(),
                        Group = c.Boolean(nullable: false),
                        NonLocations = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_FullTaxInvoice_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_FullTaxInvoice_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FullTaxMembers", t => t.FullMemberId, cascadeDelete: true)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .ForeignKey("dbo.Tickets", t => t.TicketId, cascadeDelete: true)
                .Index(t => t.FullMemberId)
                .Index(t => t.LocationId)
                .Index(t => t.TicketId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FullTaxInvoices", "TicketId", "dbo.Tickets");
            DropForeignKey("dbo.FullTaxInvoices", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.FullTaxInvoices", "FullMemberId", "dbo.FullTaxMembers");
            DropForeignKey("dbo.FullTaxAddresses", "FullMemberId", "dbo.FullTaxMembers");
            DropIndex("dbo.FullTaxInvoices", new[] { "TicketId" });
            DropIndex("dbo.FullTaxInvoices", new[] { "LocationId" });
            DropIndex("dbo.FullTaxInvoices", new[] { "FullMemberId" });
            DropIndex("dbo.FullTaxMembers", "IX_FullTaxFirstAndSecond");
            DropIndex("dbo.FullTaxAddresses", new[] { "FullMemberId" });
            DropTable("dbo.FullTaxInvoices",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_FullTaxInvoice_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_FullTaxInvoice_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.FullTaxMembers",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_FullTaxMember_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_FullTaxMember_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.FullTaxAddresses");
        }
    }
}
