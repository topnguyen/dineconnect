namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_ScreenMenuCategories1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ScreenMenuCategorySchedules", "ScreenMenuCategoryId", "dbo.ScreenMenuCategories");
            DropIndex("dbo.ScreenMenuCategorySchedules", new[] { "ScreenMenuCategoryId" });
            AddColumn("dbo.ScreenMenuCategories", "ScreenMenuCategorySchedule", c => c.String());
            DropTable("dbo.ScreenMenuCategorySchedules");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ScreenMenuCategorySchedules",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        ScreenMenuCategoryId = c.Int(nullable: false),
                        StartHour = c.Int(nullable: false),
                        StartMinute = c.Int(nullable: false),
                        EndHour = c.Int(nullable: false),
                        EndMinute = c.Int(nullable: false),
                        MonthDays = c.String(),
                        Days = c.String(maxLength: 100),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.ScreenMenuCategories", "ScreenMenuCategorySchedule");
            CreateIndex("dbo.ScreenMenuCategorySchedules", "ScreenMenuCategoryId");
            AddForeignKey("dbo.ScreenMenuCategorySchedules", "ScreenMenuCategoryId", "dbo.ScreenMenuCategories", "Id", cascadeDelete: true);
        }
    }
}
