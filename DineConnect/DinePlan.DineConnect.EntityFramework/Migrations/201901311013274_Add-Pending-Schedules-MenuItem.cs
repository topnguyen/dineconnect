namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPendingSchedulesMenuItem : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MenuItemSchedules",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StartHour = c.Int(nullable: false),
                        StartMinute = c.Int(nullable: false),
                        EndHour = c.Int(nullable: false),
                        EndMinute = c.Int(nullable: false),
                        Days = c.String(),
                        MenuItemId = c.Int(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MenuItems", t => t.MenuItemId, cascadeDelete: true)
                .Index(t => t.MenuItemId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MenuItemSchedules", "MenuItemId", "dbo.MenuItems");
            DropIndex("dbo.MenuItemSchedules", new[] { "MenuItemId" });
            DropTable("dbo.MenuItemSchedules");
        }
    }
}
