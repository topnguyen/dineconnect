namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveTicektIdFromLogger : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.PlanLoggers", "TicketId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PlanLoggers", "TicketId", c => c.Int(nullable: false));
        }
    }
}
