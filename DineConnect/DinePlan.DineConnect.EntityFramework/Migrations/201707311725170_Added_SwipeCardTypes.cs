namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_SwipeCardTypes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SwipeCardTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 20),
                        DepositRequired = c.Boolean(nullable: false),
                        DepositAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ExpiryDaysFromIssue = c.Int(),
                        RefundAllowed = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SwipeCardType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "UQ_CARDTYPENAME");
            
            AddColumn("dbo.SwipeCards", "CardTypeRefId", c => c.Int(nullable: false));
            AlterColumn("dbo.SwipeCards", "ExpiryDaysFromIssue", c => c.Int());
            CreateIndex("dbo.SwipeCards", "CardTypeRefId");
            AddForeignKey("dbo.SwipeCards", "CardTypeRefId", "dbo.SwipeCardTypes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SwipeCards", "CardTypeRefId", "dbo.SwipeCardTypes");
            DropIndex("dbo.SwipeCardTypes", "UQ_CARDTYPENAME");
            DropIndex("dbo.SwipeCards", new[] { "CardTypeRefId" });
            AlterColumn("dbo.SwipeCards", "ExpiryDaysFromIssue", c => c.Int(nullable: false));
            DropColumn("dbo.SwipeCards", "CardTypeRefId");
            DropTable("dbo.SwipeCardTypes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SwipeCardType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
