namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class increaselengthtaginmenuitem : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MenuItems", "Tag", c => c.String(maxLength: 500));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MenuItems", "Tag", c => c.String(maxLength: 50));
        }
    }
}
