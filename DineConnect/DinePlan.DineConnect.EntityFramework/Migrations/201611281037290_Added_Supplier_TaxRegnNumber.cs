namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_Supplier_TaxRegnNumber : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Suppliers", "TaxRegistrationNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Suppliers", "TaxRegistrationNumber");
        }
    }
}
