namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOdooTaxAddOn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Taxes", "AddOn", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Taxes", "AddOn");
        }
    }
}
