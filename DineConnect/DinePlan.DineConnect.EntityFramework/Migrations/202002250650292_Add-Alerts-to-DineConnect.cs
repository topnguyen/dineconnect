namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddAlertstoDineConnect : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Alerts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        EventId = c.Guid(nullable: false),
                        LocationId = c.Int(nullable: false),
                        AlertTypeId = c.Int(nullable: false),
                        AlertTime = c.DateTime(nullable: false),
                        TicketNumber = c.String(),
                        AlertValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AlertLog = c.String(),
                        AlertUser = c.String(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Alert_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .Index(t => t.LocationId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Alerts", "LocationId", "dbo.Locations");
            DropIndex("dbo.Alerts", new[] { "LocationId" });
            DropTable("dbo.Alerts",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Alert_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
