namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Changed_TickMessage_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TickMessageReplies", "Acknowledged", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TickMessageReplies", "Acknowledged");
        }
    }
}
