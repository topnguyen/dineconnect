namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLastTimeTruncAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AttendanceLogs", "CheckTimeTrunc", c => c.DateTime(nullable: false, storeType: "date"));
            Sql("Update AttendanceLogs set CheckTime=CheckTimeTrunc");
        }
        
        public override void Down()
        {
            DropColumn("dbo.AttendanceLogs", "CheckTimeTrunc");
        }
    }
}
