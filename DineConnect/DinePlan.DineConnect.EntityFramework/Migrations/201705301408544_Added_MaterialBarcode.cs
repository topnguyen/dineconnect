namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_MaterialBarcode : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MaterialBarCodes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Barcode = c.String(maxLength: 50),
                        MaterialRefId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MaterialBarCode_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId, cascadeDelete: true)
                .Index(t => t.MaterialRefId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MaterialBarCodes", "MaterialRefId", "dbo.Materials");
            DropIndex("dbo.MaterialBarCodes", new[] { "MaterialRefId" });
            DropTable("dbo.MaterialBarCodes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MaterialBarCode_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
