namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class Add_ParationTime_MenuPortion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MenuItemPortions", "PreparationTime", c => c.Int(nullable: true, false, 0));
        }

        public override void Down()
        {
            DropColumn("dbo.MenuItemPortions", "PreparationTime");
        }
    }
}
