namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_SalesTaxes : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.TaxTemplateMappings", "TaxRefId");
            AddForeignKey("dbo.TaxTemplateMappings", "TaxRefId", "dbo.Taxes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TaxTemplateMappings", "TaxRefId", "dbo.Taxes");
            DropIndex("dbo.TaxTemplateMappings", new[] { "TaxRefId" });
        }
    }
}
