namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Add_ImportSettings_And_Terminals_tbs : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ImportSettings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FilePath = c.String(),
                        Locations = c.String(),
                        Group = c.Boolean(nullable: false),
                        LocationTag = c.Boolean(nullable: false),
                        NonLocations = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ImportSetting_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ImportSetting_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Terminals",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        TerminalCode = c.String(),
                        IsDefault = c.Boolean(nullable: false),
                        AutoLogout = c.Boolean(nullable: false),
                        Float = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsEndWorkTimeMoneyShown = c.Boolean(nullable: false),
                        IsCollectionTerminal = c.Boolean(nullable: false),
                        WorkTimeTotalInFloat = c.Boolean(nullable: false),
                        Denominations = c.String(),
                        Oid = c.Int(nullable: false),
                        Locations = c.String(),
                        Group = c.Boolean(nullable: false),
                        LocationTag = c.Boolean(nullable: false),
                        NonLocations = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Terminal_ConnectFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Terminal_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Terminal_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Terminals",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Terminal_ConnectFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Terminal_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Terminal_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ImportSettings",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ImportSetting_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ImportSetting_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
