namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddImportedColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ImportSettings", "IsImported", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ImportSettings", "IsImported");
        }
    }
}
