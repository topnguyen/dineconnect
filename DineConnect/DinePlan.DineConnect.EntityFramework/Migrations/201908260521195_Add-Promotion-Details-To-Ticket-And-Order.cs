namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPromotionDetailsToTicketAndOrder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ConnectCardRedemptions", "RedemptionType", c => c.Int(nullable: false));
            AddColumn("dbo.ConnectCardRedemptions", "RedemptionReferences", c => c.String());
            AddColumn("dbo.ConnectCardTypes", "Name", c => c.String());
            AddColumn("dbo.Orders", "OrderPromotionDetails", c => c.String());
            AddColumn("dbo.Tickets", "TicketPromotionDetails", c => c.String());
            DropColumn("dbo.ConnectCards", "ExpiryDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ConnectCards", "ExpiryDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.Tickets", "TicketPromotionDetails");
            DropColumn("dbo.Orders", "OrderPromotionDetails");
            DropColumn("dbo.ConnectCardTypes", "Name");
            DropColumn("dbo.ConnectCardRedemptions", "RedemptionReferences");
            DropColumn("dbo.ConnectCardRedemptions", "RedemptionType");
        }
    }
}
