namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCountToProductComboItem : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductComboItems", "Count", c => c.Int(nullable: false));
            Sql("UPDATE ProductComboItems SET Count=1 where AutoSelect=1");
        }

        public override void Down()
        {
            DropColumn("dbo.ProductComboItems", "Count");
        }
    }
}
