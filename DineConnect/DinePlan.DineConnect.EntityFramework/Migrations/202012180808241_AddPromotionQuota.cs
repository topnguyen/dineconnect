namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddPromotionQuota : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PromotionQuotas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PromotionId = c.Int(nullable: false),
                        Count = c.Int(nullable: false),
                        Each = c.Boolean(nullable: false),
                        ResetType = c.Int(nullable: false),
                        ResetWeekValue = c.Int(),
                        ResetMonthValue = c.DateTime(),
                        Oid = c.Int(nullable: false),
                        Plants = c.String(),
                        Locations = c.String(),
                        NonLocations = c.String(),
                        Group = c.Boolean(nullable: false),
                        LocationTag = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PromotionQuota_ConnectFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PromotionQuota_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PromotionQuota_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Promotions", t => t.PromotionId, cascadeDelete: true)
                .Index(t => t.PromotionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PromotionQuotas", "PromotionId", "dbo.Promotions");
            DropIndex("dbo.PromotionQuotas", new[] { "PromotionId" });
            DropTable("dbo.PromotionQuotas",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PromotionQuota_ConnectFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PromotionQuota_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PromotionQuota_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
