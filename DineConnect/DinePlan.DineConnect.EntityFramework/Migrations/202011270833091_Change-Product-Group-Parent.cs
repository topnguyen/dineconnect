namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeProductGroupParent : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CategoryHierarchies", "ParentId", "dbo.CategoryHierarchies");
            DropForeignKey("dbo.Categories", "CategoryHierarchyId", "dbo.CategoryHierarchies");
            DropIndex("dbo.Categories", new[] { "CategoryHierarchyId" });
            DropIndex("dbo.CategoryHierarchies", new[] { "ParentId" });
            AddColumn("dbo.ProductGroups", "ParentId", c => c.Int());
            CreateIndex("dbo.ProductGroups", "ParentId");
            AddForeignKey("dbo.ProductGroups", "ParentId", "dbo.ProductGroups", "Id");
            DropColumn("dbo.Categories", "CategoryHierarchyId");
            DropTable("dbo.CategoryHierarchies",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CategoryHierarchy_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_CategoryHierarchy_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.CategoryHierarchies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(maxLength: 10),
                        Name = c.String(maxLength: 50),
                        ParentId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CategoryHierarchy_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_CategoryHierarchy_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Categories", "CategoryHierarchyId", c => c.Int());
            DropForeignKey("dbo.ProductGroups", "ParentId", "dbo.ProductGroups");
            DropIndex("dbo.ProductGroups", new[] { "ParentId" });
            DropColumn("dbo.ProductGroups", "ParentId");
            CreateIndex("dbo.CategoryHierarchies", "ParentId");
            CreateIndex("dbo.Categories", "CategoryHierarchyId");
            AddForeignKey("dbo.Categories", "CategoryHierarchyId", "dbo.CategoryHierarchies", "Id");
            AddForeignKey("dbo.CategoryHierarchies", "ParentId", "dbo.CategoryHierarchies", "Id");
        }
    }
}
