namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_LocGroup_CardLimits : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LocationGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LocationGroup_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_LocationGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Locations", "LocationGroupId", c => c.Int());
            AddColumn("dbo.SwipeCards", "CanExcludeTaxAndCharges", c => c.Boolean(nullable: false));
            AddColumn("dbo.SwipeCards", "DailyLimit", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SwipeCards", "WeeklyLimit", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SwipeCards", "MonthlyLimit", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SwipeCards", "YearlyLimit", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SwipeCardTypes", "CanExcludeTaxAndCharges", c => c.Boolean(nullable: false));
            AddColumn("dbo.SwipeCardTypes", "DailyLimit", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SwipeCardTypes", "WeeklyLimit", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SwipeCardTypes", "MonthlyLimit", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SwipeCardTypes", "YearlyLimit", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.OrderTagGroups", "Locations", c => c.String());
            CreateIndex("dbo.Locations", "LocationGroupId");
            AddForeignKey("dbo.Locations", "LocationGroupId", "dbo.LocationGroups", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Locations", "LocationGroupId", "dbo.LocationGroups");
            DropIndex("dbo.Locations", new[] { "LocationGroupId" });
            DropColumn("dbo.OrderTagGroups", "Locations");
            DropColumn("dbo.SwipeCardTypes", "YearlyLimit");
            DropColumn("dbo.SwipeCardTypes", "MonthlyLimit");
            DropColumn("dbo.SwipeCardTypes", "WeeklyLimit");
            DropColumn("dbo.SwipeCardTypes", "DailyLimit");
            DropColumn("dbo.SwipeCardTypes", "CanExcludeTaxAndCharges");
            DropColumn("dbo.SwipeCards", "YearlyLimit");
            DropColumn("dbo.SwipeCards", "MonthlyLimit");
            DropColumn("dbo.SwipeCards", "WeeklyLimit");
            DropColumn("dbo.SwipeCards", "DailyLimit");
            DropColumn("dbo.SwipeCards", "CanExcludeTaxAndCharges");
            DropColumn("dbo.Locations", "LocationGroupId");
            DropTable("dbo.LocationGroups",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LocationGroup_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_LocationGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
