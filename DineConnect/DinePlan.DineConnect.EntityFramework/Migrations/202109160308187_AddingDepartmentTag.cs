namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingDepartmentTag : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Departments", "Tag", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Departments", "Tag");
        }
    }
}
