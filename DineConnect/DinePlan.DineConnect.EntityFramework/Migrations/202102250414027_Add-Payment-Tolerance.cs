namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPaymentTolerance : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PaymentTypes", "PaymentTenderType", c => c.Int(nullable: false));
            AddColumn("dbo.Terminals", "AcceptTolerance", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Terminals", "AcceptTolerance");
            DropColumn("dbo.PaymentTypes", "PaymentTenderType");
        }
    }
}
