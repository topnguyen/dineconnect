namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_SwipeLedger_ShiftRefId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SwipeCardLedgers", "ShiftRefId", c => c.Int());
            CreateIndex("dbo.SwipeCardLedgers", "ShiftRefId");
            AddForeignKey("dbo.SwipeCardLedgers", "ShiftRefId", "dbo.SwipeShifts", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SwipeCardLedgers", "ShiftRefId", "dbo.SwipeShifts");
            DropIndex("dbo.SwipeCardLedgers", new[] { "ShiftRefId" });
            DropColumn("dbo.SwipeCardLedgers", "ShiftRefId");
        }
    }
}
