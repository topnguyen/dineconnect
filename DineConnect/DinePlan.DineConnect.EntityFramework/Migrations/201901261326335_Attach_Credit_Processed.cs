namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Attach_Credit_Processed : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tickets", "CreditProcessed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tickets", "CreditProcessed");
        }
    }
}
