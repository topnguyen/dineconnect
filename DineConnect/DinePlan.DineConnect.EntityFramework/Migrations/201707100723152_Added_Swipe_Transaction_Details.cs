namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_Swipe_Transaction_Details : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MemberCards",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MemberRefId = c.Int(nullable: false),
                        CardRefId = c.Int(nullable: false),
                        IsPrimaryCard = c.Boolean(nullable: false),
                        PrimaryCardRefId = c.Int(),
                        IssueDate = c.DateTime(nullable: false),
                        ReturnDate = c.DateTime(),
                        IsActive = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MemberCard_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_MemberCard_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SwipeCards", t => t.CardRefId, cascadeDelete: true)
                .ForeignKey("dbo.SwipeMembers", t => t.MemberRefId, cascadeDelete: true)
                .Index(t => t.MemberRefId)
                .Index(t => t.CardRefId);
            
            CreateTable(
                "dbo.SwipeCards",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CardNumber = c.String(),
                        Balance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ActiveStatus = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SwipeCard_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SwipeCard_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SwipeCardLedgers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LedgerTime = c.DateTime(nullable: false),
                        MemberRefId = c.Int(nullable: false),
                        CardRefId = c.Int(nullable: false),
                        PrimaryCardRefId = c.Int(nullable: false),
                        TransactionType = c.Int(nullable: false),
                        Description = c.String(),
                        OpeningBalance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Credit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Debit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ClosingBalance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SwipeCardLedger_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SwipeCardLedger_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SwipeCards", t => t.CardRefId, cascadeDelete: true)
                .ForeignKey("dbo.SwipeMembers", t => t.MemberRefId, cascadeDelete: true)
                .Index(t => t.MemberRefId)
                .Index(t => t.CardRefId);
            
            CreateTable(
                "dbo.SwipeTransactionDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TransactionTime = c.DateTime(nullable: false),
                        MemberRefId = c.Int(nullable: false),
                        CardRefId = c.Int(nullable: false),
                        PrimaryCardRefId = c.Int(nullable: false),
                        TransactionType = c.Int(nullable: false),
                        Description = c.String(),
                        Credit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Debit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SwipeTransactionDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SwipeTransactionDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SwipeCards", t => t.CardRefId, cascadeDelete: true)
                .ForeignKey("dbo.SwipeMembers", t => t.MemberRefId, cascadeDelete: true)
                .Index(t => t.MemberRefId)
                .Index(t => t.CardRefId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SwipeTransactionDetails", "MemberRefId", "dbo.SwipeMembers");
            DropForeignKey("dbo.SwipeTransactionDetails", "CardRefId", "dbo.SwipeCards");
            DropForeignKey("dbo.SwipeCardLedgers", "MemberRefId", "dbo.SwipeMembers");
            DropForeignKey("dbo.SwipeCardLedgers", "CardRefId", "dbo.SwipeCards");
            DropForeignKey("dbo.MemberCards", "MemberRefId", "dbo.SwipeMembers");
            DropForeignKey("dbo.MemberCards", "CardRefId", "dbo.SwipeCards");
            DropIndex("dbo.SwipeTransactionDetails", new[] { "CardRefId" });
            DropIndex("dbo.SwipeTransactionDetails", new[] { "MemberRefId" });
            DropIndex("dbo.SwipeCardLedgers", new[] { "CardRefId" });
            DropIndex("dbo.SwipeCardLedgers", new[] { "MemberRefId" });
            DropIndex("dbo.MemberCards", new[] { "CardRefId" });
            DropIndex("dbo.MemberCards", new[] { "MemberRefId" });
            DropTable("dbo.SwipeTransactionDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SwipeTransactionDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SwipeTransactionDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.SwipeCardLedgers",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SwipeCardLedger_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SwipeCardLedger_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.SwipeCards",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SwipeCard_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SwipeCard_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.MemberCards",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MemberCard_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_MemberCard_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
