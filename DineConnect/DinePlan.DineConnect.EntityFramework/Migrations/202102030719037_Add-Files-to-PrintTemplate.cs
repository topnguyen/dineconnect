namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFilestoPrintTemplate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PrintTemplates", "Files", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PrintTemplates", "Files");
        }
    }
}
