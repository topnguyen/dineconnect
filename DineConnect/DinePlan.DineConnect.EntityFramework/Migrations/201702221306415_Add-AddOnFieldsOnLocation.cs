namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAddOnFieldsOnLocation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Locations", "AddOn", c => c.String());
            DropColumn("dbo.Locations", "TallyCompanyName");
            DropColumn("dbo.Locations", "LocationServiceUrl");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Locations", "LocationServiceUrl", c => c.String());
            AddColumn("dbo.Locations", "TallyCompanyName", c => c.String());
            DropColumn("dbo.Locations", "AddOn");
        }
    }
}
