namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOrderTag : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TransactionOrderTags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrderId = c.Int(nullable: false),
                        OrderTagGroupId = c.Int(nullable: false),
                        OrderTagId = c.Int(nullable: false),
                        MenuItemPortionId = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 3),
                        Quantity = c.Decimal(nullable: false, precision: 18, scale: 3),
                        TagName = c.String(),
                        TagNote = c.String(),
                        TagValue = c.String(),
                        TaxFree = c.Boolean(nullable: false),
                        AddTagPriceToOrderPrice = c.Boolean(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Orders", t => t.OrderId, cascadeDelete: true)
                .Index(t => t.OrderId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TransactionOrderTags", "OrderId", "dbo.Orders");
            DropIndex("dbo.TransactionOrderTags", new[] { "OrderId" });
            DropTable("dbo.TransactionOrderTags");
        }
    }
}
