namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddPortionToFullEntity : DbMigration
    {
        public override void Up()
        {
            AlterTableAnnotations(
                "dbo.MenuItemPortions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        MenuItemId = c.Int(nullable: false),
                        MultiPlier = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 3),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_MenuItemPortion_SoftDelete",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AddColumn("dbo.MenuItemPortions", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.MenuItemPortions", "DeleterUserId", c => c.Long());
            AddColumn("dbo.MenuItemPortions", "DeletionTime", c => c.DateTime());
            AddColumn("dbo.MenuItemPortions", "LastModificationTime", c => c.DateTime());
            AddColumn("dbo.MenuItemPortions", "LastModifierUserId", c => c.Long());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MenuItemPortions", "LastModifierUserId");
            DropColumn("dbo.MenuItemPortions", "LastModificationTime");
            DropColumn("dbo.MenuItemPortions", "DeletionTime");
            DropColumn("dbo.MenuItemPortions", "DeleterUserId");
            DropColumn("dbo.MenuItemPortions", "IsDeleted");
            AlterTableAnnotations(
                "dbo.MenuItemPortions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        MenuItemId = c.Int(nullable: false),
                        MultiPlier = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 3),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_MenuItemPortion_SoftDelete",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
        }
    }
}
