namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Files_col_to_ScreenCategorty_And_ScreenMenuItem_tbs : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ScreenMenuCategories", "Files", c => c.String());
            AddColumn("dbo.ScreenMenuCategories", "DownloadImage", c => c.Guid(nullable: false));
            AddColumn("dbo.ScreenMenuItems", "Files", c => c.String());
            AddColumn("dbo.ScreenMenuItems", "DownloadImage", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ScreenMenuItems", "DownloadImage");
            DropColumn("dbo.ScreenMenuItems", "Files");
            DropColumn("dbo.ScreenMenuCategories", "DownloadImage");
            DropColumn("dbo.ScreenMenuCategories", "Files");
        }
    }
}