namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLocationDepartment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Departments", "Locations", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Departments", "Locations");
        }
    }
}
