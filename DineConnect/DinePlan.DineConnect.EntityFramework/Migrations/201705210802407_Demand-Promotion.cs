namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DemandPromotion : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DemandPromotions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PromotionId = c.Int(nullable: false),
                        PromotionValueType = c.Int(nullable: false),
                        ButtonCaption = c.String(maxLength: 50),
                        ButtonColor = c.String(maxLength: 10),
                        PromotionValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PromotionOverride = c.Boolean(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Promotions", t => t.PromotionId, cascadeDelete: true)
                .Index(t => t.PromotionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DemandPromotions", "PromotionId", "dbo.Promotions");
            DropIndex("dbo.DemandPromotions", new[] { "PromotionId" });
            DropTable("dbo.DemandPromotions");
        }
    }
}
