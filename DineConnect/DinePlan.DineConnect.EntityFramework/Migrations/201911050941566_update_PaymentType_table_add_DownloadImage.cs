namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_PaymentType_table_add_DownloadImage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PaymentTypes", "DownloadImage", c => c.Guid());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PaymentTypes", "DownloadImage");
        }
    }
}
