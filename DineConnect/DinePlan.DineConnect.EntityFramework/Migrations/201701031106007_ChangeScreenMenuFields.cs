namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeScreenMenuFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ScreenMenuCategories", "MenuItemButtonHeight", c => c.Int(nullable: false));
            DropColumn("dbo.ScreenMenuCategories", "MenuItemButtonColor");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ScreenMenuCategories", "MenuItemButtonColor", c => c.String());
            DropColumn("dbo.ScreenMenuCategories", "MenuItemButtonHeight");
        }
    }
}
