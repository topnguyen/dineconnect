namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_IssueMaterialGroupCategory : DbMigration
    {
        public override void Up()
        {
            AlterTableAnnotations(
                "dbo.MaterialGroupCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MaterialGroupCategoryName = c.String(nullable: false),
                        MaterialGroupRefId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_MaterialGroupCategory_MustHaveTenant",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AddColumn("dbo.MaterialGroupCategories", "TenantId", c => c.Int(nullable: false));
            AddColumn("dbo.Issues", "MaterialGroupCategoryRefId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Issues", "MaterialGroupCategoryRefId");
            DropColumn("dbo.MaterialGroupCategories", "TenantId");
            AlterTableAnnotations(
                "dbo.MaterialGroupCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MaterialGroupCategoryName = c.String(nullable: false),
                        MaterialGroupRefId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_MaterialGroupCategory_MustHaveTenant",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
        }
    }
}
