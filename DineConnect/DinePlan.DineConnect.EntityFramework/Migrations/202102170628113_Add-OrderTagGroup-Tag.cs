namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOrderTagGroupTag : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderTags", "Tag", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.OrderTags", "Tag");
        }
    }
}
