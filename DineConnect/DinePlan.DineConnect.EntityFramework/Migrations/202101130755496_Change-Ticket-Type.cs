namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeTicketType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Numerators", "OutputFormat", c => c.String());
            AddColumn("dbo.Numerators", "ResetNumerator", c => c.Boolean(nullable: false));
            AddColumn("dbo.Numerators", "ResetNumeratorType", c => c.Int(nullable: false));
            DropColumn("dbo.Numerators", "TicketFormat");
            DropColumn("dbo.Numerators", "IsApplyLuhanTicketNumber");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Numerators", "IsApplyLuhanTicketNumber", c => c.Boolean(nullable: false));
            AddColumn("dbo.Numerators", "TicketFormat", c => c.String());
            DropColumn("dbo.Numerators", "ResetNumeratorType");
            DropColumn("dbo.Numerators", "ResetNumerator");
            DropColumn("dbo.Numerators", "OutputFormat");
        }
    }
}
