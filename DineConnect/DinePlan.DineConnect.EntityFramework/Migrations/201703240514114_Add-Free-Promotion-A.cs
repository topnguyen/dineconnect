namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFreePromotionA : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PriceTags", "Name", c => c.String(maxLength: 10));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PriceTags", "Name", c => c.String());
        }
    }
}
