namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Supplier_MaterialLink : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Suppliers", "LoadOnlyLinkedMaterials", c => c.Boolean(nullable: false));
            Sql("Update Suppliers SET LoadOnlyLinkedMaterials = 1");
        }
        
        public override void Down()
        {
            DropColumn("dbo.Suppliers", "LoadOnlyLinkedMaterials");
        }
    }
}
