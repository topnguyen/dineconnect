namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_3Digits_6Digits_SecondPart : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.SupplierMaterials", "MinimumOrderQuantity", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.IssueRecipeDetails", "RecipeProductionQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.IssueRecipeDetails", "CompletedQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.MaterialRecipeTypes", "PrdBatchQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
            AlterColumn("dbo.MenuItemWastageDetails", "WastageQty", c => c.Decimal(nullable: false, precision: 18, scale: 6));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MenuItemWastageDetails", "WastageQty", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.MaterialRecipeTypes", "PrdBatchQty", c => c.Decimal(nullable: false, precision: 18, scale: 3));
            AlterColumn("dbo.IssueRecipeDetails", "CompletedQty", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.IssueRecipeDetails", "RecipeProductionQty", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.SupplierMaterials", "MinimumOrderQuantity", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
