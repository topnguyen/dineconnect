// <auto-generated />
namespace DinePlan.DineConnect.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Modify_DelAggItem_DelAggVariantGroup : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Modify_DelAggItem_DelAggVariantGroup));
        
        string IMigrationMetadata.Id
        {
            get { return "202111291546353_Modify_DelAggItem_DelAggVariantGroup"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
