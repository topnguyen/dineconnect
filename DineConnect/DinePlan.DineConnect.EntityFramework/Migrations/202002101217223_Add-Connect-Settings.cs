namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddConnectSettings : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ConnectAddOns", "Settings", c => c.String(maxLength: 1000));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ConnectAddOns", "Settings", c => c.String(maxLength: 200));
        }
    }
}
