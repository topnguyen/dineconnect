namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMenuItemMandatory : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ScreenMenuItems", "MenuItemId", "dbo.MenuItems");
            DropIndex("dbo.ScreenMenuItems", new[] { "MenuItemId" });
            AlterColumn("dbo.ScreenMenuItems", "MenuItemId", c => c.Int(nullable: false));
            CreateIndex("dbo.ScreenMenuItems", "MenuItemId");
            AddForeignKey("dbo.ScreenMenuItems", "MenuItemId", "dbo.MenuItems", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ScreenMenuItems", "MenuItemId", "dbo.MenuItems");
            DropIndex("dbo.ScreenMenuItems", new[] { "MenuItemId" });
            AlterColumn("dbo.ScreenMenuItems", "MenuItemId", c => c.Int());
            CreateIndex("dbo.ScreenMenuItems", "MenuItemId");
            AddForeignKey("dbo.ScreenMenuItems", "MenuItemId", "dbo.MenuItems", "Id");
        }
    }
}
