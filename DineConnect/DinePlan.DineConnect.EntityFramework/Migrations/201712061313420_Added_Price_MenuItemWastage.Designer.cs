// <auto-generated />
namespace DinePlan.DineConnect.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Added_Price_MenuItemWastage : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Added_Price_MenuItemWastage));
        
        string IMigrationMetadata.Id
        {
            get { return "201712061313420_Added_Price_MenuItemWastage"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
