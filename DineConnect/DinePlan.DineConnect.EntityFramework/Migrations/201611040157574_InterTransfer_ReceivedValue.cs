namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InterTransfer_ReceivedValue : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InterTransfers", "ReceivedValue", c => c.Decimal(precision: 18, scale: 6));
        }
        
        public override void Down()
        {
            DropColumn("dbo.InterTransfers", "ReceivedValue");
        }
    }
}
