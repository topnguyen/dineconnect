namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_DelAggCategory_Nullable : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.DelAggCategories", new[] { "DelAggCatParentId" });
            AlterColumn("dbo.DelAggCategories", "DelAggCatParentId", c => c.Int());
            CreateIndex("dbo.DelAggCategories", "DelAggCatParentId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.DelAggCategories", new[] { "DelAggCatParentId" });
            AlterColumn("dbo.DelAggCategories", "DelAggCatParentId", c => c.Int(nullable: false));
            CreateIndex("dbo.DelAggCategories", "DelAggCatParentId");
        }
    }
}
