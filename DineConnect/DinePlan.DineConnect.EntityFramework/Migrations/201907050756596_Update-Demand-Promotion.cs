namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateDemandPromotion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DemandPromotions", "AuthenticationRequired", c => c.Boolean(nullable: false));
            AddColumn("dbo.TicketDiscountPromotions", "AuthenticationRequired", c => c.Boolean(nullable: false));
            DropColumn("dbo.DemandPromotions", "ReferenceCode");
            DropColumn("dbo.TicketDiscountPromotions", "ButtonColor");
            DropColumn("dbo.TicketDiscountPromotions", "PromotionOverride");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TicketDiscountPromotions", "PromotionOverride", c => c.Boolean(nullable: false));
            AddColumn("dbo.TicketDiscountPromotions", "ButtonColor", c => c.String(maxLength: 10));
            AddColumn("dbo.DemandPromotions", "ReferenceCode", c => c.String());
            DropColumn("dbo.TicketDiscountPromotions", "AuthenticationRequired");
            DropColumn("dbo.DemandPromotions", "AuthenticationRequired");
        }
    }
}
