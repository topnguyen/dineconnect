namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEntityInFutureInformation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FutureDateInformations", "EntityId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FutureDateInformations", "EntityId");
        }
    }
}
