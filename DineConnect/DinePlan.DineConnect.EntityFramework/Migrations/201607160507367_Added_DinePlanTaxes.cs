namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_DinePlanTaxes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DinePlanTaxes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TaxName = c.String(nullable: false, maxLength: 50),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DinePlanTax_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DinePlanTax_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DinePlanTaxLocations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DinePlanTaxRefId = c.Int(nullable: false),
                        LocationRefId = c.Int(nullable: false),
                        TaxPercentage = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DinePlanTaxLocation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationRefId, cascadeDelete: true)
                .Index(t => t.LocationRefId);
            
            CreateTable(
                "dbo.DinePlanTaxMappings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DinePlanTaxLocationRefId = c.Int(nullable: false),
                        CategoryId = c.Int(),
                        MenuItemId = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DinePlanTaxMapping_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DinePlanTaxLocations", "LocationRefId", "dbo.Locations");
            DropIndex("dbo.DinePlanTaxLocations", new[] { "LocationRefId" });
            DropTable("dbo.DinePlanTaxMappings",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DinePlanTaxMapping_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DinePlanTaxLocations",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DinePlanTaxLocation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DinePlanTaxes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DinePlanTax_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DinePlanTax_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
