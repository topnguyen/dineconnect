namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Add_MenuMappingDepartment : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MenuMappingDepartments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MenuMappingRefId = c.Int(nullable: false),
                        DepartmentRefId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MenuMappingDepartment_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_MenuMappingDepartment_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Departments", t => t.DepartmentRefId, cascadeDelete: true)
                .ForeignKey("dbo.MaterialMenuMappings", t => t.MenuMappingRefId, cascadeDelete: true)
                .Index(t => t.MenuMappingRefId)
                .Index(t => t.DepartmentRefId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MenuMappingDepartments", "MenuMappingRefId", "dbo.MaterialMenuMappings");
            DropForeignKey("dbo.MenuMappingDepartments", "DepartmentRefId", "dbo.Departments");
            DropIndex("dbo.MenuMappingDepartments", new[] { "DepartmentRefId" });
            DropIndex("dbo.MenuMappingDepartments", new[] { "MenuMappingRefId" });
            DropTable("dbo.MenuMappingDepartments",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MenuMappingDepartment_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_MenuMappingDepartment_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
