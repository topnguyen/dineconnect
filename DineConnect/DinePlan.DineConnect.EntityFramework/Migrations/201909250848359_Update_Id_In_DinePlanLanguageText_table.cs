namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Id_In_DinePlanLanguageText_table : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.DinePlanLanguageTexts");
            AlterColumn("dbo.DinePlanLanguageTexts", "Id", c => c.Long(nullable: false, identity: true));
            AddPrimaryKey("dbo.DinePlanLanguageTexts", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.DinePlanLanguageTexts");
            AlterColumn("dbo.DinePlanLanguageTexts", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.DinePlanLanguageTexts", "Id");
        }
    }
}
