namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Attach_Credit : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tickets", "Credit", c => c.Boolean(nullable: false));
            AddColumn("dbo.Tickets", "ReferenceNumber", c => c.String(maxLength: 20));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tickets", "ReferenceNumber");
            DropColumn("dbo.Tickets", "Credit");
        }
    }
}
