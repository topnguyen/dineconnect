// <auto-generated />
namespace DinePlan.DineConnect.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddCodeProductComboGroup : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddCodeProductComboGroup));
        
        string IMigrationMetadata.Id
        {
            get { return "202103120520479_Add-Code-Product-Combo-Group"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
