namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TransactionTypeChange : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TransactionTypes", "Tax", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TransactionTypes", "Tax");
        }
    }
}
