namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddNumeratorTicketType : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TicketTypeConfigurations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketTypeConfiguration_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TicketTypeConfiguration_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Numerators",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NumberFormat = c.String(),
                        TicketFormat = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Numerator_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Numerator_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TicketTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TicketInvoiceNumeratorId = c.Int(),
                        TicketNumeratorId = c.Int(),
                        OrderNumeratorId = c.Int(),
                        PromotionNumeratorId = c.Int(),
                        FullTaxNumeratord = c.Int(),
                        QueueNumeratorId = c.Int(),
                        SaleTransactionType_Id = c.Int(),
                        Locations = c.String(),
                        NonLocations = c.String(),
                        Group = c.Boolean(nullable: false),
                        LocationTag = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketType_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TicketType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Numerators", t => t.FullTaxNumeratord)
                .ForeignKey("dbo.Numerators", t => t.TicketInvoiceNumeratorId)
                .ForeignKey("dbo.Numerators", t => t.OrderNumeratorId)
                .ForeignKey("dbo.Numerators", t => t.PromotionNumeratorId)
                .ForeignKey("dbo.Numerators", t => t.QueueNumeratorId)
                .ForeignKey("dbo.TransactionTypes", t => t.SaleTransactionType_Id)
                .ForeignKey("dbo.Numerators", t => t.TicketNumeratorId)
                .Index(t => t.TicketInvoiceNumeratorId)
                .Index(t => t.TicketNumeratorId)
                .Index(t => t.OrderNumeratorId)
                .Index(t => t.PromotionNumeratorId)
                .Index(t => t.FullTaxNumeratord)
                .Index(t => t.QueueNumeratorId)
                .Index(t => t.SaleTransactionType_Id);
            
            AddColumn("dbo.Departments", "TicketTypeId", c => c.Int());
            AddColumn("dbo.Terminals", "TicketTypeId", c => c.Int());
            CreateIndex("dbo.Departments", "TicketTypeId");
            CreateIndex("dbo.Terminals", "TicketTypeId");
            AddForeignKey("dbo.Departments", "TicketTypeId", "dbo.TicketTypeConfigurations", "Id");
            AddForeignKey("dbo.Terminals", "TicketTypeId", "dbo.TicketTypeConfigurations", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TicketTypes", "TicketNumeratorId", "dbo.Numerators");
            DropForeignKey("dbo.TicketTypes", "SaleTransactionType_Id", "dbo.TransactionTypes");
            DropForeignKey("dbo.TicketTypes", "QueueNumeratorId", "dbo.Numerators");
            DropForeignKey("dbo.TicketTypes", "PromotionNumeratorId", "dbo.Numerators");
            DropForeignKey("dbo.TicketTypes", "OrderNumeratorId", "dbo.Numerators");
            DropForeignKey("dbo.TicketTypes", "TicketInvoiceNumeratorId", "dbo.Numerators");
            DropForeignKey("dbo.TicketTypes", "FullTaxNumeratord", "dbo.Numerators");
            DropForeignKey("dbo.Terminals", "TicketTypeId", "dbo.TicketTypeConfigurations");
            DropForeignKey("dbo.Departments", "TicketTypeId", "dbo.TicketTypeConfigurations");
            DropIndex("dbo.TicketTypes", new[] { "SaleTransactionType_Id" });
            DropIndex("dbo.TicketTypes", new[] { "QueueNumeratorId" });
            DropIndex("dbo.TicketTypes", new[] { "FullTaxNumeratord" });
            DropIndex("dbo.TicketTypes", new[] { "PromotionNumeratorId" });
            DropIndex("dbo.TicketTypes", new[] { "OrderNumeratorId" });
            DropIndex("dbo.TicketTypes", new[] { "TicketNumeratorId" });
            DropIndex("dbo.TicketTypes", new[] { "TicketInvoiceNumeratorId" });
            DropIndex("dbo.Terminals", new[] { "TicketTypeId" });
            DropIndex("dbo.Departments", new[] { "TicketTypeId" });
            DropColumn("dbo.Terminals", "TicketTypeId");
            DropColumn("dbo.Departments", "TicketTypeId");
            DropTable("dbo.TicketTypes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketType_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TicketType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Numerators",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Numerator_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Numerator_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TicketTypeConfigurations",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketTypeConfiguration_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TicketTypeConfiguration_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
