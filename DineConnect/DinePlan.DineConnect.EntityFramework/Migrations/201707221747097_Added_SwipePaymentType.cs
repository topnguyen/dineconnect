namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_SwipePaymentType : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SwipePaymentTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 50),
                        AcceptChange = c.Boolean(nullable: false),
                        Hide = c.Boolean(nullable: false),
                        AccountCode = c.String(maxLength: 50),
                        SortOrder = c.Int(nullable: false),
                        TopupAcceptFlag = c.Boolean(nullable: false),
                        RefundAcceptFlag = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SwipePaymentType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SwipePaymentTypes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SwipePaymentType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
