namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPromotionDetialsCode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Promotions", "Code", c => c.String());
            AddColumn("dbo.Promotions", "RefCode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Promotions", "RefCode");
            DropColumn("dbo.Promotions", "Code");
        }
    }
}
