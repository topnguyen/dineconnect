namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMenuItemTypeUpSellingItem : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UpMenuItems", "ProductType", c => c.Int(nullable: false));
            Sql("Update dbo.UpMenuItems Set ProductType = 2");

        }

        public override void Down()
        {
            DropColumn("dbo.UpMenuItems", "ProductType");
        }
    }
}
