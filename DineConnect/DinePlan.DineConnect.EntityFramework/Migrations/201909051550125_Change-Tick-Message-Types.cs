namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeTickMessageTypes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TickMessages", "MessageHeading", c => c.String());
            AddColumn("dbo.TickMessages", "MessageContent", c => c.String());
            AddColumn("dbo.TickMessages", "MessageType", c => c.Int(nullable: false));
            AddColumn("dbo.TickMessages", "OneTimeMessage", c => c.Boolean(nullable: false));
            AddColumn("dbo.TickMessages", "MessageUntil", c => c.DateTime(nullable: false));
            DropColumn("dbo.TickMessages", "Message");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TickMessages", "Message", c => c.String());
            DropColumn("dbo.TickMessages", "MessageUntil");
            DropColumn("dbo.TickMessages", "OneTimeMessage");
            DropColumn("dbo.TickMessages", "MessageType");
            DropColumn("dbo.TickMessages", "MessageContent");
            DropColumn("dbo.TickMessages", "MessageHeading");
        }
    }
}
