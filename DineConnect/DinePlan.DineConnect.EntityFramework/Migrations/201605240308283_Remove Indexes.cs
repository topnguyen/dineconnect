namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveIndexes : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Companies", "Code");
            DropIndex("dbo.Companies", "Name");
            DropIndex("dbo.Customers", "CustomerName");
            DropIndex("dbo.CustomerTagDefinitions", "TagCode");
            DropIndex("dbo.CustomerTagDefinitions", "CustomerTagName");
            DropIndex("dbo.Materials", "MaterialName");
            DropIndex("dbo.Materials", "MaterialPetName");
            DropIndex("dbo.HouseReports", "ReportKey");
            DropIndex("dbo.Suppliers", "SupplierName");
            DropIndex("dbo.Taxes", "TaxName");
            DropIndex("dbo.Recipes", "RecipeName");
            DropIndex("dbo.Recipes", "RecipeShortName");
            DropIndex("dbo.RecipeGroups", "RecipeGroupName");
            DropIndex("dbo.RecipeGroups", "RecipeGroupShortName");
            DropIndex("dbo.Templates", "Name");
        }
        
        public override void Down()
        {
            CreateIndex("dbo.Templates", "Name", unique: true, name: "Name");
            CreateIndex("dbo.RecipeGroups", "RecipeGroupShortName", unique: true, name: "RecipeGroupShortName");
            CreateIndex("dbo.RecipeGroups", "RecipeGroupName", unique: true, name: "RecipeGroupName");
            CreateIndex("dbo.Recipes", "RecipeShortName", unique: true, name: "RecipeShortName");
            CreateIndex("dbo.Recipes", "RecipeName", unique: true, name: "RecipeName");
            CreateIndex("dbo.Taxes", "TaxName", unique: true, name: "TaxName");
            CreateIndex("dbo.Suppliers", "SupplierName", unique: true, name: "SupplierName");
            CreateIndex("dbo.HouseReports", "ReportKey", unique: true, name: "ReportKey");
            CreateIndex("dbo.Materials", "MaterialPetName", unique: true, name: "MaterialPetName");
            CreateIndex("dbo.Materials", "MaterialName", unique: true, name: "MaterialName");
            CreateIndex("dbo.CustomerTagDefinitions", "TagName", unique: true, name: "CustomerTagName");
            CreateIndex("dbo.CustomerTagDefinitions", "TagCode", unique: true, name: "TagCode");
            CreateIndex("dbo.Customers", "CustomerName", unique: true, name: "CustomerName");
            CreateIndex("dbo.Companies", "Name", unique: true, name: "Name");
            CreateIndex("dbo.Companies", "Code", unique: true, name: "Code");
        }
    }
}
