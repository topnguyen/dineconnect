namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingUnitChangesForSupplier : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AdjustmentDetails", "ConversionWithDefaultUnit", c => c.Decimal(nullable: false, precision: 24, scale: 14));
            AddColumn("dbo.UnitConversions", "CreatedBasedOnRecursive", c => c.Boolean(nullable: false));
            AddColumn("dbo.ClosingStockDetails", "ConversionWithDefaultUnit", c => c.Decimal(nullable: false, precision: 24, scale: 14));
            AddColumn("dbo.ClosingStockUnitWiseDetails", "ConversionWithDefaultUnit", c => c.Decimal(nullable: false, precision: 24, scale: 14));
            AddColumn("dbo.InterTransferDetails", "ConversionWithDefaultUnit", c => c.Decimal(nullable: false, precision: 24, scale: 14));
            AddColumn("dbo.InterTransferReceivedDetails", "ConversionWithDefaultUnit", c => c.Decimal(nullable: false, precision: 24, scale: 14));
            AddColumn("dbo.InvoiceDetails", "ConversionWithDefaultUnit", c => c.Decimal(nullable: false, precision: 24, scale: 14));
            AddColumn("dbo.InwardDirectCreditDetails", "ConversionWithDefaultUnit", c => c.Decimal(nullable: false, precision: 24, scale: 14));
            AddColumn("dbo.IssueDetails", "ConversionWithDefaultUnit", c => c.Decimal(nullable: false, precision: 24, scale: 14));
            AddColumn("dbo.IssueRecipeDetails", "ConversionWithDefaultUnit", c => c.Decimal(nullable: false, precision: 24, scale: 14));
            AddColumn("dbo.ProductionDetails", "ConversionWithDefaultUnit", c => c.Decimal(nullable: false, precision: 24, scale: 14));
            AddColumn("dbo.PurchaseOrderDetails", "ConversionWithDefaultUnit", c => c.Decimal(nullable: false, precision: 24, scale: 14));
            AddColumn("dbo.PurchaseReturnDetails", "ConversionWithDefaultUnit", c => c.Decimal(nullable: false, precision: 24, scale: 14));
            AddColumn("dbo.RequestDetails", "ConversionWithDefaultUnit", c => c.Decimal(nullable: false, precision: 24, scale: 14));
            AddColumn("dbo.RequestRecipeDetails", "ConversionWithDefaultUnit", c => c.Decimal(nullable: false, precision: 24, scale: 14));
            AddColumn("dbo.ReturnDetails", "ConversionWithDefaultUnit", c => c.Decimal(nullable: false, precision: 24, scale: 14));
            AddColumn("dbo.SalesDeliveryOrderDetails", "ConversionWithDefaultUnit", c => c.Decimal(nullable: false, precision: 24, scale: 14));
            AddColumn("dbo.SalesInvoiceDetails", "ConversionWithDefaultUnit", c => c.Decimal(nullable: false, precision: 24, scale: 14));
            AddColumn("dbo.SalesOrderDetails", "ConversionWithDefaultUnit", c => c.Decimal(nullable: false, precision: 24, scale: 14));
            AddColumn("dbo.YieldInput", "ConversionWithDefaultUnit", c => c.Decimal(nullable: false, precision: 24, scale: 14));
            AddColumn("dbo.YieldOutput", "ConversionWithDefaultUnit", c => c.Decimal(nullable: false, precision: 24, scale: 14));
        }
        
        public override void Down()
        {
            DropColumn("dbo.YieldOutput", "ConversionWithDefaultUnit");
            DropColumn("dbo.YieldInput", "ConversionWithDefaultUnit");
            DropColumn("dbo.SalesOrderDetails", "ConversionWithDefaultUnit");
            DropColumn("dbo.SalesInvoiceDetails", "ConversionWithDefaultUnit");
            DropColumn("dbo.SalesDeliveryOrderDetails", "ConversionWithDefaultUnit");
            DropColumn("dbo.ReturnDetails", "ConversionWithDefaultUnit");
            DropColumn("dbo.RequestRecipeDetails", "ConversionWithDefaultUnit");
            DropColumn("dbo.RequestDetails", "ConversionWithDefaultUnit");
            DropColumn("dbo.PurchaseReturnDetails", "ConversionWithDefaultUnit");
            DropColumn("dbo.PurchaseOrderDetails", "ConversionWithDefaultUnit");
            DropColumn("dbo.ProductionDetails", "ConversionWithDefaultUnit");
            DropColumn("dbo.IssueRecipeDetails", "ConversionWithDefaultUnit");
            DropColumn("dbo.IssueDetails", "ConversionWithDefaultUnit");
            DropColumn("dbo.InwardDirectCreditDetails", "ConversionWithDefaultUnit");
            DropColumn("dbo.InvoiceDetails", "ConversionWithDefaultUnit");
            DropColumn("dbo.InterTransferReceivedDetails", "ConversionWithDefaultUnit");
            DropColumn("dbo.InterTransferDetails", "ConversionWithDefaultUnit");
            DropColumn("dbo.ClosingStockUnitWiseDetails", "ConversionWithDefaultUnit");
            DropColumn("dbo.ClosingStockDetails", "ConversionWithDefaultUnit");
            DropColumn("dbo.UnitConversions", "CreatedBasedOnRecursive");
            DropColumn("dbo.AdjustmentDetails", "ConversionWithDefaultUnit");
        }
    }
}
