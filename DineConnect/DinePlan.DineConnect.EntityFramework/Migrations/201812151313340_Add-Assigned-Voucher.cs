namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAssignedVoucher : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GiftVouchers", "Assigned", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.GiftVouchers", "Assigned");
        }
    }
}
