namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_PriceTagLocationPrices : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PriceTagLocationPrices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PriceTagDefinitionId = c.Int(nullable: false),
                        LocationId = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .ForeignKey("dbo.PriceTagDefinitions", t => t.PriceTagDefinitionId, cascadeDelete: true)
                .Index(t => t.PriceTagDefinitionId)
                .Index(t => t.LocationId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PriceTagLocationPrices", "PriceTagDefinitionId", "dbo.PriceTagDefinitions");
            DropForeignKey("dbo.PriceTagLocationPrices", "LocationId", "dbo.Locations");
            DropIndex("dbo.PriceTagLocationPrices", new[] { "LocationId" });
            DropIndex("dbo.PriceTagLocationPrices", new[] { "PriceTagDefinitionId" });
            DropTable("dbo.PriceTagLocationPrices");
        }
    }
}
