namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDepartmentCode : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PromotionSchedules", "Days", c => c.String(maxLength: 100));
            AddColumn("dbo.Departments", "Code", c => c.String(maxLength: 100));

        }
        
        public override void Down()
        {
            DropColumn("dbo.Departments", "Code");
            AlterColumn("dbo.PromotionSchedules", "Days", c => c.String(maxLength: 10));
        }
    }
}
