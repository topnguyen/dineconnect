namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPrintJobExcludeTax : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PrintJobs", "ExcludeTax", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PrintJobs", "ExcludeTax");
        }
    }
}
