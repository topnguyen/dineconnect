namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Ticket_IncreaseDataSize : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Tickets", "IX_FirstAndSecond");
            AlterColumn("dbo.Tickets", "TicketNumber", c => c.String(maxLength: 100));
            AlterColumn("dbo.Tickets", "ReferenceNumber", c => c.String(maxLength: 100));
            AlterColumn("dbo.Tickets", "QueueNumber", c => c.String(maxLength: 100));
            CreateIndex("dbo.Tickets", new[] { "LocationId", "TicketNumber" }, unique: true, name: "IX_FirstAndSecond");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Tickets", "IX_FirstAndSecond");
            AlterColumn("dbo.Tickets", "QueueNumber", c => c.String(maxLength: 20));
            AlterColumn("dbo.Tickets", "ReferenceNumber", c => c.String(maxLength: 20));
            AlterColumn("dbo.Tickets", "TicketNumber", c => c.String(maxLength: 30));
            CreateIndex("dbo.Tickets", new[] { "LocationId", "TicketNumber" }, unique: true, name: "IX_FirstAndSecond");
        }
    }
}
