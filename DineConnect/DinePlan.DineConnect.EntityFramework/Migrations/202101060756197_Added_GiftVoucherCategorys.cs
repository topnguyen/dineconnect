namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_GiftVoucherCategorys : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.GiftVoucherTypes", "CategoryId", "dbo.Categories");
            DropIndex("dbo.GiftVoucherTypes", new[] { "CategoryId" });
            CreateTable(
                "dbo.GiftVoucherCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(maxLength: 10),
                        Name = c.String(maxLength: 50),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_GiftVoucherCategory_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_GiftVoucherCategory_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.GiftVoucherTypes", "CategoryId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.GiftVoucherTypes", "CategoryId", c => c.Int());
            DropTable("dbo.GiftVoucherCategories",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_GiftVoucherCategory_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_GiftVoucherCategory_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            CreateIndex("dbo.GiftVoucherTypes", "CategoryId");
            AddForeignKey("dbo.GiftVoucherTypes", "CategoryId", "dbo.Categories", "Id");
        }
    }
}
