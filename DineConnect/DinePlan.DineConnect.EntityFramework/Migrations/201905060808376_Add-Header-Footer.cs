namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddHeaderFooter : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Locations", "ReceiptHeader", c => c.String());
            AddColumn("dbo.Locations", "ReceiptFooter", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Locations", "ReceiptFooter");
            DropColumn("dbo.Locations", "ReceiptHeader");
        }
    }
}
