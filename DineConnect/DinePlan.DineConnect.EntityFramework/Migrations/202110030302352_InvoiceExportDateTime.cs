namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InvoiceExportDateTime : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Invoices", "ExportDateTimeForExternalImport", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Invoices", "ExportDateTimeForExternalImport");
        }
    }
}
