namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPaymentTypeLocation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PaymentTypes", "Locations", c => c.String());
            AddColumn("dbo.PaymentTypes", "NonLocations", c => c.String());
            DropColumn("dbo.PaymentTypes", "Hide");
            DropColumn("dbo.PaymentTypes", "ButtonHeader");
            DropColumn("dbo.PaymentTypes", "Departments");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PaymentTypes", "Departments", c => c.String());
            AddColumn("dbo.PaymentTypes", "ButtonHeader", c => c.String());
            AddColumn("dbo.PaymentTypes", "Hide", c => c.Boolean(nullable: false));
            DropColumn("dbo.PaymentTypes", "NonLocations");
            DropColumn("dbo.PaymentTypes", "Locations");
        }
    }
}
