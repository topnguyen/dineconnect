namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOriginalPriceToOrder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "OriginalPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "OriginalPrice");
        }
    }
}
