namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FranchiseChanges : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LocationCategoryShares",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationId = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                        SharingPercentage = c.Decimal(nullable: false, precision: 18, scale: 2),
                        InclusiveOfTax = c.Boolean(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .Index(t => t.LocationId)
                .Index(t => t.CategoryId);
            
            AddColumn("dbo.Locations", "TaxInclusive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LocationCategoryShares", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.LocationCategoryShares", "CategoryId", "dbo.Categories");
            DropIndex("dbo.LocationCategoryShares", new[] { "CategoryId" });
            DropIndex("dbo.LocationCategoryShares", new[] { "LocationId" });
            DropColumn("dbo.Locations", "TaxInclusive");
            DropTable("dbo.LocationCategoryShares");
        }
    }
}
