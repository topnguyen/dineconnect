namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeDelAggItems : DbMigration
    {
        public override void Up()
        {
            RenameTable("DelAggModiferGroupItems","DelAggModifierGroupItems");
            RenameColumn("DelAggModifierGroupItems","DelAggModiferGroupId","DelAggModifierGroupId");
            AddColumn("dbo.DelAggModifierGroupItems", "Addon", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DelAggModifierGroupItems", "Addon");
        }
    }
}
