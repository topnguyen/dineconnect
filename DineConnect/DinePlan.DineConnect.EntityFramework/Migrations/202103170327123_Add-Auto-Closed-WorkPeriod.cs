namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAutoClosedWorkPeriod : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WorkPeriods", "AutoClosed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.WorkPeriods", "AutoClosed");
        }
    }
}
