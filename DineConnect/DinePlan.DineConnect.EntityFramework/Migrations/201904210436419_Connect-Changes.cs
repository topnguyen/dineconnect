namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ConnectChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DemandPromotions", "ReferenceCode", c => c.String());
            AddColumn("dbo.FreeItemPromotionExecutions", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.FreeItemPromotions", "Confirmation", c => c.Boolean(nullable: false));
            AddColumn("dbo.FreeItemPromotions", "TotalAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.FreeItemPromotions", "MultipleTimes", c => c.Boolean(nullable: false));
            AddColumn("dbo.ProductComboItems", "AddSeperately", c => c.Boolean(nullable: false));
            AddColumn("dbo.ScreenMenuCategories", "NumeratorType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ScreenMenuCategories", "NumeratorType");
            DropColumn("dbo.ProductComboItems", "AddSeperately");
            DropColumn("dbo.FreeItemPromotions", "MultipleTimes");
            DropColumn("dbo.FreeItemPromotions", "TotalAmount");
            DropColumn("dbo.FreeItemPromotions", "Confirmation");
            DropColumn("dbo.FreeItemPromotionExecutions", "Price");
            DropColumn("dbo.DemandPromotions", "ReferenceCode");
        }
    }
}
