namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Change_DelAgg_DbStructure : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DelAggVariants", "DelAggItem_Id", "dbo.DelAggItems");
            DropForeignKey("dbo.DelAggVariantGroupItems", "DelAggItemId", "dbo.DelAggItems");
            DropForeignKey("dbo.DelAggVariantGroupItems", "DelAggVariantGroupId", "dbo.DelAggVariantGroups");
            DropForeignKey("dbo.DelAggItems", "DelAggTaxId", "dbo.DelAggTaxes");
            DropForeignKey("dbo.DelAggLocationItems", "DelAggItemId", "dbo.DelAggItems");
            DropForeignKey("dbo.DelAggLocationItems", "DelAggModifierId", "dbo.DelAggModifiers");
            DropForeignKey("dbo.DelAggLocationItems", "DelAggVariantId", "dbo.DelAggVariants");
            DropIndex("dbo.DelAggItems", new[] { "DelAggTaxId" });
            DropIndex("dbo.DelAggVariants", new[] { "DelAggItem_Id" });
            DropIndex("dbo.DelAggLocationItems", new[] { "DelAggItemId" });
            DropIndex("dbo.DelAggLocationItems", new[] { "DelAggVariantId" });
            DropIndex("dbo.DelAggLocationItems", new[] { "DelAggModifierId" });
            DropIndex("dbo.DelAggVariantGroupItems", new[] { "DelAggVariantGroupId" });
            DropIndex("dbo.DelAggVariantGroupItems", new[] { "DelAggItemId" });
            AddColumn("dbo.DelAggCharges", "Code", c => c.String());
            AddColumn("dbo.DelAggItemGroups", "Code", c => c.String());
            AddColumn("dbo.DelAggItemGroups", "Description", c => c.String());
            AddColumn("dbo.DelAggItems", "DelAggVariantGroupId", c => c.Int(nullable: false));
            AddColumn("dbo.DelAggVariantGroups", "Code", c => c.String());
            AddColumn("dbo.DelAggModifierGroups", "Code", c => c.String());
            AlterColumn("dbo.DelAggItems", "DelAggTaxId", c => c.Int());
            AlterColumn("dbo.DelAggLocationItems", "DelAggItemId", c => c.Int());
            AlterColumn("dbo.DelAggLocationItems", "DelAggVariantId", c => c.Int());
            AlterColumn("dbo.DelAggLocationItems", "DelAggModifierId", c => c.Int());
            CreateIndex("dbo.DelAggItems", "DelAggVariantGroupId");
            CreateIndex("dbo.DelAggItems", "DelAggTaxId");
            CreateIndex("dbo.DelAggLocationItems", "DelAggItemId");
            CreateIndex("dbo.DelAggLocationItems", "DelAggVariantId");
            CreateIndex("dbo.DelAggLocationItems", "DelAggModifierId");
            AddForeignKey("dbo.DelAggItems", "DelAggVariantGroupId", "dbo.DelAggVariantGroups", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DelAggItems", "DelAggTaxId", "dbo.DelAggTaxes", "Id");
            AddForeignKey("dbo.DelAggLocationItems", "DelAggItemId", "dbo.DelAggItems", "Id");
            AddForeignKey("dbo.DelAggLocationItems", "DelAggModifierId", "dbo.DelAggModifiers", "Id");
            AddForeignKey("dbo.DelAggLocationItems", "DelAggVariantId", "dbo.DelAggVariants", "Id");
            DropColumn("dbo.DelAggItems", "VariantExists");
            DropColumn("dbo.DelAggVariants", "DelAggItem_Id");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.DelAggVariantGroupItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DelAggVariantGroupId = c.Int(nullable: false),
                        DelAggItemId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggVariantGroupItem_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggVariantGroupItem_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.DelAggVariants", "DelAggItem_Id", c => c.Int());
            AddColumn("dbo.DelAggItems", "VariantExists", c => c.Boolean(nullable: false));
            DropForeignKey("dbo.DelAggLocationItems", "DelAggVariantId", "dbo.DelAggVariants");
            DropForeignKey("dbo.DelAggLocationItems", "DelAggModifierId", "dbo.DelAggModifiers");
            DropForeignKey("dbo.DelAggLocationItems", "DelAggItemId", "dbo.DelAggItems");
            DropForeignKey("dbo.DelAggItems", "DelAggTaxId", "dbo.DelAggTaxes");
            DropForeignKey("dbo.DelAggItems", "DelAggVariantGroupId", "dbo.DelAggVariantGroups");
            DropIndex("dbo.DelAggLocationItems", new[] { "DelAggModifierId" });
            DropIndex("dbo.DelAggLocationItems", new[] { "DelAggVariantId" });
            DropIndex("dbo.DelAggLocationItems", new[] { "DelAggItemId" });
            DropIndex("dbo.DelAggItems", new[] { "DelAggTaxId" });
            DropIndex("dbo.DelAggItems", new[] { "DelAggVariantGroupId" });
            AlterColumn("dbo.DelAggLocationItems", "DelAggModifierId", c => c.Int(nullable: false));
            AlterColumn("dbo.DelAggLocationItems", "DelAggVariantId", c => c.Int(nullable: false));
            AlterColumn("dbo.DelAggLocationItems", "DelAggItemId", c => c.Int(nullable: false));
            AlterColumn("dbo.DelAggItems", "DelAggTaxId", c => c.Int(nullable: false));
            DropColumn("dbo.DelAggModifierGroups", "Code");
            DropColumn("dbo.DelAggVariantGroups", "Code");
            DropColumn("dbo.DelAggItems", "DelAggVariantGroupId");
            DropColumn("dbo.DelAggItemGroups", "Description");
            DropColumn("dbo.DelAggItemGroups", "Code");
            DropColumn("dbo.DelAggCharges", "Code");
            CreateIndex("dbo.DelAggVariantGroupItems", "DelAggItemId");
            CreateIndex("dbo.DelAggVariantGroupItems", "DelAggVariantGroupId");
            CreateIndex("dbo.DelAggLocationItems", "DelAggModifierId");
            CreateIndex("dbo.DelAggLocationItems", "DelAggVariantId");
            CreateIndex("dbo.DelAggLocationItems", "DelAggItemId");
            CreateIndex("dbo.DelAggVariants", "DelAggItem_Id");
            CreateIndex("dbo.DelAggItems", "DelAggTaxId");
            AddForeignKey("dbo.DelAggLocationItems", "DelAggVariantId", "dbo.DelAggVariants", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DelAggLocationItems", "DelAggModifierId", "dbo.DelAggModifiers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DelAggLocationItems", "DelAggItemId", "dbo.DelAggItems", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DelAggItems", "DelAggTaxId", "dbo.DelAggTaxes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DelAggVariantGroupItems", "DelAggVariantGroupId", "dbo.DelAggVariantGroups", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DelAggVariantGroupItems", "DelAggItemId", "dbo.DelAggItems", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DelAggVariants", "DelAggItem_Id", "dbo.DelAggItems", "Id");
        }
    }
}
