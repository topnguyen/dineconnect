namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyInvoiceAccountDate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Invoices", "AccountDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Invoices", "AccountDate", c => c.DateTime());
        }
    }
}
