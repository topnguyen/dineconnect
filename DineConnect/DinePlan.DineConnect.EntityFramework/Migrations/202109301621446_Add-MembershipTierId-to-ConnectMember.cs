namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMembershipTierIdtoConnectMember : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ConnectMembers", "MembershipTierId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ConnectMembers", "MembershipTierId");
        }
    }
}
