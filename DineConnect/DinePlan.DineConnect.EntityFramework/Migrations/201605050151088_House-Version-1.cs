namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class HouseVersion1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.IssueDetails", "RecipeRefId", "dbo.Recipes");
            DropForeignKey("dbo.MaterialMenuMappings", "MaterialRefId", "dbo.Recipes");
            DropForeignKey("dbo.ReturnDetails", "RecipeRefId", "dbo.Recipes");
            DropIndex("dbo.IssueDetails", new[] { "RecipeRefId" });
            DropIndex("dbo.ReturnDetails", new[] { "RecipeRefId" });
            RenameColumn(table: "dbo.PurchaseOrders", name: "ShippingLocationId", newName: "__mig_tmp__0");
            RenameColumn(table: "dbo.PurchaseOrders", name: "LocationRefId", newName: "ShippingLocationId");
            RenameColumn(table: "dbo.PurchaseOrders", name: "__mig_tmp__0", newName: "LocationRefId");
            RenameIndex(table: "dbo.PurchaseOrders", name: "IX_LocationRefId", newName: "__mig_tmp__0");
            RenameIndex(table: "dbo.PurchaseOrders", name: "IX_ShippingLocationId", newName: "IX_LocationRefId");
            RenameIndex(table: "dbo.PurchaseOrders", name: "__mig_tmp__0", newName: "IX_ShippingLocationId");
            CreateTable(
                "dbo.MaterialRecipeTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MaterailRefId = c.Int(nullable: false),
                        PrdBatchQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LifeTimeInMinutes = c.Int(nullable: false),
                        AlarmFlag = c.Boolean(nullable: false),
                        AlarmTimeInMinutes = c.Int(nullable: false),
                        FixedCost = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MaterialRecipeTypes_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TaxTemplateMappings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TaxRefId = c.Int(nullable: false),
                        CompanyRefId = c.Int(),
                        LocationRefId = c.Int(),
                        MaterialGroupRefId = c.Int(),
                        MaterialGroupCategoryRefId = c.Int(),
                        MaterialRefId = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TaxTemplateMapping_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyRefId)
                .ForeignKey("dbo.Locations", t => t.LocationRefId)
                .ForeignKey("dbo.MaterialGroupCategories", t => t.MaterialGroupCategoryRefId)
                .ForeignKey("dbo.MaterialGroups", t => t.MaterialGroupRefId)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .Index(t => t.CompanyRefId)
                .Index(t => t.LocationRefId)
                .Index(t => t.MaterialGroupRefId)
                .Index(t => t.MaterialGroupCategoryRefId)
                .Index(t => t.MaterialRefId);
            
            AlterTableAnnotations(
                "dbo.CustomerTagDefinitions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TagCode = c.String(nullable: false, maxLength: 30),
                        TagName = c.String(nullable: false, maxLength: 30),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_CustomerTagDefinition_MustHaveTenant",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.InwardDirectCreditDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DcRefId = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        DcReceivedQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DcConvertedQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnitRefId = c.Int(nullable: false),
                        MaterialReceivedStatus = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_InwardDirectCreditDetail_SoftDelete",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AddColumn("dbo.Customers", "CustomerTagRefId", c => c.Int(nullable: false));
            AddColumn("dbo.CustomerTagDefinitions", "TenantId", c => c.Int(nullable: false));
            AddColumn("dbo.Invoices", "IsDirectInvoice", c => c.Boolean(nullable: false));
            AddColumn("dbo.InvoiceTaxDetails", "TaxRate", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.InvoiceTaxDetails", "TaxValue", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Taxes", "SortOrder", c => c.Int(nullable: false));
            AddColumn("dbo.Taxes", "Rounding", c => c.Int(nullable: false));
            AddColumn("dbo.InwardDirectCreditDetails", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.InwardDirectCreditDetails", "DeleterUserId", c => c.Long());
            AddColumn("dbo.InwardDirectCreditDetails", "DeletionTime", c => c.DateTime());
            AddColumn("dbo.InwardDirectCreditDetails", "LastModificationTime", c => c.DateTime());
            AddColumn("dbo.InwardDirectCreditDetails", "LastModifierUserId", c => c.Long());
            AddColumn("dbo.InwardDirectCreditDetails", "CreationTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.InwardDirectCreditDetails", "CreatorUserId", c => c.Long());
            AddColumn("dbo.MaterialMenuMappings", "MenuQuantitySold", c => c.Int(nullable: false));
            AddColumn("dbo.PurchaseOrderTaxDetails", "TaxRate", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.PurchaseOrderTaxDetails", "TaxValue", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            CreateIndex("dbo.Customers", "CustomerTagRefId");
            CreateIndex("dbo.IssueDetails", "MaterialRefId");
            CreateIndex("dbo.ReturnDetails", "MaterialRefId");
            AddForeignKey("dbo.Customers", "CustomerTagRefId", "dbo.CustomerTagDefinitions", "Id");
            AddForeignKey("dbo.IssueDetails", "MaterialRefId", "dbo.Materials", "Id");
            AddForeignKey("dbo.MaterialMenuMappings", "MaterialRefId", "dbo.Materials", "Id");
            AddForeignKey("dbo.ReturnDetails", "MaterialRefId", "dbo.Materials", "Id");
            DropColumn("dbo.Customers", "CustomerPriceTagCode");
            DropPrimaryKey("dbo.HouseReports");
            DropPrimaryKey("dbo.ReturnDetails");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Customers", "CustomerPriceTagCode", c => c.String(nullable: false));
            DropForeignKey("dbo.TaxTemplateMappings", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.TaxTemplateMappings", "MaterialGroupRefId", "dbo.MaterialGroups");
            DropForeignKey("dbo.TaxTemplateMappings", "MaterialGroupCategoryRefId", "dbo.MaterialGroupCategories");
            DropForeignKey("dbo.TaxTemplateMappings", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.TaxTemplateMappings", "CompanyRefId", "dbo.Companies");
            DropForeignKey("dbo.ReturnDetails", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.MaterialMenuMappings", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.IssueDetails", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.Customers", "CustomerTagRefId", "dbo.CustomerTagDefinitions");
            DropIndex("dbo.TaxTemplateMappings", new[] { "MaterialRefId" });
            DropIndex("dbo.TaxTemplateMappings", new[] { "MaterialGroupCategoryRefId" });
            DropIndex("dbo.TaxTemplateMappings", new[] { "MaterialGroupRefId" });
            DropIndex("dbo.TaxTemplateMappings", new[] { "LocationRefId" });
            DropIndex("dbo.TaxTemplateMappings", new[] { "CompanyRefId" });
            DropIndex("dbo.ReturnDetails", new[] { "MaterialRefId" });
            DropIndex("dbo.IssueDetails", new[] { "MaterialRefId" });
            DropIndex("dbo.Customers", new[] { "CustomerTagRefId" });
            DropColumn("dbo.PurchaseOrderTaxDetails", "TaxValue");
            DropColumn("dbo.PurchaseOrderTaxDetails", "TaxRate");
            DropColumn("dbo.MaterialMenuMappings", "MenuQuantitySold");
            DropColumn("dbo.InwardDirectCreditDetails", "CreatorUserId");
            DropColumn("dbo.InwardDirectCreditDetails", "CreationTime");
            DropColumn("dbo.InwardDirectCreditDetails", "LastModifierUserId");
            DropColumn("dbo.InwardDirectCreditDetails", "LastModificationTime");
            DropColumn("dbo.InwardDirectCreditDetails", "DeletionTime");
            DropColumn("dbo.InwardDirectCreditDetails", "DeleterUserId");
            DropColumn("dbo.InwardDirectCreditDetails", "IsDeleted");
            DropColumn("dbo.Taxes", "Rounding");
            DropColumn("dbo.Taxes", "SortOrder");
            DropColumn("dbo.InvoiceTaxDetails", "TaxValue");
            DropColumn("dbo.InvoiceTaxDetails", "TaxRate");
            DropColumn("dbo.Invoices", "IsDirectInvoice");
            DropColumn("dbo.CustomerTagDefinitions", "TenantId");
            DropColumn("dbo.Customers", "CustomerTagRefId");
            AlterTableAnnotations(
                "dbo.InwardDirectCreditDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DcRefId = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        DcReceivedQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DcConvertedQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnitRefId = c.Int(nullable: false),
                        MaterialReceivedStatus = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_InwardDirectCreditDetail_SoftDelete",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.CustomerTagDefinitions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TagCode = c.String(nullable: false, maxLength: 30),
                        TagName = c.String(nullable: false, maxLength: 30),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_CustomerTagDefinition_MustHaveTenant",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            DropTable("dbo.TaxTemplateMappings",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TaxTemplateMapping_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.MaterialRecipeTypes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MaterialRecipeTypes_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            RenameIndex(table: "dbo.PurchaseOrders", name: "IX_ShippingLocationId", newName: "__mig_tmp__0");
            RenameIndex(table: "dbo.PurchaseOrders", name: "IX_LocationRefId", newName: "IX_ShippingLocationId");
            RenameIndex(table: "dbo.PurchaseOrders", name: "__mig_tmp__0", newName: "IX_LocationRefId");
            RenameColumn(table: "dbo.PurchaseOrders", name: "LocationRefId", newName: "__mig_tmp__0");
            RenameColumn(table: "dbo.PurchaseOrders", name: "ShippingLocationId", newName: "LocationRefId");
            RenameColumn(table: "dbo.PurchaseOrders", name: "__mig_tmp__0", newName: "ShippingLocationId");
            CreateIndex("dbo.ReturnDetails", "RecipeRefId");
            CreateIndex("dbo.IssueDetails", "RecipeRefId");
            AddForeignKey("dbo.ReturnDetails", "RecipeRefId", "dbo.Recipes", "Id");
            AddForeignKey("dbo.MaterialMenuMappings", "MaterialRefId", "dbo.Recipes", "Id");
            AddForeignKey("dbo.IssueDetails", "RecipeRefId", "dbo.Recipes", "Id");
        }
    }
}
