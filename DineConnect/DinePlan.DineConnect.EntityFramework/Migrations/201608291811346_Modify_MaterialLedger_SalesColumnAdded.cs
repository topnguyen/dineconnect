namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_MaterialLedger_SalesColumnAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MaterialLedgers", "Sales", c => c.Decimal(nullable: false, precision: 18, scale: 6));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MaterialLedgers", "Sales");
        }
    }
}
