namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPromotionSyncIdToOrder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "PromotionSyncId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "PromotionSyncId");
        }
    }
}
