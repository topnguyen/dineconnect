namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddOrganizationIdToAllEntities : DbMigration
    {
        public override void Up()
        {
            AlterTableAnnotations(
                "dbo.MenuItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 100),
                        BarCode = c.String(maxLength: 50),
                        AliasCode = c.String(maxLength: 50),
                        AliasName = c.String(maxLength: 100),
                        ItemDescription = c.String(maxLength: 1000),
                        ForceQuantity = c.Boolean(nullable: false),
                        ForceChangePrice = c.Boolean(nullable: false),
                        CategoryId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        ProductType = c.Int(nullable: false),
                        Tag = c.String(maxLength: 50),
                        Locations = c.String(),
                        Group = c.Boolean(nullable: false),
                        TransactionTypeId = c.Int(),
                        Files = c.String(),
                        HsnCode = c.String(maxLength: 50),
                        Oid = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_MenuItem_ConnectFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(),
                        TenantId = c.Int(nullable: false),
                        Oid = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_Category_ConnectFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.PriceTags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        Name = c.String(maxLength: 10),
                        Locations = c.String(),
                        Group = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        Departments = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Oid = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_PriceTag_ConnectFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Promotions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Name = c.String(),
                        TenantId = c.Int(nullable: false),
                        PromotionTypeId = c.Int(nullable: false),
                        Locations = c.String(),
                        Group = c.Boolean(nullable: false),
                        Active = c.Boolean(nullable: false),
                        SortOrder = c.Int(nullable: false),
                        Filter = c.String(),
                        Oid = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_Promotion_ConnectFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.OrderTagGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        MaxSelectedItems = c.Int(nullable: false),
                        MinSelectedItems = c.Int(nullable: false),
                        SortOrder = c.Int(nullable: false),
                        AddTagPriceToOrderPrice = c.Boolean(nullable: false),
                        SaveFreeTags = c.Boolean(nullable: false),
                        FreeTagging = c.Boolean(nullable: false),
                        TaxFree = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        Prefix = c.String(),
                        Locations = c.String(),
                        Group = c.Boolean(nullable: false),
                        Oid = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_OrderTagGroup_ConnectFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.ScreenMenus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Locations = c.String(),
                        Group = c.Boolean(nullable: false),
                        CategoryColumnCount = c.Int(nullable: false),
                        CategoryColumnWidthRate = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        Departments = c.String(),
                        Oid = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ScreenMenu_ConnectFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.TicketTagGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SortOrder = c.Int(nullable: false),
                        Locations = c.String(),
                        Group = c.Boolean(nullable: false),
                        Departments = c.String(),
                        SaveFreeTags = c.Boolean(nullable: false),
                        FreeTagging = c.Boolean(nullable: false),
                        ForceValue = c.Boolean(nullable: false),
                        AskBeforeCreatingTicket = c.Boolean(nullable: false),
                        DataType = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        SubTagId = c.Int(nullable: false),
                        Oid = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_TicketTagGroup_ConnectFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AddColumn("dbo.MenuItems", "Oid", c => c.Int(nullable: false));
            AddColumn("dbo.Categories", "Oid", c => c.Int(nullable: false));
            AddColumn("dbo.PriceTags", "Oid", c => c.Int(nullable: false));
            AddColumn("dbo.Promotions", "Oid", c => c.Int(nullable: false));
            AddColumn("dbo.OrderTagGroups", "Oid", c => c.Int(nullable: false));
            AddColumn("dbo.ScreenMenus", "Oid", c => c.Int(nullable: false));
            AddColumn("dbo.TicketTagGroups", "Oid", c => c.Int(nullable: false));


            Sql("UPDATE MenuItems set Oid=1");
            Sql("UPDATE Categories set Oid=1");
            Sql("UPDATE PriceTags set Oid=1");
            Sql("UPDATE Promotions set Oid=1");
            Sql("UPDATE OrderTagGroups set Oid=1");
            Sql("UPDATE ScreenMenus set Oid=1");
            Sql("UPDATE TicketTagGroups set Oid=1");
        }

        public override void Down()
        {
            DropColumn("dbo.TicketTagGroups", "Oid");
            DropColumn("dbo.ScreenMenus", "Oid");
            DropColumn("dbo.OrderTagGroups", "Oid");
            DropColumn("dbo.Promotions", "Oid");
            DropColumn("dbo.PriceTags", "Oid");
            DropColumn("dbo.Categories", "Oid");
            DropColumn("dbo.MenuItems", "Oid");
            AlterTableAnnotations(
                "dbo.TicketTagGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SortOrder = c.Int(nullable: false),
                        Locations = c.String(),
                        Group = c.Boolean(nullable: false),
                        Departments = c.String(),
                        SaveFreeTags = c.Boolean(nullable: false),
                        FreeTagging = c.Boolean(nullable: false),
                        ForceValue = c.Boolean(nullable: false),
                        AskBeforeCreatingTicket = c.Boolean(nullable: false),
                        DataType = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        SubTagId = c.Int(nullable: false),
                        Oid = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_TicketTagGroup_ConnectFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.ScreenMenus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Locations = c.String(),
                        Group = c.Boolean(nullable: false),
                        CategoryColumnCount = c.Int(nullable: false),
                        CategoryColumnWidthRate = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        Departments = c.String(),
                        Oid = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ScreenMenu_ConnectFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.OrderTagGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        MaxSelectedItems = c.Int(nullable: false),
                        MinSelectedItems = c.Int(nullable: false),
                        SortOrder = c.Int(nullable: false),
                        AddTagPriceToOrderPrice = c.Boolean(nullable: false),
                        SaveFreeTags = c.Boolean(nullable: false),
                        FreeTagging = c.Boolean(nullable: false),
                        TaxFree = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        Prefix = c.String(),
                        Locations = c.String(),
                        Group = c.Boolean(nullable: false),
                        Oid = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_OrderTagGroup_ConnectFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Promotions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Name = c.String(),
                        TenantId = c.Int(nullable: false),
                        PromotionTypeId = c.Int(nullable: false),
                        Locations = c.String(),
                        Group = c.Boolean(nullable: false),
                        Active = c.Boolean(nullable: false),
                        SortOrder = c.Int(nullable: false),
                        Filter = c.String(),
                        Oid = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_Promotion_ConnectFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.PriceTags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        Name = c.String(maxLength: 10),
                        Locations = c.String(),
                        Group = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        Departments = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Oid = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_PriceTag_ConnectFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(),
                        TenantId = c.Int(nullable: false),
                        Oid = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_Category_ConnectFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.MenuItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 100),
                        BarCode = c.String(maxLength: 50),
                        AliasCode = c.String(maxLength: 50),
                        AliasName = c.String(maxLength: 100),
                        ItemDescription = c.String(maxLength: 1000),
                        ForceQuantity = c.Boolean(nullable: false),
                        ForceChangePrice = c.Boolean(nullable: false),
                        CategoryId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        ProductType = c.Int(nullable: false),
                        Tag = c.String(maxLength: 50),
                        Locations = c.String(),
                        Group = c.Boolean(nullable: false),
                        TransactionTypeId = c.Int(),
                        Files = c.String(),
                        HsnCode = c.String(maxLength: 50),
                        Oid = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_MenuItem_ConnectFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
        }
    }
}
