namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeMembershipTier202109301 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EngageMembershipTiers", "RewardingVoucherId", c => c.Int());
            AddColumn("dbo.EngageMembershipTiers", "TextOfBenefits", c => c.String(nullable: false));
            CreateIndex("dbo.EngageMembershipTiers", "RewardingVoucherId");
            AddForeignKey("dbo.EngageMembershipTiers", "RewardingVoucherId", "dbo.GiftVoucherTypes", "Id");
            DropColumn("dbo.EngageMembershipTiers", "AmountToEarnOnePoint");
            DropColumn("dbo.EngageMembershipTiers", "TextOfBenefit");
        }
        
        public override void Down()
        {
            AddColumn("dbo.EngageMembershipTiers", "TextOfBenefit", c => c.String(nullable: false));
            AddColumn("dbo.EngageMembershipTiers", "AmountToEarnOnePoint", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropForeignKey("dbo.EngageMembershipTiers", "RewardingVoucherId", "dbo.GiftVoucherTypes");
            DropIndex("dbo.EngageMembershipTiers", new[] { "RewardingVoucherId" });
            DropColumn("dbo.EngageMembershipTiers", "TextOfBenefits");
            DropColumn("dbo.EngageMembershipTiers", "RewardingVoucherId");
        }
    }
}
