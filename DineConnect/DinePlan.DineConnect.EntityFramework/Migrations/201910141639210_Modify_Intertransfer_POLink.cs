namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Intertransfer_POLink : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InterTransferDetails", "PurchaseOrderRefId", c => c.Int());
            AddColumn("dbo.InterTransfers", "DoesPOCreatedForThisRequest", c => c.Boolean(nullable: false));
            CreateIndex("dbo.InterTransferDetails", "PurchaseOrderRefId");
            AddForeignKey("dbo.InterTransferDetails", "PurchaseOrderRefId", "dbo.PurchaseOrders", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InterTransferDetails", "PurchaseOrderRefId", "dbo.PurchaseOrders");
            DropIndex("dbo.InterTransferDetails", new[] { "PurchaseOrderRefId" });
            DropColumn("dbo.InterTransfers", "DoesPOCreatedForThisRequest");
            DropColumn("dbo.InterTransferDetails", "PurchaseOrderRefId");
        }
    }
}
