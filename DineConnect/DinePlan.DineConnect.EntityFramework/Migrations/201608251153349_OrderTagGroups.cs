namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class OrderTagGroups : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrderTagGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        MaxSelectedItems = c.Int(nullable: false),
                        MinSelectedItems = c.Int(nullable: false),
                        SortOrder = c.Int(nullable: false),
                        AddTagPriceToOrderPrice = c.Boolean(nullable: false),
                        SaveFreeTags = c.Boolean(nullable: false),
                        FreeTagging = c.Boolean(nullable: false),
                        TaxFree = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_OrderTagGroup_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_OrderTagGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OrderTagMaps",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrderTagGroupId = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                        MenuItemId = c.Int(nullable: false),
                        PortionId = c.Int(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OrderTagGroups", t => t.OrderTagGroupId, cascadeDelete: true)
                .Index(t => t.OrderTagGroupId);
            
            CreateTable(
                "dbo.OrderTags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SortOrder = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OrderTagGroupId = c.Int(nullable: false),
                        MenuItemId = c.Int(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OrderTagGroups", t => t.OrderTagGroupId, cascadeDelete: true)
                .Index(t => t.OrderTagGroupId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderTags", "OrderTagGroupId", "dbo.OrderTagGroups");
            DropForeignKey("dbo.OrderTagMaps", "OrderTagGroupId", "dbo.OrderTagGroups");
            DropIndex("dbo.OrderTags", new[] { "OrderTagGroupId" });
            DropIndex("dbo.OrderTagMaps", new[] { "OrderTagGroupId" });
            DropTable("dbo.OrderTags");
            DropTable("dbo.OrderTagMaps");
            DropTable("dbo.OrderTagGroups",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_OrderTagGroup_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_OrderTagGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
