namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddCardDetails : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MenuItemTaxDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DinePlanTaxId = c.Int(nullable: false),
                        MenuItemId = c.Int(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DinePlanTaxes", t => t.DinePlanTaxId, cascadeDelete: true)
                .ForeignKey("dbo.MenuItems", t => t.MenuItemId, cascadeDelete: true)
                .Index(t => t.DinePlanTaxId)
                .Index(t => t.MenuItemId);
            
            CreateTable(
                "dbo.ConnectCardRedemptions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ConnectCardId = c.Int(nullable: false),
                        TicketNumber = c.String(),
                        LocationId = c.Int(nullable: false),
                        RedemptionDate = c.DateTime(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ConnectCards", t => t.ConnectCardId, cascadeDelete: true)
                .Index(t => t.ConnectCardId);
            
            CreateTable(
                "dbo.ConnectCards",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ConnectCardTypeId = c.Int(nullable: false),
                        CardNo = c.String(maxLength: 30),
                        TenantId = c.Int(nullable: false),
                        ExpiryDate = c.DateTime(nullable: false),
                        Redeemed = c.Boolean(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ConnectCardTypes", t => t.ConnectCardTypeId, cascadeDelete: true)
                .Index(t => t.ConnectCardTypeId)
                .Index(t => t.CardNo, unique: true);
            
            CreateTable(
                "dbo.ConnectCardTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UseOneTime = c.Boolean(nullable: false),
                        Oid = c.Int(nullable: false),
                        Length = c.Int(nullable: false),
                        Locations = c.String(),
                        Group = c.Boolean(nullable: false),
                        NonLocations = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ConnectCardType_ConnectFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ConnectCardType_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ConnectCardType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ConnectCardRedemptions", "ConnectCardId", "dbo.ConnectCards");
            DropForeignKey("dbo.ConnectCards", "ConnectCardTypeId", "dbo.ConnectCardTypes");
            DropForeignKey("dbo.MenuItemTaxDetails", "MenuItemId", "dbo.MenuItems");
            DropForeignKey("dbo.MenuItemTaxDetails", "DinePlanTaxId", "dbo.DinePlanTaxes");
            DropIndex("dbo.ConnectCards", new[] { "CardNo" });
            DropIndex("dbo.ConnectCards", new[] { "ConnectCardTypeId" });
            DropIndex("dbo.ConnectCardRedemptions", new[] { "ConnectCardId" });
            DropIndex("dbo.MenuItemTaxDetails", new[] { "MenuItemId" });
            DropIndex("dbo.MenuItemTaxDetails", new[] { "DinePlanTaxId" });
            DropTable("dbo.ConnectCardTypes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ConnectCardType_ConnectFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ConnectCardType_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ConnectCardType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ConnectCards");
            DropTable("dbo.ConnectCardRedemptions");
            DropTable("dbo.MenuItemTaxDetails");
        }
    }
}
