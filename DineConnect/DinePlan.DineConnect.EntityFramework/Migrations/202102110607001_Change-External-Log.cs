namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeExternalLog : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExternalLogs", "LogTimeTrunc", c => c.DateTime(nullable: false, storeType: "date"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ExternalLogs", "LogTimeTrunc");
        }
    }
}
