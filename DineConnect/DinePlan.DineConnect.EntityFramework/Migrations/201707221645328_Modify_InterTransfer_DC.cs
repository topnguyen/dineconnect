namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_InterTransfer_DC : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InterTransfers", "VehicleNumber", c => c.String());
            AddColumn("dbo.InterTransfers", "PersonInCharge", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.InterTransfers", "PersonInCharge");
            DropColumn("dbo.InterTransfers", "VehicleNumber");
        }
    }
}
