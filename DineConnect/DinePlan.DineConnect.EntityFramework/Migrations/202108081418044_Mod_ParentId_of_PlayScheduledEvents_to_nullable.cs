namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Mod_ParentId_of_PlayScheduledEvents_to_nullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PlayScheduledEvents", "ParentId", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PlayScheduledEvents", "ParentId", c => c.Int(nullable: false));
        }
    }
}
