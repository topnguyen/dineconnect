namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Add_LocationTag_Table : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Locations", "LocationGroupId", "dbo.LocationGroups");
            DropIndex("dbo.Locations", new[] { "LocationGroupId" });
            CreateTable(
                "dbo.LocationTags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LocationTag_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_LocationTag_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LocationGroupLocations",
                c => new
                    {
                        LocationGroup_Id = c.Int(nullable: false),
                        Location_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.LocationGroup_Id, t.Location_Id })
                .ForeignKey("dbo.LocationGroups", t => t.LocationGroup_Id, cascadeDelete: true)
                .ForeignKey("dbo.Locations", t => t.Location_Id, cascadeDelete: true)
                .Index(t => t.LocationGroup_Id)
                .Index(t => t.Location_Id);
            
            CreateTable(
                "dbo.LocationTagLocations",
                c => new
                    {
                        LocationTag_Id = c.Int(nullable: false),
                        Location_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.LocationTag_Id, t.Location_Id })
                .ForeignKey("dbo.LocationTags", t => t.LocationTag_Id, cascadeDelete: true)
                .ForeignKey("dbo.Locations", t => t.Location_Id, cascadeDelete: true)
                .Index(t => t.LocationTag_Id)
                .Index(t => t.Location_Id);
            
            DropColumn("dbo.Locations", "LocationGroupId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Locations", "LocationGroupId", c => c.Int());
            DropForeignKey("dbo.LocationTagLocations", "Location_Id", "dbo.Locations");
            DropForeignKey("dbo.LocationTagLocations", "LocationTag_Id", "dbo.LocationTags");
            DropForeignKey("dbo.LocationGroupLocations", "Location_Id", "dbo.Locations");
            DropForeignKey("dbo.LocationGroupLocations", "LocationGroup_Id", "dbo.LocationGroups");
            DropIndex("dbo.LocationTagLocations", new[] { "Location_Id" });
            DropIndex("dbo.LocationTagLocations", new[] { "LocationTag_Id" });
            DropIndex("dbo.LocationGroupLocations", new[] { "Location_Id" });
            DropIndex("dbo.LocationGroupLocations", new[] { "LocationGroup_Id" });
            DropTable("dbo.LocationTagLocations");
            DropTable("dbo.LocationGroupLocations");
            DropTable("dbo.LocationTags",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LocationTag_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_LocationTag_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            CreateIndex("dbo.Locations", "LocationGroupId");
            AddForeignKey("dbo.Locations", "LocationGroupId", "dbo.LocationGroups", "Id");
        }
    }
}
