namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Material_Transfer_StkAdj_Units : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Materials", "ReceivingUnitId", c => c.Int());
            AddColumn("dbo.Materials", "TransferUnitId", c => c.Int());
            AddColumn("dbo.Materials", "StockAdjustmentUnitId", c => c.Int());
            CreateIndex("dbo.Materials", "ReceivingUnitId");
            CreateIndex("dbo.Materials", "TransferUnitId");
            CreateIndex("dbo.Materials", "StockAdjustmentUnitId");
            AddForeignKey("dbo.Materials", "ReceivingUnitId", "dbo.Units", "Id");
            AddForeignKey("dbo.Materials", "StockAdjustmentUnitId", "dbo.Units", "Id");
            AddForeignKey("dbo.Materials", "TransferUnitId", "dbo.Units", "Id");
            Sql("Update dbo.Materials Set ReceivingUnitId = DefaultUnitId");
            Sql("Update dbo.Materials Set TransferUnitId = IssueUnitId");
            Sql("Update dbo.Materials Set StockAdjustmentUnitId = IssueUnitId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Materials", "TransferUnitId", "dbo.Units");
            DropForeignKey("dbo.Materials", "StockAdjustmentUnitId", "dbo.Units");
            DropForeignKey("dbo.Materials", "ReceivingUnitId", "dbo.Units");
            DropIndex("dbo.Materials", new[] { "StockAdjustmentUnitId" });
            DropIndex("dbo.Materials", new[] { "TransferUnitId" });
            DropIndex("dbo.Materials", new[] { "ReceivingUnitId" });
            DropColumn("dbo.Materials", "StockAdjustmentUnitId");
            DropColumn("dbo.Materials", "TransferUnitId");
            DropColumn("dbo.Materials", "ReceivingUnitId");
        }
    }
}
