namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddDepartmentGroup : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DepartmentGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(),
                        AddOns = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DepartmentGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Departments", "AddOns", c => c.String());
            AddColumn("dbo.Departments", "DepartmentGroupId", c => c.Int());
            CreateIndex("dbo.Departments", "DepartmentGroupId");
            AddForeignKey("dbo.Departments", "DepartmentGroupId", "dbo.DepartmentGroups", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Departments", "DepartmentGroupId", "dbo.DepartmentGroups");
            DropIndex("dbo.Departments", new[] { "DepartmentGroupId" });
            DropColumn("dbo.Departments", "DepartmentGroupId");
            DropColumn("dbo.Departments", "AddOns");
            DropTable("dbo.DepartmentGroups",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DepartmentGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
