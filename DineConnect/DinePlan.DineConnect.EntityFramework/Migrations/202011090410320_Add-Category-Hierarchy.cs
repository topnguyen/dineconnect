namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddCategoryHierarchy : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CategoryHierarchies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.String(maxLength: 10),
                        Name = c.String(maxLength: 50),
                        ParentId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CategoryHierarchy_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_CategoryHierarchy_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CategoryHierarchies", t => t.ParentId)
                .Index(t => t.ParentId);
            
            AddColumn("dbo.Categories", "CategoryHierarchyId", c => c.Int());
            AlterColumn("dbo.ConnectAddOns", "Settings", c => c.String(maxLength: 3000));
            CreateIndex("dbo.Categories", "CategoryHierarchyId");
            AddForeignKey("dbo.Categories", "CategoryHierarchyId", "dbo.CategoryHierarchies", "Id");
            DropColumn("dbo.ConnectCardRedemptions", "RedemptionType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ConnectCardRedemptions", "RedemptionType", c => c.Int(nullable: false));
            DropForeignKey("dbo.Categories", "CategoryHierarchyId", "dbo.CategoryHierarchies");
            DropForeignKey("dbo.CategoryHierarchies", "ParentId", "dbo.CategoryHierarchies");
            DropIndex("dbo.CategoryHierarchies", new[] { "ParentId" });
            DropIndex("dbo.Categories", new[] { "CategoryHierarchyId" });
            AlterColumn("dbo.ConnectAddOns", "Settings", c => c.String(maxLength: 1000));
            DropColumn("dbo.Categories", "CategoryHierarchyId");
            DropTable("dbo.CategoryHierarchies",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CategoryHierarchy_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_CategoryHierarchy_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
