namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_UnitRefId_InvoiceDetail : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InvoiceDetails", "UnitRefId", c => c.Int(nullable: true));

            Sql("UPDATE InvoiceDetails SET InvoiceDetails.UnitRefId = (SELECT Materials.DefaultUnitId from Materials where InvoiceDetails.MaterialRefId=Materials.ID)");

            AlterColumn("dbo.InvoiceDetails", "UnitRefId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.InvoiceDetails", "UnitRefId");
        }
    }
}
