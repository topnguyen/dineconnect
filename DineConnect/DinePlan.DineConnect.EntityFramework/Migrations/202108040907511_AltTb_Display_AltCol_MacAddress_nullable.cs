namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AltTb_Display_AltCol_MacAddress_nullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PlayDisplays", "MacAddress", c => c.String(maxLength: 17));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PlayDisplays", "MacAddress", c => c.String(nullable: false, maxLength: 17));
        }
    }
}
