namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAddressRemoveOrderType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DeliveryTickets", "Address", c => c.String());
            DropColumn("dbo.DeliveryTickets", "OrderType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DeliveryTickets", "OrderType", c => c.Int(nullable: false));
            DropColumn("dbo.DeliveryTickets", "Address");
        }
    }
}
