namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddQuickDineInActive : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Locations", "QuickDineInActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Locations", "QuickDineInActive");
        }
    }
}
