namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeDeliveryStructure : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DeliveryAggregators", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.DeliveryAggPlatforms", "DeliveryAggregatorId", "dbo.DeliveryAggregators");
            DropIndex("dbo.DeliveryAggPlatforms", new[] { "DeliveryAggregatorId" });
            DropIndex("dbo.DeliveryAggregators", new[] { "LocationId" });
            AddColumn("dbo.DelAggLocMappings", "MenuContents", c => c.String());
            DropTable("dbo.DeliveryAggPlatforms",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DeliveryAggPlatform_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DeliveryAggregators",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DeliveryAggregator_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DeliveryAggregator_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.DeliveryAggregators",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DeliveryAggregator_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DeliveryAggregator_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DeliveryAggPlatforms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DeliveryAggregatorId = c.Int(nullable: false),
                        DeliveryAggType = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                        TotalActiveItems = c.Int(nullable: false),
                        TotalInActiveItems = c.Int(nullable: false),
                        MenuContents = c.String(),
                        MerchantCode = c.String(),
                        PartnerMerchantCode = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DeliveryAggPlatform_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.DelAggLocMappings", "MenuContents");
            CreateIndex("dbo.DeliveryAggregators", "LocationId");
            CreateIndex("dbo.DeliveryAggPlatforms", "DeliveryAggregatorId");
            AddForeignKey("dbo.DeliveryAggPlatforms", "DeliveryAggregatorId", "dbo.DeliveryAggregators", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DeliveryAggregators", "LocationId", "dbo.Locations", "Id", cascadeDelete: true);
        }
    }
}
