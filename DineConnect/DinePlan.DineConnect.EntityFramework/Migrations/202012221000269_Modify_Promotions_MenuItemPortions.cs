namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Promotions_MenuItemPortions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DemandPromotionExecutions", "MenuItemPortionId", c => c.Int(nullable: false));
            AddColumn("dbo.FixedPromotions", "MenuItemPortionId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FixedPromotions", "MenuItemPortionId");
            DropColumn("dbo.DemandPromotionExecutions", "MenuItemPortionId");
        }
    }
}
