namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPromotionCategoryTag : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PromotionCategories", "Tag", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PromotionCategories", "Tag");
        }
    }
}
