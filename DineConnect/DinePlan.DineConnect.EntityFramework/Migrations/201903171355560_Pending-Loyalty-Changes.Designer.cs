// <auto-generated />
namespace DinePlan.DineConnect.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class PendingLoyaltyChanges : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(PendingLoyaltyChanges));
        
        string IMigrationMetadata.Id
        {
            get { return "201903171355560_Pending-Loyalty-Changes"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
