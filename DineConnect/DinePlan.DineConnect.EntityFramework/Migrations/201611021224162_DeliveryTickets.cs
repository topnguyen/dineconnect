namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class DeliveryTickets : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DeliveryOrders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MenuItemId = c.Int(nullable: false),
                        MenuItemName = c.String(),
                        PortionName = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Tax = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Quantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Note = c.String(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                        DeliveryTicket_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DeliveryTickets", t => t.DeliveryTicket_Id)
                .Index(t => t.DeliveryTicket_Id);
            
            CreateTable(
                "dbo.DeliveryTickets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationId = c.Int(nullable: false),
                        LastOrderDate = c.DateTime(nullable: false),
                        TotalAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Note = c.String(),
                        TenantId = c.Int(nullable: false),
                        SyncId = c.Int(nullable: false),
                        DeliveryStatus = c.Int(nullable: false),
                        ConnectMemberId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DeliveryTicket_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DeliveryTicket_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ConnectMembers", t => t.ConnectMemberId, cascadeDelete: true)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .Index(t => t.LocationId)
                .Index(t => t.ConnectMemberId);
            
           
            
            AddColumn("dbo.Locations", "LocationServiceUrl", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DeliveryOrders", "DeliveryTicket_Id", "dbo.DeliveryTickets");
            DropForeignKey("dbo.DeliveryTickets", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.DeliveryTickets", "ConnectMemberId", "dbo.ConnectMembers");
            DropIndex("dbo.DeliveryTickets", new[] { "ConnectMemberId" });
            DropIndex("dbo.DeliveryTickets", new[] { "LocationId" });
            DropIndex("dbo.DeliveryOrders", new[] { "DeliveryTicket_Id" });
            DropColumn("dbo.Locations", "LocationServiceUrl");
            DropTable("dbo.DeliveryTickets",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DeliveryTicket_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DeliveryTicket_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DeliveryOrders");
        }
    }
}
