namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCombo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductComboes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MenuItemId = c.Int(nullable: false),
                        SyncId = c.Int(nullable: false),
                        SortOrder = c.Int(nullable: false),
                        AddPriceToOrderPrice = c.Boolean(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProductComboGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SortOrder = c.Int(nullable: false),
                        Minimum = c.Int(nullable: false),
                        ProductComboId = c.Int(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ProductComboes", t => t.ProductComboId, cascadeDelete: true)
                .Index(t => t.ProductComboId);
            
            CreateTable(
                "dbo.ProductComboItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ProductComboGroupId = c.Int(nullable: false),
                        MenuItemId = c.Int(nullable: false),
                        AutoSelect = c.Boolean(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SortOrder = c.Int(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MenuItems", t => t.MenuItemId, cascadeDelete: true)
                .ForeignKey("dbo.ProductComboGroups", t => t.ProductComboGroupId, cascadeDelete: true)
                .Index(t => t.ProductComboGroupId)
                .Index(t => t.MenuItemId);
            
            AddColumn("dbo.MenuItems", "ProductType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductComboGroups", "ProductComboId", "dbo.ProductComboes");
            DropForeignKey("dbo.ProductComboItems", "ProductComboGroupId", "dbo.ProductComboGroups");
            DropForeignKey("dbo.ProductComboItems", "MenuItemId", "dbo.MenuItems");
            DropIndex("dbo.ProductComboItems", new[] { "MenuItemId" });
            DropIndex("dbo.ProductComboItems", new[] { "ProductComboGroupId" });
            DropIndex("dbo.ProductComboGroups", new[] { "ProductComboId" });
            DropColumn("dbo.MenuItems", "ProductType");
            DropTable("dbo.ProductComboItems");
            DropTable("dbo.ProductComboGroups");
            DropTable("dbo.ProductComboes");
        }
    }
}
