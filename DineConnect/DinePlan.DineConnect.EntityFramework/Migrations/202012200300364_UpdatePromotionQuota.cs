namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatePromotionQuota : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PromotionQuotas", "QuotaAmount", c => c.Int(nullable: false));
            AddColumn("dbo.PromotionQuotas", "QuotaUsed", c => c.Int(nullable: false));
            AddColumn("dbo.PromotionQuotas", "LastResetDate", c => c.DateTime());
            AddColumn("dbo.PromotionQuotas", "PlantResetDate", c => c.DateTime());
            DropColumn("dbo.PromotionQuotas", "Count");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PromotionQuotas", "Count", c => c.Int(nullable: false));
            DropColumn("dbo.PromotionQuotas", "PlantResetDate");
            DropColumn("dbo.PromotionQuotas", "LastResetDate");
            DropColumn("dbo.PromotionQuotas", "QuotaUsed");
            DropColumn("dbo.PromotionQuotas", "QuotaAmount");
        }
    }
}
