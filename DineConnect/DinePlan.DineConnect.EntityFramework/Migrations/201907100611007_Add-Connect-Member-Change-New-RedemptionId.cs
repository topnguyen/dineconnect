namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddConnectMemberChangeNewRedemptionId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MemberPoints", "RedemtionId", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MemberPoints", "RedemtionId");
        }
    }
}
