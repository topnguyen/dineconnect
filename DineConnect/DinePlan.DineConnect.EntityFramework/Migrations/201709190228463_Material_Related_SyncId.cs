namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Material_Related_SyncId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Materials", "SyncId", c => c.Int());
            AddColumn("dbo.MaterialGroupCategories", "SyncId", c => c.Int());
            AddColumn("dbo.MaterialGroups", "SyncId", c => c.Int());
            AddColumn("dbo.SupplierMaterials", "SyncId", c => c.Int());
            AddColumn("dbo.Taxes", "SyncId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Taxes", "SyncId");
            DropColumn("dbo.SupplierMaterials", "SyncId");
            DropColumn("dbo.MaterialGroups", "SyncId");
            DropColumn("dbo.MaterialGroupCategories", "SyncId");
            DropColumn("dbo.Materials", "SyncId");
        }
    }
}
