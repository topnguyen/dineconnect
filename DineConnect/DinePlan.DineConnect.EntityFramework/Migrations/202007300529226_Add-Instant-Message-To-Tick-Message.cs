namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddInstantMessageToTickMessage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TickMessages", "InstantMessage", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TickMessages", "InstantMessage");
        }
    }
}
