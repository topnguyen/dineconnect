namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_RegistrationKey_to_Display : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PlayDisplays", "RegistrationKey", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PlayDisplays", "RegistrationKey");
        }
    }
}
