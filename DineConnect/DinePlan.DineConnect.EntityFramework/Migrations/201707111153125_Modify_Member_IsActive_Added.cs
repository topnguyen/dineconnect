namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Member_IsActive_Added : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SwipeMembers", "IsActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SwipeMembers", "IsActive");
        }
    }
}
