namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Users1 : DbMigration
    {
        public override void Up()
        {
            Sql("update dbo.ScreenMenuCategorySchedules set StartDate=CreationTime, EndDate=CreationTime where StartDate is NULL");
            AddColumn("dbo.AbpUsers", "UserJsonData", c => c.String());
            AlterColumn("dbo.ScreenMenuCategorySchedules", "StartDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.ScreenMenuCategorySchedules", "EndDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.AbpUsers", "PasswordPolicy");
            DropColumn("dbo.AbpUsers", "GLTGV");
            DropColumn("dbo.AbpUsers", "GLTGB");
            DropColumn("dbo.AbpUsers", "Language");
            DropColumn("dbo.AbpUsers", "Tel1_Number");
            DropColumn("dbo.AbpUsers", "Tel1_Ext");
            DropColumn("dbo.AbpUsers", "FaxNumber");
            DropColumn("dbo.AbpUsers", "Mobile");
            DropColumn("dbo.AbpUsers", "Department");
            DropColumn("dbo.AbpUsers", "Role_Name");
            DropColumn("dbo.AbpUsers", "NFLA");
            DropColumn("dbo.AbpUsers", "NFLA_Date");
            DropColumn("dbo.AbpUsers", "SFL_User");
            DropColumn("dbo.AbpUsers", "UserActivationCode");
            DropColumn("dbo.DepartmentGroups", "SortOrder");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DepartmentGroups", "SortOrder", c => c.Int(nullable: false));
            AddColumn("dbo.AbpUsers", "UserActivationCode", c => c.String());
            AddColumn("dbo.AbpUsers", "SFL_User", c => c.Int(nullable: false));
            AddColumn("dbo.AbpUsers", "NFLA_Date", c => c.DateTime());
            AddColumn("dbo.AbpUsers", "NFLA", c => c.Int(nullable: false));
            AddColumn("dbo.AbpUsers", "Role_Name", c => c.String());
            AddColumn("dbo.AbpUsers", "Department", c => c.String());
            AddColumn("dbo.AbpUsers", "Mobile", c => c.String());
            AddColumn("dbo.AbpUsers", "FaxNumber", c => c.String());
            AddColumn("dbo.AbpUsers", "Tel1_Ext", c => c.String());
            AddColumn("dbo.AbpUsers", "Tel1_Number", c => c.String());
            AddColumn("dbo.AbpUsers", "Language", c => c.String());
            AddColumn("dbo.AbpUsers", "GLTGB", c => c.DateTime());
            AddColumn("dbo.AbpUsers", "GLTGV", c => c.DateTime());
            AddColumn("dbo.AbpUsers", "PasswordPolicy", c => c.Int(nullable: false));
            AlterColumn("dbo.ScreenMenuCategorySchedules", "EndDate", c => c.DateTime());
            AlterColumn("dbo.ScreenMenuCategorySchedules", "StartDate", c => c.DateTime());
            DropColumn("dbo.AbpUsers", "UserJsonData");
        }
    }
}
