namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFutureDateToDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FutureDateInformations", "FutureDateTo", c => c.DateTime());
            AddColumn("dbo.FutureDateInformations", "ObjectContents", c => c.String());

            //Sql("update DinePlantaxes set TransactionTypeId=(select Top 1 TransactionTypeId from DinePlanTaxLocations where DinePlanTaxes.Id = DinePlanTaxLocations.DinePlanTaxRefId)");
            //Sql("update DinePlanTaxLocations set Locations=CONCAT('[{\"Id\":',LocationRefId,',\"Code\":\"',(select code from Locations where Id = LocationRefId),'\",\"Name\":\"',(select name from Locations where Id = LocationRefId),'\"}]')");
            //Sql("update DinePlanTaxLocations set TenantId=(select TenantId from DinePlanTaxes where DinePlanTaxes.Id = DinePlanTaxLocations.DinePlanTaxRefId)");

        }
        
        public override void Down()
        {
            DropColumn("dbo.FutureDateInformations", "ObjectContents");
            DropColumn("dbo.FutureDateInformations", "FutureDateTo");
        }
    }
}
