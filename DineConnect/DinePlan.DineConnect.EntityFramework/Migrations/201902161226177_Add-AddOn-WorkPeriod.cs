namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAddOnWorkPeriod : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WorkPeriods", "AddOn", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.WorkPeriods", "AddOn");
        }
    }
}
