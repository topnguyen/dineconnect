namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_ManualReason_Category : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ManualReasons", "ManualReasonCategoryRefId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ManualReasons", "ManualReasonCategoryRefId");
        }
    }
}
