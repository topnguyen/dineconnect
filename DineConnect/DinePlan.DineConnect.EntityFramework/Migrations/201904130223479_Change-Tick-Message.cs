namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeTickMessage : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ReceiptContents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Header = c.String(),
                        Footer = c.String(),
                        Locations = c.String(),
                        Group = c.Boolean(nullable: false),
                        NonLocations = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ReceiptContent_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ReceiptContent_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SecondDisplays",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Images = c.String(),
                        Videos = c.String(),
                        Locations = c.String(),
                        Group = c.Boolean(nullable: false),
                        NonLocations = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SecondDisplay_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SecondDisplay_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TicketDiscountPromotions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PromotionId = c.Int(nullable: false),
                        PromotionValueType = c.Int(nullable: false),
                        ButtonCaption = c.String(maxLength: 50),
                        ButtonColor = c.String(maxLength: 10),
                        PromotionValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PromotionOverride = c.Boolean(nullable: false),
                        AskReference = c.Boolean(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Promotions", t => t.PromotionId, cascadeDelete: true)
                .Index(t => t.PromotionId);
            
            CreateTable(
                "dbo.TickMessageReplies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TickMessageId = c.Int(nullable: false),
                        LocationId = c.Int(nullable: false),
                        User = c.String(),
                        Reply = c.String(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .ForeignKey("dbo.TickMessages", t => t.TickMessageId, cascadeDelete: true)
                .Index(t => t.TickMessageId)
                .Index(t => t.LocationId);
            
            CreateTable(
                "dbo.TickMessages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Message = c.String(),
                        Acknowledgement = c.Boolean(nullable: false),
                        Locations = c.String(),
                        Group = c.Boolean(nullable: false),
                        NonLocations = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TickMessage_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TickMessage_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.GiftVoucherTypes", "NonLocations", c => c.String());
            AddColumn("dbo.MenuItems", "NonLocations", c => c.String());
            AddColumn("dbo.PriceTags", "NonLocations", c => c.String());
            AddColumn("dbo.Calculations", "NonLocations", c => c.String());
            AddColumn("dbo.ConnectTableGroups", "NonLocations", c => c.String());
            AddColumn("dbo.Promotions", "NonLocations", c => c.String());
            AddColumn("dbo.Departments", "NonLocations", c => c.String());
            AddColumn("dbo.DinePlanUsers", "NonLocations", c => c.String());
            AddColumn("dbo.OrderTagGroups", "NonLocations", c => c.String());
            AddColumn("dbo.RestrictDates", "NonLocations", c => c.String());
            AddColumn("dbo.ScreenMenus", "NonLocations", c => c.String());
            AddColumn("dbo.TicketTagGroups", "NonLocations", c => c.String());
            AddColumn("dbo.TillAccounts", "NonLocations", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TickMessageReplies", "TickMessageId", "dbo.TickMessages");
            DropForeignKey("dbo.TickMessageReplies", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.TicketDiscountPromotions", "PromotionId", "dbo.Promotions");
            DropIndex("dbo.TickMessageReplies", new[] { "LocationId" });
            DropIndex("dbo.TickMessageReplies", new[] { "TickMessageId" });
            DropIndex("dbo.TicketDiscountPromotions", new[] { "PromotionId" });
            DropColumn("dbo.TillAccounts", "NonLocations");
            DropColumn("dbo.TicketTagGroups", "NonLocations");
            DropColumn("dbo.ScreenMenus", "NonLocations");
            DropColumn("dbo.RestrictDates", "NonLocations");
            DropColumn("dbo.OrderTagGroups", "NonLocations");
            DropColumn("dbo.DinePlanUsers", "NonLocations");
            DropColumn("dbo.Departments", "NonLocations");
            DropColumn("dbo.Promotions", "NonLocations");
            DropColumn("dbo.ConnectTableGroups", "NonLocations");
            DropColumn("dbo.Calculations", "NonLocations");
            DropColumn("dbo.PriceTags", "NonLocations");
            DropColumn("dbo.MenuItems", "NonLocations");
            DropColumn("dbo.GiftVoucherTypes", "NonLocations");
            DropTable("dbo.TickMessages",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TickMessage_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TickMessage_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.TickMessageReplies");
            DropTable("dbo.TicketDiscountPromotions");
            DropTable("dbo.SecondDisplays",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SecondDisplay_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SecondDisplay_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ReceiptContents",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ReceiptContent_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ReceiptContent_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
