namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Users : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AbpUsers", "PasswordPolicy", c => c.Int(nullable: false));
            AddColumn("dbo.AbpUsers", "GLTGV", c => c.DateTime());
            AddColumn("dbo.AbpUsers", "GLTGB", c => c.DateTime());
            AddColumn("dbo.AbpUsers", "Language", c => c.String());
            AddColumn("dbo.AbpUsers", "Tel1_Number", c => c.String());
            AddColumn("dbo.AbpUsers", "Tel1_Ext", c => c.String());
            AddColumn("dbo.AbpUsers", "FaxNumber", c => c.String());
            AddColumn("dbo.AbpUsers", "Mobile", c => c.String());
            AddColumn("dbo.AbpUsers", "Department", c => c.String());
            AddColumn("dbo.AbpUsers", "Role_Name", c => c.String());
            AddColumn("dbo.AbpUsers", "NFLA", c => c.Int(nullable: false));
            AddColumn("dbo.AbpUsers", "NFLA_Date", c => c.DateTime());
            AddColumn("dbo.AbpUsers", "SFL_User", c => c.Int(nullable: false));
            AddColumn("dbo.AbpUsers", "UserActivationCode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AbpUsers", "UserActivationCode");
            DropColumn("dbo.AbpUsers", "SFL_User");
            DropColumn("dbo.AbpUsers", "NFLA_Date");
            DropColumn("dbo.AbpUsers", "NFLA");
            DropColumn("dbo.AbpUsers", "Role_Name");
            DropColumn("dbo.AbpUsers", "Department");
            DropColumn("dbo.AbpUsers", "Mobile");
            DropColumn("dbo.AbpUsers", "FaxNumber");
            DropColumn("dbo.AbpUsers", "Tel1_Ext");
            DropColumn("dbo.AbpUsers", "Tel1_Number");
            DropColumn("dbo.AbpUsers", "Language");
            DropColumn("dbo.AbpUsers", "GLTGB");
            DropColumn("dbo.AbpUsers", "GLTGV");
            DropColumn("dbo.AbpUsers", "PasswordPolicy");
        }
    }
}
