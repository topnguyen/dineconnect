namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangesGiftVoucher : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GiftVoucherTypes", "AutoAttach", c => c.Boolean(nullable: false));
            AddColumn("dbo.GiftVoucherTypes", "ConfNumber", c => c.String());
            AddColumn("dbo.GiftVoucherTypes", "RedeemStartHour", c => c.Int(nullable: false));
            AddColumn("dbo.GiftVoucherTypes", "RedeemStartMinute", c => c.Int(nullable: false));
            AddColumn("dbo.GiftVoucherTypes", "RedeemEndHour", c => c.Int(nullable: false));
            AddColumn("dbo.GiftVoucherTypes", "RedeemEndMinute", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.GiftVoucherTypes", "RedeemEndMinute");
            DropColumn("dbo.GiftVoucherTypes", "RedeemEndHour");
            DropColumn("dbo.GiftVoucherTypes", "RedeemStartMinute");
            DropColumn("dbo.GiftVoucherTypes", "RedeemStartHour");
            DropColumn("dbo.GiftVoucherTypes", "ConfNumber");
            DropColumn("dbo.GiftVoucherTypes", "AutoAttach");
        }
    }
}
