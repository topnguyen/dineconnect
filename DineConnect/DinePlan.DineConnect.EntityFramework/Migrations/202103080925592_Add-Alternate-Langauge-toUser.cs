namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAlternateLangaugetoUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DinePlanUsers", "AlternateLanguageCode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DinePlanUsers", "AlternateLanguageCode");
        }
    }
}
