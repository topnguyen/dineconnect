namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Location_TransferCutoffTime : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Locations", "TransferRequestLockHours", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Locations", "TransferRequestGraceHours", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Locations", "TransferRequestGraceHours");
            DropColumn("dbo.Locations", "TransferRequestLockHours");
        }
    }
}
