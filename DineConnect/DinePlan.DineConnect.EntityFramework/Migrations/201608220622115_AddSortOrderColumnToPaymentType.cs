namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSortOrderColumnToPaymentType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PaymentTypes", "AcceptChange", c => c.Boolean(nullable: false));
            AddColumn("dbo.PaymentTypes", "SortOrder", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PaymentTypes", "SortOrder");
            DropColumn("dbo.PaymentTypes", "AcceptChange");
        }
    }
}
