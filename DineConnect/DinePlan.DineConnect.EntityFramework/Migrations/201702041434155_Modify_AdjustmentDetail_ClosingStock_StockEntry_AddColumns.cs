namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_AdjustmentDetail_ClosingStock_StockEntry_AddColumns : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AdjustmentDetails", "StockEntry", c => c.Decimal(precision: 18, scale: 3));
            AddColumn("dbo.AdjustmentDetails", "ClosingStock", c => c.Decimal(precision: 18, scale: 3));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AdjustmentDetails", "ClosingStock");
            DropColumn("dbo.AdjustmentDetails", "StockEntry");
        }
    }
}
