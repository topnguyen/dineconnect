namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_House_Db : DbMigration
    {
        public override void Up()
        {

        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Invoices", "PurchaseCategoryRefId", "dbo.PurchaseCategories");
            DropIndex("dbo.Invoices", new[] { "PurchaseCategoryRefId" });
            AlterColumn("dbo.InterTransferReceivedDetails", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.InterTransferDetails", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.ClosingStocks", "ApprovedUserId", c => c.Int());
            DropColumn("dbo.Invoices", "PurchaseCategoryRefId");
            DropColumn("dbo.ClosingStocks", "AdjustmentRefId");
            DropTable("dbo.PurchaseCategories",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PurchaseCategory_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PurchaseCategory_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
