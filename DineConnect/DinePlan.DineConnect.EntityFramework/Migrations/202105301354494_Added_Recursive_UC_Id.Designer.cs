// <auto-generated />
namespace DinePlan.DineConnect.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Added_Recursive_UC_Id : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Added_Recursive_UC_Id));
        
        string IMigrationMetadata.Id
        {
            get { return "202105301354494_Added_Recursive_UC_Id"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
