namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUomToMenuItems : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MenuItems", "Uom", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MenuItems", "Uom");
        }
    }
}
