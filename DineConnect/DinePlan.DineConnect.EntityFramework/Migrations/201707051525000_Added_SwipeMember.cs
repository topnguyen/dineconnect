namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_SwipeMember : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SwipeMembers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MemberCode = c.String(maxLength: 30),
                        Name = c.String(),
                        EmailId = c.String(),
                        AlternateEmailId = c.String(),
                        PhoneNumber = c.String(),
                        AlternatePhoneNumber = c.String(),
                        AddressLine1 = c.String(),
                        AddressLine2 = c.String(),
                        Locality = c.String(),
                        City = c.String(),
                        State = c.String(),
                        Country = c.String(),
                        PostalCode = c.String(),
                        LastVisitDate = c.DateTime(),
                        TenantId = c.Int(nullable: false),
                        DefaultAddressRefId = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SwipeMember_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SwipeMember_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.MemberCode, unique: true, name: "MemberCode");
            
            CreateTable(
                "dbo.SwipeMemberSpecialDates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SwipeMemberRefId = c.Int(nullable: false),
                        SpecialDatesRefId = c.Int(nullable: false),
                        SpecialDate = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SwipeMemberSpecialDate_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SwipeMemberSpecialDate_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SwipeMembers", t => t.SwipeMemberRefId, cascadeDelete: true)
                .Index(t => t.SwipeMemberRefId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SwipeMemberSpecialDates", "SwipeMemberRefId", "dbo.SwipeMembers");
            DropIndex("dbo.SwipeMemberSpecialDates", new[] { "SwipeMemberRefId" });
            DropIndex("dbo.SwipeMembers", "MemberCode");
            DropTable("dbo.SwipeMemberSpecialDates",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SwipeMemberSpecialDate_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SwipeMemberSpecialDate_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.SwipeMembers",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SwipeMember_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SwipeMember_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
