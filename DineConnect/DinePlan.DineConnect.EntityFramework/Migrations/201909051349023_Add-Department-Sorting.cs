namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDepartmentSorting : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Departments", "SortOrder", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Departments", "SortOrder");
        }
    }
}
