namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ConnectAddress : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MemberAddresses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ConnectMemberId = c.Int(nullable: false),
                        LocationId = c.Int(nullable: false),
                        Address = c.String(),
                        City = c.String(),
                        State = c.String(),
                        Country = c.String(),
                        PostalCode = c.String(),
                        Locality = c.String(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ConnectMembers", t => t.ConnectMemberId, cascadeDelete: true)
                .Index(t => t.ConnectMemberId);
            
            AddColumn("dbo.ConnectMembers", "Locality", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MemberAddresses", "ConnectMemberId", "dbo.ConnectMembers");
            DropIndex("dbo.MemberAddresses", new[] { "ConnectMemberId" });
            DropColumn("dbo.ConnectMembers", "Locality");
            DropTable("dbo.MemberAddresses");
        }
    }
}
