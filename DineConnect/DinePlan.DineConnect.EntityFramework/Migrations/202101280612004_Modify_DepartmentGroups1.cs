namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_DepartmentGroups1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DepartmentGroups", "SortOrder", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DepartmentGroups", "SortOrder");
        }
    }
}
