namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangePromotionCategory : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PromotionCategories", "Code", c => c.String(maxLength: 10));
            AlterColumn("dbo.PromotionCategories", "Name", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PromotionCategories", "Name", c => c.String());
            AlterColumn("dbo.PromotionCategories", "Code", c => c.String());
        }
    }
}
