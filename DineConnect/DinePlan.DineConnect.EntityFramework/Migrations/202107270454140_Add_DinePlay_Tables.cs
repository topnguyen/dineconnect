namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Add_DinePlay_Tables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PlayDaypartExceptions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Day = c.String(nullable: false, maxLength: 10),
                        StartHour = c.Int(nullable: false),
                        StartMinute = c.Int(nullable: false),
                        EndHour = c.Int(nullable: false),
                        EndMinute = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        DaypartId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DaypartException_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DaypartException_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PlayDayparts", t => t.DaypartId, cascadeDelete: true)
                .Index(t => t.DaypartId);
            
            CreateTable(
                "dbo.PlayDayparts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                        StartHour = c.Int(nullable: false),
                        StartMinute = c.Int(nullable: false),
                        EndHour = c.Int(nullable: false),
                        EndMinute = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Daypart_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Daypart_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PlayDisplayGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        Code = c.String(nullable: false, maxLength: 30),
                        Name = c.String(maxLength: 100),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DisplayGroup_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DisplayGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PlayDisplays",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 40),
                        ResolutionId = c.Int(nullable: false),
                        LocationId = c.Int(nullable: false),
                        MacAddress = c.String(nullable: false, maxLength: 17),
                        Active = c.Boolean(nullable: false),
                        Approved = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Display_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Display_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .ForeignKey("dbo.PlayResolutions", t => t.ResolutionId, cascadeDelete: true)
                .Index(t => t.ResolutionId)
                .Index(t => t.LocationId);
            
            CreateTable(
                "dbo.PlayResolutions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Width = c.Int(nullable: false),
                        Height = c.Int(nullable: false),
                        Enabled = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Resolution_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Resolution_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PlayLayoutContents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        LayoutId = c.Int(nullable: false),
                        RegionNo = c.Int(nullable: false),
                        FileName = c.String(maxLength: 255),
                        SystemFileName = c.String(nullable: false, maxLength: 255),
                        FileType = c.String(maxLength: 50),
                        DisplayLength = c.Int(nullable: false),
                        Stretched = c.Boolean(nullable: false),
                        HasFile = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LayoutContent_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_LayoutContent_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PlayLayouts", t => t.LayoutId, cascadeDelete: true)
                .Index(t => t.LayoutId);
            
            CreateTable(
                "dbo.PlayLayouts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        ResolutionId = c.Int(nullable: false),
                        Description = c.String(maxLength: 255),
                        TemplateId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Layout_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Layout_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PlayResolutions", t => t.ResolutionId, cascadeDelete: true)
                .Index(t => t.ResolutionId);
            
            CreateTable(
                "dbo.PlayScheduledEvents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                        DisplayId = c.Int(),
                        DisplayGroupId = c.Int(),
                        DaypartId = c.Int(),
                        StartTime = c.DateTime(nullable: false),
                        EndTime = c.DateTime(),
                        LayoutId = c.Int(nullable: false),
                        Priority = c.Int(nullable: false),
                        RepeatType = c.String(),
                        RepeatInterval = c.Int(),
                        RepeatTimeUnit = c.String(),
                        RepeatWeekdays = c.String(),
                        RepeatUntil = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ScheduledEvent_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ScheduledEvent_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PlayDayparts", t => t.DaypartId)
                .ForeignKey("dbo.PlayDisplays", t => t.DisplayId)
                .ForeignKey("dbo.PlayDisplayGroups", t => t.DisplayGroupId)
                .ForeignKey("dbo.PlayLayouts", t => t.LayoutId, cascadeDelete: true)
                .Index(t => t.DisplayId)
                .Index(t => t.DisplayGroupId)
                .Index(t => t.DaypartId)
                .Index(t => t.LayoutId);
            
            CreateTable(
                "dbo.PlayDisplayGroupDisplays",
                c => new
                    {
                        DisplayId = c.Int(nullable: false),
                        DisplayGroupId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.DisplayId, t.DisplayGroupId })
                .ForeignKey("dbo.PlayDisplays", t => t.DisplayId, cascadeDelete: true)
                .ForeignKey("dbo.PlayDisplayGroups", t => t.DisplayGroupId, cascadeDelete: true)
                .Index(t => t.DisplayId)
                .Index(t => t.DisplayGroupId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PlayScheduledEvents", "LayoutId", "dbo.PlayLayouts");
            DropForeignKey("dbo.PlayScheduledEvents", "DisplayGroupId", "dbo.PlayDisplayGroups");
            DropForeignKey("dbo.PlayScheduledEvents", "DisplayId", "dbo.PlayDisplays");
            DropForeignKey("dbo.PlayScheduledEvents", "DaypartId", "dbo.PlayDayparts");
            DropForeignKey("dbo.PlayLayouts", "ResolutionId", "dbo.PlayResolutions");
            DropForeignKey("dbo.PlayLayoutContents", "LayoutId", "dbo.PlayLayouts");
            DropForeignKey("dbo.PlayDisplays", "ResolutionId", "dbo.PlayResolutions");
            DropForeignKey("dbo.PlayDisplays", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.PlayDisplayGroupDisplays", "DisplayGroupId", "dbo.PlayDisplayGroups");
            DropForeignKey("dbo.PlayDisplayGroupDisplays", "DisplayId", "dbo.PlayDisplays");
            DropForeignKey("dbo.PlayDaypartExceptions", "DaypartId", "dbo.PlayDayparts");
            DropIndex("dbo.PlayDisplayGroupDisplays", new[] { "DisplayGroupId" });
            DropIndex("dbo.PlayDisplayGroupDisplays", new[] { "DisplayId" });
            DropIndex("dbo.PlayScheduledEvents", new[] { "LayoutId" });
            DropIndex("dbo.PlayScheduledEvents", new[] { "DaypartId" });
            DropIndex("dbo.PlayScheduledEvents", new[] { "DisplayGroupId" });
            DropIndex("dbo.PlayScheduledEvents", new[] { "DisplayId" });
            DropIndex("dbo.PlayLayouts", new[] { "ResolutionId" });
            DropIndex("dbo.PlayLayoutContents", new[] { "LayoutId" });
            DropIndex("dbo.PlayDisplays", new[] { "LocationId" });
            DropIndex("dbo.PlayDisplays", new[] { "ResolutionId" });
            DropIndex("dbo.PlayDaypartExceptions", new[] { "DaypartId" });
            DropTable("dbo.PlayDisplayGroupDisplays");
            DropTable("dbo.PlayScheduledEvents",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ScheduledEvent_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ScheduledEvent_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.PlayLayouts",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Layout_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Layout_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.PlayLayoutContents",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LayoutContent_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_LayoutContent_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.PlayResolutions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Resolution_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Resolution_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.PlayDisplays",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Display_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Display_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.PlayDisplayGroups",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DisplayGroup_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DisplayGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.PlayDayparts",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Daypart_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Daypart_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.PlayDaypartExceptions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DaypartException_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DaypartException_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
