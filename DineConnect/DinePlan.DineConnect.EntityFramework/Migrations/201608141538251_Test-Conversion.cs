namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TestConversion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UnitConversions", "FlatConversion", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UnitConversions", "FlatConversion");
        }
    }
}
