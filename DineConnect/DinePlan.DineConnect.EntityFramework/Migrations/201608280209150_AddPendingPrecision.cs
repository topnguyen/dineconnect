namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPendingPrecision : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MenuItemPortions", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 6));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MenuItemPortions", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
