namespace DinePlan.DineConnect.Migrations
{
    using DinePlan.DineConnect.EntityFramework.Repositories;
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_DelAgr_Changes_Added_TenantId_ForeignKey : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DelAggChargeMappings", "DelAggChargeId", c => c.Int(nullable: false));
            AddColumn("dbo.DelAggLanguages", "Language", c => c.String());
            AddColumn("dbo.DelAggLanguages", "LanguageDescription", c => c.String());
            AddColumn("dbo.DelAggLanguages", "Key", c => c.String());
            CreateIndex("dbo.Adjustments", "TenantId");
            CreateIndex("dbo.PurchaseOrders", "TenantId");
            CreateIndex("dbo.DelAggChargeMappings", "DelAggChargeId");
            CreateIndex("dbo.InterTransfers", "TenantId");
            CreateIndex("dbo.InvoiceDetails", "TenantId");
            CreateIndex("dbo.Invoices", "TenantId");
            CreateIndex("dbo.InvoiceDirectCreditLink", "TenantId");
            CreateIndex("dbo.InvoiceTaxDetails", "TenantId");
            CreateIndex("dbo.PurchaseOrderDetails", "TenantId");
            CreateIndex("dbo.PurchaseOrderTaxDetails", "TenantId");
            AddForeignKey("dbo.Adjustments", "TenantId", "dbo.AbpTenants", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PurchaseOrders", "TenantId", "dbo.AbpTenants", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DelAggChargeMappings", "DelAggChargeId", "dbo.DelAggCharges", "Id", cascadeDelete: true);
            AddForeignKey("dbo.InterTransfers", "TenantId", "dbo.AbpTenants", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Invoices", "TenantId", "dbo.AbpTenants", "Id", cascadeDelete: true);
            AddForeignKey("dbo.InvoiceDetails", "TenantId", "dbo.AbpTenants", "Id", cascadeDelete: true);
            AddForeignKey("dbo.InvoiceDirectCreditLink", "TenantId", "dbo.AbpTenants", "Id", cascadeDelete: true);
            AddForeignKey("dbo.InvoiceTaxDetails", "TenantId", "dbo.AbpTenants", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PurchaseOrderDetails", "TenantId", "dbo.AbpTenants", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PurchaseOrderTaxDetails", "TenantId", "dbo.AbpTenants", "Id", cascadeDelete: true);
            this.DropColumnIfExists("dbo.DelAggLanguages", "Langauge");
            this.DropColumnIfExists("dbo.DelAggLanguages", "LangaugeDescription");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DelAggLanguages", "LangaugeDescription", c => c.String());
            AddColumn("dbo.DelAggLanguages", "Langauge", c => c.String());
            DropForeignKey("dbo.PurchaseOrderTaxDetails", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.PurchaseOrderDetails", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.InvoiceTaxDetails", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.InvoiceDirectCreditLink", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.InvoiceDetails", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.Invoices", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.InterTransfers", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.DelAggChargeMappings", "DelAggChargeId", "dbo.DelAggCharges");
            DropForeignKey("dbo.PurchaseOrders", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.Adjustments", "TenantId", "dbo.AbpTenants");
            DropIndex("dbo.PurchaseOrderTaxDetails", new[] { "TenantId" });
            DropIndex("dbo.PurchaseOrderDetails", new[] { "TenantId" });
            DropIndex("dbo.InvoiceTaxDetails", new[] { "TenantId" });
            DropIndex("dbo.InvoiceDirectCreditLink", new[] { "TenantId" });
            DropIndex("dbo.Invoices", new[] { "TenantId" });
            DropIndex("dbo.InvoiceDetails", new[] { "TenantId" });
            DropIndex("dbo.InterTransfers", new[] { "TenantId" });
            DropIndex("dbo.DelAggChargeMappings", new[] { "DelAggChargeId" });
            DropIndex("dbo.PurchaseOrders", new[] { "TenantId" });
            DropIndex("dbo.Adjustments", new[] { "TenantId" });
            DropColumn("dbo.DelAggLanguages", "Key");
            DropColumn("dbo.DelAggLanguages", "LanguageDescription");
            DropColumn("dbo.DelAggLanguages", "Language");
            DropColumn("dbo.DelAggChargeMappings", "DelAggChargeId");
        }
    }
}
