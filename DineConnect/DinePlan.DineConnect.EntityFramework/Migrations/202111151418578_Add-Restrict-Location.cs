namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRestrictLocation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Locations", "RestrictSync", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Locations", "RestrictSync");
        }
    }
}
