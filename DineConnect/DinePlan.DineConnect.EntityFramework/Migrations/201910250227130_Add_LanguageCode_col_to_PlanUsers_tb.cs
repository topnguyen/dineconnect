namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_LanguageCode_col_to_PlanUsers_tb : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DinePlanUsers", "LanguageCode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DinePlanUsers", "LanguageCode");
        }
    }
}
