namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeItemGroupTaxMapping : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DelAggTaxMappings", "DelAggLocationGroupId", "dbo.DelAggLocationGroups");
            DropForeignKey("dbo.DelAggTaxMappings", "DelItemGroupId", "dbo.DelAggItemGroups");
            DropIndex("dbo.DelAggTaxMappings", new[] { "DelAggLocationGroupId" });
            DropIndex("dbo.DelAggTaxMappings", new[] { "DelItemGroupId" });
            AlterColumn("dbo.DelAggTaxMappings", "DelAggLocationGroupId", c => c.Int());
            AlterColumn("dbo.DelAggTaxMappings", "DelItemGroupId", c => c.Int());
            CreateIndex("dbo.DelAggTaxMappings", "DelAggLocationGroupId");
            CreateIndex("dbo.DelAggTaxMappings", "DelItemGroupId");
            AddForeignKey("dbo.DelAggTaxMappings", "DelAggLocationGroupId", "dbo.DelAggLocationGroups", "Id");
            AddForeignKey("dbo.DelAggTaxMappings", "DelItemGroupId", "dbo.DelAggItemGroups", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DelAggTaxMappings", "DelItemGroupId", "dbo.DelAggItemGroups");
            DropForeignKey("dbo.DelAggTaxMappings", "DelAggLocationGroupId", "dbo.DelAggLocationGroups");
            DropIndex("dbo.DelAggTaxMappings", new[] { "DelItemGroupId" });
            DropIndex("dbo.DelAggTaxMappings", new[] { "DelAggLocationGroupId" });
            AlterColumn("dbo.DelAggTaxMappings", "DelItemGroupId", c => c.Int(nullable: false));
            AlterColumn("dbo.DelAggTaxMappings", "DelAggLocationGroupId", c => c.Int(nullable: false));
            CreateIndex("dbo.DelAggTaxMappings", "DelItemGroupId");
            CreateIndex("dbo.DelAggTaxMappings", "DelAggLocationGroupId");
            AddForeignKey("dbo.DelAggTaxMappings", "DelItemGroupId", "dbo.DelAggItemGroups", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DelAggTaxMappings", "DelAggLocationGroupId", "dbo.DelAggLocationGroups", "Id", cascadeDelete: true);
        }
    }
}
