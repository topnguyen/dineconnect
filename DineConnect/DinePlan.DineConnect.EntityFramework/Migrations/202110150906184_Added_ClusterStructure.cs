namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_ClusterStructure : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DelAggCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Code = c.String(),
                        Description = c.String(),
                        DelAggCatParentId = c.Int(nullable: false),
                        SortOrder = c.Int(nullable: false),
                        LocalRefCode = c.String(),
                        DelTimingGroupId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggCategory_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggCategory_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DelAggCategories", t => t.DelAggCatParentId)
                .ForeignKey("dbo.DelTimingGroups", t => t.DelTimingGroupId)
                .Index(t => t.DelAggCatParentId)
                .Index(t => t.DelTimingGroupId);
            
            CreateTable(
                "dbo.DelTimingGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelTimingGroup_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelTimingGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DelAggChargeMappings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DelAggLocationGroupId = c.Int(nullable: false),
                        DelItemGroupId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggChargeMapping_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggChargeMapping_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DelAggLocationGroups", t => t.DelAggLocationGroupId)
                .ForeignKey("dbo.DelAggItemGroups", t => t.DelItemGroupId)
                .Index(t => t.DelAggLocationGroupId)
                .Index(t => t.DelItemGroupId);
            
            CreateTable(
                "dbo.DelAggLocationGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggLocationGroup_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggLocationGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DelAggItemGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggItemGroup_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggItemGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DelAggCharges",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ApplicableOn = c.String(),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ExcludeDeliveryTypes = c.String(),
                        FullFilmentTypes = c.String(),
                        LocalRefCode = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggCharge_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggCharge_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DelAggImages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DelAggImageTypeRefId = c.Int(nullable: false),
                        DelAggTypeRefId = c.Int(),
                        ReferenceId = c.Int(),
                        ImagePath = c.String(),
                        AddOns = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggImage_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggImage_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DelAggItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Code = c.String(),
                        Description = c.String(),
                        FoodType = c.String(),
                        SortOrder = c.Int(nullable: false),
                        LocalRefCode = c.String(),
                        DelAggCatId = c.Int(nullable: false),
                        MenuItemId = c.Int(nullable: false),
                        IsRecommended = c.Boolean(nullable: false),
                        Weight = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Serves = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MenuItemPortionId = c.Int(nullable: false),
                        SalesPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        VariantExists = c.Boolean(nullable: false),
                        MarkupPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DelAggTaxId = c.Int(nullable: false),
                        DelAggItemGroupId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggItem_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggItem_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DelAggCategories", t => t.DelAggCatId)
                .ForeignKey("dbo.DelAggItemGroups", t => t.DelAggItemGroupId)
                .ForeignKey("dbo.DelAggTaxes", t => t.DelAggTaxId)
                .ForeignKey("dbo.MenuItems", t => t.MenuItemId)
                .ForeignKey("dbo.MenuItemPortions", t => t.MenuItemPortionId)
                .Index(t => t.DelAggCatId)
                .Index(t => t.MenuItemId)
                .Index(t => t.MenuItemPortionId)
                .Index(t => t.DelAggTaxId)
                .Index(t => t.DelAggItemGroupId);
            
            CreateTable(
                "dbo.DelAggTaxes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        TaxType = c.String(),
                        TaxPercentage = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LocalRefCode = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggTax_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggTax_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DelAggLanguages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DelAggTypeRefId = c.Int(nullable: false),
                        Langauge = c.String(),
                        LanguageValue = c.String(),
                        LangaugeDescription = c.String(),
                        ReferenceId = c.Int(),
                        AddOns = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggLanguage_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggLanguage_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DelAggLocationItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DelAggLocMappingId = c.Int(nullable: false),
                        DelAggItemId = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        InActive = c.Boolean(nullable: false),
                        AddOns = c.String(),
                        DelAggVariantId = c.Int(nullable: false),
                        DelAggModifierId = c.Int(nullable: false),
                        DelAggPriceTypeRefId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggLocationItem_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggLocationItem_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DelAggItems", t => t.DelAggItemId)
                .ForeignKey("dbo.DelAggLocMappings", t => t.DelAggLocMappingId)
                .ForeignKey("dbo.DelAggModifiers", t => t.DelAggModifierId)
                .ForeignKey("dbo.DelAggVariants", t => t.DelAggVariantId)
                .Index(t => t.DelAggLocMappingId)
                .Index(t => t.DelAggItemId)
                .Index(t => t.DelAggVariantId)
                .Index(t => t.DelAggModifierId);
            
            CreateTable(
                "dbo.DelAggLocMappings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DelAggLocationId = c.Int(nullable: false),
                        DelAggTypeRefId = c.Int(nullable: false),
                        GatewayCode = c.String(),
                        RemoteCode = c.String(),
                        DeliveryUrl = c.String(),
                        Active = c.Boolean(nullable: false),
                        AddOns = c.String(),
                        Name = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggLocMapping_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggLocMapping_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DelAggLocations", t => t.DelAggLocationId)
                .Index(t => t.DelAggLocationId);
            
            CreateTable(
                "dbo.DelAggLocations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationId = c.Int(nullable: false),
                        AddOns = c.String(),
                        DelAggLocationGroupId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggLocation_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggLocation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DelAggLocationGroups", t => t.DelAggLocationGroupId)
                .ForeignKey("dbo.Locations", t => t.LocationId)
                .Index(t => t.LocationId)
                .Index(t => t.DelAggLocationGroupId);
            
            CreateTable(
                "dbo.DelAggModifiers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Code = c.String(),
                        DelAggModifierGroupId = c.Int(),
                        OrderTagId = c.Int(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SortOrder = c.Int(nullable: false),
                        LocalRefCode = c.String(),
                        NestedModGroupId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggModifier_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggModifier_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DelAggModifierGroups", t => t.DelAggModifierGroupId)
                .ForeignKey("dbo.DelAggModifiers", t => t.NestedModGroupId)
                .ForeignKey("dbo.OrderTags", t => t.OrderTagId)
                .Index(t => t.DelAggModifierGroupId)
                .Index(t => t.OrderTagId)
                .Index(t => t.NestedModGroupId);
            
            CreateTable(
                "dbo.DelAggModifierGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SortOrder = c.Int(nullable: false),
                        LocalRefCode = c.String(),
                        Min = c.Int(),
                        Max = c.Int(),
                        OrderTagGroupId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggModifierGroup_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggModifierGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OrderTagGroups", t => t.OrderTagGroupId)
                .Index(t => t.OrderTagGroupId);
            
            CreateTable(
                "dbo.DelAggVariants",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Code = c.String(),
                        DelAggVariantGroupId = c.Int(nullable: false),
                        MenuItemPortionId = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SortOrder = c.Int(nullable: false),
                        LocalRefCode = c.String(),
                        NestedModGroupId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggVariant_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggVariant_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DelAggVariantGroups", t => t.DelAggVariantGroupId)
                .ForeignKey("dbo.DelAggVariants", t => t.NestedModGroupId)
                .ForeignKey("dbo.MenuItemPortions", t => t.MenuItemPortionId)
                .Index(t => t.DelAggVariantGroupId)
                .Index(t => t.MenuItemPortionId)
                .Index(t => t.NestedModGroupId);
            
            CreateTable(
                "dbo.DelAggVariantGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SortOrder = c.Int(nullable: false),
                        LocalRefCode = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggVariantGroup_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggVariantGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DelAggModiferGroupItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DelAggModiferGroupId = c.Int(nullable: false),
                        DelAggItemId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggModiferGroupItem_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggModiferGroupItem_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DelAggItems", t => t.DelAggItemId)
                .ForeignKey("dbo.DelAggModifierGroups", t => t.DelAggModiferGroupId)
                .Index(t => t.DelAggModiferGroupId)
                .Index(t => t.DelAggItemId);
            
            CreateTable(
                "dbo.DelAggTaxMappings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DelAggLocationGroupId = c.Int(nullable: false),
                        DelItemGroupId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggTaxMapping_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggTaxMapping_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DelAggLocationGroups", t => t.DelAggLocationGroupId)
                .ForeignKey("dbo.DelAggItemGroups", t => t.DelItemGroupId)
                .Index(t => t.DelAggLocationGroupId)
                .Index(t => t.DelItemGroupId);
            
            CreateTable(
                "dbo.DelAggVariantGroupItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DelAggVariantGroupId = c.Int(nullable: false),
                        DelAggItemId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggVariantGroupItem_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggVariantGroupItem_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DelAggItems", t => t.DelAggItemId)
                .ForeignKey("dbo.DelAggVariantGroups", t => t.DelAggVariantGroupId)
                .Index(t => t.DelAggVariantGroupId)
                .Index(t => t.DelAggItemId);
            
            CreateTable(
                "dbo.DelTimingDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DelTimingGroupId = c.Int(nullable: false),
                        StartTime = c.DateTime(nullable: false),
                        EndTime = c.DateTime(nullable: false),
                        Days = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelTimingDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelTimingDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DelTimingGroups", t => t.DelTimingGroupId)
                .Index(t => t.DelTimingGroupId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DelTimingDetails", "DelTimingGroupId", "dbo.DelTimingGroups");
            DropForeignKey("dbo.DelAggVariantGroupItems", "DelAggVariantGroupId", "dbo.DelAggVariantGroups");
            DropForeignKey("dbo.DelAggVariantGroupItems", "DelAggItemId", "dbo.DelAggItems");
            DropForeignKey("dbo.DelAggTaxMappings", "DelItemGroupId", "dbo.DelAggItemGroups");
            DropForeignKey("dbo.DelAggTaxMappings", "DelAggLocationGroupId", "dbo.DelAggLocationGroups");
            DropForeignKey("dbo.DelAggModiferGroupItems", "DelAggModiferGroupId", "dbo.DelAggModifierGroups");
            DropForeignKey("dbo.DelAggModiferGroupItems", "DelAggItemId", "dbo.DelAggItems");
            DropForeignKey("dbo.DelAggLocationItems", "DelAggVariantId", "dbo.DelAggVariants");
            DropForeignKey("dbo.DelAggVariants", "MenuItemPortionId", "dbo.MenuItemPortions");
            DropForeignKey("dbo.DelAggVariants", "NestedModGroupId", "dbo.DelAggVariants");
            DropForeignKey("dbo.DelAggVariants", "DelAggVariantGroupId", "dbo.DelAggVariantGroups");
            DropForeignKey("dbo.DelAggLocationItems", "DelAggModifierId", "dbo.DelAggModifiers");
            DropForeignKey("dbo.DelAggModifiers", "OrderTagId", "dbo.OrderTags");
            DropForeignKey("dbo.DelAggModifiers", "NestedModGroupId", "dbo.DelAggModifiers");
            DropForeignKey("dbo.DelAggModifiers", "DelAggModifierGroupId", "dbo.DelAggModifierGroups");
            DropForeignKey("dbo.DelAggModifierGroups", "OrderTagGroupId", "dbo.OrderTagGroups");
            DropForeignKey("dbo.DelAggLocationItems", "DelAggLocMappingId", "dbo.DelAggLocMappings");
            DropForeignKey("dbo.DelAggLocMappings", "DelAggLocationId", "dbo.DelAggLocations");
            DropForeignKey("dbo.DelAggLocations", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.DelAggLocations", "DelAggLocationGroupId", "dbo.DelAggLocationGroups");
            DropForeignKey("dbo.DelAggLocationItems", "DelAggItemId", "dbo.DelAggItems");
            DropForeignKey("dbo.DelAggItems", "MenuItemPortionId", "dbo.MenuItemPortions");
            DropForeignKey("dbo.DelAggItems", "MenuItemId", "dbo.MenuItems");
            DropForeignKey("dbo.DelAggItems", "DelAggTaxId", "dbo.DelAggTaxes");
            DropForeignKey("dbo.DelAggItems", "DelAggItemGroupId", "dbo.DelAggItemGroups");
            DropForeignKey("dbo.DelAggItems", "DelAggCatId", "dbo.DelAggCategories");
            DropForeignKey("dbo.DelAggChargeMappings", "DelItemGroupId", "dbo.DelAggItemGroups");
            DropForeignKey("dbo.DelAggChargeMappings", "DelAggLocationGroupId", "dbo.DelAggLocationGroups");
            DropForeignKey("dbo.DelAggCategories", "DelTimingGroupId", "dbo.DelTimingGroups");
            DropForeignKey("dbo.DelAggCategories", "DelAggCatParentId", "dbo.DelAggCategories");
            DropIndex("dbo.DelTimingDetails", new[] { "DelTimingGroupId" });
            DropIndex("dbo.DelAggVariantGroupItems", new[] { "DelAggItemId" });
            DropIndex("dbo.DelAggVariantGroupItems", new[] { "DelAggVariantGroupId" });
            DropIndex("dbo.DelAggTaxMappings", new[] { "DelItemGroupId" });
            DropIndex("dbo.DelAggTaxMappings", new[] { "DelAggLocationGroupId" });
            DropIndex("dbo.DelAggModiferGroupItems", new[] { "DelAggItemId" });
            DropIndex("dbo.DelAggModiferGroupItems", new[] { "DelAggModiferGroupId" });
            DropIndex("dbo.DelAggVariants", new[] { "NestedModGroupId" });
            DropIndex("dbo.DelAggVariants", new[] { "MenuItemPortionId" });
            DropIndex("dbo.DelAggVariants", new[] { "DelAggVariantGroupId" });
            DropIndex("dbo.DelAggModifierGroups", new[] { "OrderTagGroupId" });
            DropIndex("dbo.DelAggModifiers", new[] { "NestedModGroupId" });
            DropIndex("dbo.DelAggModifiers", new[] { "OrderTagId" });
            DropIndex("dbo.DelAggModifiers", new[] { "DelAggModifierGroupId" });
            DropIndex("dbo.DelAggLocations", new[] { "DelAggLocationGroupId" });
            DropIndex("dbo.DelAggLocations", new[] { "LocationId" });
            DropIndex("dbo.DelAggLocMappings", new[] { "DelAggLocationId" });
            DropIndex("dbo.DelAggLocationItems", new[] { "DelAggModifierId" });
            DropIndex("dbo.DelAggLocationItems", new[] { "DelAggVariantId" });
            DropIndex("dbo.DelAggLocationItems", new[] { "DelAggItemId" });
            DropIndex("dbo.DelAggLocationItems", new[] { "DelAggLocMappingId" });
            DropIndex("dbo.DelAggItems", new[] { "DelAggItemGroupId" });
            DropIndex("dbo.DelAggItems", new[] { "DelAggTaxId" });
            DropIndex("dbo.DelAggItems", new[] { "MenuItemPortionId" });
            DropIndex("dbo.DelAggItems", new[] { "MenuItemId" });
            DropIndex("dbo.DelAggItems", new[] { "DelAggCatId" });
            DropIndex("dbo.DelAggChargeMappings", new[] { "DelItemGroupId" });
            DropIndex("dbo.DelAggChargeMappings", new[] { "DelAggLocationGroupId" });
            DropIndex("dbo.DelAggCategories", new[] { "DelTimingGroupId" });
            DropIndex("dbo.DelAggCategories", new[] { "DelAggCatParentId" });
            DropTable("dbo.DelTimingDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelTimingDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelTimingDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DelAggVariantGroupItems",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggVariantGroupItem_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggVariantGroupItem_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DelAggTaxMappings",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggTaxMapping_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggTaxMapping_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DelAggModiferGroupItems",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggModiferGroupItem_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggModiferGroupItem_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DelAggVariantGroups",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggVariantGroup_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggVariantGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DelAggVariants",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggVariant_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggVariant_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DelAggModifierGroups",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggModifierGroup_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggModifierGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DelAggModifiers",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggModifier_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggModifier_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DelAggLocations",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggLocation_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggLocation_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DelAggLocMappings",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggLocMapping_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggLocMapping_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DelAggLocationItems",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggLocationItem_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggLocationItem_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DelAggLanguages",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggLanguage_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggLanguage_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DelAggTaxes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggTax_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggTax_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DelAggItems",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggItem_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggItem_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DelAggImages",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggImage_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggImage_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DelAggCharges",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggCharge_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggCharge_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DelAggItemGroups",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggItemGroup_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggItemGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DelAggLocationGroups",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggLocationGroup_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggLocationGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DelAggChargeMappings",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggChargeMapping_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggChargeMapping_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DelTimingGroups",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelTimingGroup_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelTimingGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.DelAggCategories",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DelAggCategory_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DelAggCategory_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
