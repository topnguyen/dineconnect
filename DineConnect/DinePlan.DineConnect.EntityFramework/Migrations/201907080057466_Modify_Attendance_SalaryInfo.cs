namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Attendance_SalaryInfo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EmployeeVsSalaryTags", "MinimumAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.EmployeeVsSalaryTags", "MaximumAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.OtBasedOnSlabs", "DoesApplicableBothAmount", c => c.Boolean(nullable: false));
            AddColumn("dbo.SalaryInfos", "IsIdleStandByCostExists", c => c.Boolean(nullable: false));
            AddColumn("dbo.SalaryPaidDetails", "MinimumAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SalaryPaidDetails", "MaximumAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));

            DropForeignKey("dbo.EmployeeAttendances", "EmployeeRefId", "dbo.EmployeeInfos");
            AddColumn("dbo.EmployeeAttendances", "VerifyMode", c => c.Int(nullable: false));
            AddColumn("dbo.EmployeeAttendances", "AuthorisedBy", c => c.Int());
            CreateIndex("dbo.EmployeeAttendances", "AuthorisedBy");
            AddForeignKey("dbo.EmployeeAttendances", "AuthorisedBy", "dbo.PersonalInformations", "Id");
            AddForeignKey("dbo.EmployeeAttendances", "EmployeeRefId", "dbo.PersonalInformations", "Id");
        }

        public override void Down()
        {
            DropForeignKey("dbo.EmployeeAttendances", "EmployeeRefId", "dbo.PersonalInformations");
            DropForeignKey("dbo.EmployeeAttendances", "AuthorisedBy", "dbo.PersonalInformations");
            DropIndex("dbo.EmployeeAttendances", new[] { "AuthorisedBy" });
            DropColumn("dbo.SalaryPaidDetails", "MaximumAmount");
            DropColumn("dbo.SalaryPaidDetails", "MinimumAmount");
            DropColumn("dbo.SalaryInfos", "HourlySalaryModeRefId");
            DropColumn("dbo.SalaryInfos", "IdleStandByCost");
            DropColumn("dbo.SalaryInfos", "IsIdleStandByCostExists");
            DropColumn("dbo.OtBasedOnSlabs", "DoesApplicableBothAmount");
            DropColumn("dbo.OtBasedOnSlabs", "FixedOTAmount");
            DropColumn("dbo.EmployeeVsSalaryTags", "MaximumAmount");
            DropColumn("dbo.EmployeeVsSalaryTags", "MinimumAmount");
            DropColumn("dbo.IncentiveTags", "IsGovernmentInsurancePayable");
            DropColumn("dbo.IncentiveTags", "IsPfPayable");
            DropColumn("dbo.IncentiveTags", "DoesIssueOnlyForBillableEmployee");
            DropColumn("dbo.EmployeeAttendances", "AuthorisedBy");
            DropColumn("dbo.EmployeeAttendances", "VerifyMode");
            DropColumn("dbo.PersonalInformations", "isBillable");
            RenameIndex(table: "dbo.PersonalInformations", name: "IX_ReligionRefId", newName: "IX_EmployeeReligionRefId");
            RenameColumn(table: "dbo.PersonalInformations", name: "ReligionRefId", newName: "EmployeeReligionRefId");
            AddForeignKey("dbo.EmployeeAttendances", "EmployeeRefId", "dbo.EmployeeInfos", "Id", cascadeDelete: true);
        }
    }
}
