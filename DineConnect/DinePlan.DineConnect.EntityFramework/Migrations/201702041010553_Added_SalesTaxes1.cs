namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_SalesTaxes1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SalesInvoiceDeliveryOrderLinks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SalesInvoiceRefId = c.Int(nullable: false),
                        Sno = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        DeliveryOrderRefId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalesInvoiceDeliveryOrderLink_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalesInvoiceDeliveryOrderLink_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId, cascadeDelete: true)
                .ForeignKey("dbo.SalesDeliveryOrders", t => t.DeliveryOrderRefId, cascadeDelete: true)
                .ForeignKey("dbo.SalesInvoices", t => t.SalesInvoiceRefId, cascadeDelete: true)
                .Index(t => t.SalesInvoiceRefId)
                .Index(t => t.MaterialRefId)
                .Index(t => t.DeliveryOrderRefId);
            
            CreateTable(
                "dbo.SalesTaxTemplateMappings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SalesTaxRefId = c.Int(nullable: false),
                        CompanyRefId = c.Int(),
                        LocationRefId = c.Int(),
                        MaterialGroupRefId = c.Int(),
                        MaterialGroupCategoryRefId = c.Int(),
                        MaterialRefId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalesTaxTemplateMapping_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalesTaxTemplateMapping_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyRefId)
                .ForeignKey("dbo.Locations", t => t.LocationRefId)
                .ForeignKey("dbo.MaterialGroupCategories", t => t.MaterialGroupCategoryRefId)
                .ForeignKey("dbo.MaterialGroups", t => t.MaterialGroupRefId)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .ForeignKey("dbo.SalesTaxes", t => t.SalesTaxRefId, cascadeDelete: true)
                .Index(t => t.SalesTaxRefId)
                .Index(t => t.CompanyRefId)
                .Index(t => t.LocationRefId)
                .Index(t => t.MaterialGroupRefId)
                .Index(t => t.MaterialGroupCategoryRefId)
                .Index(t => t.MaterialRefId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SalesTaxTemplateMappings", "SalesTaxRefId", "dbo.SalesTaxes");
            DropForeignKey("dbo.SalesTaxTemplateMappings", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.SalesTaxTemplateMappings", "MaterialGroupRefId", "dbo.MaterialGroups");
            DropForeignKey("dbo.SalesTaxTemplateMappings", "MaterialGroupCategoryRefId", "dbo.MaterialGroupCategories");
            DropForeignKey("dbo.SalesTaxTemplateMappings", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.SalesTaxTemplateMappings", "CompanyRefId", "dbo.Companies");
            DropForeignKey("dbo.SalesInvoiceDeliveryOrderLinks", "SalesInvoiceRefId", "dbo.SalesInvoices");
            DropForeignKey("dbo.SalesInvoiceDeliveryOrderLinks", "DeliveryOrderRefId", "dbo.SalesDeliveryOrders");
            DropForeignKey("dbo.SalesInvoiceDeliveryOrderLinks", "MaterialRefId", "dbo.Materials");
            DropIndex("dbo.SalesTaxTemplateMappings", new[] { "MaterialRefId" });
            DropIndex("dbo.SalesTaxTemplateMappings", new[] { "MaterialGroupCategoryRefId" });
            DropIndex("dbo.SalesTaxTemplateMappings", new[] { "MaterialGroupRefId" });
            DropIndex("dbo.SalesTaxTemplateMappings", new[] { "LocationRefId" });
            DropIndex("dbo.SalesTaxTemplateMappings", new[] { "CompanyRefId" });
            DropIndex("dbo.SalesTaxTemplateMappings", new[] { "SalesTaxRefId" });
            DropIndex("dbo.SalesInvoiceDeliveryOrderLinks", new[] { "DeliveryOrderRefId" });
            DropIndex("dbo.SalesInvoiceDeliveryOrderLinks", new[] { "MaterialRefId" });
            DropIndex("dbo.SalesInvoiceDeliveryOrderLinks", new[] { "SalesInvoiceRefId" });
            DropTable("dbo.SalesTaxTemplateMappings",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalesTaxTemplateMapping_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalesTaxTemplateMapping_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.SalesInvoiceDeliveryOrderLinks",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalesInvoiceDeliveryOrderLink_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalesInvoiceDeliveryOrderLink_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
