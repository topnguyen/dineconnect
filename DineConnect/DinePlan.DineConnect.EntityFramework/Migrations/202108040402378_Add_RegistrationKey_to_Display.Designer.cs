// <auto-generated />
namespace DinePlan.DineConnect.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Add_RegistrationKey_to_Display : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Add_RegistrationKey_to_Display));
        
        string IMigrationMetadata.Id
        {
            get { return "202108040402378_Add_RegistrationKey_to_Display"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
