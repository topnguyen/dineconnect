namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_StockCycle_InterTransfer_DeliveryDate : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.MaterialStockCycleLinks", "MPK_MaterialRefId_InvCycleRefId");
            AddColumn("dbo.InterTransfers", "DeliveryDateRequested", c => c.DateTime(nullable: false));
            AddColumn("dbo.MaterialStockCycleLinks", "LocationGroupRefId", c => c.Int());
            AddColumn("dbo.MaterialStockCycleLinks", "LocationRefId", c => c.Int());
            CreateIndex("dbo.MaterialStockCycleLinks", new[] { "MaterialRefId", "InventoryCycleTagRefId", "LocationRefId", "LocationGroupRefId" }, unique: true, name: "MPK_LocationGroupId_LocationRefId_MaterialRefId_InvCycleRefId");
            AddForeignKey("dbo.MaterialStockCycleLinks", "LocationGroupRefId", "dbo.LocationGroups", "Id");
            AddForeignKey("dbo.MaterialStockCycleLinks", "LocationRefId", "dbo.Locations", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MaterialStockCycleLinks", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.MaterialStockCycleLinks", "LocationGroupRefId", "dbo.LocationGroups");
            DropIndex("dbo.MaterialStockCycleLinks", "MPK_LocationGroupId_LocationRefId_MaterialRefId_InvCycleRefId");
            DropColumn("dbo.MaterialStockCycleLinks", "LocationRefId");
            DropColumn("dbo.MaterialStockCycleLinks", "LocationGroupRefId");
            DropColumn("dbo.InterTransfers", "DeliveryDateRequested");
            CreateIndex("dbo.MaterialStockCycleLinks", new[] { "MaterialRefId", "InventoryCycleTagRefId" }, unique: true, name: "MPK_MaterialRefId_InvCycleRefId");
        }
    }
}
