namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLocationChangeSetting : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Locations", "LocationTaxCode", c => c.String());
            AddColumn("dbo.Companies", "Website", c => c.String());
            AddColumn("dbo.Companies", "Email", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Companies", "Email");
            DropColumn("dbo.Companies", "Website");
            DropColumn("dbo.Locations", "LocationTaxCode");
        }
    }
}
