namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLocationMenu : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MenuItems", "Locations", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MenuItems", "Locations");
        }
    }
}
