namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class LastSyncs : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LastSyncs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        LastPush = c.DateTime(nullable: false),
                        LastPull = c.DateTime(nullable: false),
                        Version = c.String(),
                        SystemIp = c.String(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LastSync_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .Index(t => t.LocationId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LastSyncs", "LocationId", "dbo.Locations");
            DropIndex("dbo.LastSyncs", new[] { "LocationId" });
            DropTable("dbo.LastSyncs",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LastSync_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
