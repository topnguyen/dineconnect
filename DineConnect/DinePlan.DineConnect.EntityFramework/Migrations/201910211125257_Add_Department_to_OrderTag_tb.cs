namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Department_to_OrderTag_tb : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderTagGroups", "Departments", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.OrderTagGroups", "Departments");
        }
    }
}
