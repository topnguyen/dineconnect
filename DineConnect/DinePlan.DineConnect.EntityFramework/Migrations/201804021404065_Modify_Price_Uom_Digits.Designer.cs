// <auto-generated />
namespace DinePlan.DineConnect.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Modify_Price_Uom_Digits : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Modify_Price_Uom_Digits));
        
        string IMigrationMetadata.Id
        {
            get { return "201804021404065_Modify_Price_Uom_Digits"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
