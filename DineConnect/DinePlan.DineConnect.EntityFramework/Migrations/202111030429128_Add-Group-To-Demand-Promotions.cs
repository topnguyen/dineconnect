namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddGroupToDemandPromotions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DemandPromotions", "Group", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DemandPromotions", "Group");
        }
    }
}
