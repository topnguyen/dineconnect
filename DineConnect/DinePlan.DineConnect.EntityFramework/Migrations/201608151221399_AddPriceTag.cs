namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddPriceTag : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PriceTagDefinitions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TagId = c.Int(nullable: false),
                        MenuPortionId = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MenuItemPortions", t => t.MenuPortionId, cascadeDelete: true)
                .ForeignKey("dbo.PriceTags", t => t.TagId, cascadeDelete: true)
                .Index(t => t.TagId)
                .Index(t => t.MenuPortionId);
            
            CreateTable(
                "dbo.PriceTags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        Name = c.String(),
                        Location = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PriceTag_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PriceTag_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PriceTagDefinitions", "TagId", "dbo.PriceTags");
            DropForeignKey("dbo.PriceTagDefinitions", "MenuPortionId", "dbo.MenuItemPortions");
            DropIndex("dbo.PriceTagDefinitions", new[] { "MenuPortionId" });
            DropIndex("dbo.PriceTagDefinitions", new[] { "TagId" });
            DropTable("dbo.PriceTags",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PriceTag_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PriceTag_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.PriceTagDefinitions");
        }
    }
}
