namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_AliasName_Portion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MenuItemPortions", "AliasName", c => c.String(maxLength:100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MenuItemPortions", "AliasName");
        }
    }
}
