namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_Material_Barcode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Materials", "Barcode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Materials", "Barcode");
        }
    }
}
