namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLocationGroupToAllEntities : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GiftVoucherTypes", "Group", c => c.Boolean(nullable: false));
            AddColumn("dbo.MenuItems", "Group", c => c.Boolean(nullable: false));
            AddColumn("dbo.PriceTags", "Group", c => c.Boolean(nullable: false));
            AddColumn("dbo.ConnectTableGroups", "Group", c => c.Boolean(nullable: false));
            AddColumn("dbo.Promotions", "Group", c => c.Boolean(nullable: false));
            AddColumn("dbo.Departments", "Group", c => c.Boolean(nullable: false));
            AddColumn("dbo.DinePlanUsers", "Group", c => c.Boolean(nullable: false));
            AddColumn("dbo.OrderTagGroups", "Group", c => c.Boolean(nullable: false));
            AddColumn("dbo.ScreenMenus", "Group", c => c.Boolean(nullable: false));
            AddColumn("dbo.TicketTagGroups", "Group", c => c.Boolean(nullable: false));
            AddColumn("dbo.TillAccounts", "Group", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TillAccounts", "Group");
            DropColumn("dbo.TicketTagGroups", "Group");
            DropColumn("dbo.ScreenMenus", "Group");
            DropColumn("dbo.OrderTagGroups", "Group");
            DropColumn("dbo.DinePlanUsers", "Group");
            DropColumn("dbo.Departments", "Group");
            DropColumn("dbo.Promotions", "Group");
            DropColumn("dbo.ConnectTableGroups", "Group");
            DropColumn("dbo.PriceTags", "Group");
            DropColumn("dbo.MenuItems", "Group");
            DropColumn("dbo.GiftVoucherTypes", "Group");
        }
    }
}
