namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPromotionDescription : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Promotions", "Description", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Promotions", "Description");
        }
    }
}
