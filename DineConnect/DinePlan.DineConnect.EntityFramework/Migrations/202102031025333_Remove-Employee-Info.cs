namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveEmployeeInfo : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.EmployeeInfos", "DinePlanUserId", "dbo.DinePlanUsers");
            DropForeignKey("dbo.EmployeeInfos", "EmployeeDepartmentId", "dbo.EmployeeDepartments");
            DropForeignKey("dbo.EmployeeInfos", "RawId", "dbo.EmployeeRawInfoes");
            DropForeignKey("dbo.EmployeeInfos", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.MemberCards", "EmployeeRefId", "dbo.EmployeeInfos");
            DropForeignKey("dbo.SwipeCardLedgers", "EmployeeRefId", "dbo.EmployeeInfos");
            DropIndex("dbo.EmployeeInfos", new[] { "UserId" });
            DropIndex("dbo.EmployeeInfos", new[] { "DinePlanUserId" });
            DropIndex("dbo.EmployeeInfos", new[] { "EmployeeDepartmentId" });
            DropIndex("dbo.EmployeeInfos", new[] { "RawId" });
            AddForeignKey("dbo.MemberCards", "EmployeeRefId", "dbo.PersonalInformations", "Id");
            AddForeignKey("dbo.SwipeCardLedgers", "EmployeeRefId", "dbo.PersonalInformations", "Id");
            DropTable("dbo.EmployeeInfos",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeInfo_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeInfo_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.EmployeeInfos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeCode = c.String(),
                        EmployeeName = c.String(),
                        Email = c.String(),
                        HomePhone = c.String(),
                        MobilePhone = c.String(),
                        HireDate = c.DateTime(nullable: false),
                        WageType = c.String(),
                        Wage = c.Int(nullable: false),
                        SkillLevel = c.String(),
                        MaxWeeklyHours = c.Int(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                        PunchId = c.Int(nullable: false),
                        Notes = c.String(),
                        TenantId = c.Int(nullable: false),
                        UserId = c.Long(),
                        DinePlanUserId = c.Int(),
                        EmployeeDepartmentId = c.Int(),
                        IsActive = c.Boolean(nullable: false),
                        RawId = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeInfo_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeInfo_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.SwipeCardLedgers", "EmployeeRefId", "dbo.PersonalInformations");
            DropForeignKey("dbo.MemberCards", "EmployeeRefId", "dbo.PersonalInformations");
            CreateIndex("dbo.EmployeeInfos", "RawId");
            CreateIndex("dbo.EmployeeInfos", "EmployeeDepartmentId");
            CreateIndex("dbo.EmployeeInfos", "DinePlanUserId");
            CreateIndex("dbo.EmployeeInfos", "UserId");
            AddForeignKey("dbo.SwipeCardLedgers", "EmployeeRefId", "dbo.EmployeeInfos", "Id");
            AddForeignKey("dbo.MemberCards", "EmployeeRefId", "dbo.EmployeeInfos", "Id");
            AddForeignKey("dbo.EmployeeInfos", "UserId", "dbo.AbpUsers", "Id");
            AddForeignKey("dbo.EmployeeInfos", "RawId", "dbo.EmployeeRawInfoes", "Id");
            AddForeignKey("dbo.EmployeeInfos", "EmployeeDepartmentId", "dbo.EmployeeDepartments", "Id");
            AddForeignKey("dbo.EmployeeInfos", "DinePlanUserId", "dbo.DinePlanUsers", "Id");
        }
    }
}
