namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ConnectCard_AddPrinted : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ConnectCards", "Printed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ConnectCards", "Printed");
        }
    }
}
