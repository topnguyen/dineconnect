namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTransactionTypeTax : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TransactionTypes", "Discount", c => c.Boolean(nullable: false));
            AddColumn("dbo.Tickets", "QueueNumber", c => c.String(maxLength: 20));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tickets", "QueueNumber");
            DropColumn("dbo.TransactionTypes", "Discount");
        }
    }
}
