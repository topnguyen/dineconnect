namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeDepartmentCodeConnect : DbMigration
    {
        public override void Up()
        {
            //AddColumn("dbo.Departments", "Code", c => c.String(maxLength: 100));
            AlterColumn("dbo.DepartmentGroups", "Code", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DepartmentGroups", "Code", c => c.String());
            //DropColumn("dbo.Departments", "Code");
        }
    }
}
