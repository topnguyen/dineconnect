namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeCaterCustomerAddress : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CaterCustomerAddressDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CaterCustomerId = c.Int(nullable: false),
                        Address1 = c.String(maxLength: 50),
                        Address2 = c.String(maxLength: 50),
                        Address3 = c.String(maxLength: 50),
                        City = c.String(maxLength: 50),
                        State = c.String(maxLength: 50),
                        Country = c.String(maxLength: 50),
                        ZipCode = c.String(maxLength: 50),
                        PhoneNumber = c.String(maxLength: 50),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CaterCustomers", t => t.CaterCustomerId, cascadeDelete: true)
                .Index(t => t.CaterCustomerId);
            
            DropColumn("dbo.CaterCustomers", "Address1");
            DropColumn("dbo.CaterCustomers", "Address2");
            DropColumn("dbo.CaterCustomers", "Address3");
            DropColumn("dbo.CaterCustomers", "City");
            DropColumn("dbo.CaterCustomers", "State");
            DropColumn("dbo.CaterCustomers", "Country");
            DropColumn("dbo.CaterCustomers", "ZipCode");
            DropColumn("dbo.CaterCustomers", "OrderChannel");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CaterCustomers", "OrderChannel", c => c.Int(nullable: false));
            AddColumn("dbo.CaterCustomers", "ZipCode", c => c.String(maxLength: 50));
            AddColumn("dbo.CaterCustomers", "Country", c => c.String(maxLength: 50));
            AddColumn("dbo.CaterCustomers", "State", c => c.String(maxLength: 50));
            AddColumn("dbo.CaterCustomers", "City", c => c.String(maxLength: 50));
            AddColumn("dbo.CaterCustomers", "Address3", c => c.String(maxLength: 50));
            AddColumn("dbo.CaterCustomers", "Address2", c => c.String(maxLength: 50));
            AddColumn("dbo.CaterCustomers", "Address1", c => c.String(maxLength: 50));
            DropForeignKey("dbo.CaterCustomerAddressDetails", "CaterCustomerId", "dbo.CaterCustomers");
            DropIndex("dbo.CaterCustomerAddressDetails", new[] { "CaterCustomerId" });
            DropTable("dbo.CaterCustomerAddressDetails");
        }
    }
}
