namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyDelAggItem_MenuItemPortionId : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DelAggItems", "MenuItemPortionId", "dbo.MenuItemPortions");
            DropIndex("dbo.DelAggItems", new[] { "MenuItemPortionId" });
            DropColumn("dbo.DelAggItems", "MenuItemPortionId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DelAggItems", "MenuItemPortionId", c => c.Int(nullable: false));
            CreateIndex("dbo.DelAggItems", "MenuItemPortionId");
            AddForeignKey("dbo.DelAggItems", "MenuItemPortionId", "dbo.MenuItemPortions", "Id", cascadeDelete: true);
        }
    }
}
