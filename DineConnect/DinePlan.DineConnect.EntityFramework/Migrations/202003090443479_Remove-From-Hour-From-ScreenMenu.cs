namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveFromHourFromScreenMenu : DbMigration
    {
        public override void Up()
        {
            Sql("Update dbo.ScreenMenuCategories set ToHour=0 ");
            AlterColumn("dbo.ScreenMenuCategories", "ToHour", c => c.Int(nullable: false, defaultValueSql: "0"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ScreenMenuCategories", "ToHour", c => c.String());
        }
    }
}
