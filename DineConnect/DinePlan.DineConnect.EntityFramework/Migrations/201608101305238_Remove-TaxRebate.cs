namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveTaxRebate : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Taxes", "TaxClassification");
            DropColumn("dbo.Taxes", "RebateIfAny");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Taxes", "RebateIfAny", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Taxes", "TaxClassification", c => c.String(nullable: false, maxLength: 2));
        }
    }
}
