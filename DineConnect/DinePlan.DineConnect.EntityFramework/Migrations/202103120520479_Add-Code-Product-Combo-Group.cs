namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCodeProductComboGroup : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductComboGroups", "Code", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductComboGroups", "Code");
        }
    }
}
