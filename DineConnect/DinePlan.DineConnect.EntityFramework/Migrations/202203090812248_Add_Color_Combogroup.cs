namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Color_Combogroup : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductComboGroups", "Color", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductComboGroups", "Color");
        }
    }
}
