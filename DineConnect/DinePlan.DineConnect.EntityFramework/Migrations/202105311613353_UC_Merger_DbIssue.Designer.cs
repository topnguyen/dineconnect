// <auto-generated />
namespace DinePlan.DineConnect.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class UC_Merger_DbIssue : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(UC_Merger_DbIssue));
        
        string IMigrationMetadata.Id
        {
            get { return "202105311613353_UC_Merger_DbIssue"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
