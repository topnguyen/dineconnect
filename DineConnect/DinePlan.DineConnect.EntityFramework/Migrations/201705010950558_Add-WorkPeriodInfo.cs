namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddWorkPeriodInfo : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.WorkPeriods", new[] { "LocationId" });
            AddColumn("dbo.WorkPeriods", "Wid", c => c.Int(nullable: false));
            AddColumn("dbo.WorkPeriods", "TotalSales", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.WorkPeriods", "TotalTicketCount", c => c.Int(nullable: false));
            CreateIndex("dbo.WorkPeriods", new[] { "LocationId", "Wid" }, unique: true, name: "IX_WorkPeriodFirstAndSecond");
        }
        
        public override void Down()
        {
            DropIndex("dbo.WorkPeriods", "IX_WorkPeriodFirstAndSecond");
            DropColumn("dbo.WorkPeriods", "TotalTicketCount");
            DropColumn("dbo.WorkPeriods", "TotalSales");
            DropColumn("dbo.WorkPeriods", "Wid");
            CreateIndex("dbo.WorkPeriods", "LocationId");
        }
    }
}
