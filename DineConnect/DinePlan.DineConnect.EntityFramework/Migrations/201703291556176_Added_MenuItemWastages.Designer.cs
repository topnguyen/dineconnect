// <auto-generated />
namespace DinePlan.DineConnect.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Added_MenuItemWastages : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Added_MenuItemWastages));
        
        string IMigrationMetadata.Id
        {
            get { return "201703291556176_Added_MenuItemWastages"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
