namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class WorkPeriodInformation : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.WorkPeriodInformations", "PaymentTypeId", "dbo.PaymentTypes");
            DropForeignKey("dbo.WorkPeriodInformations", "WorkPeriodId", "dbo.WorkPeriods");
            DropIndex("dbo.WorkPeriodInformations", new[] { "WorkPeriodId" });
            DropIndex("dbo.WorkPeriodInformations", new[] { "PaymentTypeId" });
            AddColumn("dbo.WorkPeriods", "StartUser", c => c.String(maxLength: 50));
            AddColumn("dbo.WorkPeriods", "EndUser", c => c.String(maxLength: 50));
            AddColumn("dbo.WorkPeriods", "WorkPeriodInformations", c => c.String());
            DropColumn("dbo.MenuItems", "Location");
            DropColumn("dbo.WorkPeriods", "UserName");
            DropColumn("dbo.WorkPeriods", "Float");
            DropTable("dbo.WorkPeriodInformations");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.WorkPeriodInformations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        WorkPeriodId = c.Int(nullable: false),
                        PaymentTypeId = c.Int(nullable: false),
                        Actual = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Entered = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.WorkPeriods", "Float", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.WorkPeriods", "UserName", c => c.String());
            AddColumn("dbo.MenuItems", "Location", c => c.String(maxLength: 50));
            DropColumn("dbo.WorkPeriods", "WorkPeriodInformations");
            DropColumn("dbo.WorkPeriods", "EndUser");
            DropColumn("dbo.WorkPeriods", "StartUser");
            CreateIndex("dbo.WorkPeriodInformations", "PaymentTypeId");
            CreateIndex("dbo.WorkPeriodInformations", "WorkPeriodId");
            AddForeignKey("dbo.WorkPeriodInformations", "WorkPeriodId", "dbo.WorkPeriods", "Id", cascadeDelete: true);
            AddForeignKey("dbo.WorkPeriodInformations", "PaymentTypeId", "dbo.PaymentTypes", "Id", cascadeDelete: true);
        }
    }
}
