using System.Collections.Generic;
using System.Data.Entity.Migrations;

namespace DinePlan.DineConnect.Migrations
{
    public partial class ConnectEntities : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ConnectEntities",
                c => new
                {
                    Id = c.Int(false, true),
                    Name = c.String(),
                    EntityTypeId = c.Int(false),
                    CustomData = c.String(),
                    CreationTime = c.DateTime(false),
                    CreatorUserId = c.Long()
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EntityTypes", t => t.EntityTypeId, true)
                .Index(t => t.EntityTypeId);

            CreateTable(
                "dbo.EntityTypes",
                c => new
                {
                    Id = c.Int(false, true),
                    Name = c.String(),
                    EntityName = c.String(),
                    PrimaryFieldName = c.String(),
                    PrimaryFieldFormat = c.String(),
                    DisplayFormat = c.String(),
                    TenantId = c.Int(false),
                    IsDeleted = c.Boolean(false),
                    DeleterUserId = c.Long(),
                    DeletionTime = c.DateTime(),
                    LastModificationTime = c.DateTime(),
                    LastModifierUserId = c.Long(),
                    CreationTime = c.DateTime(false),
                    CreatorUserId = c.Long()
                }, new Dictionary<string, object>
                {
                    {
                        "DynamicFilter_EntityType_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition"
                    },
                    {"DynamicFilter_EntityType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition"}
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.EntityCustomFields",
                c => new
                {
                    Id = c.Int(false, true),
                    Name = c.String(),
                    FieldType = c.Int(false),
                    EditingFormat = c.String(),
                    ValueSource = c.String(),
                    Hidden = c.Boolean(false),
                    EntityTypeId = c.Int(false),
                    CreationTime = c.DateTime(false),
                    CreatorUserId = c.Long()
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EntityTypes", t => t.EntityTypeId, true)
                .Index(t => t.EntityTypeId);
        }

        public override void Down()
        {
            DropForeignKey("dbo.ConnectEntities", "EntityTypeId", "dbo.EntityTypes");
            DropForeignKey("dbo.EntityCustomFields", "EntityTypeId", "dbo.EntityTypes");
            DropIndex("dbo.EntityCustomFields", new[] {"EntityTypeId"});
            DropIndex("dbo.ConnectEntities", new[] {"EntityTypeId"});
            DropTable("dbo.EntityCustomFields");
            DropTable("dbo.EntityTypes", new Dictionary<string, object>
            {
                {"DynamicFilter_EntityType_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition"},
                {"DynamicFilter_EntityType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition"}
            });
            DropTable("dbo.ConnectEntities");
        }
    }
}