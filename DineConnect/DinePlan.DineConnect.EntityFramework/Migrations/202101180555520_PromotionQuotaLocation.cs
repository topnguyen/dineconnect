namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PromotionQuotaLocation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PromotionQuotas", "LocationQuota", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PromotionQuotas", "LocationQuota");
        }
    }
}
