using System;
using System.Diagnostics;
using System.Linq;
using DinePlan.DineConnect.Connect.Entity;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Editions;
using DinePlan.DineConnect.EntityFramework;
using DinePlan.DineConnect.MultiTenancy;
using DinePlan.DineConnect.Template;

namespace DinePlan.DineConnect.Migrations.Seed
{
    public class DefaultDineConnectCreator
    {
        private readonly DineConnectDbContext _context;

        public DefaultDineConnectCreator(DineConnectDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            var debugStarted = Debugger.IsAttached;
            if (!debugStarted)
                return;

            var defaultTenant = _context.Tenants.FirstOrDefault(t => t.TenancyName.Equals("Connect"));
            if (defaultTenant == null)
            {
                defaultTenant = new Tenant("Connect", "Connect");
                var defaultEdition = _context.Editions.FirstOrDefault(e => e.Name == EditionManager.ConnectEdition);
                if (defaultEdition != null)
                {
                    defaultTenant.EditionId = defaultEdition.Id;
                }

                defaultTenant = _context.Tenants.Add(defaultTenant);
                _context.SaveChanges();
            }
            CreatePaymentTypes(defaultTenant.Id);
            CreateTransactionTypes(defaultTenant.Id);
            CreateEntityTypes(defaultTenant.Id);
            CreateSyncCreator(defaultTenant.Id);
            CreateEmailTemplates(defaultTenant.Id);
            var tenants = _context.Tenants.ToList();
            foreach (var tenant in tenants)
            {
                CreateEmailRandomPasswordTemplates(tenant.Id);
            }
            
        }

        private void CreateEmailTemplates(int id)
        {
            var syncC = _context.EmailTemplates.FirstOrDefault(e => e.Name.Equals("MEMBER VOUCHER"));
            if (syncC == null)
            {
                syncC = new EmailTemplate { Name = "MEMBER VOUCHER", TenantId = id,Template = GetMemberTemplate(),Variables=GetMemberVariable() };
                _context.EmailTemplates.Add(syncC);
            }
            _context.SaveChanges();

        }

        private void CreateEmailRandomPasswordTemplates(int id)
        {
            var syncC = _context.EmailTemplates.FirstOrDefault(e => e.Name.Equals("RANDOM PASSWORD") && e.TenantId == id);
            if (syncC == null)
            {
                syncC = new EmailTemplate { Name = "RANDOM PASSWORD", TenantId = id, Template = GetRandomPasswordTemplate(), Variables = GetRandomPasswordVariable() };
                _context.EmailTemplates.Add(syncC);
            }
            _context.SaveChanges();

        }

        private string GetMemberVariable()
        {
            return "{MEMBER NAME}, {MEMBER CODE}, {VOUCHER NO}";
        }

        private string GetMemberTemplate()
        {
            return "<h2 style='text-align: center;'>DinePlan</h2><p style='text-align: center; '>Thank you so much for Being our Customer</p><p style='text-align: center; '><b>Here is the Happy voucher for you to Claim</b></p><h1 style='text-align: center; '>{VOUCHER NO}</h1><p style='text-align: center;'><br></p><p><br></p><hr><p style='text-align: center; '>� DinePlan - 2017</p>";
        }

        private string GetRandomPasswordTemplate()
        {
            return "<h3>Dear {USERNAME} ,</h3><br/><p>Your new password is <b >{PASSWORD}</b> </p><br/><p>Best regards,</p><p>{SENDER}</p>";
        }

        private string GetRandomPasswordVariable()
        {
            return "{USERNAME}, {NEWPASSWORD}, {SENDER}";
        }

        private void CreateSyncCreator(int id)
        {
            var syncC = _context.Syncers.FirstOrDefault(e => e.Name == SyncConsts.USER);
            if (syncC == null){
                syncC = new Syncer { Name = SyncConsts.USER, TenantId = id, SyncerGuid = Guid.NewGuid()};
                _context.Syncers.Add(syncC);
            }
            syncC = _context.Syncers.FirstOrDefault(e => e.Name == SyncConsts.TAX);
            if (syncC == null)
            {
                syncC = new Syncer { Name = SyncConsts.TAX, TenantId = id, SyncerGuid = Guid.NewGuid() };
                _context.Syncers.Add(syncC);
            }
            syncC = _context.Syncers.FirstOrDefault(e => e.Name == SyncConsts.MENU);
            if (syncC == null)
            {
                syncC = new Syncer { Name = SyncConsts.MENU, TenantId = id, SyncerGuid = Guid.NewGuid() };
                _context.Syncers.Add(syncC);
            }
            _context.SaveChanges();
        }

        private void CreateEntityTypes(int id)
        {
            var defaultEntityType = _context.EntityTypes.FirstOrDefault(e => e.Name == "Members");
            if (defaultEntityType == null)
            {
                defaultEntityType = new EntityType { Name = "Members", EntityName = "Member", PrimaryFieldName = "Phone", DisplayFormat = "[Member Name]-[Phone]", TenantId = id};
                _context.EntityTypes.Add(defaultEntityType);
            }
            _context.SaveChanges();

            defaultEntityType = _context.EntityTypes.FirstOrDefault(e => e.Name == "Members");
            if (defaultEntityType != null)
            {
                var cf = _context.EntityCustomFields.FirstOrDefault(e => e.Name == "Name");
                if (cf == null)
                {
                    cf = new EntityCustomField() { Name = "Name", FieldType = 0, EntityTypeId = defaultEntityType.Id };
                    _context.EntityCustomFields.Add(cf);
                }

                cf = _context.EntityCustomFields.FirstOrDefault(e => e.Name == "Address");
                if (cf == null)
                {
                    cf = new EntityCustomField() { Name = "Address", FieldType = 1, EntityTypeId = defaultEntityType.Id };
                    _context.EntityCustomFields.Add(cf);
                }
            }
            _context.SaveChanges();

        }


        private void CreateTransactionTypes(int id)
        {
            var defaultEdition = _context.TransactionTypes.FirstOrDefault(e => e.Name == "PAYMENT");
            if (defaultEdition == null)
            {
                defaultEdition = new TransactionType { Name = "PAYMENT", TenantId = id };
                _context.TransactionTypes.Add(defaultEdition);
            }

            defaultEdition = _context.TransactionTypes.FirstOrDefault(e => e.Name == "SALE");
            if (defaultEdition == null)
            {
                defaultEdition = new TransactionType { Name = "SALE", TenantId = id };
                _context.TransactionTypes.Add(defaultEdition);
            }

            defaultEdition = _context.TransactionTypes.FirstOrDefault(e => e.Name == "SERVICE CHARGE");
            if (defaultEdition == null)
            {
                defaultEdition = new TransactionType { Name = "SERVICE CHARGE", TenantId = id };
                _context.TransactionTypes.Add(defaultEdition);
            }
            defaultEdition = _context.TransactionTypes.FirstOrDefault(e => e.Name == "DISCOUNT");
            if (defaultEdition == null)
            {
                defaultEdition = new TransactionType { Name = "DISCOUNT", TenantId = id };
                _context.TransactionTypes.Add(defaultEdition);
            }
            defaultEdition = _context.TransactionTypes.FirstOrDefault(e => e.Name == "ROUNDING");
            if (defaultEdition == null)
            {
                defaultEdition = new TransactionType { Name = "ROUNDING", TenantId = id };
                _context.TransactionTypes.Add(defaultEdition);
            }
            defaultEdition = _context.TransactionTypes.FirstOrDefault(e => e.Name == "ROUND+");
            if (defaultEdition == null)
            {
                defaultEdition = new TransactionType { Name = "ROUND+", TenantId = id };
                _context.TransactionTypes.Add(defaultEdition);
            }
            defaultEdition = _context.TransactionTypes.FirstOrDefault(e => e.Name == "ROUND-");
            if (defaultEdition == null)
            {
                defaultEdition = new TransactionType { Name = "ROUND-", TenantId = id };
                _context.TransactionTypes.Add(defaultEdition);
            }
            defaultEdition = _context.TransactionTypes.FirstOrDefault(e => e.Name == "TAX");
            if (defaultEdition == null)
            {
                defaultEdition = new TransactionType { Name = "TAX", TenantId = id };
                _context.TransactionTypes.Add(defaultEdition);
            }
            _context.SaveChanges();
        }

        private void CreatePaymentTypes(int id)
        {
            var defaultPaymentType = _context.PaymentTypes.FirstOrDefault(e => e.Name == "CASH");
            if (defaultPaymentType == null)
            {
                defaultPaymentType = new PaymentType {Name = "CASH", TenantId = id };
                _context.PaymentTypes.Add(defaultPaymentType);
            }

            defaultPaymentType = _context.PaymentTypes.FirstOrDefault(e => e.Name == "VISA");
            if (defaultPaymentType == null)
            {
                defaultPaymentType = new PaymentType {Name = "VISA", TenantId = id };
                _context.PaymentTypes.Add(defaultPaymentType);
            }

            defaultPaymentType = _context.PaymentTypes.FirstOrDefault(e => e.Name == "MASTER");
            if (defaultPaymentType == null)
            {
                defaultPaymentType = new PaymentType {Name = "MASTER", TenantId = id };
                _context.PaymentTypes.Add(defaultPaymentType);
            }
           
            defaultPaymentType = _context.PaymentTypes.FirstOrDefault(e => e.Name == "VOUCHER");
            if (defaultPaymentType == null)
            {
                defaultPaymentType = new PaymentType { Name = "VOUCHER", TenantId = id };
                _context.PaymentTypes.Add(defaultPaymentType);
            }
            defaultPaymentType = _context.PaymentTypes.FirstOrDefault(e => e.Name == "MEMBER");
            if (defaultPaymentType == null)
            {
                defaultPaymentType = new PaymentType { Name = "MEMBER", TenantId = id };
                _context.PaymentTypes.Add(defaultPaymentType);
            }

            defaultPaymentType = _context.PaymentTypes.FirstOrDefault(e => e.Name == "CARD");
            if (defaultPaymentType == null)
            {
                defaultPaymentType = new PaymentType { Name = "CARD", TenantId = id };
                _context.PaymentTypes.Add(defaultPaymentType);
            }
            _context.SaveChanges();
        }
    }
}