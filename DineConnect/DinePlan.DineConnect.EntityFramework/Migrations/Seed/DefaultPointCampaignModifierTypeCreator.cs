﻿
using DinePlan.DineConnect.Engage;
using DinePlan.DineConnect.EntityFramework;
using System.Collections.Generic;
using System.Linq;

namespace DinePlan.DineConnect.Migrations.Seed
{
    public class DefaultPointCampaignModifierTypeCreator
    {
        private readonly DineConnectDbContext _context;

        public static List<PointCampaignModifierType> Items { get; set; }
        static DefaultPointCampaignModifierTypeCreator()
        {
            Items = new List<PointCampaignModifierType> {
                new PointCampaignModifierType{ Code="BDMTH", Name="Month of customer's birthday"},
                new PointCampaignModifierType{ Code="TMPRD", Name="Time period"},
                new PointCampaignModifierType{ Code="MTIER", Name="Membership tier"},
            };
        }

        public DefaultPointCampaignModifierTypeCreator(DineConnectDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            InsertItemsToDb();
        }

        private void InsertItemsToDb()
        {
            foreach (var item in Items)
            {
                AddIfNotExists(item);
            }
        }

        private void AddIfNotExists(PointCampaignModifierType entityObject)
        {
            if (_context.PointCampaignModifierType.Any(u =>
                u.Code == entityObject.Code))
            {
                return;
            }
            _context.PointCampaignModifierType.Add(entityObject);
            _context.SaveChanges();
        }
    }
}

