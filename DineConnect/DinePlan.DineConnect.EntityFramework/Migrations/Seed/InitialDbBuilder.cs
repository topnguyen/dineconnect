﻿using EntityFramework.DynamicFilters;
using DinePlan.DineConnect.EntityFramework;
using Abp.Domain.Uow;

namespace DinePlan.DineConnect.Migrations.Seed
{
    public class InitialDbBuilder
    {
        private readonly DineConnectDbContext _context;
     
        public InitialDbBuilder(DineConnectDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            _context.DisableAllFilters();

            new DefaultEditionCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new DefaultTenantRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();
            new DefaultDineConnectCreator(_context).Create();
            new DefaultUnitCreators(_context).Create();
            new DefaultReportKeyCreator(_context).Create();
            new DefaultCustomerTagDefinition(_context).Create();
            new DefaultWorkDayCreators(_context).Create();
            new DefaultSkillSetCreators(_context).Create();
            new DefaultEmployeeCostCentreCreators(_context).CreateEmployeeCostCentre();
            new DefaultEmployeeCostCentreCreators(_context).CreateDepartment();
            new DefaultEmployeeCostCentreCreators(_context).CreateResident();
            new DefaultEmployeeCostCentreCreators(_context).CreateRace();
            new DefaultEmployeeCostCentreCreators(_context).CreateReligion();
            new DefaultEmployeeCostCentreCreators(_context).CreateJobTitleMaster();
            new DefaultEmployeeCostCentreCreators(_context).CreateEmployeeSalaryClassification();
            new DefaultEmployeeCostCentreCreators(_context).CreateSection();
            new DefaultMemberAccountTypeCreator(_context).Create();
            new DefaultMemberServiceInfoTypeCreator(_context).Create();
            new DefaultPointCampaignModifierTypeCreator(_context).Create();
            _context.SaveChanges();
        }
    }
}
