﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.EntityFramework;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.MultiTenancy;

namespace DinePlan.DineConnect.Migrations.Seed
{
    public class DefaultReportKeyCreator
    {
        public static List<HouseReport> InitialReports { get; }

        private readonly DineConnectDbContext _context;
        static DefaultReportKeyCreator()

        {
            InitialReports = new List<HouseReport>
            {
                new HouseReport("STOCK","STOCK AS ON DATE","USER"),
                new HouseReport("CLOSESTOCK","CLOSING STOCK AS ON DATE","USER"),
                //new HouseReport("INHANDLESSTHANROL","MATERIAL NEEDS TO ORDER","USER"),
                new HouseReport("RATEVIEW","MATERIAL RATE VIEW","USER"),
                //new HouseReport("ANALYTICAL","ANALYTICAL REPORT","USER"),
                new HouseReport("ACTUALVSSTANDARD","ACTUAL VS STANDARD","USER"),
                new HouseReport("SUPPLIERRATEVIEW","SUPPLIER MATERIAL RATE VIEW","USER"),
                new HouseReport("PRODUCTANALYSIS","PRODUCT WISE ANALYSIS REPORT","USER"),
            };

        }

        public DefaultReportKeyCreator(DineConnectDbContext context)
        {
            _context = context;
        }



        public void Create()
        {
            var debugStarted = Debugger.IsAttached;
            if (!debugStarted)
                return;

            var defaultTenant = _context.Tenants.FirstOrDefault(t => t.TenancyName.Equals("Connect"));
            if (defaultTenant != null)
            {
                CreateReports(defaultTenant);
            }
        }

        private void CreateReports(Tenant defaultTenant)
        {
            foreach (var report in InitialReports)
            {
                AddReportsIfNotExists(report);
            }

        }

        private void AddReportsIfNotExists(HouseReport report)
        {
            if (_context.HouseReports.Any(u => u.ReportKey == report.ReportKey))
            {
                return;
            }
            _context.HouseReports.Add(report);
            _context.SaveChanges();
        }

    }
}
