﻿using System.Collections.Generic;
using System.Linq;
using Abp.Localization;
using DinePlan.DineConnect.EntityFramework;

namespace DinePlan.DineConnect.Migrations.Seed
{
    public class DefaultLanguagesCreator
    {
        public static List<ApplicationLanguage> InitialLanguages { get; private set; }

        private readonly DineConnectDbContext _context;

        static DefaultLanguagesCreator()
        {
            InitialLanguages = new List<ApplicationLanguage>
            {
                new ApplicationLanguage(null, "ar", "العربية", "famfamfam-flag-sa"),
                new ApplicationLanguage(null, "en", "English", "famfamfam-flag-gb"),
                new ApplicationLanguage(null, "id", "Bahasa Indonesia", "famfamfam-flag-id"),
                new ApplicationLanguage(null, "hi-IN", "हिंदी", "famfamfam-flag-in"),
                new ApplicationLanguage(null, "ms", "Bahasa Melayu", "famfamfam-flag-my"),
                new ApplicationLanguage(null, "bg", "Norsk Bokmål", "famfamfam-flag-bv"),
                new ApplicationLanguage(null, "ta-IN", "தமிழ்", "famfamfam-flag-in"),
                new ApplicationLanguage(null, "th", "ภาษาไทย", "famfamfam-flag-th"),
                new ApplicationLanguage(null, "de", "German", "famfamfam-flag-de"),
                new ApplicationLanguage(null, "zh-CN", "简体中文", "famfamfam-flag-cn")
            };
        }

        public DefaultLanguagesCreator(DineConnectDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateLanguages();
        }

        private void CreateLanguages()
        {
            foreach (var language in InitialLanguages)
            {
                AddLanguageIfNotExists(language);
            }
        }

        private void AddLanguageIfNotExists(ApplicationLanguage language)
        {
            if (_context.Languages.Any(l => l.TenantId == language.TenantId && l.Name == language.Name))
            {
                return;
            }

            _context.Languages.Add(language);

            _context.SaveChanges();
        }
    }
}