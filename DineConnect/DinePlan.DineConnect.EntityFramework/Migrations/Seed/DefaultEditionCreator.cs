using System.Linq;
using Abp.Application.Editions;
using Abp.Application.Features;
using DinePlan.DineConnect.Editions;
using DinePlan.DineConnect.EntityFramework;

namespace DinePlan.DineConnect.Migrations.Seed
{
    public class DefaultEditionCreator
    {
        private readonly DineConnectDbContext _context;

        public DefaultEditionCreator(DineConnectDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateEditions();
        }

        private void CreateEditions()
        {

            var defaultEdition = _context.Editions.FirstOrDefault(e => e.Name == EditionManager.ConnectEdition);
            if (defaultEdition == null)
            {
                defaultEdition = new Edition { Name = EditionManager.ConnectEdition, DisplayName = EditionManager.ConnectEdition };
                _context.Editions.Add(defaultEdition);
            }
           
            _context.SaveChanges();

        }
    }
}