﻿using DinePlan.DineConnect.Engage;
using DinePlan.DineConnect.EntityFramework;
using System.Collections.Generic;
using System.Linq;

namespace DinePlan.DineConnect.Migrations.Seed
{
    public class DefaultMemberServiceInfoTypeCreator
    {
        private readonly DineConnectDbContext _context;

        public static List<MemberServiceInfoType> Items { get; set; }
        static DefaultMemberServiceInfoTypeCreator()
        {
            Items = new List<MemberServiceInfoType> { 
                new MemberServiceInfoType{ Code=MemberServiceInfoTypeEnum.Ess.ToString(), Description="eService Slip"},
                new MemberServiceInfoType{ Code=MemberServiceInfoTypeEnum.Nwl.ToString(), Description="News Letter"},
                new MemberServiceInfoType{ Code=MemberServiceInfoTypeEnum.EssNotiEmail.ToString(), Description="eService Slip Noti Email"},
                new MemberServiceInfoType{ Code=MemberServiceInfoTypeEnum.EssNotiSms.ToString(), Description="eService Slip Noti Sms"}
            };
        }

        public DefaultMemberServiceInfoTypeCreator(DineConnectDbContext context)
        {
            _context = context;
        }

        public void Create() {
            InsertItemsToDb();
        }

        private void InsertItemsToDb() {
            foreach (var item in Items)
            {
                AddIfNotExists(item);
            }
        }

        private void AddIfNotExists(MemberServiceInfoType entityObject) {
            if (_context.MemberServiceInfoType.Any(u =>
                u.Code == entityObject.Code))
            {
                return;
            }
            _context.MemberServiceInfoType.Add(entityObject);
            _context.SaveChanges();
        }
    }
}
