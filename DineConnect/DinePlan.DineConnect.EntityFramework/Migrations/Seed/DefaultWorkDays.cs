﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.EntityFramework;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.Hr;
using DinePlan.DineConnect.MultiTenancy;

namespace DinePlan.DineConnect.Migrations.Seed
{
    public class DefaultWorkDayCreators
    {
        private readonly DineConnectDbContext _context;
        public static List<WorkDay> InitialWorkDays { get; }


        static DefaultWorkDayCreators()
        {
            InitialWorkDays = new List<WorkDay>
            {
                new WorkDay(1,0,"Sunday","Sun",0,0,2,0,0),
                new WorkDay(1,1,"Monday","Mon",8,1,(decimal) 1.5,1,0),
                new WorkDay(1,2,"Tuesday","Tue",8,1,(decimal)1.5,1,0),
                new WorkDay(1,3,"Wednesday","Wed",8,1,(decimal)1.5,1,0),
                new WorkDay(1,4,"Thursday","Thu",8,1,(decimal)1.5,1,0),
                new WorkDay(1,5,"Friday","Fri",8,1,(decimal)1.5,1,0),
                new WorkDay(1,6,"Saturday","Sat",4,0,(decimal)1.5,0.5m,0),
            };
        }


        public DefaultWorkDayCreators(DineConnectDbContext context)
        {
            _context = context;
        }
        public void Create()
        {
            CreateWorkDays();
        }

        private void CreateWorkDays()
        {
            var tenants =  _context.Tenants.ToList();
            foreach (var tenant in tenants)
            {
                foreach (var workDay in InitialWorkDays)
                {
                    workDay.TenantId = tenant.Id;
                    AddWorkDaysIfNotExists(workDay);
                }
            }

        }

        private void AddWorkDaysIfNotExists(WorkDay workDay)
        {
            if (_context.WorkDays.Any(u => u.TenantId == workDay.TenantId && u.DayOfWeekRefId == workDay.DayOfWeekRefId && u.EmployeeRefId==null))
            {
                return;
            }
            _context.WorkDays.Add(workDay);
            _context.SaveChanges();
        }

    }
}
