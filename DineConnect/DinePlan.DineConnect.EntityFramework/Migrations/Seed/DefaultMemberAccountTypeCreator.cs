﻿using DinePlan.DineConnect.Engage;
using DinePlan.DineConnect.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Migrations.Seed
{
    public class DefaultMemberAccountTypeCreator
    {
        private readonly DineConnectDbContext _context;

        public static List<MemberAccountType> InitialMemberAccountTypes { get; }
        static DefaultMemberAccountTypeCreator()
        {
            InitialMemberAccountTypes = new List<MemberAccountType> { 
                new MemberAccountType{ Code = "PNT", Description = "Loyalty Point"},
                new MemberAccountType{ Code = "VCH", Description = "Gift Voucher"},
                new MemberAccountType{ Code = "STV", Description = "Stored Value"},
                new MemberAccountType{ Code = "STM", Description = "Stamp"},
                new MemberAccountType{ Code = "PPI", Description = "Prepaid Item"}
            };
        }

        public DefaultMemberAccountTypeCreator(DineConnectDbContext context)
        {
            _context = context;
        }

        public void Create() {
            CreateMemberAccountTypes();
        }

        private void CreateMemberAccountTypes() {
            //var tenants = _context.Tenants.ToList();
            ////foreach (var tenant in tenants)
            //{
                foreach (var acctType in InitialMemberAccountTypes)
                {
                    AddAcctTypesIfNotExists(acctType);
                }
            //}
        }

        private void AddAcctTypesIfNotExists(MemberAccountType acctType)
        {
            if (_context.MemberAccountType.Any(u =>
                u.Code == acctType.Code))
            {
                return;
            }
            _context.MemberAccountType.Add(acctType);
            _context.SaveChanges();
        }
    }
}
