﻿using System.Linq;
using Abp.Configuration;
using Abp.Localization;
using Abp.Net.Mail;
using DinePlan.DineConnect.EntityFramework;

namespace DinePlan.DineConnect.Migrations.Seed
{
    public class DefaultSettingsCreator
    {
        private readonly DineConnectDbContext _context;

        public DefaultSettingsCreator(DineConnectDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            //Emailing
            AddSettingIfNotExists(EmailSettingNames.DefaultFromAddress, "admin@dineplan.net");
            AddSettingIfNotExists(EmailSettingNames.DefaultFromDisplayName, "DineConnect");
            AddSettingIfNotExists(EmailSettingNames.Smtp.Host, "smtp.sendgrid.net");
            AddSettingIfNotExists(EmailSettingNames.Smtp.Port, "587");
            AddSettingIfNotExists(EmailSettingNames.Smtp.UserName, "mykeyapi");
            AddSettingIfNotExists(EmailSettingNames.Smtp.Password, "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");
            AddSettingIfNotExists(EmailSettingNames.Smtp.EnableSsl, "false");
            AddSettingIfNotExists(EmailSettingNames.Smtp.UseDefaultCredentials, "false");


            //Languages
            AddSettingIfNotExists(LocalizationSettingNames.DefaultLanguage, "en");
        }

        private void AddSettingIfNotExists(string name, string value, int? tenantId = null)
        {
            if (_context.Settings.Any(s => s.Name == name && s.TenantId == tenantId && s.UserId == null))
            {
                return;
            }

            _context.Settings.Add(new Setting(tenantId, null, name, value));
            _context.SaveChanges();
        }
    }
}