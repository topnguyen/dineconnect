﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.EntityFramework;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.Hr;
using DinePlan.DineConnect.MultiTenancy;

namespace DinePlan.DineConnect.Migrations.Seed
{
    class DefaultEmployeeCostCentreCreators
    {
        private readonly DineConnectDbContext _context;

        public static List<EmployeeCostCentreMaster> InitialList { get; }
        public static List<EmployeeResidentStatus> ResidentStatusList { get; }
        public static List<EmployeeDepartmentMaster> DepartmentList { get; }
        public static List<EmployeeRace> RacesList { get; }
        public static List<EmployeeReligion> ReligionsList { get; }

        public static List<JobTitleMaster> JobTitleList { get; }

        public static List<EmployeeSalaryClassification> SalaryClassificationList { get; }
        public static List<EmployeeSectionMaster> SectionList { get; }

        //$scope.costcentre = ['ADMIN', 'SNR-MGMT', 'OPERATION', 'TRANSPORT', 'OTHERS', 'MAINTENANCE'].sort();


        static DefaultEmployeeCostCentreCreators()
        {
            InitialList = new List<EmployeeCostCentreMaster>
            {
               new EmployeeCostCentreMaster(1,"Admin"),
               new EmployeeCostCentreMaster(1,"SNR-MGMT"),
               new EmployeeCostCentreMaster(1,"Finance"),
               new EmployeeCostCentreMaster(1,"Operation"),
               new EmployeeCostCentreMaster(1,"Transport"),
               new EmployeeCostCentreMaster(1,"Maintenance"),
               new EmployeeCostCentreMaster(1,"Marketing"),
            };

            //$scope.residents = ['Citizen', 'PR', 'WP', 'SP', 'EP', 'RC KPL', 'RC DYN', 'LOC'].sort();

            ResidentStatusList = new List<EmployeeResidentStatus>
            {
                new EmployeeResidentStatus(1,"Citizen"),
                new EmployeeResidentStatus(1,"PR"),
                new EmployeeResidentStatus(1,"WP"),
                new EmployeeResidentStatus(1,"SP"),
                new EmployeeResidentStatus(1,"EP"),
                new EmployeeResidentStatus(1,"LOC"),
            };

            //$scope.departments = ['Administration', 'Director', 'Finance', 'Human Resource', 'Manager', 'Marketing', 'NDT & ANDT', 'Research and Development', 'Sales', 'Support'].sort();

            DepartmentList = new List<EmployeeDepartmentMaster>
            {
                new EmployeeDepartmentMaster(1,"Administration"),
                new EmployeeDepartmentMaster(1,"Director"),
                new EmployeeDepartmentMaster(1,"Finance"),
                new EmployeeDepartmentMaster(1,"Human Resource"),
                new EmployeeDepartmentMaster(1,"Manager"),
                new EmployeeDepartmentMaster(1,"Marketing"),
                new EmployeeDepartmentMaster(1,"NDT & ANDT"),
                new EmployeeDepartmentMaster(1,"Research and Development"),
                new EmployeeDepartmentMaster(1,"Sales"),
                new EmployeeDepartmentMaster(1,"Support"),
            };

            //$scope.races = ['Chinese', 'Eurasian', 'Iban', 'Indian', 'Kadazan', 'Malay', 'Burmese', 'Others'].sort();

            RacesList = new List<EmployeeRace>
            {
                new EmployeeRace(1,"Chinese"),
                new EmployeeRace(1,"Eurasian"),
                new EmployeeRace(1,"Iban"),
                new EmployeeRace(1,"Indian"),
                new EmployeeRace(1,"Kadazan"),
                new EmployeeRace(1,"Malay"),
                new EmployeeRace(1,"Burmese"),
                new EmployeeRace(1,"Others"),
            };

            ReligionsList = new List<EmployeeReligion>
            {
                new EmployeeReligion(1,"Buddhist"),
                new EmployeeReligion(1,"Christian"),
                new EmployeeReligion(1,"Hindu"),
                new EmployeeReligion(1,"Islam"),
                new EmployeeReligion(1,"None"),
                new EmployeeReligion(1,"Others")
            };

            JobTitleList = new List<JobTitleMaster>
            {
                new JobTitleMaster("Manager"),
                new JobTitleMaster("Kitchen Manager"),
                new JobTitleMaster("Supervisor"),
                new JobTitleMaster("Cashier"),
                new JobTitleMaster("Cook"),
                new JobTitleMaster("Steward"),
                new JobTitleMaster("Waiter"),
                new JobTitleMaster("Driver"),
                new JobTitleMaster("Baker"),
                new JobTitleMaster("Prep Cook"),
                new JobTitleMaster("Bartender"),                
            };



            SalaryClassificationList = new List<EmployeeSalaryClassification>
            {
                new EmployeeSalaryClassification(1,"Direct"),
                new EmployeeSalaryClassification(1,"Indirect"),
            };

            SectionList = new List<EmployeeSectionMaster>
            {
                new EmployeeSectionMaster(1,"Administration"),
                new EmployeeSectionMaster(1,"Director"),
            };


        }

        public DefaultEmployeeCostCentreCreators(DineConnectDbContext context)
        {
            _context = context;
        }

        public void CreateEmployeeCostCentre()
        {
            CreateEmployeeCostCentreList();
        }

        private void CreateEmployeeCostCentreList()
        {
            var tenants = _context.Tenants.ToList();
            foreach (var tenant in tenants)
            {
                foreach (var lst in InitialList)
                {
                    lst.TenantId = tenant.Id;
                    AddCostCentreIfNotExists(lst);
                }
            }

        }

        private void AddCostCentreIfNotExists(EmployeeCostCentreMaster dto)
        {
            if (_context.EmployeeCostCentreMasters.Any(u => u.TenantId == dto.TenantId && u.CostCentre == dto.CostCentre))
            {
                return;
            }
            _context.EmployeeCostCentreMasters.Add(dto);
            _context.SaveChanges();
        }


        public void CreateResident()
        {
            CreateResidentStatus();
        }

        private void CreateResidentStatus()
        {
            var tenants = _context.Tenants.ToList();
            foreach (var tenant in tenants)
            {
                foreach (var lst in ResidentStatusList)
                {
                    lst.TenantId = tenant.Id;
                    AddResidentStatusListIfNotExists(lst);
                }
            }

        }

        private void AddResidentStatusListIfNotExists(EmployeeResidentStatus dto)
        {
            if (_context.EmployeeResidentStatuses.Any(u => u.TenantId == dto.TenantId && u.ResidentStatus == dto.ResidentStatus))
            {
                return;
            }
            _context.EmployeeResidentStatuses.Add(dto);
            _context.SaveChanges();
        }

        public void CreateRace()
        {
            CreateRaceStatus();
        }

        private void CreateRaceStatus()
        {
            var tenants = _context.Tenants.ToList();
            foreach (var tenant in tenants)
            {
                foreach (var lst in RacesList)
                {
                    lst.TenantId = tenant.Id;
                    AddRaceListIfNotExists(lst);
                }
            }

        }

        private void AddRaceListIfNotExists(EmployeeRace dto)
        {
            if (_context.EmployeeRaces.Any(u => u.TenantId == dto.TenantId && u.RaceName == dto.RaceName))
            {
                return;
            }
            _context.EmployeeRaces.Add(dto);
            _context.SaveChanges();
        }

        public void CreateDepartment()
        {
            CreateDepartmentList();
        }

        private void CreateDepartmentList()
        {
            var tenants = _context.Tenants.ToList();
            foreach (var tenant in tenants)
            {
                foreach (var lst in DepartmentList)
                {
                    lst.TenantId = tenant.Id;
                    AddDepartmentListIfNotExists(lst);
                }
            }

        }

        private void AddDepartmentListIfNotExists(EmployeeDepartmentMaster dto)
        {
            if (_context.EmployeeDepartmentMasters.Any(u => u.TenantId == dto.TenantId && u.DepartmentName == dto.DepartmentName))
            {
                return;
            }
            _context.EmployeeDepartmentMasters.Add(dto);
            _context.SaveChanges();
        }

        public void CreateReligion()
        {
            CreateReligionList();
        }

        private void CreateReligionList()
        {
            var tenants = _context.Tenants.ToList();
            foreach (var tenant in tenants)
            {
                foreach (var lst in ReligionsList)
                {
                    lst.TenantId = tenant.Id;
                    AddReligionsListIfNotExists(lst);
                }
            }

        }

        private void AddReligionsListIfNotExists(EmployeeReligion dto)
        {
            if (_context.EmployeeReligions.Any(u => u.TenantId == dto.TenantId && u.ReligionName == dto.ReligionName))
            {
                return;
            }
            _context.EmployeeReligions.Add(dto);
            _context.SaveChanges();
        }

        public void CreateJobTitleMaster()
        {
            CreateJobTitleList();
        }


        private void CreateJobTitleList()
        {
            var tenants = _context.Tenants.ToList();
            foreach (var tenant in tenants)
            {
                foreach (var lst in JobTitleList)
                {
                    lst.TenantId = tenant.Id;
                    AddJobTitleListIfNotExists(lst);
                }
            }

        }


        private void AddJobTitleListIfNotExists(JobTitleMaster dto)
        {
            if (_context.JobTitleMasters.Any(u => u.TenantId == dto.TenantId && u.JobTitle == dto.JobTitle))
            {
                return;
            }
            _context.JobTitleMasters.Add(dto);
            _context.SaveChanges();
        }



        public void CreateEmployeeSalaryClassification()
        {
            CreateEmployeeSalaryClassificationList();
        }

        private void CreateEmployeeSalaryClassificationList()
        {
            var tenants = _context.Tenants.ToList();
            foreach (var tenant in tenants)
            {
                foreach (var lst in SalaryClassificationList)
                {
                    lst.TenantId = tenant.Id;
                    AddSalaryClassificationIfNotExists(lst);
                }
            }

        }

        private void AddSalaryClassificationIfNotExists(EmployeeSalaryClassification dto)
        {
            if (_context.EmployeeSalaryClassifications.Any(u => u.TenantId == dto.TenantId && u.SalaryClassification == dto.SalaryClassification))
            {
                return;
            }
            _context.EmployeeSalaryClassifications.Add(dto);
            _context.SaveChanges();
        }


        public void CreateSection()
        {
            CreateSectionList();
        }


        private void CreateSectionList()
        {
            var tenants = _context.Tenants.ToList();
            foreach (var tenant in tenants)
            {
                foreach (var lst in SectionList)
                {
                    lst.TenantId = tenant.Id;
                    AddSectionListIfNotExists(lst);
                }
            }

        }


        private void AddSectionListIfNotExists(EmployeeSectionMaster dto)
        {
            if (_context.EmployeeSectionMasters.Any(u => u.TenantId == dto.TenantId && u.SectionName == dto.SectionName))
            {
                return;
            }
            _context.EmployeeSectionMasters.Add(dto);
            _context.SaveChanges();
        }


    }
}