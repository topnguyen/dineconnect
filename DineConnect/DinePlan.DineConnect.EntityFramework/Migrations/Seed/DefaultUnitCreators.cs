﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.EntityFramework;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.MultiTenancy;

namespace DinePlan.DineConnect.Migrations.Seed
{
    public class DefaultUnitCreators
    {
        private readonly DineConnectDbContext _context;
        public static List<Unit> InitialUnits { get; }
        public static List<UnitConversionInitial> InitialUnitConversions { get; }

        static DefaultUnitCreators()

        {
            InitialUnits = new List<Unit>
            {
                new Unit("KG"),
                new Unit("G"),
                new Unit("L"),
                new Unit("ML"),
                new Unit("M"),
                new Unit("NO"),
                new Unit("PC")
            };
            InitialUnitConversions = new List<UnitConversionInitial>
            {
                new UnitConversionInitial(1, "KG", "G", 1000),
                new UnitConversionInitial(1, "G", "KG", Convert.ToDecimal(0.001)),
                new UnitConversionInitial(1, "L", "ML", 1000),
                new UnitConversionInitial(1, "ML", "L", Convert.ToDecimal(0.001))
               


            };
        }

        public DefaultUnitCreators(DineConnectDbContext context)
        {
            _context = context;
        }

     

        public void Create()
        {
            var debugStarted = Debugger.IsAttached;
            if (!debugStarted)
                return;

            var defaultTenant = _context.Tenants.FirstOrDefault(t => t.TenancyName.Equals("Connect"));
            if (defaultTenant != null)
            {
             CreateUnits(defaultTenant);
            }
        }

        private void CreateUnits(Tenant defaultTenant)
        {
            foreach (var unit in InitialUnits)
            {
                unit.TenantId = defaultTenant.Id;
                AddUnitsIfNotExists(unit);
            }

            foreach (var unitcon in InitialUnitConversions)
            {
                unitcon.TenantId = defaultTenant.Id;
                AddUnitConversionsIfNotExists(unitcon);
            }
        }

        private void AddUnitsIfNotExists(Unit unit)
        {
            if (_context.Units.Any(u => u.TenantId == unit.TenantId && u.Name == unit.Name))
            {
                return;
            }
            _context.Units.Add(unit);
            _context.SaveChanges();
        }

        private void AddUnitConversionsIfNotExists(UnitConversionInitial uc)
        {
            int baseUnitId, refUnitId;
            baseUnitId = _context.Units.FirstOrDefault(u => u.Name.Equals(uc.BaseName)).Id;
            refUnitId = _context.Units.FirstOrDefault(u => u.Name.Equals(uc.RefName)).Id;

            if (_context.UnitConversions.Any(u => u.BaseUnitId == baseUnitId && u.RefUnitId == refUnitId))
            {
                return;
            }
            _context.UnitConversions.Add(new UnitConversion(baseUnitId, refUnitId, uc.Conversion));
            _context.SaveChanges();
        }

        public class UnitConversionInitial
        {
            public UnitConversionInitial(int argTenantId, string argBaseName, string argRefName, decimal argConversion)
            {
                TenantId = argTenantId;
                BaseName = argBaseName;
                RefName = argRefName;
                Conversion = argConversion;
            }

            public int TenantId { get; set; }
            public string BaseName { get; set; }
            public string RefName { get; set; }
            public decimal Conversion { get; set; }
        }
    }
}