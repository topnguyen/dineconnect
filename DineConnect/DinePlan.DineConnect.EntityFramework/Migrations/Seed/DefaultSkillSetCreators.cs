﻿using System.Collections.Generic;
using System.Linq;
using Abp.Configuration;
using Abp.Localization;
using Abp.Net.Mail;
using DinePlan.DineConnect.EntityFramework;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Migrations.Seed
{
    public class DefaultSkillSetCreators
    {
        private readonly DineConnectDbContext _context;
        public static List<SkillSet> InitialSkillSets { get; }

        public DefaultSkillSetCreators(DineConnectDbContext context)
        {
            _context = context;
        }
        

        static DefaultSkillSetCreators()
        {
            InitialSkillSets = new List<SkillSet>
            {
                new SkillSet(1,"Office","Office"),
                new SkillSet(1,"Driver","Driver"),
                new SkillSet(1,"Accounting","Accounting"),
                new SkillSet(1,"General Worker","General Worker")
            };
        }

        public void Create()
        {
            CreateSkillSets();
        }

        private void CreateSkillSets()
        {
            var tenants = _context.Tenants.ToList();
            foreach (var tenant in tenants)
            {
                foreach (var skillset in InitialSkillSets)
                {
                    skillset.TenantId = tenant.Id;
                    skillset.IsActive = true;
                    AddSkillSetsIfNotExists(skillset);
                }
            }
        }

        private void AddSkillSetsIfNotExists(SkillSet skillset)
        {
            if (_context.SkillSets.Any(u => u.SkillCode == skillset.SkillCode))
            {
                return;
            }
            _context.SkillSets.Add(skillset);
            _context.SaveChanges();
        }
    }
}
