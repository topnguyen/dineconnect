﻿using System.Diagnostics;
using System.Linq;
using Abp.Authorization;
using Abp.Authorization.Roles;
using Abp.Authorization.Users;
using Abp.MultiTenancy;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Authorization.Roles;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Editions;
using DinePlan.DineConnect.EntityFramework;
using DinePlan.DineConnect.MultiTenancy;
using Microsoft.AspNet.Identity;
using Abp.Domain.Uow;
using Abp.Auditing;

namespace DinePlan.DineConnect.Migrations.Seed
{
    /// <summary>
    /// This class is used to default tenants, roles and users when application is installed.
    /// It's also used in tests as startup state of the system.
    /// </summary>
    public class DefaultTenantRoleAndUserCreator
    {
        private readonly DineConnectDbContext _context;

        public DefaultTenantRoleAndUserCreator(DineConnectDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateHostAndUsers();
            var debugStarted = Debugger.IsAttached;
            if(debugStarted)
                CreateDefaultTenantAndUsers();
        }

        private void CreateHostAndUsers()
        {
            //Admin role for host

            var adminRoleForHost = _context.Roles.FirstOrDefault(r => r.TenantId == null && r.Name == StaticRoleNames.Host.Admin);
            if (adminRoleForHost == null)
            {
                adminRoleForHost = _context.Roles.Add(new Role(null, StaticRoleNames.Host.Admin, StaticRoleNames.Host.Admin) { IsStatic = true, IsDefault = true });
                _context.SaveChanges();
            }

            //admin user for host

            var adminUserForHost = _context.Users.FirstOrDefault(u => u.TenantId == null && u.UserName == User.AdminUserName);
            if (adminUserForHost == null)
            {
                adminUserForHost = _context.Users.Add(
                    new User
                    {
                        TenantId = null,
                        UserName = User.AdminUserName,
                        Name = "admin",
                        Surname = "admin",
                        EmailAddress = "admin@dineplan.net",
                        IsEmailConfirmed = true,
                        ShouldChangePasswordOnNextLogin = false,
                        IsActive = true,
                        Password = new PasswordHasher().HashPassword("1234")
                    });
                _context.SaveChanges();

                _context.UserRoles.Add(new UserRole(adminUserForHost.Id, adminRoleForHost.Id));
                _context.SaveChanges();

                var permissions = PermissionFinder
                    .GetAllPermissions(new AppAuthorizationProvider())
                    .Where(p => p.MultiTenancySides.HasFlag(MultiTenancySides.Host))
                    .ToList();

                foreach (var permission in permissions)
                {
                    if (!permission.IsGrantedByDefault)
                    {
                        _context.Permissions.Add(
                            new RolePermissionSetting
                            {
                                Name = permission.Name,
                                IsGranted = true,
                                RoleId = adminRoleForHost.Id
                            });
                    }
                }

                _context.SaveChanges();
            }
        }

        private void CreateDefaultTenantAndUsers()
        {
            //Default tenant

            var defaultTenant = _context.Tenants.FirstOrDefault(t => t.TenancyName.Equals("connect"));
            if (defaultTenant == null)
            {
                defaultTenant = new Tenant("Connect", "Connect");
                var defaultEdition = _context.Editions.FirstOrDefault(e => e.Name == EditionManager.ConnectEdition);
                if (defaultEdition != null)
                {
                    defaultTenant.EditionId = defaultEdition.Id;
                }

                defaultTenant = _context.Tenants.Add(defaultTenant);
                _context.SaveChanges();
            }

            
            //Admin role for 'Default' tenant
            var adminRoleForDefaultTenant = _context.Roles.FirstOrDefault(r => r.TenantId == defaultTenant.Id && r.Name == StaticRoleNames.Tenants.Admin);
            if (adminRoleForDefaultTenant == null)
            {
                adminRoleForDefaultTenant = _context.Roles.Add(new Role(defaultTenant.Id, StaticRoleNames.Tenants.Admin, StaticRoleNames.Tenants.Admin) { IsStatic = true });
                _context.SaveChanges();
            }

            //User role for 'Default' tenant

            var userRoleForDefaultTenant = _context.Roles.FirstOrDefault(r => r.TenantId == defaultTenant.Id && r.Name == StaticRoleNames.Tenants.User);
            if (userRoleForDefaultTenant == null)
            {
                _context.Roles.Add(new Role(defaultTenant.Id, StaticRoleNames.Tenants.User, StaticRoleNames.Tenants.User) { IsStatic = true, IsDefault = true });
                _context.SaveChanges();
            }
            var memberRole = _context.Roles.FirstOrDefault(r => r.TenantId == defaultTenant.Id && r.Name == StaticRoleNames.Tenants.Member);
            if (memberRole == null)
            {
                _context.Roles.Add(new Role(defaultTenant.Id, StaticRoleNames.Tenants.Member, StaticRoleNames.Tenants.Member) { IsStatic = true});
                _context.SaveChanges();
            }

            var employeeRole = _context.Roles.FirstOrDefault(r => r.TenantId == defaultTenant.Id && r.Name == StaticRoleNames.Tenants.Employee);
            if (employeeRole == null)
            {
                _context.Roles.Add(new Role(defaultTenant.Id, StaticRoleNames.Tenants.Employee, StaticRoleNames.Tenants.Employee) { IsStatic = true });
                _context.SaveChanges();
            }
            //admin user for 'Default' tenant

            var adminUserForDefaultTenant = _context.Users.FirstOrDefault(u => u.TenantId == defaultTenant.Id && u.UserName == User.AdminUserName);
            if (adminUserForDefaultTenant == null)
            {
                adminUserForDefaultTenant = User.CreateTenantAdminUser(defaultTenant.Id, "connect@dineplan.net", "123qwe");
                adminUserForDefaultTenant.IsEmailConfirmed = true;
                adminUserForDefaultTenant.IsActive = true;

                _context.Users.Add(adminUserForDefaultTenant);
                _context.SaveChanges();

                //Assign Admin role to admin user
                _context.UserRoles.Add(new UserRole(adminUserForDefaultTenant.Id, adminRoleForDefaultTenant.Id));
                _context.SaveChanges();

                //Grant all permissions
                var permissions = PermissionFinder
                    .GetAllPermissions(new AppAuthorizationProvider())
                    .Where(p => p.MultiTenancySides.HasFlag(MultiTenancySides.Tenant))
                    .ToList();

                foreach (var permission in permissions)
                {
                    if (!permission.IsGrantedByDefault)
                    {
                        _context.Permissions.Add(
                            new RolePermissionSetting
                            {
                                Name = permission.Name,
                                IsGranted = true,
                                RoleId = adminRoleForDefaultTenant.Id
                            });
                    }
                }
                _context.SaveChanges();
            }
        }
    }
}
