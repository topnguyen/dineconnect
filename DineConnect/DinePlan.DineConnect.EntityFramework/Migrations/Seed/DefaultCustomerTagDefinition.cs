﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.EntityFramework;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.MultiTenancy;

namespace DinePlan.DineConnect.Migrations.Seed
{
    class DefaultCustomerTagDefinition
    {
        private readonly DineConnectDbContext _context;

        public static List<CustomerTagDefinition> InitializeCustomerTag { get; }
        static DefaultCustomerTagDefinition()
        {
            InitializeCustomerTag = new List<CustomerTagDefinition>
            {
                new CustomerTagDefinition("DEFAULT","DEFAULT")
            };
        }
        public DefaultCustomerTagDefinition(DineConnectDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            var debugStarted = Debugger.IsAttached;
            if (!debugStarted)
                return;

            var defaultTenant = _context.Tenants.FirstOrDefault(t => t.TenancyName.Equals("Connect"));
            if(defaultTenant != null)
            {
                CreateCustomerTagDefinition(defaultTenant);
            }
        }

        private void CreateCustomerTagDefinition(Tenant defaultTenant)
        {
            foreach (var ctag in InitializeCustomerTag)
            {
                ctag.TenantId = defaultTenant.Id;
                AddCustomerTagIfNotExists(ctag);
            }
        }

        private void AddCustomerTagIfNotExists(CustomerTagDefinition ctag)
        {
            if (_context.CustomerTagDefinitions.Any(u => u.TenantId == ctag.TenantId && u.TagCode == ctag.TagCode && u.TagName == ctag.TagName))
            {
                return;
            }
            _context.CustomerTagDefinitions.Add(ctag);
            _context.SaveChanges();
        }

    }
}
