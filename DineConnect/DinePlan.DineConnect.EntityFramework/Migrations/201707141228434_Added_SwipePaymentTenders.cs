namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_SwipePaymentTenders : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SwipePaymentTenders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SwipeCardLedgerRefId = c.Int(nullable: false),
                        PaymentTypeId = c.Int(nullable: false),
                        TenderedAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PaymentUserName = c.String(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PaymentTypes", t => t.PaymentTypeId, cascadeDelete: true)
                .ForeignKey("dbo.SwipeCardLedgers", t => t.SwipeCardLedgerRefId, cascadeDelete: true)
                .Index(t => t.SwipeCardLedgerRefId)
                .Index(t => t.PaymentTypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SwipePaymentTenders", "SwipeCardLedgerRefId", "dbo.SwipeCardLedgers");
            DropForeignKey("dbo.SwipePaymentTenders", "PaymentTypeId", "dbo.PaymentTypes");
            DropIndex("dbo.SwipePaymentTenders", new[] { "PaymentTypeId" });
            DropIndex("dbo.SwipePaymentTenders", new[] { "SwipeCardLedgerRefId" });
            DropTable("dbo.SwipePaymentTenders");
        }
    }
}
