namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeSyncerDetails : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.LocationSyncers", "Syncer_Id", "dbo.Syncers");
            DropIndex("dbo.LocationSyncers", new[] { "Syncer_Id" });
            AlterColumn("dbo.LocationSyncers", "Syncer_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.LocationSyncers", "Syncer_Id");
            AddForeignKey("dbo.LocationSyncers", "Syncer_Id", "dbo.Syncers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LocationSyncers", "Syncer_Id", "dbo.Syncers");
            DropIndex("dbo.LocationSyncers", new[] { "Syncer_Id" });
            AlterColumn("dbo.LocationSyncers", "Syncer_Id", c => c.Int());
            CreateIndex("dbo.LocationSyncers", "Syncer_Id");
            AddForeignKey("dbo.LocationSyncers", "Syncer_Id", "dbo.Syncers", "Id");
        }
    }
}
