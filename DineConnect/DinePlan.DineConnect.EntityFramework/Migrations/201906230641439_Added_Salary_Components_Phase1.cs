namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_Salary_Components_Phase1 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.WorkDays", "TenantId_DayOfWeekRefId");
            DropIndex("dbo.WorkDays", "TenantId_DayOfWeekRefName");
            DropIndex("dbo.WorkDays", "DayOfWeekShortName_Tenant");
            CreateTable(
                "dbo.EmployeeCostCentreMasters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CostCentre = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeCostCentreMaster_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeCostCentreMaster_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.EmployeeRaces",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RaceName = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeRace_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeRace_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.EmployeeReligions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ReligionName = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeReligion_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeReligion_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.EmployeeResidentStatuses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ResidentStatus = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeResidentStatus_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeResidentStatus_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.EmployeeDepartmentMasters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DepartmentName = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeDepartmentMaster_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeDepartmentMaster_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.EmployeeVsIncentives",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeRefId = c.Int(nullable: false),
                        IncentiveTagRefId = c.Int(nullable: false),
                        EffectiveFrom = c.DateTime(nullable: false),
                        RollBackFrom = c.DateTime(),
                        ReasonRemarks = c.String(),
                        RemoveRemarks = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeVsIncentive_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeVsIncentive_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.IncentiveTags", t => t.IncentiveTagRefId)
                .ForeignKey("dbo.PersonalInformations", t => t.EmployeeRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.EmployeeRefId)
                .Index(t => t.IncentiveTagRefId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.IncentiveTags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IncentivePayModeId = c.Int(nullable: false),
                        AllowanceOrDeductionRefId = c.Int(nullable: false),
                        IncentiveCategoryRefId = c.Int(nullable: false),
                        IncentiveTagCode = c.String(maxLength: 30),
                        IncentiveTagName = c.String(maxLength: 50),
                        IncentiveDescription = c.String(),
                        HoursBased = c.Boolean(nullable: false),
                        FullWorkHours = c.Boolean(nullable: false),
                        MinimumHours = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MinimumOtHours = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MinimumClaimedHours = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IncludeInConsolidationPerDay = c.Boolean(nullable: false),
                        IncentiveAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IncenitveBasedOnSalaryTags = c.Boolean(nullable: false),
                        SalaryTagRefId = c.Int(),
                        ProportianteValueOfSalaryTags = c.Decimal(precision: 18, scale: 2),
                        OverTimeBasedIncentive = c.Boolean(nullable: false),
                        DisplayNotApplicable = c.Boolean(nullable: false),
                        HourlyClaimableTimeSheets = c.Boolean(nullable: false),
                        IncentiveEntryMode = c.Int(nullable: false),
                        MinRange = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MaxRange = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CanIssueToIdleStatus = c.Boolean(nullable: false),
                        ActiveStatus = c.Boolean(nullable: false),
                        CanBeAssignedToAllEmployees = c.Boolean(nullable: false),
                        OnlyOnePersonOfTheTeamCanReceived = c.Boolean(nullable: false),
                        TimeSheetRequiredForGivenDate = c.Boolean(nullable: false),
                        IsMaterialQuantityBasedIncentiveFlag = c.Boolean(nullable: false),
                        IsCertificateMandatory = c.Boolean(nullable: false),
                        CalculationFormula = c.String(),
                        AnySpecialFormulaRefId = c.Boolean(nullable: false),
                        FormulaRefId = c.Int(),
                        CalculationFormulaMinimumValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CalculationFormulaMaximumValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OnlyAllowedOnWorkDay = c.Boolean(nullable: false),
                        WorkDayHourBasedIncentive = c.Boolean(nullable: false),
                        IsSytemGeneratedIncentive = c.Boolean(nullable: false),
                        IsItDeductableFromGrossSalary = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_IncentiveTag_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_IncentiveTag_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.IncentiveCategories", t => t.IncentiveCategoryRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.IncentiveCategoryRefId)
                .Index(t => t.IncentiveTagCode, unique: true, name: "IncentiveTagCode")
                .Index(t => t.IncentiveTagName, unique: true, name: "IncentiveTagName")
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.IncentiveCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IncentiveCategoryCode = c.String(maxLength: 30),
                        IncentiveCategoryName = c.String(maxLength: 50),
                        BasedOnSkillSet = c.Boolean(nullable: false),
                        BasedOnQuantity = c.Boolean(nullable: false),
                        MaximumIncenitivePerQuanityPerTimeSheet = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_IncentiveCategory_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_IncentiveCategory_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.IncentiveCategoryCode, unique: true, name: "IncentiveCategoryCode")
                .Index(t => t.IncentiveCategoryName, unique: true, name: "IncentiveCategoryName")
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.EmployeeVsSalaryTags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeRefId = c.Int(nullable: false),
                        SalaryRefId = c.Int(nullable: false),
                        SalaryTagAliasName = c.String(),
                        SalaryTagCalculationFormula = c.String(),
                        EffectiveFrom = c.DateTime(nullable: false),
                        RollBackFrom = c.DateTime(),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Remarks = c.String(),
                        RemoveRemarks = c.String(),
                        IsDynamicAmount = c.Boolean(nullable: false),
                        IsFixedAmount = c.Boolean(nullable: false),
                        SalaryReferenceEntityRefId = c.Int(),
                        CalculationPayModeRefId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeVsSalaryTag_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeVsSalaryTag_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PersonalInformations", t => t.EmployeeRefId)
                .ForeignKey("dbo.IncentiveTags", t => t.SalaryRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.EmployeeRefId)
                .Index(t => t.SalaryRefId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.IncentiveExcludedLists",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IncentiveTagRefId = c.Int(nullable: false),
                        ExcludedIncentiveTagRefId = c.Int(nullable: false),
                        ReasonRemarks = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_IncentiveExcludedList_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_IncentiveExcludedList_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.IncentiveTags", t => t.ExcludedIncentiveTagRefId)
                .ForeignKey("dbo.IncentiveTags", t => t.IncentiveTagRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.IncentiveTagRefId)
                .Index(t => t.ExcludedIncentiveTagRefId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.IncentiveVsEntities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IncentiveTagRefId = c.Int(nullable: false),
                        EntityType = c.Int(nullable: false),
                        EntityRefId = c.Int(nullable: false),
                        ExcludeFlag = c.Boolean(nullable: false),
                        EitherOr_ForSameEntity = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_IncentiveVsEntity_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_IncentiveVsEntity_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.IncentiveTags", t => t.IncentiveTagRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.IncentiveTagRefId)
                .Index(t => t.TenantId);
            
            CreateTable(
                "dbo.ManualIncentives",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IncentiveDate = c.DateTime(nullable: false),
                        EmployeeRefId = c.Int(nullable: false),
                        IncentiveTagRefId = c.Int(nullable: false),
                        IncentiveAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ManualOtHours = c.Decimal(precision: 18, scale: 2),
                        Remarks = c.String(),
                        RecommendedUserId = c.Int(),
                        ApprovedUserId = c.Int(),
                        ApprovedDate = c.DateTime(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ManualIncentive_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ManualIncentive_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.IncentiveTags", t => t.IncentiveTagRefId)
                .ForeignKey("dbo.PersonalInformations", t => t.EmployeeRefId)
                .ForeignKey("dbo.AbpTenants", t => t.TenantId)
                .Index(t => t.EmployeeRefId)
                .Index(t => t.IncentiveTagRefId)
                .Index(t => t.TenantId);
            
            AddColumn("dbo.PersonalInformations", "EmployeeResidentStatusRefId", c => c.Int(nullable: false));
            AddColumn("dbo.PersonalInformations", "RaceRefId", c => c.Int(nullable: false));
            AddColumn("dbo.PersonalInformations", "EmployeeCostCentreRefId", c => c.Int(nullable: false));
            AddColumn("dbo.PersonalInformations", "EmployeeReligionRefId", c => c.Int(nullable: false));
            AddColumn("dbo.SalaryInfos", "MaximumOTAllowedEffectiveFrom", c => c.DateTime());
            AddColumn("dbo.SalaryInfos", "IncentiveAndOTCalculationPeriodRefId", c => c.Int(nullable: false));
            AddColumn("dbo.SalaryInfos", "MaximumAllowedOTHoursOnPublicHoliday", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.WorkDays", "EmployeeRefId", c => c.Int());
            AddColumn("dbo.WorkDays", "NoOfWorkDays", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.WorkDays", "MaximumOTAllowedHours", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.WorkDays", "DayOfWeekShortName", c => c.String(nullable: false, maxLength: 15));
            CreateIndex("dbo.PersonalInformations", "EmployeeResidentStatusRefId");
            CreateIndex("dbo.PersonalInformations", "RaceRefId");
            CreateIndex("dbo.PersonalInformations", "EmployeeCostCentreRefId");
            CreateIndex("dbo.PersonalInformations", "EmployeeReligionRefId");
            CreateIndex("dbo.WorkDays", new[] { "TenantId", "DayOfWeekRefId", "EmployeeRefId" }, unique: true, name: "TenantId_DayOfWeekRefId_EmployeeRefId");
            CreateIndex("dbo.WorkDays", new[] { "TenantId", "DayOfWeekRefName", "EmployeeRefId" }, unique: true, name: "TenantId_DayOfWeekRefName_EmployeeRefId");
            AddForeignKey("dbo.PersonalInformations", "EmployeeCostCentreRefId", "dbo.EmployeeCostCentreMasters", "Id");
            AddForeignKey("dbo.PersonalInformations", "RaceRefId", "dbo.EmployeeRaces", "Id");
            AddForeignKey("dbo.PersonalInformations", "EmployeeReligionRefId", "dbo.EmployeeReligions", "Id");
            AddForeignKey("dbo.PersonalInformations", "EmployeeResidentStatusRefId", "dbo.EmployeeResidentStatuses", "Id");
            AddForeignKey("dbo.WorkDays", "EmployeeRefId", "dbo.PersonalInformations", "Id");
            DropColumn("dbo.SalaryInfos", "Allowance");
            DropColumn("dbo.SalaryInfos", "Deduction");
            DropColumn("dbo.SalaryInfos", "Accomodation");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SalaryInfos", "Accomodation", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SalaryInfos", "Deduction", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SalaryInfos", "Allowance", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropForeignKey("dbo.WorkDays", "EmployeeRefId", "dbo.PersonalInformations");
            DropForeignKey("dbo.ManualIncentives", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.ManualIncentives", "EmployeeRefId", "dbo.PersonalInformations");
            DropForeignKey("dbo.ManualIncentives", "IncentiveTagRefId", "dbo.IncentiveTags");
            DropForeignKey("dbo.IncentiveVsEntities", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.IncentiveVsEntities", "IncentiveTagRefId", "dbo.IncentiveTags");
            DropForeignKey("dbo.IncentiveExcludedLists", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.IncentiveExcludedLists", "IncentiveTagRefId", "dbo.IncentiveTags");
            DropForeignKey("dbo.IncentiveExcludedLists", "ExcludedIncentiveTagRefId", "dbo.IncentiveTags");
            DropForeignKey("dbo.EmployeeVsSalaryTags", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.EmployeeVsSalaryTags", "SalaryRefId", "dbo.IncentiveTags");
            DropForeignKey("dbo.EmployeeVsSalaryTags", "EmployeeRefId", "dbo.PersonalInformations");
            DropForeignKey("dbo.EmployeeVsIncentives", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.EmployeeVsIncentives", "EmployeeRefId", "dbo.PersonalInformations");
            DropForeignKey("dbo.EmployeeVsIncentives", "IncentiveTagRefId", "dbo.IncentiveTags");
            DropForeignKey("dbo.IncentiveTags", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.IncentiveTags", "IncentiveCategoryRefId", "dbo.IncentiveCategories");
            DropForeignKey("dbo.IncentiveCategories", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.EmployeeDepartmentMasters", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.PersonalInformations", "EmployeeResidentStatusRefId", "dbo.EmployeeResidentStatuses");
            DropForeignKey("dbo.EmployeeResidentStatuses", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.PersonalInformations", "EmployeeReligionRefId", "dbo.EmployeeReligions");
            DropForeignKey("dbo.EmployeeReligions", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.PersonalInformations", "RaceRefId", "dbo.EmployeeRaces");
            DropForeignKey("dbo.EmployeeRaces", "TenantId", "dbo.AbpTenants");
            DropForeignKey("dbo.PersonalInformations", "EmployeeCostCentreRefId", "dbo.EmployeeCostCentreMasters");
            DropForeignKey("dbo.EmployeeCostCentreMasters", "TenantId", "dbo.AbpTenants");
            DropIndex("dbo.WorkDays", "TenantId_DayOfWeekRefName_EmployeeRefId");
            DropIndex("dbo.WorkDays", "TenantId_DayOfWeekRefId_EmployeeRefId");
            DropIndex("dbo.ManualIncentives", new[] { "TenantId" });
            DropIndex("dbo.ManualIncentives", new[] { "IncentiveTagRefId" });
            DropIndex("dbo.ManualIncentives", new[] { "EmployeeRefId" });
            DropIndex("dbo.IncentiveVsEntities", new[] { "TenantId" });
            DropIndex("dbo.IncentiveVsEntities", new[] { "IncentiveTagRefId" });
            DropIndex("dbo.IncentiveExcludedLists", new[] { "TenantId" });
            DropIndex("dbo.IncentiveExcludedLists", new[] { "ExcludedIncentiveTagRefId" });
            DropIndex("dbo.IncentiveExcludedLists", new[] { "IncentiveTagRefId" });
            DropIndex("dbo.EmployeeVsSalaryTags", new[] { "TenantId" });
            DropIndex("dbo.EmployeeVsSalaryTags", new[] { "SalaryRefId" });
            DropIndex("dbo.EmployeeVsSalaryTags", new[] { "EmployeeRefId" });
            DropIndex("dbo.IncentiveCategories", new[] { "TenantId" });
            DropIndex("dbo.IncentiveCategories", "IncentiveCategoryName");
            DropIndex("dbo.IncentiveCategories", "IncentiveCategoryCode");
            DropIndex("dbo.IncentiveTags", new[] { "TenantId" });
            DropIndex("dbo.IncentiveTags", "IncentiveTagName");
            DropIndex("dbo.IncentiveTags", "IncentiveTagCode");
            DropIndex("dbo.IncentiveTags", new[] { "IncentiveCategoryRefId" });
            DropIndex("dbo.EmployeeVsIncentives", new[] { "TenantId" });
            DropIndex("dbo.EmployeeVsIncentives", new[] { "IncentiveTagRefId" });
            DropIndex("dbo.EmployeeVsIncentives", new[] { "EmployeeRefId" });
            DropIndex("dbo.EmployeeDepartmentMasters", new[] { "TenantId" });
            DropIndex("dbo.EmployeeResidentStatuses", new[] { "TenantId" });
            DropIndex("dbo.EmployeeReligions", new[] { "TenantId" });
            DropIndex("dbo.EmployeeRaces", new[] { "TenantId" });
            DropIndex("dbo.EmployeeCostCentreMasters", new[] { "TenantId" });
            DropIndex("dbo.PersonalInformations", new[] { "EmployeeReligionRefId" });
            DropIndex("dbo.PersonalInformations", new[] { "EmployeeCostCentreRefId" });
            DropIndex("dbo.PersonalInformations", new[] { "RaceRefId" });
            DropIndex("dbo.PersonalInformations", new[] { "EmployeeResidentStatusRefId" });
            AlterColumn("dbo.WorkDays", "DayOfWeekShortName", c => c.String(nullable: false, maxLength: 10));
            DropColumn("dbo.WorkDays", "MaximumOTAllowedHours");
            DropColumn("dbo.WorkDays", "NoOfWorkDays");
            DropColumn("dbo.WorkDays", "EmployeeRefId");
            DropColumn("dbo.SalaryInfos", "MaximumAllowedOTHoursOnPublicHoliday");
            DropColumn("dbo.SalaryInfos", "IncentiveAndOTCalculationPeriodRefId");
            DropColumn("dbo.SalaryInfos", "MaximumOTAllowedEffectiveFrom");
            DropColumn("dbo.PersonalInformations", "EmployeeReligionRefId");
            DropColumn("dbo.PersonalInformations", "EmployeeCostCentreRefId");
            DropColumn("dbo.PersonalInformations", "RaceRefId");
            DropColumn("dbo.PersonalInformations", "EmployeeResidentStatusRefId");
            DropTable("dbo.ManualIncentives",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ManualIncentive_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ManualIncentive_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.IncentiveVsEntities",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_IncentiveVsEntity_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_IncentiveVsEntity_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.IncentiveExcludedLists",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_IncentiveExcludedList_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_IncentiveExcludedList_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EmployeeVsSalaryTags",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeVsSalaryTag_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeVsSalaryTag_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.IncentiveCategories",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_IncentiveCategory_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_IncentiveCategory_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.IncentiveTags",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_IncentiveTag_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_IncentiveTag_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EmployeeVsIncentives",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeVsIncentive_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeVsIncentive_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EmployeeDepartmentMasters",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeDepartmentMaster_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeDepartmentMaster_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EmployeeResidentStatuses",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeResidentStatus_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeResidentStatus_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EmployeeReligions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeReligion_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeReligion_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EmployeeRaces",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeRace_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeRace_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EmployeeCostCentreMasters",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EmployeeCostCentreMaster_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EmployeeCostCentreMaster_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            CreateIndex("dbo.WorkDays", new[] { "TenantId", "DayOfWeekShortName" }, unique: true, name: "DayOfWeekShortName_Tenant");
            CreateIndex("dbo.WorkDays", new[] { "TenantId", "DayOfWeekRefName" }, unique: true, name: "TenantId_DayOfWeekRefName");
            CreateIndex("dbo.WorkDays", new[] { "TenantId", "DayOfWeekRefId" }, unique: true, name: "TenantId_DayOfWeekRefId");
        }
    }
}
