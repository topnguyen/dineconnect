namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_QuoteHeader : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CaterQuotationHeaders", "LocationRefId", c => c.Int(nullable: false,defaultValue:1));
            AddColumn("dbo.CaterQuotationHeaders", "QuoteTotalAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            CreateIndex("dbo.CaterQuotationHeaders", "LocationRefId");
            AddForeignKey("dbo.CaterQuotationHeaders", "LocationRefId", "dbo.Locations", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CaterQuotationHeaders", "LocationRefId", "dbo.Locations");
            DropIndex("dbo.CaterQuotationHeaders", new[] { "LocationRefId" });
            DropColumn("dbo.CaterQuotationHeaders", "QuoteTotalAmount");
            DropColumn("dbo.CaterQuotationHeaders", "LocationRefId");
        }
    }
}
