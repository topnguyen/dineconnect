namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_SalesOrder_Delivery_SalesInvoice : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SalesDeliveryOrderDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DeliveryRefId = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        DeliveryQty = c.Decimal(nullable: false, precision: 18, scale: 3),
                        BillConvertedQty = c.Decimal(nullable: false, precision: 18, scale: 3),
                        UnitRefId = c.Int(nullable: false),
                        MaterialReceivedStatus = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalesDeliveryOrderDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .ForeignKey("dbo.SalesDeliveryOrders", t => t.DeliveryRefId)
                .ForeignKey("dbo.Units", t => t.UnitRefId)
                .Index(t => t.DeliveryRefId)
                .Index(t => t.MaterialRefId)
                .Index(t => t.UnitRefId);
            
            CreateTable(
                "dbo.SalesDeliveryOrders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DeliveryDate = c.DateTime(nullable: false),
                        LocationRefId = c.Int(nullable: false),
                        CustomerRefId = c.Int(nullable: false),
                        DcNumberGivenByCustomer = c.String(nullable: false),
                        IsDcCompleted = c.Boolean(nullable: false),
                        SoReferenceCode = c.String(),
                        SoRefId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalesDeliveryOrder_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalesDeliveryOrder_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customers", t => t.CustomerRefId)
                .ForeignKey("dbo.Locations", t => t.LocationRefId)
                .ForeignKey("dbo.SalesOrders", t => t.SoRefId)
                .Index(t => t.LocationRefId)
                .Index(t => t.CustomerRefId)
                .Index(t => t.SoRefId);
            
            CreateTable(
                "dbo.SalesOrders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SoDate = c.DateTime(nullable: false),
                        SoReferenceCode = c.String(nullable: false),
                        ShippingLocationId = c.Int(nullable: false),
                        QuoteReference = c.String(),
                        CustomerRefId = c.Int(nullable: false),
                        SoNetAmount = c.Decimal(nullable: false, precision: 18, scale: 3),
                        Remarks = c.String(),
                        LocationRefId = c.Int(nullable: false),
                        DeliveryDateExpected = c.DateTime(nullable: false),
                        CreditDays = c.Int(nullable: false),
                        ApprovedTime = c.DateTime(),
                        SoCurrentStatus = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalesOrder_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalesOrder_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customers", t => t.CustomerRefId)
                .ForeignKey("dbo.Locations", t => t.LocationRefId)
                .ForeignKey("dbo.Locations", t => t.ShippingLocationId)
                .Index(t => t.ShippingLocationId)
                .Index(t => t.CustomerRefId)
                .Index(t => t.LocationRefId);
            
            CreateTable(
                "dbo.SalesInvoiceDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SalesInvoiceRefId = c.Int(nullable: false),
                        Sno = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        TotalQty = c.Decimal(nullable: false, precision: 18, scale: 3),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 3),
                        TotalAmount = c.Decimal(nullable: false, precision: 18, scale: 3),
                        DiscountFlag = c.String(nullable: false, maxLength: 1),
                        DiscountPercentage = c.Decimal(nullable: false, precision: 18, scale: 3),
                        DiscountAmount = c.Decimal(nullable: false, precision: 18, scale: 3),
                        UnitRefId = c.Int(nullable: false),
                        SalesTaxAmount = c.Decimal(nullable: false, precision: 18, scale: 3),
                        NetAmount = c.Decimal(nullable: false, precision: 18, scale: 3),
                        Remarks = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalesInvoiceDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalesInvoiceDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .ForeignKey("dbo.SalesInvoices", t => t.SalesInvoiceRefId)
                .ForeignKey("dbo.Units", t => t.UnitRefId)
                .Index(t => t.SalesInvoiceRefId)
                .Index(t => t.MaterialRefId)
                .Index(t => t.UnitRefId);
            
            CreateTable(
                "dbo.SalesInvoices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationRefId = c.Int(nullable: false),
                        InvoiceDate = c.DateTime(nullable: false),
                        InvoiceMode = c.String(nullable: false),
                        AccountDate = c.DateTime(nullable: false),
                        CustomerRefId = c.Int(nullable: false),
                        InvoiceNumber = c.String(nullable: false),
                        DcFromDate = c.DateTime(nullable: false),
                        DcToDate = c.DateTime(nullable: false),
                        TotalDiscountAmount = c.Decimal(nullable: false, precision: 18, scale: 3),
                        TotalShipmentCharges = c.Decimal(nullable: false, precision: 18, scale: 3),
                        RoundedAmount = c.Decimal(nullable: false, precision: 18, scale: 3),
                        InvoiceAmount = c.Decimal(nullable: false, precision: 18, scale: 3),
                        IsDirectSalesInvoice = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalesInvoice_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalesInvoice_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customers", t => t.CustomerRefId)
                .ForeignKey("dbo.Locations", t => t.LocationRefId)
                .Index(t => t.LocationRefId)
                .Index(t => t.CustomerRefId);
            
            CreateTable(
                "dbo.SalesInvoiceTaxDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SalesInvoiceRefId = c.Int(nullable: false),
                        Sno = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        SalesTaxRefId = c.Int(nullable: false),
                        SalesTaxRate = c.Decimal(nullable: false, precision: 18, scale: 3),
                        SalesTaxValue = c.Decimal(nullable: false, precision: 18, scale: 3),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalesInvoiceTaxDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalesInvoiceTaxDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SalesInvoices", t => t.SalesInvoiceRefId)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .ForeignKey("dbo.SalesTaxes", t => t.SalesTaxRefId)
                .Index(t => t.SalesInvoiceRefId)
                .Index(t => t.MaterialRefId)
                .Index(t => t.SalesTaxRefId);
            
            CreateTable(
                "dbo.SalesTaxes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TaxName = c.String(nullable: false, maxLength: 50),
                        Percentage = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TaxCalculationMethod = c.String(nullable: false),
                        SortOrder = c.Int(nullable: false),
                        Rounding = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalesTax_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalesTax_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SalesOrderDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SoRefId = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        QtyOrdered = c.Decimal(nullable: false, precision: 18, scale: 3),
                        QtyReceived = c.Decimal(nullable: false, precision: 18, scale: 3),
                        UnitRefId = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 3),
                        TotalAmount = c.Decimal(nullable: false, precision: 18, scale: 3),
                        DiscountOption = c.String(nullable: false, maxLength: 1),
                        DiscountValue = c.Decimal(nullable: false, precision: 18, scale: 3),
                        TaxAmount = c.Decimal(nullable: false, precision: 18, scale: 3),
                        NetAmount = c.Decimal(nullable: false, precision: 18, scale: 3),
                        SoStatus = c.Boolean(nullable: false),
                        Remarks = c.String(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalesOrderDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalesOrderDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .ForeignKey("dbo.SalesOrders", t => t.SoRefId)
                .ForeignKey("dbo.Units", t => t.UnitRefId)
                .Index(t => t.SoRefId)
                .Index(t => t.MaterialRefId)
                .Index(t => t.UnitRefId);
            
            CreateTable(
                "dbo.SalesOrderTaxDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SoRefId = c.Int(nullable: false),
                        MaterialRefId = c.Int(nullable: false),
                        SalesTaxRefId = c.Int(nullable: false),
                        SalesTaxRate = c.Decimal(nullable: false, precision: 18, scale: 3),
                        SalesTaxValue = c.Decimal(nullable: false, precision: 18, scale: 3),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalesOrderTaxDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalesOrderTaxDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Materials", t => t.MaterialRefId)
                .ForeignKey("dbo.SalesOrders", t => t.SoRefId)
                .ForeignKey("dbo.SalesTaxes", t => t.SalesTaxRefId)
                .Index(t => t.SoRefId)
                .Index(t => t.MaterialRefId)
                .Index(t => t.SalesTaxRefId);
            
       
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProductComboes", "SortOrder", c => c.Int(nullable: false));
            AddColumn("dbo.ProductComboes", "SyncId", c => c.Int(nullable: false));
            DropForeignKey("dbo.SalesOrderTaxDetails", "SalesTaxRefId", "dbo.SalesTaxes");
            DropForeignKey("dbo.SalesOrderTaxDetails", "SoRefId", "dbo.SalesOrders");
            DropForeignKey("dbo.SalesOrderTaxDetails", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.SalesOrderDetails", "UnitRefId", "dbo.Units");
            DropForeignKey("dbo.SalesOrderDetails", "SoRefId", "dbo.SalesOrders");
            DropForeignKey("dbo.SalesOrderDetails", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.SalesInvoiceTaxDetails", "SalesTaxRefId", "dbo.SalesTaxes");
            DropForeignKey("dbo.SalesInvoiceTaxDetails", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.SalesInvoiceTaxDetails", "SalesInvoiceRefId", "dbo.SalesInvoices");
            DropForeignKey("dbo.SalesInvoiceDetails", "UnitRefId", "dbo.Units");
            DropForeignKey("dbo.SalesInvoiceDetails", "SalesInvoiceRefId", "dbo.SalesInvoices");
            DropForeignKey("dbo.SalesInvoices", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.SalesInvoices", "CustomerRefId", "dbo.Customers");
            DropForeignKey("dbo.SalesInvoiceDetails", "MaterialRefId", "dbo.Materials");
            DropForeignKey("dbo.SalesDeliveryOrderDetails", "UnitRefId", "dbo.Units");
            DropForeignKey("dbo.SalesDeliveryOrderDetails", "DeliveryRefId", "dbo.SalesDeliveryOrders");
            DropForeignKey("dbo.SalesDeliveryOrders", "SoRefId", "dbo.SalesOrders");
            DropForeignKey("dbo.SalesOrders", "ShippingLocationId", "dbo.Locations");
            DropForeignKey("dbo.SalesOrders", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.SalesOrders", "CustomerRefId", "dbo.Customers");
            DropForeignKey("dbo.SalesDeliveryOrders", "LocationRefId", "dbo.Locations");
            DropForeignKey("dbo.SalesDeliveryOrders", "CustomerRefId", "dbo.Customers");
            DropForeignKey("dbo.SalesDeliveryOrderDetails", "MaterialRefId", "dbo.Materials");
            DropIndex("dbo.SalesOrderTaxDetails", new[] { "SalesTaxRefId" });
            DropIndex("dbo.SalesOrderTaxDetails", new[] { "MaterialRefId" });
            DropIndex("dbo.SalesOrderTaxDetails", new[] { "SoRefId" });
            DropIndex("dbo.SalesOrderDetails", new[] { "UnitRefId" });
            DropIndex("dbo.SalesOrderDetails", new[] { "MaterialRefId" });
            DropIndex("dbo.SalesOrderDetails", new[] { "SoRefId" });
            DropIndex("dbo.SalesInvoiceTaxDetails", new[] { "SalesTaxRefId" });
            DropIndex("dbo.SalesInvoiceTaxDetails", new[] { "MaterialRefId" });
            DropIndex("dbo.SalesInvoiceTaxDetails", new[] { "SalesInvoiceRefId" });
            DropIndex("dbo.SalesInvoices", new[] { "CustomerRefId" });
            DropIndex("dbo.SalesInvoices", new[] { "LocationRefId" });
            DropIndex("dbo.SalesInvoiceDetails", new[] { "UnitRefId" });
            DropIndex("dbo.SalesInvoiceDetails", new[] { "MaterialRefId" });
            DropIndex("dbo.SalesInvoiceDetails", new[] { "SalesInvoiceRefId" });
            DropIndex("dbo.SalesOrders", new[] { "LocationRefId" });
            DropIndex("dbo.SalesOrders", new[] { "CustomerRefId" });
            DropIndex("dbo.SalesOrders", new[] { "ShippingLocationId" });
            DropIndex("dbo.SalesDeliveryOrders", new[] { "SoRefId" });
            DropIndex("dbo.SalesDeliveryOrders", new[] { "CustomerRefId" });
            DropIndex("dbo.SalesDeliveryOrders", new[] { "LocationRefId" });
            DropIndex("dbo.SalesDeliveryOrderDetails", new[] { "UnitRefId" });
            DropIndex("dbo.SalesDeliveryOrderDetails", new[] { "MaterialRefId" });
            DropIndex("dbo.SalesDeliveryOrderDetails", new[] { "DeliveryRefId" });
        
            DropTable("dbo.SalesOrderTaxDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalesOrderTaxDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalesOrderTaxDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.SalesOrderDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalesOrderDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalesOrderDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.SalesTaxes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalesTax_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalesTax_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.SalesInvoiceTaxDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalesInvoiceTaxDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalesInvoiceTaxDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.SalesInvoices",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalesInvoice_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalesInvoice_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.SalesInvoiceDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalesInvoiceDetail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalesInvoiceDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.SalesOrders",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalesOrder_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalesOrder_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.SalesDeliveryOrders",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalesDeliveryOrder_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_SalesDeliveryOrder_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.SalesDeliveryOrderDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SalesDeliveryOrderDetail_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
