namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_TicketTagGroup : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TicketTagGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SortOrder = c.Int(nullable: false),
                        Locations = c.String(),
                        Departments = c.String(),
                        SaveFreeTags = c.Boolean(nullable: false),
                        FreeTagging = c.Boolean(nullable: false),
                        ForceValue = c.Boolean(nullable: false),
                        AskBeforeCreatingTicket = c.Boolean(nullable: false),
                        DataType = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        SubTagId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketTagGroup_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TicketTagGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TicketTags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        TicketTagGroupId = c.Int(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TicketTagGroups", t => t.TicketTagGroupId, cascadeDelete: true)
                .Index(t => t.TicketTagGroupId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TicketTags", "TicketTagGroupId", "dbo.TicketTagGroups");
            DropIndex("dbo.TicketTags", new[] { "TicketTagGroupId" });
            DropTable("dbo.TicketTags");
            DropTable("dbo.TicketTagGroups",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TicketTagGroup_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TicketTagGroup_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
