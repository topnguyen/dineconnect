namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPaymentTimeTruc : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tickets", "LastPaymentTimeTruc", c => c.DateTime(nullable: false, storeType: "date"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tickets", "LastPaymentTimeTruc");
        }
    }
}
