namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_ReProcess_PostDatatable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PostDatas", "IsReProcess", c => c.Boolean(nullable: false, defaultValue:false)) ;
            AddColumn("dbo.PostDatas", "ReProcessCount", c => c.Int(nullable: false, defaultValue:0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PostDatas", "ReProcessCount");
            DropColumn("dbo.PostDatas", "IsReProcess");
        }
    }
}
