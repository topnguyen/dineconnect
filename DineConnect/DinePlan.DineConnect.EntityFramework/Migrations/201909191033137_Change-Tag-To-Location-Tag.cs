namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeTagToLocationTag : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GiftVoucherTypes", "LocationTag", c => c.Boolean(nullable: false));
            AddColumn("dbo.MenuItems", "LocationTag", c => c.Boolean(nullable: false));
            AddColumn("dbo.PriceTags", "LocationTag", c => c.Boolean(nullable: false));
            AddColumn("dbo.DinePlanUsers", "LocationTag", c => c.Boolean(nullable: false));
            AddColumn("dbo.Departments", "LocationTag", c => c.Boolean(nullable: false));
            AddColumn("dbo.Calculations", "LocationTag", c => c.Boolean(nullable: false));
            AddColumn("dbo.ConnectCardTypes", "LocationTag", c => c.Boolean(nullable: false));
            AddColumn("dbo.ConnectTableGroups", "LocationTag", c => c.Boolean(nullable: false));
            AddColumn("dbo.Promotions", "LocationTag", c => c.Boolean(nullable: false));
            AddColumn("dbo.PaymentTypes", "LocationTag", c => c.Boolean(nullable: false));
            AddColumn("dbo.FullTaxMembers", "LocationTag", c => c.Boolean(nullable: false));
            AddColumn("dbo.FullTaxInvoices", "LocationTag", c => c.Boolean(nullable: false));
            AddColumn("dbo.FutureDateInformations", "LocationTag", c => c.Boolean(nullable: false));
            AddColumn("dbo.OrderTagGroups", "LocationTag", c => c.Boolean(nullable: false));
            AddColumn("dbo.PlanReasons", "LocationTag", c => c.Boolean(nullable: false));
            AddColumn("dbo.ReceiptContents", "LocationTag", c => c.Boolean(nullable: false));
            AddColumn("dbo.ScreenMenus", "LocationTag", c => c.Boolean(nullable: false));
            AddColumn("dbo.SecondDisplays", "LocationTag", c => c.Boolean(nullable: false));
            AddColumn("dbo.TicketTagGroups", "LocationTag", c => c.Boolean(nullable: false));
            AddColumn("dbo.TickMessages", "LocationTag", c => c.Boolean(nullable: false));
            AddColumn("dbo.TillAccounts", "LocationTag", c => c.Boolean(nullable: false));
            DropColumn("dbo.GiftVoucherTypes", "Tag");
            DropColumn("dbo.PriceTags", "Tag");
            DropColumn("dbo.DinePlanUsers", "Tag");
            DropColumn("dbo.Departments", "Tag");
            DropColumn("dbo.Calculations", "Tag");
            DropColumn("dbo.ConnectCardTypes", "Tag");
            DropColumn("dbo.ConnectTableGroups", "Tag");
            DropColumn("dbo.Promotions", "Tag");
            DropColumn("dbo.PaymentTypes", "Tag");
            DropColumn("dbo.FullTaxMembers", "Tag");
            DropColumn("dbo.FullTaxInvoices", "Tag");
            DropColumn("dbo.FutureDateInformations", "Tag");
            DropColumn("dbo.OrderTagGroups", "Tag");
            DropColumn("dbo.PlanReasons", "Tag");
            DropColumn("dbo.ReceiptContents", "Tag");
            DropColumn("dbo.ScreenMenus", "Tag");
            DropColumn("dbo.SecondDisplays", "Tag");
            DropColumn("dbo.TicketTagGroups", "Tag");
            DropColumn("dbo.TickMessages", "Tag");
            DropColumn("dbo.TillAccounts", "Tag");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TillAccounts", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.TickMessages", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.TicketTagGroups", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.SecondDisplays", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.ScreenMenus", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.ReceiptContents", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.PlanReasons", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.OrderTagGroups", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.FutureDateInformations", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.FullTaxInvoices", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.FullTaxMembers", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.PaymentTypes", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.Promotions", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.ConnectTableGroups", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.ConnectCardTypes", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.Calculations", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.Departments", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.DinePlanUsers", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.PriceTags", "Tag", c => c.Boolean(nullable: false));
            AddColumn("dbo.GiftVoucherTypes", "Tag", c => c.Boolean(nullable: false));
            DropColumn("dbo.TillAccounts", "LocationTag");
            DropColumn("dbo.TickMessages", "LocationTag");
            DropColumn("dbo.TicketTagGroups", "LocationTag");
            DropColumn("dbo.SecondDisplays", "LocationTag");
            DropColumn("dbo.ScreenMenus", "LocationTag");
            DropColumn("dbo.ReceiptContents", "LocationTag");
            DropColumn("dbo.PlanReasons", "LocationTag");
            DropColumn("dbo.OrderTagGroups", "LocationTag");
            DropColumn("dbo.FutureDateInformations", "LocationTag");
            DropColumn("dbo.FullTaxInvoices", "LocationTag");
            DropColumn("dbo.FullTaxMembers", "LocationTag");
            DropColumn("dbo.PaymentTypes", "LocationTag");
            DropColumn("dbo.Promotions", "LocationTag");
            DropColumn("dbo.ConnectTableGroups", "LocationTag");
            DropColumn("dbo.ConnectCardTypes", "LocationTag");
            DropColumn("dbo.Calculations", "LocationTag");
            DropColumn("dbo.Departments", "LocationTag");
            DropColumn("dbo.DinePlanUsers", "LocationTag");
            DropColumn("dbo.PriceTags", "LocationTag");
            DropColumn("dbo.MenuItems", "LocationTag");
            DropColumn("dbo.GiftVoucherTypes", "LocationTag");
        }
    }
}
