namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOrderTagstoDeliveryOrder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DeliveryOrders", "OrderTags", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DeliveryOrders", "OrderTags");
        }
    }
}
