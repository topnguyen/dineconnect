namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFreePromotion : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FreePromotionExecutions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FreePromotionId = c.Int(nullable: false),
                        MenuItemId = c.Int(nullable: false),
                        PortionName = c.String(),
                        MenuItemPortionId = c.Int(nullable: false),
                        From = c.Boolean(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FreePromotions", t => t.FreePromotionId, cascadeDelete: true)
                .Index(t => t.FreePromotionId);
            
            CreateTable(
                "dbo.FreePromotions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PromotionId = c.Int(nullable: false),
                        AllFrom = c.Boolean(nullable: false),
                        AllTo = c.Boolean(nullable: false),
                        FromCount = c.Int(nullable: false),
                        ToCount = c.Int(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Promotions", t => t.PromotionId, cascadeDelete: true)
                .Index(t => t.PromotionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FreePromotionExecutions", "FreePromotionId", "dbo.FreePromotions");
            DropForeignKey("dbo.FreePromotions", "PromotionId", "dbo.Promotions");
            DropIndex("dbo.FreePromotions", new[] { "PromotionId" });
            DropIndex("dbo.FreePromotionExecutions", new[] { "FreePromotionId" });
            DropTable("dbo.FreePromotions");
            DropTable("dbo.FreePromotionExecutions");
        }
    }
}
