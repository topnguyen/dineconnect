namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Supplier_SyncId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Suppliers", "SyncId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Suppliers", "SyncId");
        }
    }
}
