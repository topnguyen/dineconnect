namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PriceTagChanges : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.PriceTags", "Location");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PriceTags", "Location", c => c.String());
        }
    }
}
