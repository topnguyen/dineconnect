namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_InterTransfer_UnitRefId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InterTransferDetails", "UnitRefId", c => c.Int(nullable: true));
            Sql("UPDATE InterTransferDetails SET InterTransferDetails.UnitRefId = (SELECT Materials.DefaultUnitId from Materials where InterTransferDetails.MaterialRefId=Materials.ID)");
            AlterColumn("dbo.InterTransferDetails", "UnitRefId", c => c.Int(nullable: false));


            AddColumn("dbo.InterTransferReceivedDetails", "UnitRefId", c => c.Int(nullable: true));
            Sql("UPDATE InterTransferReceivedDetails SET InterTransferReceivedDetails.UnitRefId = (SELECT Materials.DefaultUnitId from Materials where InterTransferReceivedDetails.MaterialRefId=Materials.ID)");
            AlterColumn("dbo.InterTransferReceivedDetails", "UnitRefId", c => c.Int(nullable: false));

            CreateIndex("dbo.InterTransferDetails", "UnitRefId");
            CreateIndex("dbo.InterTransferReceivedDetails", "UnitRefId");
            AddForeignKey("dbo.InterTransferDetails", "UnitRefId", "dbo.Units", "Id");
            AddForeignKey("dbo.InterTransferReceivedDetails", "UnitRefId", "dbo.Units", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InterTransferReceivedDetails", "UnitRefId", "dbo.Units");
            DropForeignKey("dbo.InterTransferDetails", "UnitRefId", "dbo.Units");
            DropIndex("dbo.InterTransferReceivedDetails", new[] { "UnitRefId" });
            DropIndex("dbo.InterTransferDetails", new[] { "UnitRefId" });
            DropColumn("dbo.InterTransferReceivedDetails", "UnitRefId");
            DropColumn("dbo.InterTransferDetails", "UnitRefId");
        }
    }
}
