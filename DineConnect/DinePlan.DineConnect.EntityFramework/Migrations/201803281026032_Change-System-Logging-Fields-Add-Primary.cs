namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeSystemLoggingFieldsAddPrimary : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.SystemLoggings");
            AddColumn("dbo.SystemLoggings", "system_logging_guid", c => c.Guid(nullable: false));
            AddColumn("dbo.SystemLoggings", "entered_date", c => c.DateTime());
            AddColumn("dbo.SystemLoggings", "log_application", c => c.String());
            AlterColumn("dbo.SystemLoggings", "log_date", c => c.String());
            AddPrimaryKey("dbo.SystemLoggings", "system_logging_guid");
            DropColumn("dbo.SystemLoggings", "log_id");

            //Sql(
            //    "ALTER TABLE [dbo].[SystemLoggings] ADD  CONSTRAINT [DF_system_logging_system_logging_guid]  DEFAULT (newid()) FOR [system_logging_guid]");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SystemLoggings", "log_id", c => c.Guid(nullable: false));
            DropPrimaryKey("dbo.SystemLoggings");
            AlterColumn("dbo.SystemLoggings", "log_date", c => c.DateTime(nullable: false));
            DropColumn("dbo.SystemLoggings", "log_application");
            DropColumn("dbo.SystemLoggings", "entered_date");
            DropColumn("dbo.SystemLoggings", "system_logging_guid");
            AddPrimaryKey("dbo.SystemLoggings", "log_id");
        }
    }
}
