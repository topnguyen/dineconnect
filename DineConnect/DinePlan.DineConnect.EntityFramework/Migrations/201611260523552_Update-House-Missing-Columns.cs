namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateHouseMissingColumns : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Requests", "RecipeRefId", "dbo.Materials");
            DropForeignKey("dbo.RequestDetails", "RecipeRefId", "dbo.Materials");
            DropIndex("dbo.Requests", new[] { "RecipeRefId" });
            DropIndex("dbo.RequestDetails", new[] { "RecipeRefId" });
            DropColumn("dbo.Requests", "RecipeRefId");
            DropColumn("dbo.Requests", "RecipeProductionQty");
            DropColumn("dbo.Requests", "MultipleBatchProductionAllowed");
            DropColumn("dbo.RequestDetails", "RecipeRefId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.RequestDetails", "RecipeRefId", c => c.Int());
            AddColumn("dbo.Requests", "MultipleBatchProductionAllowed", c => c.Boolean(nullable: false));
            AddColumn("dbo.Requests", "RecipeProductionQty", c => c.Decimal(precision: 18, scale: 6));
            AddColumn("dbo.Requests", "RecipeRefId", c => c.Int());
            CreateIndex("dbo.RequestDetails", "RecipeRefId");
            CreateIndex("dbo.Requests", "RecipeRefId");
            AddForeignKey("dbo.RequestDetails", "RecipeRefId", "dbo.Materials", "Id");
            AddForeignKey("dbo.Requests", "RecipeRefId", "dbo.Materials", "Id");
        }
    }
}
