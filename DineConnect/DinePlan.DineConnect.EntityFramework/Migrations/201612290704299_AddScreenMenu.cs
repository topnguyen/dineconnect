namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddScreenMenu : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ScreenMenuCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        MostUsedItemsCategory = c.Boolean(nullable: false),
                        ImagePath = c.String(),
                        MainButtonHeight = c.Int(nullable: false),
                        MainButtonColor = c.String(),
                        MainFontSize = c.Double(nullable: false),
                        ColumnCount = c.Int(nullable: false),
                        MenuItemButtonColor = c.String(),
                        MaxItems = c.Int(nullable: false),
                        PageCount = c.Int(nullable: false),
                        WrapText = c.Boolean(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                        ScreenMenu_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ScreenMenus", t => t.ScreenMenu_Id)
                .Index(t => t.ScreenMenu_Id);
            
            CreateTable(
                "dbo.ScreenMenuItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ImagePath = c.String(),
                        ShowAliasName = c.Boolean(nullable: false),
                        AutoSelect = c.Boolean(nullable: false),
                        ButtonColor = c.String(),
                        FontSize = c.Double(nullable: false),
                        SubMenuTag = c.String(),
                        OrderTags = c.String(),
                        ItemPortion = c.String(),
                        MenuItemId = c.Int(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                        ScreenMenuCategory_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MenuItems", t => t.MenuItemId)
                .ForeignKey("dbo.ScreenMenuCategories", t => t.ScreenMenuCategory_Id)
                .Index(t => t.MenuItemId)
                .Index(t => t.ScreenMenuCategory_Id);
            
            CreateTable(
                "dbo.ScreenMenus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Locations = c.String(),
                        CategoryColumnCount = c.Int(nullable: false),
                        CategoryColumnWidthRate = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ScreenMenu_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ScreenMenu_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ScreenMenuCategories", "ScreenMenu_Id", "dbo.ScreenMenus");
            DropForeignKey("dbo.ScreenMenuItems", "ScreenMenuCategory_Id", "dbo.ScreenMenuCategories");
            DropForeignKey("dbo.ScreenMenuItems", "MenuItemId", "dbo.MenuItems");
            DropIndex("dbo.ScreenMenuItems", new[] { "ScreenMenuCategory_Id" });
            DropIndex("dbo.ScreenMenuItems", new[] { "MenuItemId" });
            DropIndex("dbo.ScreenMenuCategories", new[] { "ScreenMenu_Id" });
            DropTable("dbo.ScreenMenus",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ScreenMenu_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ScreenMenu_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.ScreenMenuItems");
            DropTable("dbo.ScreenMenuCategories");
        }
    }
}
