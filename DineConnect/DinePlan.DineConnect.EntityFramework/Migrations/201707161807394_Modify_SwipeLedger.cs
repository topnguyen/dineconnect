namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_SwipeLedger : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SwipeCardLedgers", "TicketId", "dbo.Tickets");
            DropIndex("dbo.SwipeCardLedgers", new[] { "TicketId" });
            AddColumn("dbo.SwipeCardLedgers", "LocationId", c => c.Int());
            AddColumn("dbo.SwipeCardLedgers", "TicketNumber", c => c.String(maxLength: 15));
            CreateIndex("dbo.SwipeCardLedgers", "LocationId");
            AddForeignKey("dbo.SwipeCardLedgers", "LocationId", "dbo.Locations", "Id");
            DropColumn("dbo.SwipeCardLedgers", "TicketId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SwipeCardLedgers", "TicketId", c => c.Int());
            DropForeignKey("dbo.SwipeCardLedgers", "LocationId", "dbo.Locations");
            DropIndex("dbo.SwipeCardLedgers", new[] { "LocationId" });
            DropColumn("dbo.SwipeCardLedgers", "TicketNumber");
            DropColumn("dbo.SwipeCardLedgers", "LocationId");
            CreateIndex("dbo.SwipeCardLedgers", "TicketId");
            AddForeignKey("dbo.SwipeCardLedgers", "TicketId", "dbo.Tickets", "Id");
        }
    }
}
