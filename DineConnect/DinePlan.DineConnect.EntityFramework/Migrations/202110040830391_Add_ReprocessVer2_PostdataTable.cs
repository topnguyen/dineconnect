namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_ReprocessVer2_PostdataTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PostDatas", "ReFail", c => c.Int(nullable: false, defaultValue: 0));
            AddColumn("dbo.PostDatas", "RePass", c => c.Int(nullable: false, defaultValue: 0));
            AddColumn("dbo.PostDatas", "ResultReProcess", c => c.Boolean(nullable: false, defaultValue: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PostDatas", "ResultReProcess");
            DropColumn("dbo.PostDatas", "RePass");
            DropColumn("dbo.PostDatas", "ReFail");
        }
    }
}
