namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFixedPromotion : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FixedPromotions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PromotionId = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                        MenuItemId = c.Int(nullable: false),
                        PromotionValueType = c.Int(nullable: false),
                        PromotionValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Promotions", t => t.PromotionId, cascadeDelete: true)
                .Index(t => t.PromotionId);
            
            AddColumn("dbo.Promotions", "Active", c => c.Boolean(nullable: false));
            AddColumn("dbo.Promotions", "SortOrder", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FixedPromotions", "PromotionId", "dbo.Promotions");
            DropIndex("dbo.FixedPromotions", new[] { "PromotionId" });
            DropColumn("dbo.Promotions", "SortOrder");
            DropColumn("dbo.Promotions", "Active");
            DropTable("dbo.FixedPromotions");
        }
    }
}
