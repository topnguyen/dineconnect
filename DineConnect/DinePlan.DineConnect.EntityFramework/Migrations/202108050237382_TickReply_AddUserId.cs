namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TickReply_AddUserId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TickMessageReplies", "UserId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TickMessageReplies", "UserId");
        }
    }
}
