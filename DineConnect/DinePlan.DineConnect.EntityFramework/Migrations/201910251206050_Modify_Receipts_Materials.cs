namespace DinePlan.DineConnect.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modify_Receipts_Materials : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Materials", "ConvertAsZeroStockWhenClosingStockZero", c => c.Boolean(nullable: false));
            AddColumn("dbo.InwardDirectCredits", "PurchaseOrderReferenceFromOtherErp", c => c.String());
            AddColumn("dbo.InwardDirectCredits", "InvoiceNumberReferenceFromOtherErp", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.InwardDirectCredits", "InvoiceNumberReferenceFromOtherErp");
            DropColumn("dbo.InwardDirectCredits", "PurchaseOrderReferenceFromOtherErp");
            DropColumn("dbo.Materials", "ConvertAsZeroStockWhenClosingStockZero");
        }
    }
}
