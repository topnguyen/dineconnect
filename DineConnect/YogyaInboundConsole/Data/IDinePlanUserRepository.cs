﻿using DinePlan.DineConnect.Connect.Users;

namespace YogyaInboundConsole.Data
{
    public interface IDinePlanUserRepository : IRepository<DinePlanUser, int>
    {
    }
}