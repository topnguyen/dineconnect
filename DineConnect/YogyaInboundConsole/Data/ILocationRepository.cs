﻿using DinePlan.DineConnect.Connect.Master;

namespace YogyaInboundConsole.Data
{
    public interface ILocationRepository : IRepository<Location, int>
    {
    }
}