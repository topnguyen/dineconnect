﻿using DinePlan.DineConnect.Connect.Users;

namespace YogyaInboundConsole.Data
{
    public interface IDinePlanUserRoleRepository : IRepository<DinePlanUserRole, int>
    {
    }
}