﻿using DinePlan.DineConnect.Connect.Master;
using YogyaInboundConsole.Data;
using YogyaInboundConsole.Data.Repositories;

namespace YogyaInBound.Data.Repositories
{
    public class LocationGroupRepository : Repository<LocationGroup, int>, ILocationGroupRepository
    {
        public LocationGroupRepository(YogyaInBoundDbContext context) : base(context)
        {
        }
    }
}