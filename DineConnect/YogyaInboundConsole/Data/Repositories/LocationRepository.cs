﻿using DinePlan.DineConnect.Connect.Master;
using YogyaInboundConsole.Data;
using YogyaInboundConsole.Data.Repositories;

namespace YogyaInBound.Data.Repositories
{
    public class LocationRepository : Repository<Location, int>, ILocationRepository
    {
        public LocationRepository(YogyaInBoundDbContext context) : base(context)
        {
        }
    }
}