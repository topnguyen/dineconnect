﻿using YogyaInboundConsole.Data;

namespace YogyaInBound.Data.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly YogyaInBoundDbContext _context;

        public UnitOfWork(YogyaInBoundDbContext context)
        {
            _context = context;

            Categories = new CategoryRepository(context);
            Departments = new DepartmentRepository(context);
            Locations = new LocationRepository(context);
            LocationGroups = new LocationGroupRepository(context);
            MenuItems = new MenuItemRepository(context);
            DinePlanUsers = new DinePlanUserRepository(context);
            DinePlanUserRoles = new DinePlanUserRoleRepository(context);
        }

        public ICategoryRepository Categories { get; }
        public IDepartmentRepository Departments { get; }
        public ILocationRepository Locations { get; }
        public ILocationGroupRepository LocationGroups { get; }
        public IMenuItemRepository MenuItems { get; }
        public IDinePlanUserRepository DinePlanUsers { get; }
        public IDinePlanUserRoleRepository DinePlanUserRoles { get; }


        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}