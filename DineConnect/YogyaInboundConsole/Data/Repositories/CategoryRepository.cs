﻿using DinePlan.DineConnect.Connect.Master;
using YogyaInboundConsole.Data;
using YogyaInboundConsole.Data.Repositories;

namespace YogyaInBound.Data.Repositories
{
    public class CategoryRepository : Repository<Category, int>, ICategoryRepository
    {
        public CategoryRepository(YogyaInBoundDbContext context) : base(context)
        {
        }
    }
}