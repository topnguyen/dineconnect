﻿using DinePlan.DineConnect.Connect.Menu;
using YogyaInboundConsole.Data;
using YogyaInboundConsole.Data.Repositories;

namespace YogyaInBound.Data.Repositories
{
    public class MenuItemRepository : Repository<MenuItem, int>, IMenuItemRepository
    {
        public MenuItemRepository(YogyaInBoundDbContext context) : base(context)
        {
        }
    }
}