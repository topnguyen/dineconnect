﻿using Abp.Authorization.Users;
using DinePlan.DineConnect.Connect.Users;
using YogyaInboundConsole.Data;
using YogyaInboundConsole.Data.Repositories;

namespace YogyaInBound.Data.Repositories
{
    public class DinePlanUserRoleRepository : Repository<DinePlanUserRole, int>, IDinePlanUserRoleRepository
    {
        public DinePlanUserRoleRepository(YogyaInBoundDbContext context) : base(context)
        {
        }
    }
}