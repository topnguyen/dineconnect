﻿using DinePlan.DineConnect.Connect.Master;
using YogyaInboundConsole.Data;
using YogyaInboundConsole.Data.Repositories;

namespace YogyaInBound.Data.Repositories
{
    public class DepartmentRepository : Repository<Department, int>, IDepartmentRepository
    {
        public DepartmentRepository(YogyaInBoundDbContext context) : base(context)
        {
        }
    }
}