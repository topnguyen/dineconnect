﻿using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Users;
using YogyaInboundConsole.Data;
using YogyaInboundConsole.Data.Repositories;

namespace YogyaInBound.Data.Repositories
{
    public class DinePlanUserRepository : Repository<DinePlanUser, int>, IDinePlanUserRepository
    {
        public DinePlanUserRepository(YogyaInBoundDbContext context) : base(context)
        {
        }
    }
}