﻿using DinePlan.DineConnect.Connect.Menu;

namespace YogyaInboundConsole.Data
{
    public interface IMenuItemRepository : IRepository<MenuItem, int>
    {
    }
}