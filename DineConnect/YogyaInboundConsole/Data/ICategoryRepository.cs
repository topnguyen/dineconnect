﻿using DinePlan.DineConnect.Connect.Master;

namespace YogyaInboundConsole.Data
{
    public interface ICategoryRepository : IRepository<Category, int>
    {
    }
}