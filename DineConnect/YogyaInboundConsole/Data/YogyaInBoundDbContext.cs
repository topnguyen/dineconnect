﻿using System.Configuration;
using System.Data.Entity;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Users;

namespace YogyaInboundConsole.Data
{
    public class YogyaInBoundDbContext : DbContext
    {
        public virtual DbSet<LocationGroup> LocationGroups { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<MenuItem> MenuItems { get; set; }
        public virtual DbSet<MenuItemPortion> MenuItemPortions { get; set; }
        public virtual DbSet<DinePlanUser> DinePlanUsers { get; set; }
        public virtual DbSet<DinePlanUserRole> DinePlanUserRoles{ get; set; }

        public YogyaInBoundDbContext(): base("Default")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Category>().Ignore(e => e.Locations);
            modelBuilder.Entity<LocationGroup>().Ignore(e => e.Locations);
            modelBuilder.Entity<Location>()
                .Ignore(x => x.LocationGroups)
                .Ignore(x => x.LocationTags);

        }
    }
}