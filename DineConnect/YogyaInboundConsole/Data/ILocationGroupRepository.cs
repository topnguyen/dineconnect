﻿using DinePlan.DineConnect.Connect.Master;

namespace YogyaInboundConsole.Data
{
    public interface ILocationGroupRepository : IRepository<LocationGroup, int>
    {
    }
}