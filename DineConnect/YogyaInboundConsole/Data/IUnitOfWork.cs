﻿using System;
using YogyaInBound.Data;

namespace YogyaInboundConsole.Data
{
    public interface IUnitOfWork : IDisposable
    {
        ICategoryRepository Categories { get; }

        IDepartmentRepository Departments { get; }

        ILocationRepository Locations { get; }

        ILocationGroupRepository LocationGroups { get; }

        IMenuItemRepository MenuItems { get; }
        IDinePlanUserRepository DinePlanUsers { get; }
        IDinePlanUserRoleRepository DinePlanUserRoles { get; }


        int Complete();
    }
}