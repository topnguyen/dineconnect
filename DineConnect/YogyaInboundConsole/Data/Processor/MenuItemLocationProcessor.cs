﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master;
using Newtonsoft.Json;
using YogyaInBound.Data.Dto;
using YogyaInBound.Data.Repositories;
using YogyaInboundConsole.Data;

namespace YogyaInBound.Data.Processor
{
    public class MenuItemLocationProcessor
    {
        public static void Process(List<MenuItemLocationDto> groups, IUnitOfWork uow, AppConfig appConfig)
        {
            foreach (var dto in groups)
            {
                var product = uow.MenuItems.Find(a => a.AliasCode.Equals(dto.ProductCode)).LastOrDefault();

                if (product == null)
                {
                    throw new System.Exception("Product Code is not available : "+ dto.ProductCode);
                }

                AddOnMenuItemMargin addOnMargin = new AddOnMenuItemMargin()
                {
                    LocationCode = dto.LocationCode,
                    Participation = dto.Participation,
                    Discount = dto.Participation,
                    Margin = dto.Margin
                };

                AddOnMenuItem addOn = new AddOnMenuItem()
                {
                    Margins = new List<AddOnMenuItemMargin>
                    {
                        addOnMargin
                    }
                };

                AddOnOutput addOnOutput = new AddOnOutput()
                {
                    Yogya = JsonConvert.SerializeObject(addOn)
                };
                bool addUpdate = false;
                if (!string.IsNullOrEmpty(product.AddOns))
                {
                    addOnOutput = JsonConvert.DeserializeObject<AddOnOutput>(product.AddOns);
                    if (addOnOutput != null)
                    {
                        addOn = JsonConvert.DeserializeObject<AddOnMenuItem>(addOnOutput.Yogya);
                        if (addOn != null)
                        {
                            if (addOn.Margins != null && addOn.Margins.Any())
                            {
                                if (dto.OperationFlag == 0)
                                {
                                    var lastAvailable =
                                        addOn.Margins.LastOrDefault(a => a.LocationCode.Equals(dto.LocationCode));

                                    if (lastAvailable != null)
                                    {
                                        addOn.Margins.Remove(lastAvailable);
                                        addOnOutput.Yogya = JsonConvert.SerializeObject(addOn);
                                        product.AddOns = JsonConvert.SerializeObject(addOnOutput);
                                        uow.MenuItems.Update(product);
                                    }
                                }
                                else
                                {
                                    var lastAvailable =
                                        addOn.Margins.LastOrDefault(a => a.LocationCode.Equals(dto.LocationCode));

                                    if (lastAvailable != null)
                                    {
                                        lastAvailable.Participation = dto.Participation;
                                        lastAvailable.LocationCode = dto.LocationCode;
                                        lastAvailable.Discount = dto.Discount;
                                        lastAvailable.Margin = dto.Margin;
                                        addOnOutput.Yogya = JsonConvert.SerializeObject(addOn);
                                        product.AddOns = JsonConvert.SerializeObject(addOnOutput);
                                        uow.MenuItems.Update(product);
                                    }
                                    else
                                    {
                                        addOn.Margins.Add(new AddOnMenuItemMargin()
                                        {
                                            Margin = dto.Margin,
                                            Participation = dto.Participation,
                                            Discount = dto.Discount,
                                            LocationCode = dto.LocationCode
                                        });

                                        addOnOutput.Yogya = JsonConvert.SerializeObject(addOn);
                                        product.AddOns = JsonConvert.SerializeObject(addOnOutput);
                                        uow.MenuItems.Update(product);

                                    }
                                }
                            }
                            else
                            {
                                addUpdate = true;
                            }
                        }
                        else
                        {
                            addUpdate = true;
                        }
                    }

                }
                else
                {
                    addUpdate = true;
                }


                if (addUpdate)
                {
                    if (dto.OperationFlag==1)
                    {
                        addOnMargin = new AddOnMenuItemMargin()
                        {
                            LocationCode = dto.LocationCode,
                            Participation = dto.Participation,
                            Discount = dto.Participation,
                            Margin = dto.Margin
                        };

                        addOn = new AddOnMenuItem()
                        {
                            Margins = new List<AddOnMenuItemMargin>
                            {
                                addOnMargin
                            }
                        };

                        addOnOutput = new AddOnOutput()
                        {
                            Yogya = JsonConvert.SerializeObject(addOn)
                        };

                        product.AddOns = JsonConvert.SerializeObject(addOnOutput);
                        uow.MenuItems.Update(product);
                    }
                }
            }
        }
    }
}
