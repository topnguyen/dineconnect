﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using Newtonsoft.Json;
using YogyaInBound.Data.Dto;
using YogyaInBound.Data.Repositories;
using YogyaInboundConsole.Data;

namespace YogyaInBound.Data.Processor
{
    public class MenuItemProcessor
    {
        public static void Process(List<MenuItemDto> groups, IUnitOfWork uow, AppConfig appConfig)
        {
            foreach (var lgo in groups)
            {
                var locationAvailable = uow.Locations.Find(t => t.ReferenceCode == lgo.LocationCode && t.TenantId == appConfig.Tenant).LastOrDefault();
                if (locationAvailable == null)
                {
                    throw new System.Exception("Location is not available in DineConnect, Location Code: " + lgo.LocationCode);
                }

                var lastCategory = uow.Categories.Find(t => t.Code.Equals(lgo.CategoryCode) && t.TenantId == appConfig.Tenant).LastOrDefault();

                if(lastCategory==null)
                    throw new System.Exception("Category is not available : MenuItem Code : " + lgo.Code);
                
                var lastItem = uow.MenuItems.Find(t => t.AliasCode.Equals(lgo.Code) && t.TenantId == appConfig.Tenant).LastOrDefault();


                AddOnMenuItem addOnItem = new AddOnMenuItem()
                {
                    ArticleType = lgo.ArticleType,
                    Image= lgo.Image
                };
                AddOnOutput addOnOutput = new AddOnOutput()
                {
                    Yogya = JsonConvert.SerializeObject(addOnItem)
                };


                if (lastItem != null)
                {
                    lastItem.Name = lgo.Name;
                    lastItem.IsDeleted = lgo.OperationFlag.Equals(0);
                    lastItem.CategoryId = lastCategory.Id;
                    lastItem.BarCode = lgo.Barcode;
                    lastItem.AddOns = JsonConvert.SerializeObject(addOnOutput);

                    if (lastItem.IsDeleted)
                    {
                        lastItem.DeletionTime = DateTime.Now;
                        lastItem.DeleterUserId = 1;
                    }
                    else
                    {
                        lastItem.DeleterUserId = null;
                        lastItem.DeletionTime = null;
                    }

                    var addL = false;

                    var allLocation = new List<SimpleLocationDto>();
                    if (!lastItem.Group)
                    {
                        if (!string.IsNullOrEmpty(lastItem.Locations))
                        {
                            allLocation = JsonConvert.DeserializeObject<List<SimpleLocationDto>>(lastItem.Locations);
                            if (allLocation.Any() && allLocation.Select(a => a.Id).Contains(locationAvailable.Id))
                            {
                                var item = allLocation.SingleOrDefault(x => x.Id == locationAvailable.Id);
                                if (item == null)
                                    addL = true;
                            }

                            lastItem.Locations = JsonConvert.SerializeObject(allLocation);
                        }else
                        {
                            addL = true;
                        }
                    }

                    if (addL)
                    {
                        allLocation.Add(new SimpleLocationDto()
                        {
                            Id = locationAvailable.Id,
                            Name = locationAvailable.Name
                        });

                        lastItem.Locations = JsonConvert.SerializeObject(allLocation);
                    }

                    if (lastItem.Portions.Any())
                    {
                        var lastPor = lastItem.Portions.Last();
                        lastPor.Price = lgo.Price;
                    }

                    uow.MenuItems.Update(lastItem);
                }
                else
                {
                    if (lgo.OperationFlag.Equals(1))
                    {
                        lastItem = new MenuItem
                        {
                            AliasCode = lgo.Code,
                            Name = lgo.Name,
                            CategoryId = lastCategory.Id,
                            BarCode = lgo.Barcode,
                            AddOns = JsonConvert.SerializeObject(addOnOutput),
                            TenantId = appConfig.Tenant,
                            Portions = new List<MenuItemPortion>
                            {
                                new MenuItemPortion() {Name = "Normal", Price = lgo.Price}
                            },
                            Locations = JsonConvert.SerializeObject(new List<SimpleLocationDto>
                            {
                                new SimpleLocationDto(){Id = locationAvailable.Id, Name = locationAvailable.Name, Code = locationAvailable.Code}
                            }),
                            Oid = appConfig.CompanyId
                        };
                        uow.MenuItems.Add(lastItem);
                    }
                }
            }
        }
    }
}
