﻿using System;
using System.Collections.Generic;
using System.Linq;
using DinePlan.DineConnect.Connect.Master;
using Newtonsoft.Json;
using YogyaInBound;
using YogyaInBound.Data;
using YogyaInBound.Data.Dto;
using YogyaInBound.Data.Processor;

namespace YogyaInboundConsole.Data.Processor
{
    public class LocationProcessor
    {
        public static void Process(List<LocationDto> groups, IUnitOfWork uow, AppConfig appConfig, YogyaInBoundDbContext context)
        {
            foreach (var lgo in groups)
            {
                var locationGroup = uow.LocationGroups.Find(t => t.Code.Equals(lgo.LocationGroupCode) && t.TenantId == appConfig.Tenant).LastOrDefault();

                if (locationGroup != null)
                {
                    var fLocation = uow.Locations.Find(t => t.Code.Equals(lgo.LocationCode) && t.TenantId == appConfig.Tenant).LastOrDefault();
                   
                    AddOnLocation addOnLocation = new AddOnLocation()
                    {
                        Code = lgo.TenantCode,
                        Logo = lgo.Logo
                    };
                    AddOnOutput addOnOutput = new AddOnOutput()
                    {
                        Yogya = JsonConvert.SerializeObject(addOnLocation)
                    };


                    if (fLocation != null)
                    {
                        fLocation.IsDeleted = lgo.OperationFlag.Equals(0); 
                        bool addLg = false;
                        if (fLocation.LocationGroups.Any())
                        {
                            var avaCode = fLocation.LocationGroups.LastOrDefault(a => a.Id == locationGroup.Id);
                            if (avaCode == null)
                            {
                                addLg = true;
                            }
                        }
                        else
                        {
                            addLg = true;
                        }
                        fLocation.Name = lgo.LocationName;
                        fLocation.ReferenceCode = lgo.TenantCode;

                        if(addLg)
                            fLocation.LocationGroups.Add(locationGroup);
                        fLocation.AddOn = JsonConvert.SerializeObject(addOnOutput);

                        if (fLocation.IsDeleted)
                        {
                            fLocation.DeletionTime = DateTime.Now;
                            fLocation.DeleterUserId = 1;
                        }
                        else
                        {
                            fLocation.DeleterUserId = null;
                            fLocation.DeletionTime = null;

                        }

                        uow.Locations.Update(fLocation);
                    }
                    else
                    {
                        if (lgo.OperationFlag.Equals(1))
                        {
                            
                            fLocation = new Location()
                            {
                                Code = lgo.LocationCode,
                                Name = lgo.LocationName,
                                ReferenceCode = lgo.TenantCode,
                                LocationGroups = new List<LocationGroup>{locationGroup},
                                TenantId = appConfig.Tenant,
                                AddOn = JsonConvert.SerializeObject(addOnOutput),
                                CompanyRefId = appConfig.CompanyId
                            };
                            uow.Locations.Add(fLocation);

                            uow.Complete();

                            if (fLocation.Id > 0)
                            {
                                string insQry = "SET IDENTITY_INSERT [dbo].[AbpOrganizationUnits] ON; " +
                                                "INSERT INTO  [dbo].[AbpOrganizationUnits] ([Id],[TenantId],[ParentId],[Code]," +
                                                "[DisplayName],[IsDeleted],[CreationTime],[CreatorUserId]) " +
                                                "VALUES(" + fLocation.Id + "," +appConfig.Tenant.ToString() + ", NULL ,'" + fLocation.Name
                                                + "','" + fLocation.Name + "',0,GETDATE()," + 1 + ");" +
                                                " SET IDENTITY_INSERT [dbo].[AbpOrganizationUnits] OFF; ";
                                context.Database.ExecuteSqlCommand(insQry);
                            }

                            fLocation.OrganizationUnitId = fLocation.Id;
                            uow.Locations.Update(fLocation);
                        }
                    }
                    
                }
                else
                {
                    throw new System.Exception("Location Group is not available : " + lgo.LocationGroupCode);
                }
            }
        }
    }
}
