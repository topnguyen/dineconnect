﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Users;
using YogyaInBound.Data.Dto;
using YogyaInBound.Data.Repositories;
using YogyaInboundConsole.Data;

namespace YogyaInBound.Data.Processor
{
    public class UserProcessor
    {
        public static void Process(List<EmployeeDto> groups, IUnitOfWork uow, AppConfig appConfig)
        {
            foreach (var lgo in groups)
            {
                var lastAvailable = uow.DinePlanUserRoles.Find(t => t.Id == lgo.RoleId && t.TenantId == appConfig.Tenant).LastOrDefault();

                if (lastAvailable == null)
                {
                    throw new System.Exception("UserRole is not available in DineConnect, Role Id : " + lgo.RoleId);
                }
                var userAvaialable = uow.DinePlanUsers.Find(t => t.Name.Equals(lgo.UserName) && t.TenantId == appConfig.Tenant).LastOrDefault();
                if (userAvaialable != null)
                {
                    userAvaialable.Name = lgo.UserName;
                    userAvaialable.IsDeleted = lgo.OperationFlag.Equals(0);
                    userAvaialable.PinCode = lgo.PinCode;
                    userAvaialable.DinePlanUserRoleId = lgo.RoleId;

                    if (userAvaialable.IsDeleted)
                    {
                        userAvaialable.DeletionTime = DateTime.Now;
                        userAvaialable.DeleterUserId = 1;
                    }
                    else
                    {
                        userAvaialable.DeleterUserId = null;
                        userAvaialable.DeletionTime = null;
                    }
                    uow.DinePlanUsers.Update(userAvaialable);
                }
                else
                {
                    if (lgo.OperationFlag.Equals(1))
                    {
                        userAvaialable = new DinePlanUser()
                        {
                            Name = lgo.UserName,
                            PinCode = lgo.PinCode,
                            DinePlanUserRoleId = lastAvailable.Id,
                            TenantId = appConfig.Tenant
                        };
                        uow.DinePlanUsers.Add(userAvaialable);
                    }
                }
            }

        }
    }
}
