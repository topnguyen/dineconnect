﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YogyaInBound.Data.Processor
{
    public class AddOnOutput
    {
        public string Yogya { get; set; }
    }

    public class AddOnLocation{
        public string Code { get; set; }
        public string Logo { get; set; }

    }
    public class AddOnMenuItemMargin{
        public string LocationCode { get; set; }
        public string Margin { get; set; }
        public string Participation { get; set; }
        public string Discount { get; set; }

    }

    public class AddOnMenuItem{
        public string ArticleType { get; set; }
        public string Image { get; set; }

        public List<AddOnMenuItemMargin> Margins { get; set; }

        public AddOnMenuItem()
        {
            Margins = new List<AddOnMenuItemMargin>();
        }

    }
}
