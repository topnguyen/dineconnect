﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master;
using Newtonsoft.Json;
using YogyaInBound.Data.Dto;
using YogyaInBound.Data.Repositories;
using YogyaInboundConsole.Data;

namespace YogyaInBound.Data.Processor
{
    public class UserStoreProcessor
    {
        public static void Process(List<EmployeeStoreDto> groups, IUnitOfWork uow, AppConfig appConfig)
        {
            foreach (var lgo in groups)
            {
                var locationGroupAvailable = uow.LocationGroups.Find(t => t.Code == lgo.LocationGroup && t.TenantId == appConfig.Tenant).LastOrDefault();

                if (locationGroupAvailable == null)
                {
                    throw new System.Exception("LocationGroup is not available in DineConnect, LocationGroup Code: " + lgo.LocationGroup);
                }
                var userAvaialable = uow.DinePlanUsers.Find(t => t.Name.Equals(lgo.UserName) && t.TenantId == appConfig.Tenant).LastOrDefault();
                
                if (userAvaialable == null)
                {
                    throw new System.Exception("User is not available in DineConnect, User Code: " + lgo.UserName);
                }

                if (lgo.OperationFlag==0)
                {
                    if (userAvaialable.Group && !string.IsNullOrEmpty(userAvaialable.Locations))
                    {
                        var allLoca = JsonConvert.DeserializeObject<List<SimpleLocationDto>>(userAvaialable.Locations);
                        if (allLoca.Any() && allLoca.Select(a=>a.Id).Contains(locationGroupAvailable.Id))
                        {
                            var item = allLoca.SingleOrDefault(x => x.Id == locationGroupAvailable.Id);
                            if (item != null)
                                allLoca.Remove(item);
                        }
                        userAvaialable.Locations = JsonConvert.SerializeObject(allLoca);
                    }
                }
                else
                {
                    userAvaialable.Group = true;
                    List<SimpleLocationDto> allLocations = new List<SimpleLocationDto>();
                    bool addLg = false;
                    if (userAvaialable.Group && !string.IsNullOrEmpty(userAvaialable.Locations))
                    {
                        allLocations = JsonConvert.DeserializeObject<List<SimpleLocationDto>>(userAvaialable.Locations);
                        var item = allLocations.SingleOrDefault(x => x.Id == locationGroupAvailable.Id);
                        if (item == null)
                        {
                            addLg = true;
                        }
                    }
                    else
                    {
                        addLg = true;
                    }

                    if (addLg)
                    {
                        allLocations.Add(new SimpleLocationDto()
                        {
                            Id = locationGroupAvailable.Id,
                            Name = locationGroupAvailable.Name
                        });
                    }
                    userAvaialable.Locations = JsonConvert.SerializeObject(allLocations);
                }
                
               
            }

        }
    }
}
