﻿using System;
using System.Collections.Generic;
using System.Linq;
using DinePlan.DineConnect.Connect.Master;
using YogyaInBound;
using YogyaInBound.Data;
using YogyaInBound.Data.Dto;

namespace YogyaInboundConsole.Data.Processor
{
    public class CategoryProcessor
    {
        public static void Process(List<CategoryDto> groups, IUnitOfWork uow, AppConfig appConfig)
        {
            foreach (var lgo in groups)
            {
                var lastAvailable = uow.Categories.Find(t => t.Code.Equals(lgo.Code) && t.TenantId == appConfig.Tenant).LastOrDefault();

                if (lastAvailable != null)
                {
                    lastAvailable.Name = lgo.Name;
                    lastAvailable.IsDeleted = lgo.OperationFlag.Equals(0);

                    if (lastAvailable.IsDeleted)
                    {
                        lastAvailable.DeletionTime = DateTime.Now;
                        lastAvailable.DeleterUserId = 1;
                    }
                    else
                    {
                        lastAvailable.DeleterUserId = null;
                        lastAvailable.DeletionTime = null;
                    }
                    uow.Categories.Update(lastAvailable);
                }
                else
                {
                    if (lgo.OperationFlag.Equals(1))
                    {
                        lastAvailable = new Category()
                        {
                            Code = lgo.Code,
                            Name = lgo.Name,
                            TenantId = appConfig.Tenant,
                            Oid = appConfig.CompanyId
                        };
                        uow.Categories.Add(lastAvailable);
                    }
                }
            }
        }
    }
}
