﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master;
using YogyaInBound.Data.Dto;
using YogyaInboundConsole.Data;

namespace YogyaInBound.Data.Processor
{
    public class DepartmentProcessor
    {
        public static void Process(List<DepartmentDto> groups, IUnitOfWork uow, AppConfig appConfig)
        {
            foreach (var lgo in groups)
            {
                var lastAvailable = uow.Departments.Find(t => t.Code.Equals(lgo.Code) && t.TenantId == appConfig.Tenant).LastOrDefault();

                if (lastAvailable != null)
                {
                    lastAvailable.Name = lgo.Name;
                    lastAvailable.IsDeleted = lgo.OperationFlag.Equals(0);

                    if (lastAvailable.IsDeleted)
                    {
                        lastAvailable.DeletionTime = DateTime.Now;
                        lastAvailable.DeleterUserId = 1;
                    }
                    else
                    {
                        lastAvailable.DeleterUserId = null;
                        lastAvailable.DeletionTime = null;
                    }
                    uow.Departments.Update(lastAvailable);
                }
                else
                {
                    if (lgo.OperationFlag.Equals(1))
                    {
                        lastAvailable = new Department()
                        {
                            Code = lgo.Code,
                            Name = lgo.Name,
                            TenantId = appConfig.Tenant
                        };
                        uow.Departments.Add(lastAvailable);
                    }
                }
            }
        }
    }
}
