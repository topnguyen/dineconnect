﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YogyaInBound.Data.Dto
{
    public class LocationGroupDto
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int OperationFlag { get; set; }

    }

    public class LocationDto
    {
        public int OperationFlag { get; set; }
        public string TenantCode { get; set; }
        public string LocationCode { get; set; }
        public string LocationName { get; set; }
        public string LocationGroupCode { get; set; }
        public string Logo { get; set; }


    }

    public class DepartmentDto
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int OperationFlag { get; set; }

    }

    public class CategoryDto
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int OperationFlag { get; set; }

    }

    public class MenuItemDto
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int OperationFlag { get; set; }
        public string CategoryCode { get; set; }
        public decimal Price { get; set; }
        public decimal TaxPercentage { get; set; }
        public string TaxName { get; set; }
        public string LocationCode  { get; set; }
        public string Barcode  { get; set; }
        public string ArticleType  { get; set; }
        public string Image  { get; set; }

    }
    public class MenuItemDepartmentDto
    {
        public int OperationFlag { get; set; }
        public string LocationCode  { get; set; }        
        public string DepartmentCode  { get; set; }
        public string ProductCode  { get; set; }
        public decimal Price  { get; set; }
        public string Margin  { get; set; }


    }public class MenuItemLocationDto
    {
        public int OperationFlag { get; set; }
        public string LocationCode  { get; set; }        
        public string ProductCode  { get; set; }
        public string Margin  { get; set; }
        public string Participation  { get; set; }
        public string Discount  { get; set; }



    }
    public class EmployeeDto
    {
        public string UserName { get; set; }
        public string PinCode { get; set; }
        public int RoleId { get; set; }
        public int OperationFlag { get; set; }


    }

    public class EmployeeStoreDto
    {
        public int OperationFlag { get; set; }
        public string UserName { get; set; }
        public string LocationGroup { get; set; }

    }

    public class SimpleLocationDto
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }

    }
}
