﻿using DinePlan.DineConnect.Connect.Master;

namespace YogyaInboundConsole.Data
{
    public interface IDepartmentRepository : IRepository<Department, int>
    {
    }
}