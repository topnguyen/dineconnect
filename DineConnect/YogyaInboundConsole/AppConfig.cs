﻿namespace YogyaInBound
{
    public class AppConfig
    {
        public string InputFolder { get; set; }
        public string SuccessFolder { get; set; }
        public string FailFolder { get; set; }
        
        public int Tenant { get; set; }
        public int CompanyId { get; set; }
    }
}