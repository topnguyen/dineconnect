﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Castle.Core.Logging;
using Castle.Windsor;

using YogyaInBound.Data;
using YogyaInBound.Data.Dto;
using YogyaInBound.Data.Exception;
using YogyaInBound.Data.Processor;
using YogyaInboundConsole.Data;
using YogyaInboundConsole.Data.Processor;

namespace YogyaInBound
{
    public class CoreService
    {
        private readonly AppConfig _config;
        private readonly IWindsorContainer _container;
        private readonly YogyaInBoundDbContext _context;

        public CoreService(
            IWindsorContainer container,
            AppConfig config, YogyaInBoundDbContext context)
        {
            _container = container;
            _config = config;
            Logger = NullLogger.Instance;
            _context = context;
        }

        public ILogger Logger { get; set; }


        private void InsertDataToDb(List<string[]> items, string objName, IUnitOfWork uow, string firstIndex)
        {

            if (!string.IsNullOrEmpty(firstIndex))
            {
                if (!objName.Equals(firstIndex))
                {
                    return;
                }
            }

            switch (objName)
            {
                case "location_groups":
                    var groups = items.Select(l => new LocationGroupDto
                    {
                        OperationFlag = Convert.ToInt32(l[0]),
                        Code = l[1],
                        Name = l[2]
                    }).ToList();
                    LocationGroupProcessor.Process(groups, uow,_container.Resolve<AppConfig>());
                    break;

                case "locations":
                    var locations = items.Select(l => new LocationDto
                    {
                        OperationFlag = Convert.ToInt32(l[0]),
                        LocationCode = l[1],
                        TenantCode = l[2],
                        LocationName = l[3],
                        LocationGroupCode = l[4],
                        Logo = l[5]
                        
                    }).ToList();
                    LocationProcessor.Process(locations, uow,_container.Resolve<AppConfig>(),_context);
                    break;


                case "departments":
                    var departments = items.Select(l => new DepartmentDto()
                    {
                        OperationFlag = Convert.ToInt32(l[0]),
                        Code = l[1],
                        Name = l[2]
                    }).ToList();
                    DepartmentProcessor.Process(departments, uow,_container.Resolve<AppConfig>());
                    break;

                case "categories":
                    var categories = items.Select(l => new CategoryDto()
                    {
                        OperationFlag = Convert.ToInt32(l[0]),
                        Code = l[1],
                        Name = l[2]
                    }).ToList();
                    CategoryProcessor.Process(categories, uow,_container.Resolve<AppConfig>());
                    break;

                case "products":
                    var menuItems = items.Select(l => new MenuItemDto()
                    {
                        OperationFlag = Convert.ToInt32(l[0]),
                        Code = l[1],
                        Name = l[2],
                        CategoryCode = l[3],
                        Price = Convert.ToDecimal(l[4]),
                        TaxPercentage =Convert.ToDecimal(l[5]),
                        TaxName = l[6],
                        LocationCode = l[7],
                        Barcode = l[8],
                        ArticleType = l[9],
                        Image = l[10],
                    }).ToList();
                    MenuItemProcessor.Process(menuItems, uow,_container.Resolve<AppConfig>());
                    break;
                case "product_locations":
                    var menuItemLocations = items.Select(l => new MenuItemLocationDto()
                    {
                        OperationFlag = Convert.ToInt32(l[0]),
                        LocationCode = l[1],
                        ProductCode = l[2],
                        Margin = l[3],
                        Participation = l[4],
                        Discount = l[5],

                    }).ToList();
                    MenuItemLocationProcessor.Process(menuItemLocations, uow,_container.Resolve<AppConfig>());
                    break;
                case "product_departments ":
                    var menuItemDepartments = items.Select(l => new MenuItemDepartmentDto()
                    {
                        OperationFlag = Convert.ToInt32(l[0]),
                        LocationCode = l[1],
                        DepartmentCode = l[2],
                        ProductCode = l[3],
                        Price = Convert.ToDecimal(l[4]),
                        Margin = l[5]
                    }).ToList();
                    MenuItemDepartmentProcessor.Process(menuItemDepartments, uow,_container.Resolve<AppConfig>());
                    break;

                case "employees":
                    var employees = items.Select(l => new EmployeeDto()
                    {
                        OperationFlag = Convert.ToInt32(l[0]),
                        UserName = l[1],
                        PinCode = l[2],
                        RoleId = Convert.ToInt32(l[3])
                    }).ToList();
                    UserProcessor.Process(employees, uow,_container.Resolve<AppConfig>());
                    break;

                case "employee_stores":
                    var eStores = items.Select(l => new EmployeeStoreDto()
                    {
                        OperationFlag = Convert.ToInt32(l[0]),
                        UserName = l[1],
                        LocationGroup = l[2]
                    }).ToList();
                    UserStoreProcessor.Process(eStores, uow,_container.Resolve<AppConfig>());
                    break;
            }
        }


       

        public void Start()
        {
            Logger.Info("Job Starts");

            for (int i = 1; i < 4; i++)
            {
                Logger.Info("Cycle : " + i);
                var inputFiles = GetFiles();
                Logger.Info($"Processing {inputFiles.Count} files.");

                foreach (var file in inputFiles)
                {
                    try
                    {
                        Logger.Info($"Starting the File {file.Name}");

                        var items = ParseFile(file.FullName);
                        var objName = file.Name.Substring(0, file.Name.LastIndexOf("_"));
                        using (var uow = _container.Resolve<IUnitOfWork>())
                        {
                            InsertDataToDb(items, objName, uow, "");
                            uow.Complete();
                        }
                        MoveFile(file, true);
                        Logger.Info($"Finishing the file {file.Name}");

                    }
                    catch (Exception exception)
                    {
                        Logger.Error($"Error in the file {file.Name}.");
                        Logger.Error(DinePlanLogger.Log(exception));

                        if(i==3)
                            MoveFile(file, false);
                    }
                }
            }
            Logger.Info("Job Ends");
        }

        private List<FileInfo> GetFiles()
        {
            var path = _config.InputFolder;

            return Directory.EnumerateFiles(path, "*.psv")
                .Select(file => new FileInfo(file))
                .ToList();
        }

        public List<string[]> ParseFile(string path)
        {
            var data = File.ReadAllLines(path)
                .Select(line => line.Split('|'))
                .ToList();

            return data;
        }

        private void MoveFile(FileInfo file, bool isSuccess)
        {
            var folderPath = isSuccess ? _config.SuccessFolder : _config.FailFolder;
            var filePath = $@"{folderPath}\{file.Name}";
            if(File.Exists(filePath))
            {
                var guiString = Guid.NewGuid().ToString("N");
                filePath = $@"{folderPath}\{guiString}-{file.Name}";

            }
            File.Move(file.FullName, filePath);
        }
    }
}