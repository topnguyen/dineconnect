﻿using System;
using System.Configuration;
using System.IO;
using Castle.Core.Logging;
using Castle.Facilities.Logging;
using Castle.MicroKernel.Registration;
using Castle.Services.Logging.Log4netIntegration;
using Castle.Windsor;
using YogyaInBound;
using YogyaInBound.Data;
using YogyaInBound.Data.Repositories;
using YogyaInboundConsole.Data;

namespace YogyaInboundConsole

{
    public class Program
    {
        private static void Main(string[] args)
        {
            var container = new WindsorContainer();
            container.AddFacility<LoggingFacility>(f => f.LogUsing<Log4netFactory>());

            var logger = container.Resolve<ILogger>();

            var appConfig = GetAppConfig(logger);

            container.Register(
                Component.For<CoreService>().LifestyleSingleton(),
                Component.For<AppConfig>().Instance(appConfig).LifestyleSingleton(),
                Component.For<IWindsorContainer>().Instance(container).LifestyleSingleton(),
                Component.For<YogyaInBoundDbContext>().LifestyleTransient(),
                Component.For<IUnitOfWork>().ImplementedBy<UnitOfWork>().LifestyleTransient()
            );

            var uow = container.Resolve<CoreService>();
            uow.Start();
        }

        private static AppConfig GetAppConfig(ILogger logger)
        {
            try
            {
                var appConfig = new AppConfig
                {
                    InputFolder = ConfigurationManager.AppSettings["InputFolderLocation"],
                    SuccessFolder = ConfigurationManager.AppSettings["SuccessFolderLocation"],
                    FailFolder = ConfigurationManager.AppSettings["FailFolderLocation"]
                };

                var tenant = ConfigurationManager.AppSettings["TenantId"];
                appConfig.Tenant = int.TryParse(tenant, out var second) ? second : 1;
                appConfig.CompanyId = int.TryParse(tenant, out var third) ? third : 1;

                ValidateFolder(logger, appConfig.InputFolder, "Initial Folder - Available");
                ValidateFolder(logger, appConfig.SuccessFolder, "Completed Folder - Available");
                ValidateFolder(logger, appConfig.FailFolder, "Failure Folder - Available");

                return appConfig;
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                throw;
            }
        }

        private static void ValidateFolder(ILogger logger, string path, string folderName)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            logger.Info($"{folderName}'s information was checked successfully.");
        }
    }
}