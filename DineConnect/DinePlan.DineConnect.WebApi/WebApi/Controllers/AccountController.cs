﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Abp.Authorization.Users;
using Abp.Configuration;
using Abp.UI;
using Abp.Web.Models;
using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Authorization.Roles;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.MultiTenancy;
using DinePlan.DineConnect.WebApi.Models;

namespace DinePlan.DineConnect.WebApi.Controllers
{
    public class AuthenticationAjaxResponse : AjaxResponse
    {
        public AuthenticationAjaxResponse(object result) : base(result)
        {
        }
        public long UserId { get; set; }
        public int TenantId { get; set; }
        public string TimeZoneUtc { get; set; }
        public bool Member { get; set; }



    }
    public class AccountController : DineConnectApiControllerBase
    {
        public static OAuthBearerAuthenticationOptions OAuthBearerOptions { get; private set; }

        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;

        private readonly IUserAppService _userAppService;

        private readonly AbpLoginResultTypeHelper _abpLoginResultTypeHelper;

        static AccountController()
        {
            OAuthBearerOptions = new OAuthBearerAuthenticationOptions();
        }

        public AccountController(
            UserManager userManager, IUserAppService roleAppService,
            AbpLoginResultTypeHelper abpLoginResultTypeHelper, RoleManager roleManager)
        {
            _userManager = userManager;
            _abpLoginResultTypeHelper = abpLoginResultTypeHelper;
            _userAppService = roleAppService;
            _roleManager = roleManager;
        }

        [HttpPost]
        public async Task<AjaxResponse> Authenticate(LoginModel loginModel)
        {
            CheckModelState();

            var loginResult = await GetLoginResultAsync(
                loginModel.UsernameOrEmailAddress,
                loginModel.Password,
                loginModel.TenancyName
                );
            var ticket = new AuthenticationTicket(loginResult.Identity, new AuthenticationProperties());
            var currentUtc = new SystemClock().UtcNow;
            ticket.Properties.IssuedUtc = currentUtc;
            ticket.Properties.ExpiresUtc = currentUtc.Add(TimeSpan.FromMinutes(1000));
            var returnObject =  new AuthenticationAjaxResponse(OAuthBearerOptions.AccessTokenFormat.Protect(ticket));
            

            if (loginResult?.User != null)
            {
                returnObject.UserId = loginResult.User.Id;
                returnObject.TenantId = loginResult.User.TenantId??0;
                returnObject.TimeZoneUtc = SettingManager.GetSettingValue(AppSettings.ConnectSettings.TimeZoneUtc);
            }

            if (loginResult?.User?.TenantId != null)
            {
                var memberRole = await _userAppService.CheckMemberRoleExists(loginResult.User.Id, loginResult.User.TenantId.Value);
                if (memberRole)
                {
                    returnObject.Member = true;
                }
            }

            return returnObject;
        }

        [HttpPost]
        private async Task<AbpUserManager<Tenant, Role, User>.AbpLoginResult> GetLoginResultAsync(string usernameOrEmailAddress, string password, string tenancyName)
        {
            var loginResult = await _userManager.LoginAsync(usernameOrEmailAddress, password, tenancyName);
            switch (loginResult.Result)
            {
                case AbpLoginResultType.Success:
                    return loginResult;
                default:
                    throw _abpLoginResultTypeHelper.CreateExceptionForFailedLoginAttempt(loginResult.Result, usernameOrEmailAddress, tenancyName);
            }
        }
        protected virtual void CheckModelState()
        {
            if (!ModelState.IsValid)
            {
                throw new UserFriendlyException("Invalid request!");
            }
        }
    }
}
