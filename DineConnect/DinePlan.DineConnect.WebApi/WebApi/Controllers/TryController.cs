﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using Abp.Authorization.Users;
using Abp.Web.Models;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Authorization.Roles;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.MultiTenancy;
using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Security;

namespace DinePlan.DineConnect.WebApi.Controllers
{
    public class TryController : DineConnectApiControllerBase
    {
        private readonly AbpLoginResultTypeHelper _abpLoginResultTypeHelper;
        private readonly UserManager _userManager;

        public TryController(
            UserManager userManager,
            AbpLoginResultTypeHelper abpLoginResultTypeHelper)
        {
            _userManager = userManager;
            _abpLoginResultTypeHelper = abpLoginResultTypeHelper;
        }

        private string DEFAULT_USER => "admin";
        private string DEFAULT_PASSWORD => "123qwe";
        private string DEFAULT_TENANCY => "Connect";

        [HttpPost]
        public async Task<AjaxResponse> Hello()
        {
            var loginResult = await GetLoginResultAsync(
                DEFAULT_USER,
                DEFAULT_PASSWORD,
                DEFAULT_TENANCY
                );

            var ticket = new AuthenticationTicket(loginResult.Identity, new AuthenticationProperties());

            var currentUtc = new SystemClock().UtcNow;
            ticket.Properties.IssuedUtc = currentUtc;
            ticket.Properties.ExpiresUtc = currentUtc.Add(TimeSpan.FromMinutes(30));

            return new AjaxResponse(AccountController.OAuthBearerOptions.AccessTokenFormat.Protect(ticket));
        }

        private async Task<AbpUserManager<Tenant, Role, User>.AbpLoginResult> GetLoginResultAsync(
            string usernameOrEmailAddress, string password, string tenancyName)
        {
            var loginResult = await _userManager.LoginAsync(usernameOrEmailAddress, password, tenancyName);

            switch (loginResult.Result)
            {
                case AbpLoginResultType.Success:
                    return loginResult;
                default:
                    throw _abpLoginResultTypeHelper.CreateExceptionForFailedLoginAttempt(loginResult.Result,
                        usernameOrEmailAddress, tenancyName);
            }
        }
    }
}