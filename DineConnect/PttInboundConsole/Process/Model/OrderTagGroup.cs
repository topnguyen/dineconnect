﻿
// NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://pttor.com/i_sap_s4_rt/product/optionproductsmaster_send/")]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://pttor.com/i_sap_s4_rt/product/optionproductsmaster_send/", IsNullable = false)]
public partial class OptionProductMasterSend_MT
{

    private MATOPTION mATOPTIONField;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
    public MATOPTION MATOPTION
    {
        get
        {
            return this.mATOPTIONField;
        }
        set
        {
            this.mATOPTIONField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
public partial class MATOPTION
{

    private string aCTIONCODEField;

    private string mAT_NOField;

    private string sTATUSField;

    private string vALID_FROMField;

    private string vALID_TOField;

    private MATOPTIONMATOPTION_KEY[] mATOPTION_KEYField;

    /// <remarks/>
    public string ACTIONCODE
    {
        get
        {
            return this.aCTIONCODEField;
        }
        set
        {
            this.aCTIONCODEField = value;
        }
    }

    /// <remarks/>
    public string MAT_NO
    {
        get
        {
            return this.mAT_NOField;
        }
        set
        {
            this.mAT_NOField = value;
        }
    }

    /// <remarks/>
    public string STATUS
    {
        get
        {
            return this.sTATUSField;
        }
        set
        {
            this.sTATUSField = value;
        }
    }

    /// <remarks/>
    public string VALID_FROM
    {
        get
        {
            return this.vALID_FROMField;
        }
        set
        {
            this.vALID_FROMField = value;
        }
    }

    /// <remarks/>
    public string VALID_TO
    {
        get
        {
            return this.vALID_TOField;
        }
        set
        {
            this.vALID_TOField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("MATOPTION_KEY")]
    public MATOPTIONMATOPTION_KEY[] MATOPTION_KEY
    {
        get
        {
            return this.mATOPTION_KEYField;
        }
        set
        {
            this.mATOPTION_KEYField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class MATOPTIONMATOPTION_KEY
{

    private string oPT_GRPField;

    private string sEL_ITEMField;

    private string kEYField;

    private MATOPTIONMATOPTION_KEYKEY_DESC[] kEY_DESCField;

    /// <remarks/>
    public string OPT_GRP
    {
        get
        {
            return this.oPT_GRPField;
        }
        set
        {
            this.oPT_GRPField = value;
        }
    }

    /// <remarks/>
    public string SEL_ITEM
    {
        get
        {
            return this.sEL_ITEMField;
        }
        set
        {
            this.sEL_ITEMField = value;
        }
    }

    /// <remarks/>
    public string KEY
    {
        get
        {
            return this.kEYField;
        }
        set
        {
            this.kEYField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("KEY_DESC")]
    public MATOPTIONMATOPTION_KEYKEY_DESC[] KEY_DESC
    {
        get
        {
            return this.kEY_DESCField;
        }
        set
        {
            this.kEY_DESCField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class MATOPTIONMATOPTION_KEYKEY_DESC
{

    private string lANG_KEYField;

    private string oPT_DESCField;

    private string dESC_KEYField;

    /// <remarks/>
    public string LANG_KEY
    {
        get
        {
            return this.lANG_KEYField;
        }
        set
        {
            this.lANG_KEYField = value;
        }
    }

    /// <remarks/>
    public string OPT_DESC
    {
        get
        {
            return this.oPT_DESCField;
        }
        set
        {
            this.oPT_DESCField = value;
        }
    }

    /// <remarks/>
    public string DESC_KEY
    {
        get
        {
            return this.dESC_KEYField;
        }
        set
        {
            this.dESC_KEYField = value;
        }
    }
}

