﻿using System;

namespace PttInboundConsole.Process.Model
{
    public class CategoryModel
    {
        public string OrganizationCode { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Hierarchy { get; set; }

    }

    public class ItemModel
    {
        public string OrganizationCode { get; set; }
        public string Category { get; set; }
        public string Code { get; set; }

        public string Name { get; set; }
        public string AliasName { get; set; }
        public string Uom { get; set; }
        public string Description { get; set; }
        public bool IsWeightProduct { get; set; }
        public bool IsDeleted{ get; set; }
        public int NoTax { get; set; }

        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo{ get; set; }
        public decimal Price { get; set; }
        public decimal IncludedPrice { get; set; }

    }


}
