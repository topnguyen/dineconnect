﻿
// NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://pttor.com/i_sap_s4_rt/master/materialgrouphierarchy_send/")]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://pttor.com/i_sap_s4_rt/master/materialgrouphierarchy_send/", IsNullable = false)]
public partial class MaterialGroupHierarchySend_MT
{

    private WPDWGR01MG_GENERAL[] wPDWGR01Field;

    /// <remarks/>
    [System.Xml.Serialization.XmlArrayAttribute(Namespace = "")]
    [System.Xml.Serialization.XmlArrayItemAttribute("MG_GENERAL", IsNullable = false)]
    public WPDWGR01MG_GENERAL[] WPDWGR01
    {
        get
        {
            return this.wPDWGR01Field;
        }
        set
        {
            this.wPDWGR01Field = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class WPDWGR01MG_GENERAL
{
    private string brandField;

    private string pLANTField;

    private string iNDField;

    private string mGH_CODEField;

    private WPDWGR01MG_GENERALMGH_DESC[] mGH_DESCField;

    private WPDWGR01MG_GENERALMGH_DETAIL mGH_DETAILField;

    /// <remarks/>
    public string PLANT
    {
        get
        {
            return this.pLANTField;
        }
        set
        {
            this.pLANTField = value;
        }
    }
    public string BRAND
    {
        get
        {
            return this.brandField;
        }
        set
        {
            this.brandField = value;
        }
    }
    /// <remarks/>
    public string IND
    {
        get
        {
            return this.iNDField;
        }
        set
        {
            this.iNDField = value;
        }
    }

    /// <remarks/>
    public string MGH_CODE
    {
        get
        {
            return this.mGH_CODEField;
        }
        set
        {
            this.mGH_CODEField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("MGH_DESC")]
    public WPDWGR01MG_GENERALMGH_DESC[] MGH_DESC
    {
        get
        {
            return this.mGH_DESCField;
        }
        set
        {
            this.mGH_DESCField = value;
        }
    }

    /// <remarks/>
    public WPDWGR01MG_GENERALMGH_DETAIL MGH_DETAIL
    {
        get
        {
            return this.mGH_DETAILField;
        }
        set
        {
            this.mGH_DETAILField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class WPDWGR01MG_GENERALMGH_DESC
{

    private string lANGUAGEField;

    private string dESCRIPTIONField;

    private string dESCRIPTION_2Field;

    /// <remarks/>
    public string LANGUAGE
    {
        get
        {
            return this.lANGUAGEField;
        }
        set
        {
            this.lANGUAGEField = value;
        }
    }

    /// <remarks/>
    public string DESCRIPTION
    {
        get
        {
            return this.dESCRIPTIONField;
        }
        set
        {
            this.dESCRIPTIONField = value;
        }
    }

    /// <remarks/>
    public string DESCRIPTION_2
    {
        get
        {
            return this.dESCRIPTION_2Field;
        }
        set
        {
            this.dESCRIPTION_2Field = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class WPDWGR01MG_GENERALMGH_DETAIL
{

    private string lEVELField;

    private string hIER_LVField;

    /// <remarks/>
    public string LEVEL
    {
        get
        {
            return this.lEVELField;
        }
        set
        {
            this.lEVELField = value;
        }
    }

    /// <remarks/>
    public string HIER_LV
    {
        get
        {
            return this.hIER_LVField;
        }
        set
        {
            this.hIER_LVField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
public partial class WPDWGR01
{

    private WPDWGR01MG_GENERAL[] mG_GENERALField;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("MG_GENERAL")]
    public WPDWGR01MG_GENERAL[] MG_GENERAL
    {
        get
        {
            return this.mG_GENERALField;
        }
        set
        {
            this.mG_GENERALField = value;
        }
    }
}

