﻿
// NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://pttor.com/i_sap_s4_rt/master/combomasterdata_send/")]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://pttor.com/i_sap_s4_rt/master/combomasterdata_send/", IsNullable = false)]
public partial class ComboMasterDataSend_MT
{

    private COMBODOWNLOAD cOMBODOWNLOADField;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
    public COMBODOWNLOAD COMBODOWNLOAD
    {
        get
        {
            return this.cOMBODOWNLOADField;
        }
        set
        {
            this.cOMBODOWNLOADField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
public partial class COMBODOWNLOAD
{

    private COMBODOWNLOADCOMBOMAT cOMBOMATField;

    /// <remarks/>
    public COMBODOWNLOADCOMBOMAT COMBOMAT
    {
        get
        {
            return this.cOMBOMATField;
        }
        set
        {
            this.cOMBOMATField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class COMBODOWNLOADCOMBOMAT
{

    private string aCTIONCODEField;

    private string mAT_NOField;

    private string sTATUSField;

    private string vALID_FROMField;

    private string vALID_TOField;

    private COMBODOWNLOADCOMBOMATCOMBO_GRPSET[] cOMBO_GRPSETField;

    /// <remarks/>
    public string ACTIONCODE
    {
        get
        {
            return this.aCTIONCODEField;
        }
        set
        {
            this.aCTIONCODEField = value;
        }
    }

    /// <remarks/>
    public string MAT_NO
    {
        get
        {
            return this.mAT_NOField;
        }
        set
        {
            this.mAT_NOField = value;
        }
    }

    /// <remarks/>
    public string STATUS
    {
        get
        {
            return this.sTATUSField;
        }
        set
        {
            this.sTATUSField = value;
        }
    }

    /// <remarks/>
    public string VALID_FROM
    {
        get
        {
            return this.vALID_FROMField;
        }
        set
        {
            this.vALID_FROMField = value;
        }
    }

    /// <remarks/>
    public string VALID_TO
    {
        get
        {
            return this.vALID_TOField;
        }
        set
        {
            this.vALID_TOField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("COMBO_GRPSET")]
    public COMBODOWNLOADCOMBOMATCOMBO_GRPSET[] COMBO_GRPSET
    {
        get
        {
            return this.cOMBO_GRPSETField;
        }
        set
        {
            this.cOMBO_GRPSETField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class COMBODOWNLOADCOMBOMATCOMBO_GRPSET
{

    private string gRP_SET_NOField;

    private decimal mIN_ORDField;

    private decimal mAX_ORDField;

    private COMBODOWNLOADCOMBOMATCOMBO_GRPSETGRPSET_DESC[] gRPSET_DESCField;

    private COMBODOWNLOADCOMBOMATCOMBO_GRPSETGRPSET_DETAIL[] gRPSET_DETAILField;

    /// <remarks/>
    public string GRP_SET_NO
    {
        get
        {
            return this.gRP_SET_NOField;
        }
        set
        {
            this.gRP_SET_NOField = value;
        }
    }

    /// <remarks/>
    public decimal MIN_ORD
    {
        get
        {
            return this.mIN_ORDField;
        }
        set
        {
            this.mIN_ORDField = value;
        }
    }

    /// <remarks/>
    public decimal MAX_ORD
    {
        get
        {
            return this.mAX_ORDField;
        }
        set
        {
            this.mAX_ORDField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("GRPSET_DESC")]
    public COMBODOWNLOADCOMBOMATCOMBO_GRPSETGRPSET_DESC[] GRPSET_DESC
    {
        get
        {
            return this.gRPSET_DESCField;
        }
        set
        {
            this.gRPSET_DESCField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("GRPSET_DETAIL")]
    public COMBODOWNLOADCOMBOMATCOMBO_GRPSETGRPSET_DETAIL[] GRPSET_DETAIL
    {
        get
        {
            return this.gRPSET_DETAILField;
        }
        set
        {
            this.gRPSET_DETAILField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class COMBODOWNLOADCOMBOMATCOMBO_GRPSETGRPSET_DESC
{

    private string lANG_GRPSETField;

    private string dESC_GRPSETField;

    /// <remarks/>
    public string LANG_GRPSET
    {
        get
        {
            return this.lANG_GRPSETField;
        }
        set
        {
            this.lANG_GRPSETField = value;
        }
    }

    /// <remarks/>
    public string DESC_GRPSET
    {
        get
        {
            return this.dESC_GRPSETField;
        }
        set
        {
            this.dESC_GRPSETField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class COMBODOWNLOADCOMBOMATCOMBO_GRPSETGRPSET_DETAIL
{

    private string mATCPN_NOField;

    private decimal mIN_ORDField;

    private decimal mAX_ORDField;

    private string uNITField;

    private string aDDI_PRICEField;

    private string dEFAULTField;

    /// <remarks/>
    public string MATCPN_NO
    {
        get
        {
            return this.mATCPN_NOField;
        }
        set
        {
            this.mATCPN_NOField = value;
        }
    }

    /// <remarks/>
    public decimal MIN_ORD
    {
        get
        {
            return this.mIN_ORDField;
        }
        set
        {
            this.mIN_ORDField = value;
        }
    }

    /// <remarks/>
    public decimal MAX_ORD
    {
        get
        {
            return this.mAX_ORDField;
        }
        set
        {
            this.mAX_ORDField = value;
        }
    }

    /// <remarks/>
    public string UNIT
    {
        get
        {
            return this.uNITField;
        }
        set
        {
            this.uNITField = value;
        }
    }

    /// <remarks/>
    public string ADDI_PRICE
    {
        get
        {
            return this.aDDI_PRICEField;
        }
        set
        {
            this.aDDI_PRICEField = value;
        }
    }

    /// <remarks/>
    public string DEFAULT
    {
        get
        {
            return this.dEFAULTField;
        }
        set
        {
            this.dEFAULTField = value;
        }
    }
}

