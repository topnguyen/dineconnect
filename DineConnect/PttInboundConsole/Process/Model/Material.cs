﻿
// NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
public partial class MaterialDataSend_MT
{

    private MaterialDataSend_MTMATERIAL[] mATERIALDOWNLOADField;

    /// <remarks/>
    [System.Xml.Serialization.XmlArrayItemAttribute("MATERIAL", IsNullable = false)]
    public MaterialDataSend_MTMATERIAL[] MATERIALDOWNLOAD
    {
        get
        {
            return this.mATERIALDOWNLOADField;
        }
        set
        {
            this.mATERIALDOWNLOADField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class MaterialDataSend_MTMATERIAL
{

    private string pLANTCODEField;

    private string mATERIALNUMBERField;

    private string aCTIONField;

    private string mgField;

    private string dISCOUNTFLAGField;

    private string pRICEREQFLAGField;

    private string nOTRETURNFLAGField;

    private string tAXTYPEField;

    private string cOMBOFLAGField;

    private string oRBRANDField;

    private string oRBRAND_DESCRField;

    private MaterialDataSend_MTMATERIALMATERIAL_DESC[] mATERIAL_DESCField;

    private MaterialDataSend_MTMATERIALUNIT_INFO[] uNIT_INFOField;

    /// <remarks/>
    public string PLANTCODE
    {
        get
        {
            return this.pLANTCODEField;
        }
        set
        {
            this.pLANTCODEField = value;
        }
    }

    /// <remarks/>
    public string MATERIALNUMBER
    {
        get
        {
            return this.mATERIALNUMBERField;
        }
        set
        {
            this.mATERIALNUMBERField = value;
        }
    }

    /// <remarks/>
    public string ACTION
    {
        get
        {
            return this.aCTIONField;
        }
        set
        {
            this.aCTIONField = value;
        }
    }

    /// <remarks/>
    public string MG
    {
        get
        {
            return this.mgField;
        }
        set
        {
            this.mgField = value;
        }
    }

    /// <remarks/>
    public string DISCOUNTFLAG
    {
        get
        {
            return this.dISCOUNTFLAGField;
        }
        set
        {
            this.dISCOUNTFLAGField = value;
        }
    }

    /// <remarks/>
    public string PRICEREQFLAG
    {
        get
        {
            return this.pRICEREQFLAGField;
        }
        set
        {
            this.pRICEREQFLAGField = value;
        }
    }

    /// <remarks/>
    public string NOTRETURNFLAG
    {
        get
        {
            return this.nOTRETURNFLAGField;
        }
        set
        {
            this.nOTRETURNFLAGField = value;
        }
    }

    /// <remarks/>
    public string TAXTYPE
    {
        get
        {
            return this.tAXTYPEField;
        }
        set
        {
            this.tAXTYPEField = value;
        }
    }

    /// <remarks/>
    public string COMBOFLAG
    {
        get
        {
            return this.cOMBOFLAGField;
        }
        set
        {
            this.cOMBOFLAGField = value;
        }
    }

    /// <remarks/>
    public string ORBRAND
    {
        get
        {
            return this.oRBRANDField;
        }
        set
        {
            this.oRBRANDField = value;
        }
    }

    /// <remarks/>
    public string ORBRAND_DESCR
    {
        get
        {
            return this.oRBRAND_DESCRField;
        }
        set
        {
            this.oRBRAND_DESCRField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("MATERIAL_DESC")]
    public MaterialDataSend_MTMATERIALMATERIAL_DESC[] MATERIAL_DESC
    {
        get
        {
            return this.mATERIAL_DESCField;
        }
        set
        {
            this.mATERIAL_DESCField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("UNIT_INFO")]
    public MaterialDataSend_MTMATERIALUNIT_INFO[] UNIT_INFO
    {
        get
        {
            return this.uNIT_INFOField;
        }
        set
        {
            this.uNIT_INFOField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class MaterialDataSend_MTMATERIALMATERIAL_DESC
{

    private string dESC_LANGField;

    private string mATERIALDESCField;

    /// <remarks/>
    public string DESC_LANG
    {
        get
        {
            return this.dESC_LANGField;
        }
        set
        {
            this.dESC_LANGField = value;
        }
    }

    /// <remarks/>
    public string MATERIALDESC
    {
        get
        {
            return this.mATERIALDESCField;
        }
        set
        {
            this.mATERIALDESCField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class MaterialDataSend_MTMATERIALUNIT_INFO
{

    private string uOMCODEField;
    private string sALESUNIT;


    private string eANCODEField;

    private bool eANCODEFieldSpecified;

    private MaterialDataSend_MTMATERIALUNIT_INFOSHORTTEXT_DESC[] sHORTTEXT_DESCField;

    private MaterialDataSend_MTMATERIALUNIT_INFOPRICECONDITION[] pRICECONDITIONField;

    private MaterialDataSend_MTMATERIALUNIT_INFOPRICELIST[] pRICELISTField;

    private MaterialDataSend_MTMATERIALUNIT_INFOPRICECOND_SALECHANNEL[] pRICECOND_SALECHANNELField;

    private MaterialDataSend_MTMATERIALUNIT_INFOPRICELIST_SALECHANNEL[] pRICELIST_SALECHANNELField;
    public string SALESUNIT
    {
        get
        {
            return this.sALESUNIT;
        }
        set
        {
            this.sALESUNIT = value;
        }
    }


    /// <remarks/>
    public string UOMCODE
    {
        get
        {
            return this.uOMCODEField;
        }
        set
        {
            this.uOMCODEField = value;
        }
    }

    /// <remarks/>
    public string EANCODE
    {
        get
        {
            return this.eANCODEField;
        }
        set
        {
            this.eANCODEField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool EANCODESpecified
    {
        get
        {
            return this.eANCODEFieldSpecified;
        }
        set
        {
            this.eANCODEFieldSpecified = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("SHORTTEXT_DESC")]
    public MaterialDataSend_MTMATERIALUNIT_INFOSHORTTEXT_DESC[] SHORTTEXT_DESC
    {
        get
        {
            return this.sHORTTEXT_DESCField;
        }
        set
        {
            this.sHORTTEXT_DESCField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("PRICECONDITION")]
    public MaterialDataSend_MTMATERIALUNIT_INFOPRICECONDITION[] PRICECONDITION
    {
        get
        {
            return this.pRICECONDITIONField;
        }
        set
        {
            this.pRICECONDITIONField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("PRICELIST")]
    public MaterialDataSend_MTMATERIALUNIT_INFOPRICELIST[] PRICELIST
    {
        get
        {
            return this.pRICELISTField;
        }
        set
        {
            this.pRICELISTField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("PRICECOND_SALECHANNEL")]
    public MaterialDataSend_MTMATERIALUNIT_INFOPRICECOND_SALECHANNEL[] PRICECOND_SALECHANNEL
    {
        get
        {
            return this.pRICECOND_SALECHANNELField;
        }
        set
        {
            this.pRICECOND_SALECHANNELField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("PRICELIST_SALECHANNEL")]
    public MaterialDataSend_MTMATERIALUNIT_INFOPRICELIST_SALECHANNEL[] PRICELIST_SALECHANNEL
    {
        get
        {
            return this.pRICELIST_SALECHANNELField;
        }
        set
        {
            this.pRICELIST_SALECHANNELField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class MaterialDataSend_MTMATERIALUNIT_INFOSHORTTEXT_DESC
{

    private string sHORTTEXT_LANGField;

    private string sHORTTEXTField;

    /// <remarks/>
    public string SHORTTEXT_LANG
    {
        get
        {
            return this.sHORTTEXT_LANGField;
        }
        set
        {
            this.sHORTTEXT_LANGField = value;
        }
    }

    /// <remarks/>
    public string SHORTTEXT
    {
        get
        {
            return this.sHORTTEXTField;
        }
        set
        {
            this.sHORTTEXTField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class MaterialDataSend_MTMATERIALUNIT_INFOPRICECONDITION
{

    private string cONDITIONTYPEField;

    private string pROMOTIONNOField;

    private string vALID_FROMField;

    private string vALID_TOField;

    private decimal pRICEField;

    private string cURRENCYField;

    /// <remarks/>
    public string CONDITIONTYPE
    {
        get
        {
            return this.cONDITIONTYPEField;
        }
        set
        {
            this.cONDITIONTYPEField = value;
        }
    }

    /// <remarks/>
    public string PROMOTIONNO
    {
        get
        {
            return this.pROMOTIONNOField;
        }
        set
        {
            this.pROMOTIONNOField = value;
        }
    }

    /// <remarks/>
    public string VALID_FROM
    {
        get
        {
            return this.vALID_FROMField;
        }
        set
        {
            this.vALID_FROMField = value;
        }
    }

    /// <remarks/>
    public string VALID_TO
    {
        get
        {
            return this.vALID_TOField;
        }
        set
        {
            this.vALID_TOField = value;
        }
    }

    /// <remarks/>
    public decimal PRICE
    {
        get
        {
            return this.pRICEField;
        }
        set
        {
            this.pRICEField = value;
        }
    }

    /// <remarks/>
    public string CURRENCY
    {
        get
        {
            return this.cURRENCYField;
        }
        set
        {
            this.cURRENCYField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class MaterialDataSend_MTMATERIALUNIT_INFOPRICELIST
{

    private string pL_TYPEField;

    private decimal pL_PRICEField;

    private string pL_CURRENCYField;

    private string pL_VALID_FROMField;

    private string pL_VALID_TOField;

    /// <remarks/>
    public string PL_TYPE
    {
        get
        {
            return this.pL_TYPEField;
        }
        set
        {
            this.pL_TYPEField = value;
        }
    }

    /// <remarks/>
    public decimal PL_PRICE
    {
        get
        {
            return this.pL_PRICEField;
        }
        set
        {
            this.pL_PRICEField = value;
        }
    }

    /// <remarks/>
    public string PL_CURRENCY
    {
        get
        {
            return this.pL_CURRENCYField;
        }
        set
        {
            this.pL_CURRENCYField = value;
        }
    }

    /// <remarks/>
    public string PL_VALID_FROM
    {
        get
        {
            return this.pL_VALID_FROMField;
        }
        set
        {
            this.pL_VALID_FROMField = value;
        }
    }

    /// <remarks/>
    public string PL_VALID_TO
    {
        get
        {
            return this.pL_VALID_TOField;
        }
        set
        {
            this.pL_VALID_TOField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class MaterialDataSend_MTMATERIALUNIT_INFOPRICECOND_SALECHANNEL
{

    private string cONDITIONTYPEField;

    private string sALECHANNELField;

    private decimal pC_SC_PRICEField;

    private string pC_SC_CURRENCYField;

    private string pC_SC_VALID_FROMField;

    private string pC_SC_VALID_TOField;

    /// <remarks/>
    public string CONDITIONTYPE
    {
        get
        {
            return this.cONDITIONTYPEField;
        }
        set
        {
            this.cONDITIONTYPEField = value;
        }
    }

    /// <remarks/>
    public string SALECHANNEL
    {
        get
        {
            return this.sALECHANNELField;
        }
        set
        {
            this.sALECHANNELField = value;
        }
    }

    /// <remarks/>
    public decimal PC_SC_PRICE
    {
        get
        {
            return this.pC_SC_PRICEField;
        }
        set
        {
            this.pC_SC_PRICEField = value;
        }
    }

    /// <remarks/>
    public string PC_SC_CURRENCY
    {
        get
        {
            return this.pC_SC_CURRENCYField;
        }
        set
        {
            this.pC_SC_CURRENCYField = value;
        }
    }

    /// <remarks/>
    public string PC_SC_VALID_FROM
    {
        get
        {
            return this.pC_SC_VALID_FROMField;
        }
        set
        {
            this.pC_SC_VALID_FROMField = value;
        }
    }

    /// <remarks/>
    public string PC_SC_VALID_TO
    {
        get
        {
            return this.pC_SC_VALID_TOField;
        }
        set
        {
            this.pC_SC_VALID_TOField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class MaterialDataSend_MTMATERIALUNIT_INFOPRICELIST_SALECHANNEL
{

    private string pL_TYPEField;

    private string sALECHANNELField;

    private decimal pL_SC_PRICEField;

    private string pL_SC_CURRENCYField;

    private string pL_SC_VALID_FROMField;

    private string pL_SC_VALID_TOField;

    /// <remarks/>
    public string PL_TYPE
    {
        get
        {
            return this.pL_TYPEField;
        }
        set
        {
            this.pL_TYPEField = value;
        }
    }

    /// <remarks/>
    public string SALECHANNEL
    {
        get
        {
            return this.sALECHANNELField;
        }
        set
        {
            this.sALECHANNELField = value;
        }
    }

    /// <remarks/>
    public decimal PL_SC_PRICE
    {
        get
        {
            return this.pL_SC_PRICEField;
        }
        set
        {
            this.pL_SC_PRICEField = value;
        }
    }

    /// <remarks/>
    public string PL_SC_CURRENCY
    {
        get
        {
            return this.pL_SC_CURRENCYField;
        }
        set
        {
            this.pL_SC_CURRENCYField = value;
        }
    }

    /// <remarks/>
    public string PL_SC_VALID_FROM
    {
        get
        {
            return this.pL_SC_VALID_FROMField;
        }
        set
        {
            this.pL_SC_VALID_FROMField = value;
        }
    }

    /// <remarks/>
    public string PL_SC_VALID_TO
    {
        get
        {
            return this.pL_SC_VALID_TOField;
        }
        set
        {
            this.pL_SC_VALID_TOField = value;
        }
    }
}

