﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace PttInboundConsole.Process.Model
{
    [XmlRoot(ElementName="DESCRIPTION")]
    public class DESCRIPTION {
        [XmlElement(ElementName="LANGUAGEKEY")]
        public string LANGUAGEKEY { get; set; }
       
        [XmlElement(ElementName="DESCRIPTION")]
        public string LANGUAGEDESCRIPTION { get; set; }
    }

    [XmlRoot(ElementName="PLANTLIST")]
    public class PLANTLIST {
        [XmlElement(ElementName="PLANT")]
        public string PLANT { get; set; }
    }

    [XmlRoot(ElementName="PLANTGROUP")]
    public class PLANTGROUP {
        [XmlElement(ElementName="PLANTGROUPCODE")]
        public string PLANTGROUPCODE { get; set; }
        [XmlElement(ElementName="ACTIONCODE")]
        public string ACTIONCODE { get; set; }
        [XmlElement(ElementName="PLANTCLASSGROUP")]
        public string PLANTCLASSGROUP { get; set; }
        [XmlElement(ElementName="CLASSSTARTDATE")]
        public string CLASSSTARTDATE { get; set; }
        [XmlElement(ElementName="CLASSENDDATE")]
        public string CLASSENDDATE { get; set; }
        [XmlElement(ElementName="DESCRIPTION")]
        public List<DESCRIPTION> DESCRIPTION { get; set; }
        [XmlElement(ElementName="PLANTLIST")]
        public List<PLANTLIST> PLANTLIST { get; set; }
    }

    [XmlRoot(ElementName="PLANTGROUPDOWNLOAD")]
    public class PLANTGROUPDOWNLOAD {
        [XmlElement(ElementName="PLANTGROUP")]
        public PLANTGROUP PLANTGROUP { get; set; }
    }

}