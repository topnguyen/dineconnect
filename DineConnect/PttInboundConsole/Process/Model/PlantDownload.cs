﻿
// NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://pttor.com/i_sap_s4_rt/master/plantmaster_send/")]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://pttor.com/i_sap_s4_rt/master/plantmaster_send/", IsNullable = false)]
public partial class PlantMasterSend_MT
{

    private PLANT[] pLANTField;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("PLANT", Namespace = "")]
    public PLANT[] PLANT
    {
        get
        {
            return this.pLANTField;
        }
        set
        {
            this.pLANTField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
public partial class PLANT
{

    private string pLANTCODEField;
    private string cLASSField;
    private string vatRegField;
    private string taxId1;
    private string taxId3;


    private string nAME1Field;

    private string nAME2Field;

    private string nAME3Field;

    private string nAME4Field;

    private string aCTIONCODEField;

    private string oPENDATEField;

    private string cLOSEDATEField;

    private string hOUSE_NUM1Field;

    private bool hOUSE_NUM1FieldSpecified;

    private string sTREET1Field;

    private string sTREET2Field;

    private string sTREET3Field;

    private string sTREET4Field;

    private string sTREET5Field;

    private string dISTRICTField;

    private string cITYField;

    private string cOUNTRYField;

    private string pOSTALCODEField;

    private string eMAILField;

    private string tELEPHONEField;

    private string lANGUAGEField;

    private string bLOCKREASField;

    private bool bLOCKREASFieldSpecified;

    private string bLOCKFROMField;

    private string bLOCKTOField;

    private string sHOPCODEField;

    private string pLANTPROFField;

    private string pLANTCATEField;

    private string sHOPIDField;

    private ulong mIDField;

    private string sOLDTOField;

    private string sHIPTOField;

    private PLANTADDR_INTER aDDR_INTERField;

    private PLANTBRANCH[] bRANCHField;

    /// <remarks/>
    public string PLANTCODE
    {
        get
        {
            return this.pLANTCODEField;
        }
        set
        {
            this.pLANTCODEField = value;
        }
    }
    public string VAT_REG
    {
        get
        {
            return this.vatRegField;
        }
        set
        {
            this.vatRegField = value;
        }
    }

    public string TAX_ID1
    {
        get
        {
            return this.taxId1;
        }
        set
        {
            this.taxId1 = value;
        }
    }

    public string TAX_ID3
    {
        get
        {
            return this.taxId3;
        }
        set
        {
            this.taxId3 = value;
        }
    }

    public string CLASS
    {
        get
        {
            return this.cLASSField;
        }
        set
        {
            this.cLASSField = value;
        }
    }


    /// <remarks/>
    public string NAME1
    {
        get
        {
            return this.nAME1Field;
        }
        set
        {
            this.nAME1Field = value;
        }
    }

    /// <remarks/>
    public string NAME2
    {
        get
        {
            return this.nAME2Field;
        }
        set
        {
            this.nAME2Field = value;
        }
    }

    /// <remarks/>
    public string NAME3
    {
        get
        {
            return this.nAME3Field;
        }
        set
        {
            this.nAME3Field = value;
        }
    }

    /// <remarks/>
    public string NAME4
    {
        get
        {
            return this.nAME4Field;
        }
        set
        {
            this.nAME4Field = value;
        }
    }

    /// <remarks/>
    public string ACTIONCODE
    {
        get
        {
            return this.aCTIONCODEField;
        }
        set
        {
            this.aCTIONCODEField = value;
        }
    }

    /// <remarks/>
    public string OPENDATE
    {
        get
        {
            return this.oPENDATEField;
        }
        set
        {
            this.oPENDATEField = value;
        }
    }

    /// <remarks/>
    public string CLOSEDATE
    {
        get
        {
            return this.cLOSEDATEField;
        }
        set
        {
            this.cLOSEDATEField = value;
        }
    }

    /// <remarks/>
    public string HOUSE_NUM1
    {
        get
        {
            return this.hOUSE_NUM1Field;
        }
        set
        {
            this.hOUSE_NUM1Field = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool HOUSE_NUM1Specified
    {
        get
        {
            return this.hOUSE_NUM1FieldSpecified;
        }
        set
        {
            this.hOUSE_NUM1FieldSpecified = value;
        }
    }

    /// <remarks/>
    public string STREET1
    {
        get
        {
            return this.sTREET1Field;
        }
        set
        {
            this.sTREET1Field = value;
        }
    }

    /// <remarks/>
    public string STREET2
    {
        get
        {
            return this.sTREET2Field;
        }
        set
        {
            this.sTREET2Field = value;
        }
    }

    /// <remarks/>
    public string STREET3
    {
        get
        {
            return this.sTREET3Field;
        }
        set
        {
            this.sTREET3Field = value;
        }
    }

    /// <remarks/>
    public string STREET4
    {
        get
        {
            return this.sTREET4Field;
        }
        set
        {
            this.sTREET4Field = value;
        }
    }

    /// <remarks/>
    public string STREET5
    {
        get
        {
            return this.sTREET5Field;
        }
        set
        {
            this.sTREET5Field = value;
        }
    }

    /// <remarks/>
    public string DISTRICT
    {
        get
        {
            return this.dISTRICTField;
        }
        set
        {
            this.dISTRICTField = value;
        }
    }

    /// <remarks/>
    public string CITY
    {
        get
        {
            return this.cITYField;
        }
        set
        {
            this.cITYField = value;
        }
    }

    /// <remarks/>
    public string COUNTRY
    {
        get
        {
            return this.cOUNTRYField;
        }
        set
        {
            this.cOUNTRYField = value;
        }
    }

    /// <remarks/>
    public string POSTALCODE
    {
        get
        {
            return this.pOSTALCODEField;
        }
        set
        {
            this.pOSTALCODEField = value;
        }
    }

    /// <remarks/>
    public string EMAIL
    {
        get
        {
            return this.eMAILField;
        }
        set
        {
            this.eMAILField = value;
        }
    }

    /// <remarks/>
    public string TELEPHONE
    {
        get
        {
            return this.tELEPHONEField;
        }
        set
        {
            this.tELEPHONEField = value;
        }
    }

    /// <remarks/>
    public string LANGUAGE
    {
        get
        {
            return this.lANGUAGEField;
        }
        set
        {
            this.lANGUAGEField = value;
        }
    }

    /// <remarks/>
    public string BLOCKREAS
    {
        get
        {
            return this.bLOCKREASField;
        }
        set
        {
            this.bLOCKREASField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool BLOCKREASSpecified
    {
        get
        {
            return this.bLOCKREASFieldSpecified;
        }
        set
        {
            this.bLOCKREASFieldSpecified = value;
        }
    }

    /// <remarks/>
    public string BLOCKFROM
    {
        get
        {
            return this.bLOCKFROMField;
        }
        set
        {
            this.bLOCKFROMField = value;
        }
    }

    /// <remarks/>
    public string BLOCKTO
    {
        get
        {
            return this.bLOCKTOField;
        }
        set
        {
            this.bLOCKTOField = value;
        }
    }

    /// <remarks/>
    public string SHOPCODE
    {
        get
        {
            return this.sHOPCODEField;
        }
        set
        {
            this.sHOPCODEField = value;
        }
    }

    /// <remarks/>
    public string PLANTPROF
    {
        get
        {
            return this.pLANTPROFField;
        }
        set
        {
            this.pLANTPROFField = value;
        }
    }

    /// <remarks/>
    public string PLANTCATE
    {
        get
        {
            return this.pLANTCATEField;
        }
        set
        {
            this.pLANTCATEField = value;
        }
    }

    /// <remarks/>
    public string SHOPID
    {
        get
        {
            return this.sHOPIDField;
        }
        set
        {
            this.sHOPIDField = value;
        }
    }

    /// <remarks/>
    public ulong MID
    {
        get
        {
            return this.mIDField;
        }
        set
        {
            this.mIDField = value;
        }
    }

    /// <remarks/>
    public string SOLDTO
    {
        get
        {
            return this.sOLDTOField;
        }
        set
        {
            this.sOLDTOField = value;
        }
    }

    /// <remarks/>
    public string SHIPTO
    {
        get
        {
            return this.sHIPTOField;
        }
        set
        {
            this.sHIPTOField = value;
        }
    }

    /// <remarks/>
    public PLANTADDR_INTER ADDR_INTER
    {
        get
        {
            return this.aDDR_INTERField;
        }
        set
        {
            this.aDDR_INTERField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("BRANCH")]
    public PLANTBRANCH[] BRANCH
    {
        get
        {
            return this.bRANCHField;
        }
        set
        {
            this.bRANCHField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class PLANTADDR_INTER
{

    private string nAME1Field;

    private string nAME2Field;

    private bool nAME2FieldSpecified;

    private string nAME3Field;

    private bool nAME3FieldSpecified;

    private string nAME4Field;

    private bool nAME4FieldSpecified;

    private string hOUSE_NUM1Field;

    private bool hOUSE_NUM1FieldSpecified;

    private string sTREET1Field;

    private string sTREET2Field;

    private string sTREET3Field;

    private string sTREET4Field;

    private string sTREET5Field;

    private string dISTRICTField;

    private string cITYField;

    /// <remarks/>
    public string NAME1
    {
        get
        {
            return this.nAME1Field;
        }
        set
        {
            this.nAME1Field = value;
        }
    }

    /// <remarks/>
    public string NAME2
    {
        get
        {
            return this.nAME2Field;
        }
        set
        {
            this.nAME2Field = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool NAME2Specified
    {
        get
        {
            return this.nAME2FieldSpecified;
        }
        set
        {
            this.nAME2FieldSpecified = value;
        }
    }

    /// <remarks/>
    public string NAME3
    {
        get
        {
            return this.nAME3Field;
        }
        set
        {
            this.nAME3Field = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool NAME3Specified
    {
        get
        {
            return this.nAME3FieldSpecified;
        }
        set
        {
            this.nAME3FieldSpecified = value;
        }
    }

    /// <remarks/>
    public string NAME4
    {
        get
        {
            return this.nAME4Field;
        }
        set
        {
            this.nAME4Field = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool NAME4Specified
    {
        get
        {
            return this.nAME4FieldSpecified;
        }
        set
        {
            this.nAME4FieldSpecified = value;
        }
    }

    /// <remarks/>
    public string HOUSE_NUM1
    {
        get
        {
            return this.hOUSE_NUM1Field;
        }
        set
        {
            this.hOUSE_NUM1Field = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool HOUSE_NUM1Specified
    {
        get
        {
            return this.hOUSE_NUM1FieldSpecified;
        }
        set
        {
            this.hOUSE_NUM1FieldSpecified = value;
        }
    }

    /// <remarks/>
    public string STREET1
    {
        get
        {
            return this.sTREET1Field;
        }
        set
        {
            this.sTREET1Field = value;
        }
    }

    /// <remarks/>
    public string STREET2
    {
        get
        {
            return this.sTREET2Field;
        }
        set
        {
            this.sTREET2Field = value;
        }
    }

    /// <remarks/>
    public string STREET3
    {
        get
        {
            return this.sTREET3Field;
        }
        set
        {
            this.sTREET3Field = value;
        }
    }

    /// <remarks/>
    public string STREET4
    {
        get
        {
            return this.sTREET4Field;
        }
        set
        {
            this.sTREET4Field = value;
        }
    }

    /// <remarks/>
    public string STREET5
    {
        get
        {
            return this.sTREET5Field;
        }
        set
        {
            this.sTREET5Field = value;
        }
    }

    /// <remarks/>
    public string DISTRICT
    {
        get
        {
            return this.dISTRICTField;
        }
        set
        {
            this.dISTRICTField = value;
        }
    }

    /// <remarks/>
    public string CITY
    {
        get
        {
            return this.cITYField;
        }
        set
        {
            this.cITYField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class PLANTBRANCH
{

    private string bRANCHCODEField;

    private PLANTBRANCHDESC_BR dESC_BRField;

    private string dEFAULT_BRANCHField;

    private PLANTBRANCHADDR_BR aDDR_BRField;

    /// <remarks/>
    public string BRANCHCODE
    {
        get
        {
            return this.bRANCHCODEField;
        }
        set
        {
            this.bRANCHCODEField = value;
        }
    }

    /// <remarks/>
    public PLANTBRANCHDESC_BR DESC_BR
    {
        get
        {
            return this.dESC_BRField;
        }
        set
        {
            this.dESC_BRField = value;
        }
    }

    /// <remarks/>
    public string DEFAULT_BRANCH
    {
        get
        {
            return this.dEFAULT_BRANCHField;
        }
        set
        {
            this.dEFAULT_BRANCHField = value;
        }
    }

    /// <remarks/>
    public PLANTBRANCHADDR_BR ADDR_BR
    {
        get
        {
            return this.aDDR_BRField;
        }
        set
        {
            this.aDDR_BRField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class PLANTBRANCHDESC_BR
{

    private string bRANCH_LANGKEYField;

    private string bRANCH_DESCField;

    /// <remarks/>
    public string BRANCH_LANGKEY
    {
        get
        {
            return this.bRANCH_LANGKEYField;
        }
        set
        {
            this.bRANCH_LANGKEYField = value;
        }
    }

    /// <remarks/>
    public string BRANCH_DESC
    {
        get
        {
            return this.bRANCH_DESCField;
        }
        set
        {
            this.bRANCH_DESCField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class PLANTBRANCHADDR_BR
{

    private string nAME1Field;

    private string nAME2Field;

    private string nAME3Field;

    private string nAME4Field;

    private string hOUSE_NUM1Field;

    private string sTREET1Field;

    private string sTREET2Field;

    private string sTREET3Field;

    private string sTREET4Field;

    private string sTREET5Field;

    private string dISTRICTField;

    private string cITYField;

    private string pOST_CODE1Field;

    private string cOUNTRYField;

    private PLANTBRANCHADDR_BRADDR_BR_INTER aDDR_BR_INTERField;

    /// <remarks/>
    public string NAME1
    {
        get
        {
            return this.nAME1Field;
        }
        set
        {
            this.nAME1Field = value;
        }
    }

    /// <remarks/>
    public string NAME2
    {
        get
        {
            return this.nAME2Field;
        }
        set
        {
            this.nAME2Field = value;
        }
    }

    /// <remarks/>
    public string NAME3
    {
        get
        {
            return this.nAME3Field;
        }
        set
        {
            this.nAME3Field = value;
        }
    }

    /// <remarks/>
    public string NAME4
    {
        get
        {
            return this.nAME4Field;
        }
        set
        {
            this.nAME4Field = value;
        }
    }

    /// <remarks/>
    public string HOUSE_NUM1
    {
        get
        {
            return this.hOUSE_NUM1Field;
        }
        set
        {
            this.hOUSE_NUM1Field = value;
        }
    }

    /// <remarks/>
    public string STREET1
    {
        get
        {
            return this.sTREET1Field;
        }
        set
        {
            this.sTREET1Field = value;
        }
    }

    /// <remarks/>
    public string STREET2
    {
        get
        {
            return this.sTREET2Field;
        }
        set
        {
            this.sTREET2Field = value;
        }
    }

    /// <remarks/>
    public string STREET3
    {
        get
        {
            return this.sTREET3Field;
        }
        set
        {
            this.sTREET3Field = value;
        }
    }

    /// <remarks/>
    public string STREET4
    {
        get
        {
            return this.sTREET4Field;
        }
        set
        {
            this.sTREET4Field = value;
        }
    }

    /// <remarks/>
    public string STREET5
    {
        get
        {
            return this.sTREET5Field;
        }
        set
        {
            this.sTREET5Field = value;
        }
    }

    /// <remarks/>
    public string DISTRICT
    {
        get
        {
            return this.dISTRICTField;
        }
        set
        {
            this.dISTRICTField = value;
        }
    }

    /// <remarks/>
    public string CITY
    {
        get
        {
            return this.cITYField;
        }
        set
        {
            this.cITYField = value;
        }
    }

    /// <remarks/>
    public string POST_CODE1
    {
        get
        {
            return this.pOST_CODE1Field;
        }
        set
        {
            this.pOST_CODE1Field = value;
        }
    }

    /// <remarks/>
    public string COUNTRY
    {
        get
        {
            return this.cOUNTRYField;
        }
        set
        {
            this.cOUNTRYField = value;
        }
    }

    /// <remarks/>
    public PLANTBRANCHADDR_BRADDR_BR_INTER ADDR_BR_INTER
    {
        get
        {
            return this.aDDR_BR_INTERField;
        }
        set
        {
            this.aDDR_BR_INTERField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class PLANTBRANCHADDR_BRADDR_BR_INTER
{

    private string nAME1Field;

    private string nAME2Field;

    private string nAME3Field;

    private string nAME4Field;

    private string hOUSE_NUM1Field;

    private string sTREET1Field;

    private string sTREET2Field;

    private string sTREET3Field;

    private string sTREET4Field;

    private string sTREET5Field;

    private string dISTRICTField;

    private string cITYField;

    /// <remarks/>
    public string NAME1
    {
        get
        {
            return this.nAME1Field;
        }
        set
        {
            this.nAME1Field = value;
        }
    }

    /// <remarks/>
    public string NAME2
    {
        get
        {
            return this.nAME2Field;
        }
        set
        {
            this.nAME2Field = value;
        }
    }

    /// <remarks/>
    public string NAME3
    {
        get
        {
            return this.nAME3Field;
        }
        set
        {
            this.nAME3Field = value;
        }
    }

    /// <remarks/>
    public string NAME4
    {
        get
        {
            return this.nAME4Field;
        }
        set
        {
            this.nAME4Field = value;
        }
    }

    /// <remarks/>
    public string HOUSE_NUM1
    {
        get
        {
            return this.hOUSE_NUM1Field;
        }
        set
        {
            this.hOUSE_NUM1Field = value;
        }
    }

    /// <remarks/>
    public string STREET1
    {
        get
        {
            return this.sTREET1Field;
        }
        set
        {
            this.sTREET1Field = value;
        }
    }

    /// <remarks/>
    public string STREET2
    {
        get
        {
            return this.sTREET2Field;
        }
        set
        {
            this.sTREET2Field = value;
        }
    }

    /// <remarks/>
    public string STREET3
    {
        get
        {
            return this.sTREET3Field;
        }
        set
        {
            this.sTREET3Field = value;
        }
    }

    /// <remarks/>
    public string STREET4
    {
        get
        {
            return this.sTREET4Field;
        }
        set
        {
            this.sTREET4Field = value;
        }
    }

    /// <remarks/>
    public string STREET5
    {
        get
        {
            return this.sTREET5Field;
        }
        set
        {
            this.sTREET5Field = value;
        }
    }

    /// <remarks/>
    public string DISTRICT
    {
        get
        {
            return this.dISTRICTField;
        }
        set
        {
            this.dISTRICTField = value;
        }
    }

    /// <remarks/>
    public string CITY
    {
        get
        {
            return this.cITYField;
        }
        set
        {
            this.cITYField = value;
        }
    }
}

