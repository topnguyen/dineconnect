using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Formatting = System.Xml.Formatting;

namespace PttInboundConsole.Process.Utility
{
    public static class ObjectSerializer
    {
        public static void SaveSettings(string file, SettingsModel settingsObject)
        {
            var serializer = new JsonSerializer();
            serializer.Converters.Add(new JavaScriptDateTimeConverter());
            serializer.NullValueHandling = NullValueHandling.Include;

            using (var sw = new StreamWriter(file))
            {
                using (JsonWriter writer = new JsonTextWriter(sw))
                {
                    writer.Formatting = (Newtonsoft.Json.Formatting) Formatting.Indented;
                    serializer.Serialize(writer, settingsObject);
                }
            }
        }

        public static string CloneAsString(SettingsModel settingsObject)
        {
            return JsonConvert.SerializeObject(settingsObject);
        }

        public static SettingsModel Clone(SettingsModel settingsObject)
        {
            return JsonConvert.DeserializeObject<SettingsModel>(JsonConvert.SerializeObject(settingsObject)).SetDefault();
        }

        public static SettingsModel LoadSettings(string file)
        {
            try
            {
                var settings = JsonConvert.DeserializeObject<SettingsModel>(File.ReadAllText(file));
                return settings.SetDefault();
            }
            catch
            {
                // ignored
            }

            var setting = new SettingsModel().SetDefault();
            SaveSettings(file, setting);
            return setting;
        }


    }
}