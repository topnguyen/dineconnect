﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DinePlan.DineConnect.Connect.Audit;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.MultiLingual;
using PttDatabaseCommon.Data;
using PttInboundConsole.Data;
using PttInboundConsole.Process.Model;

namespace PttInboundConsole.Process.Executor
{
    public class MaterialGroupExecutor
    {
        public static string Run(IUnitOfWork uow, AppConfig appConfig, Dictionary<string, MaterialGroupHierarchySend_MT> allItems,
            PttInboundDbContext context)
        {
            var reason = "";
            try
            {

                var zeroLevels =
                    allItems.Values.SelectMany(a => a.WPDWGR01).Where(a => (a.MGH_DETAIL == null || string.IsNullOrEmpty(a.MGH_DETAIL.HIER_LV)));

                foreach (var zeroLevel in zeroLevels)
                {

                    var serverLocation = uow.Companies.Find(t => t.Code.Equals(zeroLevel.BRAND)
                                                                 && t.TenantId == appConfig.Tenant).LastOrDefault();

                    if (serverLocation == null)
                    {
                        throw new Exception("Company is not available : " + zeroLevel.BRAND);
                    }
                    int companyId = serverLocation.Id;

                    if (zeroLevel.IND.Equals("DELE"))
                    {
                        var myMessage = DeleteCategoryPgByCode(companyId, appConfig, uow,zeroLevel);
                        if (!string.IsNullOrEmpty(myMessage))
                        {
                            throw new Exception(myMessage);
                        }
                        continue;

                    }

                

                    //Level Zero
                    var zeroLevelGroup =
                        uow.ProductGroups.Find(a => a.Code.Equals(zeroLevel.MGH_CODE)
                                                    && a.TenantId == appConfig.Tenant && a.Oid == companyId)
                            .LastOrDefault();
                    var zeroGroupId = UpdateProductGroup(uow, appConfig, zeroLevel, zeroLevelGroup, null, companyId);
                    if(zeroGroupId==0)
                        continue;
                    //Level One
                    var oneLevels =
                        allItems.Values.SelectMany(a => a.WPDWGR01)
                            .Where(a => a.MGH_DETAIL.HIER_LV!=null && 
                                        a.MGH_DETAIL.HIER_LV.Equals(zeroLevel.MGH_CODE));
                    foreach (var oneLevel in oneLevels)
                    {
                        var oneLevelGroup =
                            uow.ProductGroups.Find(a => a.Code.Equals(oneLevel.MGH_CODE)
                                                        && a.TenantId == appConfig.Tenant &&
                                                        a.Oid == companyId).LastOrDefault();

                        var oneGroupId = UpdateProductGroup(uow, appConfig, oneLevel, oneLevelGroup, zeroGroupId, companyId);
                       
                        if(oneGroupId==0)
                            continue;

                        //Level Two
                        var twoLevels =
                            allItems.Values.SelectMany(a => a.WPDWGR01)
                                .Where(a => a.MGH_DETAIL.HIER_LV!=null && a.MGH_DETAIL.HIER_LV.Equals(oneLevel.MGH_CODE));

                        foreach (var twoLevel in twoLevels)
                        {
                            var twoLevelGroup =
                                uow.ProductGroups.Find(a => a.Code.Equals(twoLevel.MGH_CODE)
                                                            && a.TenantId == appConfig.Tenant &&
                                                            a.Oid == companyId).LastOrDefault();

                            var twoGroupId = UpdateProductGroup(uow, appConfig, twoLevel, twoLevelGroup, oneGroupId, companyId);

                            if(twoGroupId==0)
                                continue;

                            //Level Three
                            var threeLevels =
                                allItems.Values.SelectMany(a => a.WPDWGR01)
                                    .Where(a =>a.MGH_DETAIL.HIER_LV!=null &&  a.MGH_DETAIL.HIER_LV.Equals(twoLevel.MGH_CODE));

                            foreach (var threeLevel in threeLevels)
                            {
                                var threeLevelGroup =
                                    uow.ProductGroups.Find(a => a.Code.Equals(threeLevel.MGH_CODE)
                                                                && a.TenantId == appConfig.Tenant &&
                                                                a.Oid == companyId).LastOrDefault();

                                var threeGroupId = UpdateProductGroup(uow, appConfig, threeLevel, threeLevelGroup,
                                    twoGroupId, companyId);


                                if(threeGroupId==0)
                                    continue;

                                //Level Four
                                var fourLevels =
                                    allItems.Values.SelectMany(a => a.WPDWGR01).Where(a =>
                                        a.MGH_DETAIL.HIER_LV!=null &&  a.MGH_DETAIL.HIER_LV.Equals(threeLevel.MGH_CODE));

                                foreach (var fourLevel in fourLevels)
                                {
                                    var fourLevelGroup =
                                        uow.ProductGroups.Find(a => a.Code.Equals(fourLevel.MGH_CODE)
                                                                    && a.TenantId == appConfig.Tenant &&
                                                                    a.Oid == companyId).LastOrDefault();

                                    var fourGroupId = UpdateProductGroup(uow, appConfig, fourLevel, fourLevelGroup,
                                        threeGroupId, companyId);

                                    if(fourGroupId==0)
                                        continue;

                                    //Level Five
                                    var fiveLevels =
                                        allItems.Values.SelectMany(a => a.WPDWGR01).Where(a =>
                                            a.MGH_DETAIL.HIER_LV!=null &&  a.MGH_DETAIL.HIER_LV.Equals(fourLevel.MGH_CODE));

                                    foreach (var fiveLevel in fiveLevels)
                                    {
                                        var fifthLevelCategory =
                                            uow.Categories.Find(a => a.Code.Equals(fiveLevel.MGH_CODE)
                                                                     && a.TenantId == appConfig.Tenant &&
                                                                     a.Oid == companyId).LastOrDefault();

                                        var categoryId = UpdateCategory(uow, appConfig, fiveLevel, fifthLevelCategory,
                                            fourGroupId, companyId);
                                    }

                                }
                            }
                        }
                    }
                }

                zeroLevels =
                    allItems.Values.SelectMany(a => a.WPDWGR01).Where(a => (a.MGH_DETAIL != null && a.MGH_DETAIL.HIER_LV!=null &&
                                                                            a.MGH_DETAIL.HIER_LV.Equals("00")));

                foreach (var zeroLevel in zeroLevels)
                {

                    var serverLocation = uow.Companies.Find(t => t.Code.Equals(zeroLevel.BRAND)
                                                                 && t.TenantId == appConfig.Tenant).LastOrDefault();

                    if (serverLocation == null)
                    {
                        throw new Exception("Brand is not available : " + zeroLevel.BRAND);
                    }
                    int companyId = serverLocation.Id;

                    if (zeroLevel.IND.Equals("DELE"))
                    {
                        var myMessage = DeleteCategoryPgByCode(companyId, appConfig, uow,zeroLevel);
                        if (!string.IsNullOrEmpty(myMessage))
                        {
                            throw new Exception(myMessage);
                        }
                        continue;

                    }

                    var fifthLevelCategory =
                        uow.Categories.Find(a => a.Code.Equals(zeroLevel.MGH_CODE)
                                                 && a.TenantId == appConfig.Tenant &&
                                                 a.Oid == companyId).LastOrDefault();

                    var categoryId = UpdateCategory(uow, appConfig, zeroLevel, fifthLevelCategory,
                        null, companyId);

                   
                }

            }

            catch (Exception e)
            {
                var myDate = DateTime.Now;
                uow.ExternalLogs.Add(new ExternalLog
                {
                    AuditType =  (int)  ExternalLogType.MaterialGroupInbound,
                    CreatorUserId = appConfig.AppUserId,
                    TenantId = appConfig.Tenant,
                    CreationTime = myDate,
                    LogTime = myDate,
                    ExternalLogTrunc = myDate.Date,
                    LogData = e.Message,
                    LogDescription = FlattenException(e)
                });
                uow.Complete();
                reason = e.Message;
            }
            return reason;
        }

        private static string DeleteCategoryPgByCode(int companyId, AppConfig appConfig, IUnitOfWork uow,WPDWGR01MG_GENERAL zeroLevel)
        {
            if (!string.IsNullOrEmpty(zeroLevel.MGH_CODE))
            {
                var myCategory =
                    uow.Categories.Find(a => a.Code.Equals(zeroLevel.MGH_CODE)
                                             && a.TenantId == appConfig.Tenant &&
                                             a.Oid == companyId).LastOrDefault();

                if (myCategory != null)
                {
                    var myItem =  uow.MenuItems.Find(a => a.CategoryId == myCategory.Id
                                                            && a.TenantId == appConfig.Tenant &&
                                                            a.Oid == companyId).LastOrDefault();
                    if (myItem == null)
                    {

                        myCategory.IsDeleted = true;
                        myCategory.DeleterUserId = appConfig.AppUserId;

                        uow.Categories.Update(myCategory);
                        uow.Complete();

                        return "";
                    }
                    return "MenuItems are available and We cannot delete this Category " + zeroLevel.MGH_CODE;
                }

                var myGroup =
                    uow.ProductGroups.Find(a => a.Code.Equals(zeroLevel.MGH_CODE)
                                                && a.TenantId == appConfig.Tenant &&
                                                a.Oid == companyId).LastOrDefault();

                if (myGroup != null)
                {
                    var myChildGroup  =
                        uow.ProductGroups.Find(a =>a.ParentId!=null && a.ParentId == myGroup.Id
                                                                    && a.TenantId == appConfig.Tenant &&
                                                                    a.Oid == companyId).LastOrDefault();
                    if (myChildGroup == null)
                    {

                        var myChildCategory  =
                            uow.Categories.Find(a =>a.ProductGroupId!=null && a.ProductGroupId.Value == myGroup.Id
                                                                        && a.TenantId == appConfig.Tenant && 
                                                                        a.Oid == companyId).LastOrDefault();

                        if (myChildCategory == null || myChildCategory.IsDeleted)
                        {

                            myGroup.IsDeleted = true;
                            myGroup.DeleterUserId = appConfig.AppUserId;
                            uow.ProductGroups.Update(myGroup);
                            uow.Complete();
                            return "";
                        }
                        return "Category has the Product Group already Linked and Product Group cannot be deleted " + zeroLevel.MGH_CODE;

                    }

                    return "Product group is a parent to the Child and not possible to delete : " + zeroLevel.MGH_CODE;
                }

                return "Not allow to delete the using Group or Category";
            }

            return "No Code is available for Deletion";

        }

        private static int UpdateProductGroup(IUnitOfWork uow, AppConfig appConfig, WPDWGR01MG_GENERAL materialLevel,
            ProductGroup levelGroup, int? groupId, int companyId)
        {
            var myCode = materialLevel.MGH_CODE;
            var thaiLa = materialLevel.MGH_DESC?.LastOrDefault(a => a.LANGUAGE.Equals("TH"));
            if (thaiLa != null)
            {
                myCode = thaiLa.DESCRIPTION;
            }

            if (levelGroup != null)
            {
                if (!string.IsNullOrEmpty(materialLevel.IND) && !materialLevel.IND.Equals("DELE"))
                {
                    levelGroup.Code = materialLevel.MGH_CODE;
                    levelGroup.Name = myCode;
                    levelGroup.ParentId = groupId;
                    levelGroup.IsDeleted = false;
                    levelGroup.DeleterUserId = appConfig.AppUserId;

                }
                else
                {
                    levelGroup.IsDeleted = true;
                    levelGroup.DeleterUserId = appConfig.AppUserId;
                    levelGroup.ParentId = null;
                    
                    uow.ProductGroups.Update(levelGroup);
                    uow.Complete();
                    DeleteCategoryPgReferences(levelGroup.Id, uow, companyId);
                    return 0;
                }
                uow.ProductGroups.Update(levelGroup);
            }
            else
            {
                if (!string.IsNullOrEmpty(materialLevel.IND) && !materialLevel.IND.Equals("DELE"))
                {
                    levelGroup = new ProductGroup
                    {
                        TenantId = appConfig.Tenant,
                        Oid = companyId,
                        Code = materialLevel.MGH_CODE,
                        Name =myCode,
                        ParentId = groupId
                    };
                    uow.ProductGroups.Add(levelGroup);
                }
            }

            uow.Complete();

            if (materialLevel.MGH_DESC != null && materialLevel.MGH_DESC.Any() && levelGroup != null)
                foreach (var mghDesc in materialLevel.MGH_DESC)
                    LanguageExecutor.PushLanguage(mghDesc.LANGUAGE, mghDesc.DESCRIPTION,
                        LanguageDescriptionType.ProductGroup, levelGroup.Id, uow, appConfig);

            return levelGroup != null ? levelGroup.Id : 0;
        }

        private static void DeleteCategoryPgReferences(int? groupId, IUnitOfWork uow, int companyId)
        {

            var allPGroups = uow.ProductGroups.Find(a =>
                a.ParentId != null && a.ParentId == groupId.Value && a.Oid == companyId);

            foreach (var allGroup in allPGroups)
            {
                DeleteCategoryPgReferences(allGroup.Id,uow,companyId);
                
                allGroup.ParentId = null;
                allGroup.IsDeleted = true;
                allGroup.DeletionTime = DateTime.Now;
                uow.ProductGroups.Update(allGroup);
                uow.Complete();
            }

            if (groupId != null)
            {
                var allCategories = uow.Categories.Find(a =>
                    a.ProductGroupId != null && a.ProductGroupId == groupId.Value && a.Oid == companyId);

                foreach (var allCategory in allCategories)
                {
                    allCategory.ProductGroupId = null;
                    uow.Categories.Update(allCategory);
                    uow.Complete();
                }
            }


           
        }

      

        private static int UpdateCategory(IUnitOfWork uow, AppConfig appConfig, WPDWGR01MG_GENERAL materialLevel,
            Category levelCategory, int? groupId, int companyId)
        {

            var myCode = materialLevel.MGH_CODE;
            var thaiLa = materialLevel.MGH_DESC?.LastOrDefault(a => a.LANGUAGE.Equals("TH"));
            if (thaiLa != null)
            {
                myCode = thaiLa.DESCRIPTION;
            }

            if (levelCategory != null)
            {
                if (!string.IsNullOrEmpty(materialLevel.IND) && !materialLevel.IND.Equals("DELE"))
                {
                    levelCategory.Code = materialLevel.MGH_CODE;
                    levelCategory.Name = myCode;
                    levelCategory.ProductGroupId = groupId;
                    levelCategory.IsDeleted = false;
                    levelCategory.DeleterUserId = appConfig.AppUserId;
                }
                else
                {
                    levelCategory.IsDeleted = true;
                    levelCategory.DeleterUserId = appConfig.AppUserId;
                    levelCategory.ProductGroupId = groupId;
                }

                uow.Categories.Update(levelCategory);
            }
            else
            {
                if (!string.IsNullOrEmpty(materialLevel.IND) && !materialLevel.IND.Equals("DELE"))
                {
                    levelCategory = new Category
                    {
                        TenantId = appConfig.Tenant,
                        Oid = companyId,
                        Code = materialLevel.MGH_CODE,
                        Name = myCode,
                        ProductGroupId = groupId
                    };
                    uow.Categories.Add(levelCategory);
                }
            }

            uow.Complete();

            if (materialLevel.MGH_DESC != null && materialLevel.MGH_DESC.Any() && levelCategory != null)
                foreach (var mghDesc in materialLevel.MGH_DESC)
                    LanguageExecutor.PushLanguage(mghDesc.LANGUAGE, mghDesc.DESCRIPTION,
                        LanguageDescriptionType.Category, levelCategory.Id, uow, appConfig);

            return levelCategory != null ? levelCategory.Id : 0;
        }


        private static string FlattenException(Exception exception)
        {
            var stringBuilder = new StringBuilder();

            while (exception != null)
            {
                stringBuilder.AppendLine(exception.Message);
                stringBuilder.AppendLine(exception.StackTrace);

                exception = exception.InnerException;
            }

            return stringBuilder.ToString();
        }
    }
}