﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DinePlan.DineConnect.Connect.Audit;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.MultiLingual;
using PttDatabaseCommon.Data;
using PttInboundConsole.Data;
using PttInboundConsole.Process.Model;

namespace PttInboundConsole.Process.Executor
{
    public class LocationGroupExecutor
    {
        public static string Run(IUnitOfWork uow, AppConfig appConfig, Dictionary<string, PLANTGROUPDOWNLOAD> allItems,
            PttInboundDbContext context)
        {
            var reason = "";

            try
            {
                foreach (var lgo in allItems.Values)

                {
                    var serverItem = uow.LocationGroups.Find(t => t.Code.Equals(lgo.PLANTGROUP.PLANTGROUPCODE)
                                                                      && t.TenantId == appConfig.Tenant)
                        .LastOrDefault();

                    if (!string.IsNullOrEmpty(lgo.PLANTGROUP.ACTIONCODE) && lgo.PLANTGROUP.ACTIONCODE.Equals("DELE"))
                    {
                        if (serverItem != null)
                        {


                            serverItem.IsDeleted = true;
                            serverItem.DeleterUserId = appConfig.AppUserId;
                            serverItem.DeletionTime = DateTime.Now;
                            uow.LocationGroups.Update(serverItem);
                            uow.Complete();

                            var myLocations = serverItem.Locations;

                            foreach (var myLocation in myLocations)
                            {
                                myLocation.IsDeleted = true;
                                myLocation.DeleterUserId = appConfig.AppUserId;
                                myLocation.DeletionTime = DateTime.Now;
                                uow.Locations.Update(myLocation);
                            }
                            uow.Complete();
                        }
                        continue;
                    }

                    bool addLg = false;
                    if (serverItem != null)
                    {

                        serverItem.IsDeleted = false;
                        serverItem.DeleterUserId = null;

                    }
                    else
                    {
                        addLg = true;
                        serverItem = new LocationGroup
                        {
                            Code = lgo.PLANTGROUP.PLANTGROUPCODE,
                            Name = lgo.PLANTGROUP.PLANTGROUPCODE,
                            TenantId = appConfig.Tenant
                        };
                    }

                    
                    var enName =lgo.PLANTGROUP.DESCRIPTION.LastOrDefault(a => a.LANGUAGEKEY != null &&
                                                                              a.LANGUAGEKEY.Equals("EN"));
                    if (enName != null)
                        serverItem.Name = enName.LANGUAGEDESCRIPTION;


                    if (addLg)
                    {
                        uow.LocationGroups.Add(serverItem);
                    }
                    else
                    {
                        uow.LocationGroups.Update(serverItem);
                    }
                    uow.Complete();

                    foreach (var description in lgo.PLANTGROUP.DESCRIPTION)
                    {
                        LanguageExecutor.PushLanguage(description.LANGUAGEKEY, description.LANGUAGEDESCRIPTION,
                            LanguageDescriptionType.LocationGroup, serverItem.Id, uow, appConfig);
                    }

                    foreach (var plant in lgo.PLANTGROUP.PLANTLIST)
                    {
                        var myPlant = uow.Locations.Find(t => t.Code.Equals(plant.PLANT)
                                                              && t.TenantId == appConfig.Tenant).LastOrDefault();
                        if (myPlant != null)
                        {
                            var addLm = true;
                            if (myPlant.LocationGroups.Any())
                            {
                                var myPresent = myPlant.LocationGroups.Where(a => a.Code.Equals(serverItem.Code));
                                if (myPresent.Any()) addLm = false;
                            }

                            if (addLm)
                            {
                                myPlant.LocationGroups.Add(serverItem);
                                uow.Locations.Update(myPlant);
                                uow.Complete();
                            }
                        }
                        else
                        {
                            throw new Exception("Plant is not Available : " + plant.PLANT);
                        }
                    }
                    var myLastGroup = uow.LocationGroups.Find(t => t.Code.Equals(lgo.PLANTGROUP.PLANTGROUPCODE)
                                                                  && t.TenantId == appConfig.Tenant)
                        .LastOrDefault();
                    var allPlanCodesList = lgo.PLANTGROUP.PLANTLIST.Select(a => a.PLANT);

                    if (myLastGroup != null && myLastGroup.Locations.Any())
                    {
                        var allLocationCodesList = myLastGroup.Locations.Select(a => a.Code);

                        List<string> diffList =  allLocationCodesList.Except(allPlanCodesList).ToList();

                        foreach (var diff in diffList)
                        {
                            var myLastCode = myLastGroup.Locations.LastOrDefault(a => a.Code.Equals(diff));
                            if(myLastCode!=null)
                                myLastGroup.Locations.Remove(myLastCode);
                        }

                        uow.LocationGroups.Update(myLastGroup);
                        uow.Complete();
                    }

                }
            }
            catch (Exception e)
            {
                var myDate = DateTime.Now;
                uow.ExternalLogs.Add(new ExternalLog()
                {
                    AuditType =  (int)  ExternalLogType.LocationGroupInbound,
                    CreatorUserId = appConfig.AppUserId,
                    TenantId = appConfig.Tenant,
                    CreationTime =myDate,
                    LogTime = myDate,
                    ExternalLogTrunc = myDate.Date,
                    LogData = e.Message,
                    LogDescription = FlattenException(e)
                });
                uow.Complete();
                reason = e.Message;
            }

            return reason;
        }

        private static string FlattenException(Exception exception)
        {
            var stringBuilder = new StringBuilder();

            while (exception != null)
            {
                stringBuilder.AppendLine(exception.Message);
                stringBuilder.AppendLine(exception.StackTrace);

                exception = exception.InnerException;
            }

            return stringBuilder.ToString();
        }
    }
}