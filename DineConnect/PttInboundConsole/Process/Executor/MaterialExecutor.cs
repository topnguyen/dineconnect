﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using DinePlan.DineConnect.Connect.Audit;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.MultiLingual;
using DinePlan.DineConnect.Connect.OrderTags;
using Newtonsoft.Json;
using PttDatabaseCommon.Data;

namespace PttInboundConsole.Process.Executor
{
    public class MaterialExecutor
    {
        public static string Run(IUnitOfWork uow, AppConfig appConfig, Dictionary<string, MaterialDataSend_MT> allItems,
            PttInboundDbContext context)
        {
            var reason = "";
            try
            {
                foreach (var myItem in allItems.Values)
                foreach (var materialDown in myItem.MATERIALDOWNLOAD)
                {
                    var serverItem = uow.MenuItems.Find(t => t.AliasCode.Equals(materialDown.MATERIALNUMBER)
                                                             && t.TenantId == appConfig.Tenant).LastOrDefault();

                    var serverLocation = uow.Locations.Find(t => t.Code.Equals(materialDown.PLANTCODE)
                                                                 && t.TenantId == appConfig.Tenant).LastOrDefault();
                    if (serverLocation == null)
                        throw new Exception("Plant is not available : " + materialDown.PLANTCODE);
                    var companyId = serverLocation.CompanyRefId;


                    Category category = null;

                    var simLocation = new SimpleLocation
                    {
                        Id = serverLocation.Id,
                        Code = serverLocation.Code,
                        Name = serverLocation.Name
                    };
                    var myListLocation = new List<SimpleLocation>
                    {
                        simLocation
                    };

                    var add = false;

                    if (serverItem == null)
                    {
                        serverItem = new MenuItem {TenantId = appConfig.Tenant, Oid = serverLocation.CompanyRefId};
                        add = true;
                    }

                    if (materialDown.ACTION.Equals("DELE"))
                    {

                        if (serverItem.Category != null)
                        {
                            throw new Exception("Category is available for MenuItem : " + serverItem.Name);
                        }

                        serverItem.IsDeleted = true;
                        serverItem.DeletionTime = DateTime.Now;
                        serverItem.DeleterUserId = 1;
                        uow.MenuItems.Update(serverItem);
                        uow.Complete();

                        var alLTags = uow.OrderTags.Find(a => a.MenuItemId.Equals(serverItem.Id));
                        foreach (var orderTag in alLTags) uow.OrderTags.Remove(orderTag);
                        uow.Complete();

                        return "";
                    }

                    serverItem.IsDeleted = false;
                    serverItem.DeletionTime = DateTime.Now;
                    serverItem.DeleterUserId = 1;

                    if (!string.IsNullOrEmpty(materialDown.MG))
                    {
                        category = uow.Categories.Find(t => t.Code.Equals(materialDown.MG)
                                                            && t.TenantId == appConfig.Tenant && t.Oid == companyId)
                            .LastOrDefault();
                        if (category == null) throw new Exception("Category is not available : " + materialDown.MG);
                    }

                    serverItem.AliasCode = materialDown.MATERIALNUMBER;
                    serverItem.Name = materialDown.MATERIALNUMBER;

                    var enName = materialDown.MATERIAL_DESC.LastOrDefault(a => a.DESC_LANG != null &&
                                                                               a.DESC_LANG.Equals("EN"));
                    if (enName != null)
                        serverItem.Name = enName.MATERIALDESC;


                    if (!string.IsNullOrEmpty(materialDown.COMBOFLAG) && materialDown.COMBOFLAG.Equals("X"))
                        serverItem.ProductType = 2;
                    else
                        serverItem.ProductType = 1;
                    if (!(category is null)) serverItem.CategoryId = category.Id;

                    if (!string.IsNullOrEmpty(materialDown.DISCOUNTFLAG) && materialDown.DISCOUNTFLAG.Equals("X"))
                        serverItem.RestrictPromotion = true;
                    else
                        serverItem.RestrictPromotion = false;

                    if (!string.IsNullOrEmpty(materialDown.PRICEREQFLAG) && materialDown.PRICEREQFLAG.Equals("X"))
                        serverItem.ForceChangePrice = true;
                    else
                        serverItem.ForceChangePrice = false;

                    serverItem.LastModificationTime = DateTime.Now;
                    serverItem.Uom = materialDown.UNIT_INFO.First().UOMCODE;

                    if (!string.IsNullOrEmpty(materialDown.TAXTYPE) && materialDown.TAXTYPE.Equals("0"))
                        serverItem.NoTax = true;
                    else
                        serverItem.NoTax = false;

                    foreach (var uot in materialDown.UNIT_INFO)
                    {
                        MenuItemPortion firstPortion = null;
                        if (uot.PRICECONDITION != null && uot.PRICECONDITION.Any())
                        {
                            #region VKP0

                            var lastPrice =
                                uot.PRICECONDITION.LastOrDefault(a =>
                                    a.CONDITIONTYPE.Equals("VKP0"));

                            if (lastPrice != null)
                            {
                                var addLocationPrice = false;
                                var addPortion = false;
                                if (!serverItem.Portions.Any())
                                {
                                    addPortion = true;
                                }
                                else
                                {
                                    firstPortion = serverItem.Portions.LastOrDefault(a =>
                                        a.Name.Equals(uot.UOMCODE));
                                    if (firstPortion != null)
                                    {
                                        if (!firstPortion.Price.Equals(Convert.ToDecimal(lastPrice.PRICE)))
                                            addLocationPrice = true;
                                    }
                                    else
                                    {
                                        addPortion = true;
                                    }
                                }

                                if (addPortion)
                                {
                                    firstPortion = new MenuItemPortion
                                    {
                                        Name = uot.UOMCODE,
                                        Price = Convert.ToDecimal(lastPrice.PRICE),
                                        Barcode = uot.EANCODE
                                    };
                                    serverItem.Portions.Add(firstPortion);
                                }


                                if (add)
                                {
                                    uow.MenuItems.Add(serverItem);
                                    add = false;
                                }
                                else
                                {
                                    uow.MenuItems.Update(serverItem);
                                }

                                uow.Complete();


                                var myLocaitem =
                                    uow.LocationItems
                                        .Find(a => a.LocationId == serverLocation.Id
                                                   && a.MenuItemId == serverItem.Id)
                                        .LastOrDefault();

                                if (myLocaitem == null)
                                {
                                    var myListItem = new LocationMenuItem
                                    {
                                        LocationId = serverLocation.Id,
                                        MenuItemId = serverItem.Id
                                    };
                                    uow.LocationItems.Add(myListItem);
                                    uow.Complete();
                                }

                                var myPortion = serverItem.Portions.Last(a => a.Name.Equals(uot.UOMCODE));
                                if (addLocationPrice)
                                {
                                    var serverPrice =
                                        uow.LocationMenuItemPrices.Find(a =>
                                                a.LocationId == serverLocation.Id &&
                                                a.MenuPortionId == myPortion.Id)
                                            .LastOrDefault();

                                    if (serverPrice == null)
                                    {
                                        serverPrice = new LocationMenuItemPrice
                                        {
                                            LocationId = serverLocation.Id,
                                            MenuPortionId = myPortion.Id,
                                            Price = Convert.ToDecimal(lastPrice.PRICE)
                                        };
                                        uow.LocationMenuItemPrices.Add(serverPrice);
                                    }
                                    else
                                    {
                                        serverPrice.Price = Convert.ToDecimal(lastPrice.PRICE);
                                        uow.LocationMenuItemPrices.Update(serverPrice);
                                    }

                                    uow.Complete();
                                }

                                foreach (var materialDesc in materialDown.MATERIAL_DESC)
                                {
                                    var serverLangauge =
                                        uow.LangaugeDescriptions.Find(a =>
                                                a.ReferenceId == serverItem.Id && a.TenantId == appConfig.Tenant &&
                                                a.LanguageDescriptionType == LanguageDescriptionType.MenuItem &&
                                                a.LanguageCode.Equals(materialDesc.DESC_LANG))
                                            .LastOrDefault();

                                    if (serverLangauge == null)
                                    {
                                        serverLangauge = new LanguageDescription();
                                        uow.LangaugeDescriptions.Add(serverLangauge);
                                    }
                                    else
                                    {
                                        uow.LangaugeDescriptions.Update(serverLangauge);
                                    }

                                    serverLangauge.ReferenceId = serverItem.Id;
                                    serverLangauge.TenantId = appConfig.Tenant;
                                    serverLangauge.Name = materialDesc.MATERIALDESC;
                                    serverLangauge.LanguageCode = materialDesc.DESC_LANG;
                                    serverLangauge.LanguageDescriptionType = LanguageDescriptionType.MenuItem;
                                    uow.Complete();
                                }
                            }

                            #endregion

                            #region VKA0

                            var createPromoPrice =
                                uot.PRICECONDITION.LastOrDefault(a =>
                                    a.CONDITIONTYPE.Equals("VKA0"));

                            if (createPromoPrice != null)
                            {
                                if (firstPortion == null && serverItem != null)
                                {
                                    firstPortion = new MenuItemPortion
                                    {
                                        Name = uot.UOMCODE,
                                        Price = 0,
                                        Barcode = uot.EANCODE
                                    };
                                    serverItem.Portions.Add(firstPortion);
                                    uow.MenuItems.Update(serverItem);
                                    uow.Complete();
                                }

                                if (!string.IsNullOrEmpty(createPromoPrice.PROMOTIONNO))
                                {
                                    var myPromotion = uow.Promotions.Find(a => a.TenantId == appConfig.Tenant
                                                                               && a.Oid == companyId
                                                                               && a.Name.Equals(
                                                                                   createPromoPrice.PROMOTIONNO))
                                        .LastOrDefault();

                                    var myTagName = createPromoPrice.PROMOTIONNO;
                                    if (!string.IsNullOrEmpty(myTagName) && myTagName.Length >= 10)
                                        myTagName = myTagName.Substring(0, 10);

                                    var myTag = uow.PriceTags.Find(a => a.TenantId == appConfig.Tenant
                                                                        && a.Oid == companyId &&
                                                                        a.Name.Equals(myTagName)).LastOrDefault();


                                    if (myTag == null)
                                    {
                                        myTag = new PriceTag
                                        {
                                            Name = myTagName,
                                            TenantId = appConfig.Tenant,
                                            Oid = appConfig.Tenant,
                                            Group = false,
                                            LocationTag = false,
                                            Locations = JsonConvert.SerializeObject(myListLocation),
                                            StartDate = DateTime.Now,
                                            EndDate = DateTime.Now
                                        };
                                        uow.PriceTags.Add(myTag);
                                        uow.Complete();
                                    }

                                    var ptD = uow.PriceTagDefinitions.Find(a =>
                                        a.MenuPortionId == firstPortion.Id
                                        && a.TagId == myTag.Id).LastOrDefault();

                                    if (ptD == null)
                                    {
                                        ptD = new PriceTagDefinition
                                        {
                                            TagId = myTag.Id,
                                            Price = 0,
                                            MenuPortionId = firstPortion.Id
                                        };
                                        uow.PriceTagDefinitions.Add(ptD);
                                        uow.Complete();
                                    }

                                    var loPrice = uow.PriceTagLocations.Find(a =>
                                        a.PriceTagDefinitionId == ptD.Id
                                        && a.LocationId == serverLocation.Id).LastOrDefault();

                                    if (loPrice == null)
                                    {
                                        loPrice = new PriceTagLocationPrice
                                        {
                                            PriceTagDefinitionId = ptD.Id,
                                            LocationId = serverLocation.Id,
                                            Price = createPromoPrice.PRICE
                                        };
                                        uow.PriceTagLocations.Add(loPrice);
                                        uow.Complete();
                                    }
                                    else
                                    {
                                        loPrice.Price = createPromoPrice.PRICE;
                                        uow.PriceTagLocations.Update(loPrice);
                                        uow.Complete();
                                    }

                                    if (myPromotion == null)
                                    {
                                        myPromotion = new Promotion
                                        {
                                            Name = createPromoPrice.PROMOTIONNO,
                                            TenantId = appConfig.Tenant,
                                            Oid = appConfig.Tenant,
                                            Active = true,
                                            Code = createPromoPrice.PROMOTIONNO,
                                            PromotionTypeId = 1,
                                            Locations = JsonConvert.SerializeObject(myListLocation)
                                        };
                                        if (!string.IsNullOrEmpty(createPromoPrice.VALID_FROM) &&
                                            !string.IsNullOrEmpty(createPromoPrice.VALID_TO))
                                        {
                                            var provider = CultureInfo.InvariantCulture;

                                            var myDate = DateTime.ParseExact(createPromoPrice.VALID_FROM,
                                                "dd.MM.yyyy", provider);
                                            myPromotion.StartDate = myDate;
                                            var myEndDate = DateTime.Now.AddYears(30);
                                            if (!createPromoPrice.VALID_TO.EndsWith("9999"))
                                                myEndDate = DateTime.ParseExact(createPromoPrice.VALID_TO,
                                                    "dd.MM.yyyy", provider);

                                            myPromotion.EndDate = myEndDate;
                                        }

                                        uow.Promotions.Add(myPromotion);
                                        uow.Complete();

                                        if (myPromotion != null)
                                        {
                                            var timerPromotion = new TimerPromotion
                                            {
                                                PriceTagId = myTag.Id,
                                                PromotionId = myPromotion.Id
                                            };

                                            uow.TimerPromotions.Add(timerPromotion);
                                            uow.Complete();
                                        }
                                    }
                                    else
                                    {
                                        var myLocations = myPromotion.Locations;
                                        if (!string.IsNullOrEmpty(myLocations))
                                        {
                                            var allMyList =
                                                 JsonConvert.DeserializeObject<List<SimpleLocation>>(myLocations);
                                            if (allMyList.Any())
                                            {
                                                var currLoPreset =
                                                    allMyList.LastOrDefault(a => a.Id.Equals(serverLocation.Id));
                                                if (currLoPreset == null)
                                                {
                                                    allMyList.Add(simLocation);

                                                    myPromotion.Locations = JsonConvert.SerializeObject(allMyList);
                                                    uow.Promotions.Update(myPromotion);
                                                    uow.Complete();
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            #endregion
                        }

                        if (uot.PRICELIST != null && uot.PRICELIST.Any())
                            foreach (var myPriceList in uot.PRICELIST)
                            {
                                if (myPriceList.PL_TYPE.Equals("P1") && !string.IsNullOrEmpty(uot.SALESUNIT) && uot.SALESUNIT.ToUpper().Equals("X"))
                                {
                                    var myUpItems = uow.UpMenuItems.Find(a => a.RefMenuItemId == serverItem.Id)
                                        .ToList();
                                    if (myUpItems.Any())
                                        foreach (var upMenuItem in myUpItems)
                                        {
                                            var upLocationItem = uow.UpMenuItemLocationPrices.Find(a =>
                                                    a.UpMenuItemId == upMenuItem.Id &&
                                                    a.LocationId == serverLocation.Id)
                                                .LastOrDefault();
                                            var addUp = false;
                                            if (upLocationItem == null)
                                                upLocationItem = new UpMenuItemLocationPrice
                                                {
                                                    LocationId = serverLocation.Id,
                                                    UpMenuItemId = upMenuItem.Id
                                                };
                                            upLocationItem.Price = myPriceList.PL_PRICE;
                                            if (addUp)
                                                uow.UpMenuItemLocationPrices.Add(upLocationItem);
                                            else
                                                uow.UpMenuItemLocationPrices.Update(upLocationItem);
                                            uow.Complete();
                                        }
                                }

                                if (myPriceList.PL_TYPE.Equals("P2"))
                                {
                                    var tagGroupName = serverLocation.Code + "_" + "ADDON";
                                    var myGroup = uow.OrderTagGroups.Find(a => a.Name.Equals(tagGroupName) 
                                        && a.Oid == companyId && a.TenantId == appConfig.Tenant).LastOrDefault();

                                    if (myGroup == null)
                                    {
                                        myGroup = new OrderTagGroup()
                                        {
                                            Name = tagGroupName,
                                            Oid = companyId,
                                            TenantId = appConfig.Tenant,
                                            Locations = JsonConvert.SerializeObject(myListLocation)
                                        };

                                        uow.OrderTagGroups.Add(myGroup);
                                        uow.Complete();
                                    }

                                    var tagName = materialDown.MATERIALNUMBER+"_"+firstPortion.Name;
                                    var myOrderTag = uow.OrderTags.Find(a => a.Name.Equals(tagName) &&
                                                                           a.OrderTagGroupId == myGroup.Id).LastOrDefault();

                                    if (myOrderTag == null)
                                    {
                                        myOrderTag = new OrderTag()
                                        {
                                            Name = tagName,
                                            Price = myPriceList.PL_PRICE,
                                            OrderTagGroupId =  myGroup.Id
                                            
                                        };
                                        uow.OrderTags.Add(myOrderTag);
                                        uow.Complete();
                                    }
                                    else
                                    {
                                        myOrderTag.Price = myPriceList.PL_PRICE;
                                        uow.OrderTags.Update(myOrderTag);
                                        uow.Complete();
                                    }
                                    
                                }

                            }


                                


                        if (uot.PRICECOND_SALECHANNEL != null && uot.PRICECOND_SALECHANNEL.Any() &&
                            firstPortion != null)
                            foreach (var myChannel in uot.PRICECOND_SALECHANNEL)
                                if (myChannel.CONDITIONTYPE.Equals("VKP0"))
                                {
                                    var myDepartment = uow.Departments.Find(a => a.TenantId == appConfig.Tenant
                                        && a.Code != null && a.Code.Equals(myChannel.SALECHANNEL)).LastOrDefault();

                                    if (myDepartment == null)
                                        throw new Exception("Department is not available : " + myChannel.SALECHANNEL);


                                    var myTagName = myChannel.CONDITIONTYPE + "_" + myDepartment.Code;
                                    if (!string.IsNullOrEmpty(myTagName) && myTagName.Length >= 10)
                                        myTagName = myTagName.Substring(0, 10);

                                   
                                    var myTag = uow.PriceTags.Find(a => a.TenantId == appConfig.Tenant
                                                                        && a.Oid == companyId &&
                                                                        a.Name.Equals(myTagName)).LastOrDefault();

                                    if (myTag == null)
                                    {
                                        myTag = new PriceTag
                                        {
                                            Name = myTagName,
                                            TenantId = appConfig.Tenant,
                                            Oid = appConfig.Tenant,
                                            Group = false,
                                            LocationTag = false,
                                            StartDate = DateTime.Now,
                                            EndDate = DateTime.Now
                                        };
                                        uow.PriceTags.Add(myTag);
                                        uow.Complete();
                                    }

                                    var ptD = uow.PriceTagDefinitions.Find(a =>
                                        a.MenuPortionId == firstPortion.Id
                                        && a.TagId == myTag.Id).LastOrDefault();

                                    if (ptD == null)
                                    {
                                        ptD = new PriceTagDefinition
                                        {
                                            TagId = myTag.Id,
                                            Price = 0,
                                            MenuPortionId = firstPortion.Id
                                        };
                                        uow.PriceTagDefinitions.Add(ptD);
                                        uow.Complete();
                                    }

                                    var loPrice = uow.PriceTagLocations.Find(a =>
                                        a.PriceTagDefinitionId == ptD.Id
                                        && a.LocationId == serverLocation.Id).LastOrDefault();

                                    if (loPrice == null)
                                    {
                                        loPrice = new PriceTagLocationPrice
                                        {
                                            PriceTagDefinitionId = ptD.Id,
                                            LocationId = serverLocation.Id,
                                            Price = myChannel.PC_SC_PRICE
                                        };
                                        uow.PriceTagLocations.Add(loPrice);
                                        uow.Complete();
                                    }
                                    else
                                    {
                                        loPrice.Price = myChannel.PC_SC_PRICE;
                                        uow.PriceTagLocations.Update(loPrice);
                                        uow.Complete();
                                    }

                                    if (myDepartment.TagId == null)
                                    {
                                        myDepartment.TagId = myTag.Id;
                                        uow.Departments.Update(myDepartment);
                                        uow.Complete();
                                    }

                                }

                            
                    }
                }
            }
            catch (Exception e)
            {
                var myDate = DateTime.Now;
                uow.ExternalLogs.Add(new ExternalLog
                {
                    AuditType = (int) ExternalLogType.MaterialInbound,
                    CreatorUserId = appConfig.AppUserId,
                    TenantId = appConfig.Tenant,
                    CreationTime = myDate,
                    LogTime = myDate,
                    ExternalLogTrunc = myDate.Date,
                    LogData = e.Message,
                    LogDescription = FlattenException(e)
                });
                uow.Complete();
                reason = e.Message;
            }

            return reason;
        }

        private static string FlattenException(Exception exception)
        {
            var stringBuilder = new StringBuilder();

            while (exception != null)
            {
                stringBuilder.AppendLine(exception.Message);
                stringBuilder.AppendLine(exception.StackTrace);

                exception = exception.InnerException;
            }

            return stringBuilder.ToString();
        }
    }

    public class SimpleLocation
    {
        public virtual int Id { get; set; }
        public virtual string Code { get; set; }
        public virtual string Name { get; set; }
    }
}