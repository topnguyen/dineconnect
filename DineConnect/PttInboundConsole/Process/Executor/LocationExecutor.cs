﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DinePlan.DineConnect.Connect.Audit;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.MultiLingual;
using Newtonsoft.Json;
using PttDatabaseCommon.Data;
using PttInboundConsole.Data;

namespace PttInboundConsole.Process.Executor
{
    public class LocationExecutor
    {
        public static string Run(IUnitOfWork uow, AppConfig appConfig, Dictionary<string, PlantMasterSend_MT> allItems,
            PttInboundDbContext context)
        {
            var reason = "";
            try
            {
                foreach (var myMaster in allItems.Values)
                foreach (var lgo in myMaster.PLANT)
                {
                    var fCompany = uow.Companies.Find(t => t.Code.Equals(lgo.CLASS)
                                                            && t.TenantId == appConfig.Tenant).LastOrDefault();

                    if (fCompany == null)
                    {
                        throw new Exception("Company is not available :" + lgo.CLASS);
                    }

                    int companyId = fCompany.Id;


                    var fLocation = uow.Locations.Find(t => t.Code.Equals(lgo.PLANTCODE)
                                                            && t.TenantId == appConfig.Tenant).LastOrDefault();

                    var add = false;

                    if (!string.IsNullOrEmpty(lgo.ACTIONCODE) && lgo.ACTIONCODE.Equals("DELE"))
                    {
                        if (fLocation != null && fLocation.Id > 0)
                        {
                            fLocation.IsDeleted = true;
                            fLocation.LocationBranchId = null;
                            fLocation.DeleterUserId = appConfig.AppUserId;
                            fLocation.DeletionTime = DateTime.Now;
                            uow.Locations.Update(fLocation);
                            uow.Complete();
                            continue;

                        }
                    }
                    

                    if (fLocation == null)
                    {
                        fLocation = new Location();
                        add = true;
                    }

                    fLocation.IsDeleted = false;
                    fLocation.Code = lgo.PLANTCODE;
                    fLocation.Name = lgo.NAME1;
                    fLocation.Address1 = lgo.HOUSE_NUM1+" "+lgo.STREET1;
                    fLocation.Address2 = lgo.STREET2 +" "+lgo.STREET3;
                    fLocation.Address3 = lgo.STREET4 + " " + lgo.STREET5;
                    fLocation.City = lgo.DISTRICT;
                    fLocation.State = lgo.CITY;
                    fLocation.Country = lgo.COUNTRY;
                    fLocation.PostalCode = lgo.POSTALCODE;
                    fLocation.CompanyRefId = companyId;
                    fLocation.Email = lgo.EMAIL;
                    fLocation.PhoneNumber = lgo.TELEPHONE;
                    fLocation.TenantId = appConfig.Tenant;

                    if (lgo.ADDR_INTER != null)
                    {
                        var jsonData = new
                        {
                            InternationlName1 = lgo.ADDR_INTER.NAME1,
                            InternationlName2 = lgo.ADDR_INTER.NAME2,
                            InternationlName3 = lgo.ADDR_INTER.NAME3,
                            InternationlName4 = lgo.ADDR_INTER.NAME4,
                            InternationlHouse_num1 = lgo.ADDR_INTER.HOUSE_NUM1,
                            InternationlStreet1 = lgo.ADDR_INTER.STREET1,
                            InternationlStreet2 = lgo.ADDR_INTER.STREET2,
                            InternationlStreet3 = lgo.ADDR_INTER.STREET3,
                            InternationlStreet4 = lgo.ADDR_INTER.STREET4,
                            InternationlStreet5 = lgo.ADDR_INTER.STREET5,
                            InternationlDistrict = lgo.ADDR_INTER.DISTRICT,
                            InternationlCity = lgo.ADDR_INTER.CITY,
                            Mid = lgo.MID,
                            ShopId = lgo.SHOPID,
                            ShopCode = lgo.SHOPCODE,
                            PlantCate = lgo.PLANTCATE,
                            PlantProf = lgo.PLANTPROF,
                            ShipTo = lgo.SHIPTO,
                            AbbName = lgo.NAME1,
                            FullTaxName = lgo.NAME2+" "+lgo.NAME3+" "+lgo.NAME4,
                            VatReg = lgo.VAT_REG,
                            Tax1 = lgo.TAX_ID1,
                            Tax3 = lgo.TAX_ID3,
                            SoldTo = lgo.SOLDTO,

                        };

                        fLocation.AddOn = JsonConvert.SerializeObject(jsonData);
                    }

                    if (lgo.BRANCH != null && lgo.BRANCH.Any())
                    {
                        foreach (var myLastBranch in lgo.BRANCH.Where(a=>!string.IsNullOrEmpty(a.DEFAULT_BRANCH)))
                        {
                            if (myLastBranch?.ADDR_BR != null)
                            {


                                LocationBranch locationBranch;
                              
                                bool addLoBranch = false;

                                if (fLocation.LocationBranchId != null)
                                {
                                    locationBranch =
                                        uow.LocationBranches.Find(t => t.Id == fLocation.LocationBranchId).LastOrDefault();
                                }
                                else
                                {
                                    locationBranch = new LocationBranch();
                                    addLoBranch = true;
                                }

                                if (locationBranch != null)
                                {
                                    locationBranch.Code = myLastBranch.BRANCHCODE;

                                    locationBranch.Name =
                                        myLastBranch.ADDR_BR.NAME1 + " " + myLastBranch.ADDR_BR.NAME2 + " " +
                                        myLastBranch.ADDR_BR.NAME3 + " " + myLastBranch.ADDR_BR.NAME4;


                                    locationBranch.Address1 = myLastBranch.ADDR_BR.HOUSE_NUM1 + " " + myLastBranch.ADDR_BR.STREET1;
                                    locationBranch.Address2 = myLastBranch.ADDR_BR.STREET2 + " " +
                                                              myLastBranch.ADDR_BR.STREET3;
                                    locationBranch.Address3 = myLastBranch.ADDR_BR.STREET4 + " " +
                                                              myLastBranch.ADDR_BR.STREET5;
                                    locationBranch.City = myLastBranch.ADDR_BR.DISTRICT;
                                    locationBranch.State = myLastBranch.ADDR_BR.CITY;
                                    locationBranch.PostalCode = myLastBranch.ADDR_BR.POST_CODE1;
                                    locationBranch.Country = myLastBranch.ADDR_BR.COUNTRY;


                                    if (myLastBranch.ADDR_BR.ADDR_BR_INTER != null)
                                    {
                                        var jsonData = new
                                        {
                                            InternationlName1 = myLastBranch.ADDR_BR.ADDR_BR_INTER.NAME1,
                                            InternationlName2 = myLastBranch.ADDR_BR.ADDR_BR_INTER.NAME2,
                                            InternationlName3 = myLastBranch.ADDR_BR.ADDR_BR_INTER.NAME3,
                                            InternationlName4 = myLastBranch.ADDR_BR.ADDR_BR_INTER.NAME4,
                                            InternationlHouse_num1 = myLastBranch.ADDR_BR.ADDR_BR_INTER.HOUSE_NUM1,
                                            InternationlStreet1 = myLastBranch.ADDR_BR.ADDR_BR_INTER.STREET1,
                                            InternationlStreet2 = myLastBranch.ADDR_BR.ADDR_BR_INTER.STREET2,
                                            InternationlStreet3 = myLastBranch.ADDR_BR.ADDR_BR_INTER.STREET3,
                                            InternationlStreet4 = myLastBranch.ADDR_BR.ADDR_BR_INTER.STREET4,
                                            InternationlStreet5 = myLastBranch.ADDR_BR.ADDR_BR_INTER.STREET5,
                                            InternationlDistrict = myLastBranch.ADDR_BR.ADDR_BR_INTER.DISTRICT,
                                            InternationlCity = myLastBranch.ADDR_BR.ADDR_BR_INTER.CITY
                                        };
                                        locationBranch.AddOns = JsonConvert.SerializeObject(jsonData);
                                    }

                                    if (addLoBranch)
                                    {
                                        fLocation.LocationBranch = locationBranch;
                                    }
                                    else
                                    {
                                        uow.LocationBranches.Update(locationBranch);
                                        uow.Complete();
                                    }

                                    if (locationBranch.Id > 0)
                                    {

                                        if (myLastBranch.DESC_BR != null)
                                        {
                                            LanguageExecutor.PushLanguage(myLastBranch.DESC_BR.BRANCH_LANGKEY, myLastBranch.DESC_BR.BRANCH_DESC,
                                                LanguageDescriptionType.LocationBranch, locationBranch.Id, uow, appConfig);
                                        }
                                        
                                    }
                                }


                                break;
                            }
                        }
                    }

                    if (add)
                    {
                        uow.Locations.Add(fLocation);
                        uow.Complete();

                        if (fLocation.Id > 0)
                        {
                            var insQry = "SET IDENTITY_INSERT [dbo].[AbpOrganizationUnits] ON; " +
                                         "INSERT INTO  [dbo].[AbpOrganizationUnits] ([Id],[TenantId],[ParentId],[Code]," +
                                         "[DisplayName],[IsDeleted],[CreationTime],[CreatorUserId]) " +
                                         "VALUES(" + fLocation.Id + "," + appConfig.Tenant + ", NULL ,'" +
                                         fLocation.Code
                                         + "','" + fLocation.Code + "',0,GETDATE()," + 1 + ");" +
                                         " SET IDENTITY_INSERT [dbo].[AbpOrganizationUnits] OFF; ";
                            context.Database.ExecuteSqlCommand(insQry);
                        }

                        fLocation.OrganizationUnitId = fLocation.Id;
                    }

                    uow.Locations.Update(fLocation);
                    uow.Complete();
                }
            }
            catch (Exception e)
            {
                var myDate = DateTime.Now;
                uow.ExternalLogs.Add(new ExternalLog
                {
                    AuditType =  (int)  ExternalLogType.LocationInbound,
                    CreatorUserId = appConfig.AppUserId,
                    TenantId = appConfig.Tenant,
                    CreationTime = myDate,
                    LogTime = myDate,
                    ExternalLogTrunc = myDate.Date,
                    LogData = e.Message,
                    LogDescription = FlattenException(e)
                });
                uow.Complete();
                reason = e.Message;
            }

            return reason;
        }

        private static string FlattenException(Exception exception)
        {
            var stringBuilder = new StringBuilder();

            while (exception != null)
            {
                stringBuilder.AppendLine(exception.Message);
                stringBuilder.AppendLine(exception.StackTrace);

                exception = exception.InnerException;
            }

            return stringBuilder.ToString();
        }
    }

    
}