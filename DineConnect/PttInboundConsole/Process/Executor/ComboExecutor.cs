﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DinePlan.DineConnect.Connect.Audit;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.MultiLingual;
using PttDatabaseCommon.Data;
using PttInboundConsole.Data;

namespace PttInboundConsole.Process.Executor
{
    public class ComboExecutor
    {
        public static string Run(IUnitOfWork uow, AppConfig appConfig,
            Dictionary<string, ComboMasterDataSend_MT> allItems, PttInboundDbContext context)
        {
            var reason = "";
            try
            {
                foreach (var combo in allItems.Values)
                    if (!string.IsNullOrEmpty(combo.COMBODOWNLOAD.COMBOMAT.MAT_NO))
                    {
                        var serverItem = uow.MenuItems.Find(t => t.AliasCode.Equals(combo.COMBODOWNLOAD.COMBOMAT.MAT_NO)
                                                                 && t.TenantId == appConfig.Tenant).LastOrDefault();


                        if (serverItem == null)
                            throw new Exception("Item is not available : " + combo.COMBODOWNLOAD.COMBOMAT.MAT_NO);

                        if (!string.IsNullOrEmpty(combo.COMBODOWNLOAD.COMBOMAT.STATUS) &&
                            combo.COMBODOWNLOAD.COMBOMAT.STATUS.Equals("2") && combo.COMBODOWNLOAD.COMBOMAT.ACTIONCODE.Equals("MODI"))
                        {
                            serverItem.IsDeleted = true;
                            serverItem.DeleterUserId = appConfig.AppUserId;
                            serverItem.DeletionTime = DateTime.Now;
                            uow.MenuItems.Update(serverItem);
                            uow.Complete();
                            continue;
                        }

                        if (serverItem.ProductType == 1)
                            throw new Exception("Product is not combo Item : " + serverItem.AliasCode);

                        serverItem.IsDeleted = false;

                        var localCombo = uow.ProductComboes.Find(t => t.MenuItemId.Equals(serverItem.Id))
                            .LastOrDefault();
                        var addCombo = false;
                        if (localCombo == null)
                        {
                            addCombo = true;
                            localCombo = new ProductCombo();
                        }

                        localCombo.MenuItemId = serverItem.Id;
                        localCombo.AddPriceToOrderPrice = false;
                        var comboGroupNames = new List<string>();

                        foreach (var comboGrpset in combo.COMBODOWNLOAD.COMBOMAT.COMBO_GRPSET)
                        {
                            var lastEnString = comboGrpset.GRPSET_DESC.LastOrDefault(b => b.LANG_GRPSET.Equals("EN"));

                            var codeNo = comboGrpset.GRP_SET_NO;

                            if (lastEnString != null)
                            {
                                var comboGroup =
                                    localCombo.ComboGroups.LastOrDefault(a => a.Code!=null && a.Code.Equals(codeNo));
                                if (comboGroup == null)
                                {
                                    comboGroup = new ProductComboGroup();
                                    localCombo.ComboGroups.Add(comboGroup);
                                }

                                comboGroup.Code = codeNo;
                                comboGroup.TenantId = appConfig.Tenant;
                                comboGroup.Name = lastEnString.DESC_GRPSET;
                                comboGroup.Maximum = Convert.ToInt32(comboGrpset.MAX_ORD);
                                comboGroup.Minimum = Convert.ToInt32(comboGrpset.MIN_ORD);

                                comboGroupNames.Add(comboGroup.Name);
                                var comboItemIds = new List<int>();

                                foreach (var comboFiItem in comboGrpset.GRPSET_DETAIL)
                                {
                                    var insideItem = uow.MenuItems.Find(t => t.AliasCode.Equals(comboFiItem.MATCPN_NO)
                                                                             && t.TenantId == appConfig.Tenant)
                                        .LastOrDefault();

                                    if (insideItem!=null && !comboItemIds.Contains(insideItem.Id))
                                    {
                                        var firstPortion = insideItem.Portions.FirstOrDefault();

                                        if (firstPortion == null)
                                            return "Combo Item is not available : " + comboFiItem.MATCPN_NO;

                                        var comboItem =
                                            comboGroup.ComboItems.LastOrDefault(a =>
                                                a.MenuItemId.Equals(insideItem.Id));
                                        if (comboItem == null)
                                        {
                                            comboItem = new ProductComboItem();
                                            comboGroup.ComboItems.Add(comboItem);
                                        }

                                        comboItemIds.Add(insideItem.Id);

                                        comboItem.Name = insideItem.Name;
                                        comboItem.GroupTag = "";
                                        comboItem.MenuItemId = insideItem.Id;
                                        comboItem.MenuItemPortionId = firstPortion.Id;

                                        if (!string.IsNullOrEmpty(comboFiItem.DEFAULT) &&
                                            comboFiItem.DEFAULT.Equals("X"))
                                            comboItem.AutoSelect = true;

                                        if (!string.IsNullOrEmpty(comboFiItem.ADDI_PRICE))
                                            comboItem.Price = Convert.ToDecimal(comboFiItem.ADDI_PRICE);

                                        comboItem.Count = Convert.ToInt32(comboFiItem.MAX_ORD);
                                    }
                                    else
                                    {
                                        throw new Exception("Inner Item is not available : " + comboFiItem.MATCPN_NO);
                                    }
                                }

                                var myNotItemIds = comboGroup.ComboItems
                                    .Where(a => !comboItemIds.Contains(a.MenuItemId)).Select(a => a.MenuItemId)
                                    .ToList();

                                foreach (var myNotItemId in myNotItemIds)
                                {
                                    var lastItem =
                                        comboGroup.ComboItems.LastOrDefault(a => a.MenuItemId == myNotItemId);
                                    if (lastItem != null)
                                    {
                                        uow.ProductComboItems.Remove(lastItem);

                                    }
                                }

                                uow.Complete();
                            }
                        }

                        var myNonComboGroupNames = localCombo.ComboGroups
                            .Where(a => !comboGroupNames.Contains(a.Name)).Select(a => a.Name)
                            .ToList();

                        foreach (var myNonComboGroup in myNonComboGroupNames)
                        {
                            var lastItem =
                                localCombo.ComboGroups.LastOrDefault(a => a.Name.Equals(myNonComboGroup));
                            if (lastItem != null) localCombo.ComboGroups.Remove(lastItem);
                        }

                        if (addCombo)
                            uow.ProductComboes.Add(localCombo);
                        else
                            uow.ProductComboes.Update(localCombo);
                        uow.Complete();

                        foreach (var comboGroupSet in combo.COMBODOWNLOAD.COMBOMAT.COMBO_GRPSET)
                        {
                            var lastEnString = comboGroupSet.GRPSET_DESC.LastOrDefault(b => b.LANG_GRPSET.Equals("EN"));
                            if (lastEnString != null)
                            {
                                var comboGroup =
                                    localCombo.ComboGroups.LastOrDefault(a =>
                                        a.Name.Equals(lastEnString.DESC_GRPSET));

                                if (comboGroup != null && comboGroup.Id > 0)
                                    foreach (var desc in comboGroupSet.GRPSET_DESC)
                                        LanguageExecutor.PushLanguage(desc.LANG_GRPSET, desc.DESC_GRPSET,
                                            LanguageDescriptionType.ComboGroup, comboGroup.Id, uow, appConfig);
                            }
                        }
                    }
            }
            catch (Exception e)
            {

                var myDate = DateTime.Now;
                uow.ExternalLogs.Add(new ExternalLog
                {
                    AuditType = (int) ExternalLogType.ComboInbound,
                    CreatorUserId = appConfig.AppUserId,
                    TenantId = appConfig.Tenant,
                    CreationTime = myDate,
                    LogTime = myDate,
                    ExternalLogTrunc = myDate.Date,
                    LogData = e.Message,
                    LogDescription = FlattenException(e)
                });
                uow.Complete();
                reason = e.Message;
            }

            return reason;
        }

        private static string FlattenException(Exception exception)
        {
            var stringBuilder = new StringBuilder();

            while (exception != null)
            {
                stringBuilder.AppendLine(exception.Message);
                stringBuilder.AppendLine(exception.StackTrace);

                exception = exception.InnerException;
            }

            return stringBuilder.ToString();
        }

    }
}