﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.MultiLingual;
using PttDatabaseCommon.Data;
using PttInboundConsole.Data;

namespace PttInboundConsole.Process.Executor
{
    public class LanguageExecutor
    {
        public static void PushLanguage(string langCode, string langDesc, LanguageDescriptionType langType, 
            int serverId ,IUnitOfWork uow, AppConfig appConfig)
        {
            var serverLangauge =
                uow.LangaugeDescriptions.Find(a =>
                        a.ReferenceId == serverId && a.TenantId == appConfig.Tenant && 
                        a.LanguageDescriptionType == langType && a.LanguageCode.Equals(langCode))
                    .LastOrDefault();

            if (serverLangauge == null)
            {
                serverLangauge = new LanguageDescription();
                uow.LangaugeDescriptions.Add(serverLangauge);
            }
            else
            {
                uow.LangaugeDescriptions.Update(serverLangauge);

            }
            serverLangauge.ReferenceId = serverId;
            serverLangauge.TenantId = appConfig.Tenant;
            serverLangauge.Name = langDesc;
            serverLangauge.LanguageCode = langCode.ToLower();
            serverLangauge.LanguageDescriptionType =langType;
            uow.Complete();
        }
    }
}
