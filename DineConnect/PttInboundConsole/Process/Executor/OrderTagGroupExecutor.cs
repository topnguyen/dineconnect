﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DinePlan.DineConnect.Connect.Audit;
using DinePlan.DineConnect.Connect.MultiLingual;
using DinePlan.DineConnect.Connect.OrderTags;
using PttDatabaseCommon.Data;
using PttInboundConsole.Data;

namespace PttInboundConsole.Process.Executor
{
    public class OrderTagGroupExecutor
    {
        public static string Run(IUnitOfWork uow, AppConfig appConfig,
            Dictionary<string, OptionProductMasterSend_MT> allItems, PttInboundDbContext context)
        {
            var reason = "";
            try
            {
                foreach (var lgo in allItems.Values)
                {
                 
                    var serverItem = uow.MenuItems.Find(t => t.AliasCode.Equals(lgo.MATOPTION.MAT_NO)
                                                             && t.TenantId == appConfig.Tenant)
                        .LastOrDefault();

                    if (serverItem == null)
                        throw new Exception("Item is not available : " + lgo.MATOPTION.MAT_NO);


                    int companyId = serverItem.Oid;
                    
                   
                    if (lgo.MATOPTION.STATUS.Equals("2") && lgo.MATOPTION.ACTIONCODE.Equals("MODI"))
                    {

                        var allTagMaps =
                            uow.OrderTagMaps.Find(t => t.MenuItemId != null && t.MenuItemId == serverItem.Id);

                        foreach (var orderTagMap in allTagMaps)
                        {
                            if (orderTagMap != null)
                            {
                                var myGroup = uow.OrderTagGroups.Find(t => t.Id.Equals(orderTagMap.OrderTagGroupId)
                                                                           && t.TenantId == appConfig.Tenant &&
                                                                           t.Oid == companyId).LastOrDefault();
                                if (myGroup != null)
                                {
                                    myGroup.IsDeleted = true;
                                    myGroup.DeleterUserId = appConfig.AppUserId;
                                    myGroup.DeletionTime = DateTime.Now;

                                    uow.OrderTagGroups.Update(myGroup);
                                    uow.Complete();
                                }
                            }
                        }
                        continue;
                    }



                    foreach (var myOption in lgo.MATOPTION.MATOPTION_KEY.GroupBy(a => a.OPT_GRP))
                    {
                        var groupAdd = false;
                        var myGroupName = lgo.MATOPTION.MAT_NO + "_"+myOption.Key;

                        var myGroupKey = myOption.SelectMany(a => a.KEY_DESC)
                            .LastOrDefault(a => a.LANG_KEY.Equals("TH")) ?? myOption.SelectMany(a => a.KEY_DESC)
                            .LastOrDefault(a => a.LANG_KEY.Equals("EN"));

                        if (myGroupKey != null)
                        {
                            myGroupName = lgo.MATOPTION.MAT_NO + "_"+myGroupKey.OPT_DESC;
                        }
                        

                        var myGroup = uow.OrderTagGroups.Find(t => t.Name.Equals(myGroupName)
                                                                   && t.TenantId == appConfig.Tenant &&
                                                                   t.Oid == companyId)
                            .LastOrDefault();

                        
                       
                        if (myGroup == null)
                        {
                            myGroup = new OrderTagGroup();
                            groupAdd = true;
                        }

                        if (myGroup.Tags == null) myGroup.Tags = new List<OrderTag>();

                        if (myGroup.Maps == null) myGroup.Maps = new List<OrderTagMap>();
                        myGroup.IsDeleted = false;
                        myGroup.Name = myGroupName;
                        myGroup.TenantId = appConfig.Tenant;
                        myGroup.Oid = companyId;

                        List<string> newTags = new List<string>();
                        int selectITem = 1;
                        foreach (var myKey in myOption)
                        {
                            newTags.Add(myKey.KEY);

                            selectITem = Convert.ToInt32(myKey.SEL_ITEM);
                            var myTag = myGroup.Tags.LastOrDefault(a => a.Tag.Equals(myKey.KEY));
                            if (myTag == null)
                            {
                                myTag = new OrderTag();
                                myGroup.Tags.Add(myTag);
                            }

                            myTag.Name = myKey.KEY_DESC.LastOrDefault(a => a.LANG_KEY.Equals("EN"))?.DESC_KEY;
                            myTag.Tag = myKey.KEY;
                            myTag.AlternateName =
                                myKey.KEY;

                            var myMap = myGroup.Maps.LastOrDefault(a => a.MenuItemId.Equals(serverItem.Id));
                            if (myMap != null) continue;
                            myMap = new OrderTagMap {MenuItemId = serverItem.Id};
                            myGroup.Maps.Add(myMap);


                        }

                        if (groupAdd)
                        {
                            uow.OrderTagGroups.Add(myGroup);
                            uow.Complete();
                        }
                        else
                        {

                            uow.OrderTagGroups.Update(myGroup);
                            var allTagList = myGroup.Tags.Select(a => a.Tag);
                            List<string> diffList =  allTagList.Except(newTags).ToList();

                            foreach (var diff in diffList)
                            {
                                var myLastCode = myGroup.Tags.LastOrDefault(a => a.Tag.Equals(diff));
                                if (myLastCode != null)
                                {
                                    myGroup.Tags.Remove(myLastCode);
                                    uow.OrderTags.Remove(myLastCode);
                                }
                            }
                            uow.OrderTagGroups.Update(myGroup);
                            uow.Complete();
                        }

                        myGroup.MaxSelectedItems = selectITem == 2 ? myGroup.Tags.Count() : 1;
                        uow.OrderTagGroups.Update(myGroup);
                        uow.Complete();

                        foreach (var optimizationKeyDesc in myOption.SelectMany(a=>a.KEY_DESC))
                        {
                            LanguageExecutor.PushLanguage(optimizationKeyDesc.LANG_KEY, lgo.MATOPTION.MAT_NO+"_"+optimizationKeyDesc.OPT_DESC,
                                LanguageDescriptionType.OrderTagGroup, myGroup.Id, uow, appConfig);
                        }

                      
                        foreach (var myKey in myOption)
                        {
                            var myTag = myGroup.Tags.LastOrDefault(a => a.Tag.Equals(myKey.KEY));
                            if (myTag != null && myTag.Id > 0)
                            {
                                foreach (var myLangDesc in myKey.KEY_DESC)
                                {
                                    LanguageExecutor.PushLanguage(myLangDesc.LANG_KEY, myLangDesc.DESC_KEY,
                                        LanguageDescriptionType.OrderTag, myTag.Id, uow, appConfig);
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception e)
            {
                var myDate = DateTime.Now;
                uow.ExternalLogs.Add(new ExternalLog
                {
                    AuditType = (int) ExternalLogType.OrderTagGroupInbound,
                    CreatorUserId = appConfig.AppUserId,
                    TenantId = appConfig.Tenant,
                    CreationTime = myDate,
                    LogTime = myDate,
                    ExternalLogTrunc = myDate.Date,
                    LogData = e.Message,
                    LogDescription = FlattenException(e)
                });
                uow.Complete();
                reason = e.Message;
            }

            return reason;
        }

        private static string FlattenException(Exception exception)
        {
            var stringBuilder = new StringBuilder();

            while (exception != null)
            {
                stringBuilder.AppendLine(exception.Message);
                stringBuilder.AppendLine(exception.StackTrace);

                exception = exception.InnerException;
            }

            return stringBuilder.ToString();
        }
    }
}