﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using Abp.Configuration;
using DinePlan.DineConnect.Connect.Audit;
using DinePlan.DineConnect.SerialHelpers;
using PttDatabaseCommon.Data;
using PttInboundConsole.Data;
using PttInboundConsole.Process.Executor;
using PttInboundConsole.Process.Model;

namespace PttInboundConsole.Process
{
    public class FileProcessor
    {
        private static readonly Dictionary<string, PlantMasterSend_MT> AllLocations =
            new Dictionary<string, PlantMasterSend_MT>();

        private static readonly Dictionary<string, PLANTGROUPDOWNLOAD> AllLocationGroups =
            new Dictionary<string, PLANTGROUPDOWNLOAD>();

        private static readonly Dictionary<string, MaterialGroupHierarchySend_MT> AllMaterialGroups = new Dictionary<string, MaterialGroupHierarchySend_MT>();

        private static readonly Dictionary<string, MaterialDataSend_MT> AllMaterials =
            new Dictionary<string, MaterialDataSend_MT>();

        private static readonly Dictionary<string, ComboMasterDataSend_MT> AllComboes =
            new Dictionary<string, ComboMasterDataSend_MT>();

        private static readonly Dictionary<string, OptionProductMasterSend_MT> AllOrderTagGroups =
            new Dictionary<string, OptionProductMasterSend_MT>();

        public static string Process(string fileName, AppConfig config, IUnitOfWork unitOfWork,
            PttInboundDbContext context)
        {
            var reason = "";

            try
            {
                AllLocations.Clear();
                AllLocationGroups.Clear();
                AllMaterialGroups.Clear();
                AllMaterials.Clear();
                AllComboes.Clear();
                AllOrderTagGroups.Clear();


                var tempFileName = Path.GetFileNameWithoutExtension(fileName);
                if (string.IsNullOrEmpty(tempFileName))
                {

                    throw new Exception("File Name does not exist");
                }

                if (tempFileName.ToUpper().Contains("ZS4PIRTI007") &&
                    (config.ConsoleConfig == 0 || config.ConsoleConfig == 1))
                {
                    if (ProcessSetting(unitOfWork, "ZS4PIRTI007", config, fileName, out reason))
                    {
                        ProcessLocationGroups(fileName, out reason);
                        if (!string.IsNullOrEmpty(reason))
                        {
                            throw new Exception("ZS4PIRTI007 : " + reason);
                        }

                        if (AllLocationGroups.Any())
                            reason = LocationGroupExecutor.Run(unitOfWork, config, AllLocationGroups, context);
                    }
                    if (!string.IsNullOrEmpty(reason))
                    {
                        throw new Exception("ZS4PIRTI007 : " + reason);
                    }
                }
                else if (tempFileName.ToUpper().Contains("ZS4PIRTI003") &&
                         (config.ConsoleConfig == 0 || config.ConsoleConfig == 2))
                {
                    if (ProcessSetting(unitOfWork, "ZS4PIRTI003", config, fileName, out reason))
                    {
                        ProcessLocations(fileName, out reason);
                        if (!string.IsNullOrEmpty(reason))
                        {
                            throw new Exception("ZS4PIRTI003 : " + reason);
                        }

                        if (AllLocations.Any())
                            reason = LocationExecutor.Run(unitOfWork, config, AllLocations, context);
                    }
                    if (!string.IsNullOrEmpty(reason))
                    {
                        throw new Exception("ZS4PIRTI003 : " + reason);
                    }
                }
                else if (tempFileName.ToUpper().Contains("WPDWGR") &&
                         (config.ConsoleConfig == 0 || config.ConsoleConfig == 3))
                {
                    if (ProcessSetting(unitOfWork, "WPDWGR", config, fileName, out reason))
                    {
                        ProcessMaterialGroups(fileName, out reason);
                        if (!string.IsNullOrEmpty(reason))
                        {
                            throw new Exception("WPDWGR : " + reason);
                        }

                        if (AllMaterialGroups.Any())
                            reason = MaterialGroupExecutor.Run(unitOfWork, config, AllMaterialGroups, context);
                    }
                    if (!string.IsNullOrEmpty(reason))
                    {
                        throw new Exception("WPDWGR : " + reason);
                    }
                }
                else if (tempFileName.ToUpper().Contains("WBBDLD") &&
                         (config.ConsoleConfig == 0 || config.ConsoleConfig == 4))
                {
                    if (ProcessSetting(unitOfWork, "WBBDLD", config, fileName, out reason))
                    {
                        ProcessMaterials(fileName, out reason);
                        if (!string.IsNullOrEmpty(reason))
                        {
                            throw new Exception("WBBDLD: " + reason);
                        }

                        if (AllMaterials.Any())
                            reason = MaterialExecutor.Run(unitOfWork, config, AllMaterials, context);
                    }
                    if (!string.IsNullOrEmpty(reason))
                    {
                        throw new Exception("WBBDLD : " + reason);
                    }
                }
                else if (tempFileName.ToUpper().Contains("ZS4PIRTI016") &&
                         (config.ConsoleConfig == 0 || config.ConsoleConfig == 5))
                {
                    if (ProcessSetting(unitOfWork, "ZS4PIRTI016", config, fileName, out reason))
                    {
                        ProcessComboes(fileName, out reason);
                        if (!string.IsNullOrEmpty(reason))
                        {
                            throw new Exception("ZS4PIRTI016: " + reason);
                        }

                        if (AllComboes.Any()) reason = ComboExecutor.Run(unitOfWork, config, AllComboes, context);
                    }
                    if (!string.IsNullOrEmpty(reason))
                    {
                        throw new Exception("ZS4PIRTI016 : " + reason);
                    }
                }
                else if (tempFileName.ToUpper().Contains("ZS4PIRTI010") &&
                         (config.ConsoleConfig == 0 || config.ConsoleConfig == 6))
                {
                    if (ProcessSetting(unitOfWork, "ZS4PIRTI010", config, fileName,out reason))
                    {
                        ProcessOrderTagGroups(fileName, out reason);
                        if (!string.IsNullOrEmpty(reason))
                        {
                            throw new Exception("ZS4PIRTI010: " + reason);
                        }

                        if (AllOrderTagGroups.Any())
                            reason = OrderTagGroupExecutor.Run(unitOfWork, config, AllOrderTagGroups, context);
                    }
                    if (!string.IsNullOrEmpty(reason))
                    {
                        throw new Exception("ZS4PIRTI010 : " + reason);
                    }
                }
                else
                {
                    reason = config.ConsoleConfig == 0 ? "Wrong File" : "@";
                }

                if (!string.IsNullOrEmpty(reason))
                {
                    throw new Exception(reason);
                }
            }
            catch (Exception e)
            {

                var myDate = DateTime.Now;
                unitOfWork.ExternalLogs.Add(new ExternalLog
                {
                    AuditType = (int) ExternalLogType.InboundFile,
                    CreatorUserId = config.AppUserId,
                    TenantId = config.Tenant,
                    CreationTime = myDate,
                    LogTime = myDate,
                    ExternalLogTrunc = myDate.Date,
                    LogData = e.Message,
                    LogDescription = FlattenException(e)
                });
                unitOfWork.Complete();
                reason = e.Message;
            }

            return reason;
        }

        private static string FlattenException(Exception exception)
        {
            var stringBuilder = new StringBuilder();

            while (exception != null)
            {
                stringBuilder.AppendLine(exception.Message);
                stringBuilder.AppendLine(exception.StackTrace);

                exception = exception.InnerException;
            }

            return stringBuilder.ToString();
        }
        private static bool ProcessSetting(IUnitOfWork uow, string filePrefix, AppConfig config, string file, out string reason)
        {
            reason = "";
            try
            {
                var mySetting = uow.Settings.Find(a => a.Name.Equals(filePrefix) && a.TenantId == config.Tenant)
                    .LastOrDefault();

                var myName = Path.GetFileNameWithoutExtension(file);
                var mySplitString = myName.Split('_');
                if (mySplitString.Length > 1)
                {
                    var myValue = mySplitString[1];
                    long convertedValue = long.Parse(myValue);
                    if (mySetting == null)
                    {
                        uow.Settings.Add(new Setting()
                        {
                            Name = filePrefix,
                            Value = myValue,
                            TenantId = config.Tenant,
                            CreatorUserId = config.AppUserId
                        });
                        uow.Complete();
                        return true;
                    }
                    long mySettingValue = long.Parse(mySetting.Value);
                    if (convertedValue <= mySettingValue)
                    {
                        throw new Exception("New file is already applied " + file);
                    }
                    mySetting.Value = convertedValue.ToString();
                    uow.Settings.Update(mySetting);
                    uow.Complete();
                    return true;

                }
                throw new Exception("New file is already applied " + file);
            }
            catch (Exception e)
            {
                reason = e.Message;
                return false;
            }
        }


        #region Locations

        private static void ProcessLocations(string file, out string reason)
        {
            try
            {
                reason = "";
                var ser = new Serializer();

                var myPlantDownload = ser.DeserializeToObject<PlantMasterSend_MT>(file);
                if (myPlantDownload != null)
                {
                    if (CheckLocationModel(myPlantDownload, out reason))
                    {
                        if (!AllLocations.ContainsKey(myPlantDownload.PLANT.First().PLANTCODE))
                            AllLocations.Add(myPlantDownload.PLANT.First().PLANTCODE, myPlantDownload);
                    }
                    else
                    {
                        throw new Exception(reason);
                    }
                }
                else
                {
                    reason = "Wrong Contents";
                }
            }
            catch (Exception e)
            {
                reason = e.Message;
                throw;
            }
        }

        private static bool CheckLocationModel(PlantMasterSend_MT model, out string reason)
        {
            reason = "";

            string[] locationActionCodes = new string[]{"INIT","MODI","DELE"};

            if (model?.PLANT != null && model.PLANT.Any())
            {
                foreach (var plant in model.PLANT)
                {
                    if (string.IsNullOrEmpty(plant.PLANTCODE))
                    {
                        reason = "Store Code is Empty";
                        return false;
                    }

                    if (string.IsNullOrEmpty(plant.CLASS))
                    {
                        reason = "Organization Code is Empty";
                        return false;
                    }

                    if (string.IsNullOrEmpty(plant.ACTIONCODE))
                    {
                        reason = "Action Code is Empty";
                        return false;
                    }

                    if (!string.IsNullOrEmpty(plant.ACTIONCODE) && !locationActionCodes.Contains(plant.ACTIONCODE))
                    {
                        reason = "InCorrect Action Code has been sent";
                        return false;
                    }

                }
            }
            else
            {
                reason = "Objects are Null";
                return false;
            }

            return true;
        }

        #endregion

        #region Location Groups


        private static void ProcessLocationGroups(string file, out string reason)
        {
            try
            {
               
                reason = "";

                string[] locationActionCodes = new string[]{"INIT","MODI","DELE"};


                var ser = new Serializer();

                var myPlantDownload = ser.DeserializeToObject<PLANTGROUPDOWNLOAD>(file);
                if (myPlantDownload != null)
                {
                    if (CheckLocationGroupModel(myPlantDownload, out reason))
                    {
                        if (!AllLocationGroups.ContainsKey(myPlantDownload.PLANTGROUP.PLANTGROUPCODE))
                            AllLocationGroups.Add(myPlantDownload.PLANTGROUP.PLANTGROUPCODE, myPlantDownload);
                    }
                    else
                       throw new Exception(reason);
                }
                else
                {
                    reason = "Wrong Contents";
                }
            }
            catch (Exception e)
            {
                reason = e.Message;
                throw;
            }
        }

        private static bool CheckLocationGroupModel(PLANTGROUPDOWNLOAD model, out string reason)
        {
            reason = "";
            string[] locationActionCodes = new string[]{"INIT","MODI","DELE"};

            if (string.IsNullOrEmpty(model.PLANTGROUP.PLANTGROUPCODE))
            {
                reason = "Group Code is Empty";
                return false;
            }

            if (string.IsNullOrEmpty(model.PLANTGROUP.ACTIONCODE))
            {
                reason = "Action Code is Empty";
                return false;
            }

            if (!string.IsNullOrEmpty(model.PLANTGROUP.ACTIONCODE) && !locationActionCodes.Contains(model.PLANTGROUP.ACTIONCODE))
            {
                reason = "InCorrect Action Code has been sent";
                return false;
            }

         

            return true;
        }

        #endregion

        #region Category

        private static void ProcessMaterialGroups(string file, out string reason)
        {
            try
            {
                reason = "";
                var ser = new Serializer();

                var myPlantDownload = ser.DeserializeToObject<MaterialGroupHierarchySend_MT>(file);
                if (myPlantDownload != null)
                {
                    if (CheckMaterialGroupModel(myPlantDownload, out reason))
                    {
                        var guid = Guid.NewGuid();
                        if (!AllMaterialGroups.ContainsKey(guid.ToString()))
                            AllMaterialGroups.Add(guid.ToString(), myPlantDownload);
                    }
                    else
                    {
                        throw new Exception(reason);
                    }
                }
                else
                {
                    reason = "Wrong Contents";
                }
            }
            catch (Exception e)
            {
                reason = e.Message;
                throw;
            }
        }

        private static bool CheckMaterialGroupModel(MaterialGroupHierarchySend_MT groupModel, out string reason)
        {
            reason = "";
            string[] locationActionCodes = new string[]{"INIT","MODI","DELE"};

            foreach (var model in groupModel.WPDWGR01)
            {
                if (string.IsNullOrEmpty(model.BRAND))
                {
                    reason = "Brand Code is Empty";
                    return false;
                }

                if (string.IsNullOrEmpty(model.IND))
                {
                    reason = "Action Code is Empty";
                    return false;
                }

                if (!string.IsNullOrEmpty(model.IND) && !locationActionCodes.Contains(model.IND))
                {

                    reason = "InCorrect Action Code has been sent";
                    return false;
                }
            }
            return true;
          
        }

        #endregion

        #region Materials

        private static void ProcessMaterials(string file, out string reason)
        {
            try
            {
                reason = "";
                var ser = new Serializer();

                var myPlantDownload = ser.DeserializeToObject<MaterialDataSend_MT>(file);
                if (myPlantDownload != null)
                {
                    if (CheckMaterialModel(myPlantDownload, out reason))
                    {
                        var guid = Guid.NewGuid();
                        if (!AllMaterials.ContainsKey(guid.ToString()))
                            AllMaterials.Add(guid.ToString(), myPlantDownload);
                    }
                    else
                    {
                        throw new Exception(reason);
                    }
                }
                else
                {
                    reason = "Wrong Contents";
                }
            }
            catch (Exception e)
            {
                reason = e.Message;
                throw;
            }
        }

        private static bool CheckMaterialModel(MaterialDataSend_MT materialModel, out string reason)
        {
            reason = "";
            string[] locationActionCodes = new string[]{"INIT","DIRC","MODI","DELE"};

            foreach (var model in materialModel.MATERIALDOWNLOAD)
            {
                if (string.IsNullOrEmpty(model.ACTION))
                {
                    reason = "Action Code is Empty";
                    return false;
                }

                if (!string.IsNullOrEmpty(model.ACTION) && !locationActionCodes.Contains(model.ACTION))
                {
                    reason = "InCorrect Action Code has been sent";
                    return false;
                }
            }
            return true;
        }

        #endregion

        #region Comboes

        private static void ProcessComboes(string file, out string reason)
        {
            try
            {
                reason = "";
                var ser = new Serializer();

                var myPlantDownload = ser.DeserializeToObject<ComboMasterDataSend_MT>(file);
                if (myPlantDownload != null)
                {
                    if (CheckComboes(myPlantDownload, out reason))
                    {
                        var guid = Guid.NewGuid();
                        if (!AllComboes.ContainsKey(guid.ToString())) AllComboes.Add(guid.ToString(), myPlantDownload);
                    }
                    else
                    {
                        throw new Exception(reason);
                    }
                }
                else
                {
                    reason = "Wrong Contents";
                }
            }
            catch (Exception e)
            {
                reason = e.Message;
                throw;
            }
        }

        private static bool CheckComboes(ComboMasterDataSend_MT model, out string reason)
        {
            reason = "";
            string[] locationActionCodes = new string[]{"1","2"};
            string[] comboInitCodes = new string[]{"INIT","MODI"};

            try
            {
                if (string.IsNullOrEmpty(model.COMBODOWNLOAD.COMBOMAT.VALID_TO))
                {
                    throw new Exception("Valid To is empty");

                }

                

                if (string.IsNullOrEmpty(model.COMBODOWNLOAD.COMBOMAT.STATUS))
                {
                    throw new Exception("Not a Valid Status");

                }

                if (!string.IsNullOrEmpty(model.COMBODOWNLOAD.COMBOMAT.STATUS) && !locationActionCodes.Contains(model.COMBODOWNLOAD.COMBOMAT.STATUS))
                {

                    reason = "InCorrect Status Code has been sent";
                    return false;
                }

                if (string.IsNullOrEmpty(model.COMBODOWNLOAD.COMBOMAT.ACTIONCODE))
                {
                    throw new Exception("Not a Valid Status");

                }

                if (!string.IsNullOrEmpty(model.COMBODOWNLOAD.COMBOMAT.ACTIONCODE) && !comboInitCodes.Contains(model.COMBODOWNLOAD.COMBOMAT.ACTIONCODE))
                {

                    reason = "InCorrect Action Code has been sent";
                    return false;
                }

                if (!string.IsNullOrEmpty(model.COMBODOWNLOAD.COMBOMAT.VALID_TO) )
                {
                    if (!model.COMBODOWNLOAD.COMBOMAT.VALID_TO.EndsWith("9999"))
                    {
                        DateTime dt =
                            DateTime.ParseExact(model.COMBODOWNLOAD.COMBOMAT.VALID_TO, "dd.MM.yyyy",
                                CultureInfo.InvariantCulture);

                        dt = dt.AddDays(1).AddSeconds(-1);
                        if (dt >= DateTime.Now)
                            return true;
                        throw new Exception("Valid To is empty");
                    }

                    return true;

                }

            }
            catch (Exception e)
            {
                reason = e.Message;
                return false;
            }
            reason = "Wrong File Contents";
            return false;
        }

        #endregion

        #region OrderTagGroups

        private static void ProcessOrderTagGroups(string file, out string reason)
        {
            try
            {
                reason = "";
                var ser = new Serializer();

                var myPlantDownload = ser.DeserializeToObject<OptionProductMasterSend_MT>(file);
                if (myPlantDownload != null)
                {
                    if (CheckOrderTagGroups(myPlantDownload, out reason))
                    {
                        var guid = Guid.NewGuid();
                        if (!AllOrderTagGroups.ContainsKey(guid.ToString()))
                            AllOrderTagGroups.Add(guid.ToString(), myPlantDownload);
                    }
                    else
                    {
                        throw new Exception(reason);
                    }
                }
                else
                {
                    reason = "Wrong Contents";
                }
            }
            catch (Exception e)
            {
                reason = e.Message;
                throw;
            }
        }

        private static bool CheckOrderTagGroups(OptionProductMasterSend_MT model, out string reason)
        {
            reason = "";
            string[] locationActionCodes = new string[]{"1","2"};
            string[] comboInitCodes = new string[]{"INIT","MODI"};

            try
            {
               

                if (string.IsNullOrEmpty(model.MATOPTION.STATUS))
                {
                    throw new Exception("Not a Valid Status");

                }

                if (!string.IsNullOrEmpty(model.MATOPTION.STATUS) && !locationActionCodes.Contains(model.MATOPTION.STATUS))
                {

                    reason = "InCorrect Status Code has been sent";
                    return false;
                }

                if (string.IsNullOrEmpty(model.MATOPTION.ACTIONCODE))
                {
                    throw new Exception("Not a Valid Status");

                }

                if (!string.IsNullOrEmpty(model.MATOPTION.ACTIONCODE) && !comboInitCodes.Contains(model.MATOPTION.ACTIONCODE))
                {

                    reason = "InCorrect Action Code has been sent";
                    return false;
                }
                if (string.IsNullOrEmpty(model.MATOPTION.VALID_TO))
                {
                    throw new Exception("Valid To is empty");

                }
                if (!string.IsNullOrEmpty(model.MATOPTION.VALID_TO))
                {
                    if (!model.MATOPTION.VALID_TO.EndsWith("9999"))
                    {
                        DateTime dt =
                            DateTime.ParseExact(model.MATOPTION.VALID_TO, "dd.MM.yyyy", CultureInfo.InvariantCulture);

                        dt = dt.AddDays(1).AddSeconds(-1);

                        if (dt >= DateTime.Now)
                            return true;
                        throw new Exception("Valid To is empty");
                    }

                    return true;

                }

               
            }
            catch (Exception e)
            {
                reason = e.Message;
                return false;
            }

            reason = "Wrong File Contents";
            return false;
        }


    }

        #endregion
}
