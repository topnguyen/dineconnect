﻿namespace PttInboundConsole
{
    public class AppConfig
    {
        public string InitialFolder { get; set; }
        public string ProcessingFolder { get; set; }
        public string MinioServerUrl {get;set;}
        public string MinioApiKey {get;set;}
        public string MinioApiSecret {get;set;}
        public string MinioInitialBucket {get;set;}
        public string MinioErrorBucket {get;set;}
        public string MinioProcessedBucket {get;set;}


        public string FilePattern { get; set; }
        public int Tenant { get; set; }
        public int AppUserId { get; set; }

        public int ConsoleConfig { get; set; }

    }
}