﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Core.Logging;
using Castle.Windsor;
using DinePlan.DineConnect.Connect.Audit;
using Minio;
using Minio.DataModel;
using Minio.Exceptions;
using PttDatabaseCommon.Data;
using PttInboundConsole.Data;
using PttInboundConsole.Process;

namespace PttInboundConsole
{
    public class CoreService
    {
        public const string DateTimeFileFormat = "yyyyMMddHHmmss";
        private readonly AppConfig _config;
        private readonly IWindsorContainer _container;
        private readonly PttInboundDbContext _dbContext;
        private readonly MinioClient _minioClient;


        public CoreService(
            IWindsorContainer container,
            AppConfig config, PttInboundDbContext dbContext)
        {
            _container = container;
            _config = config;
            Logger = NullLogger.Instance;
            _dbContext = dbContext;

            _minioClient = new MinioClient
                (_config.MinioServerUrl, _config.MinioApiKey, _config.MinioApiSecret);
        }

        public ILogger Logger { get; set; }


        public  void Start()
        {
            try
            {

                Logger.Info("Job Starts");

                var myInitialBucket = _minioClient.BucketExistsAsync(_config.MinioInitialBucket).Result;
                if (!myInitialBucket)
                {
                    throw new Exception("Initial Bucket is not available : " + _config.MinioInitialBucket);
                }

                var myProcessBucket = _minioClient.BucketExistsAsync(_config.MinioProcessedBucket).Result;
                if (!myProcessBucket)
                {
                    throw new Exception("Processed Bucket is not available : " + _config.MinioProcessedBucket);
                }
                var myErroBucket = _minioClient.BucketExistsAsync(_config.MinioErrorBucket).Result;
                if (!myErroBucket)
                {
                    throw new Exception("Processed Bucket is not available : " + _config.MinioErrorBucket);
                }
             

                var allCollStr = ListFiles(_minioClient, _config.MinioInitialBucket, "", true);

                if (allCollStr!=null && allCollStr.Any())
                {
                    var inputFiles = GetFiles(allCollStr);
                    Logger.Info($"Processing {inputFiles.Count} files.");

                    var fileName = "";
                    foreach (var fileInfo in inputFiles.OrderBy(a => a.Name))
                    {
                        var isSucceed = true;
                        try
                        {
                            Logger.Info($"Processing {fileInfo.Name}");
                            fileName = MoveToProcessingHolder(fileInfo);
                            using (var uow = _container.Resolve<IUnitOfWork>())
                            {
                                var reason = FileProcessor.Process(fileName, _config, uow, _dbContext);
                                if (!string.IsNullOrEmpty(reason) && !reason.Equals("@"))
                                {
                                    throw new Exception(reason);
                                }

                                if (reason != null && reason.Equals("@"))
                                {
                                    isSucceed = false;
                                    MoveToInitialFolder(fileInfo);
                                }

                                uow.Complete();
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error($"Error=true;Filename={fileInfo.Name};{ex.Message}", ex);
                            using (var unitOfWork = _container.Resolve<IUnitOfWork>())
                            {
                                var myDate = DateTime.Now;
                                unitOfWork.ExternalLogs.Add(new ExternalLog
                                {
                                    AuditType = (int) ExternalLogType.InboundFile,
                                    CreatorUserId = _config.AppUserId,
                                    TenantId = _config.Tenant,
                                    CreationTime = myDate,
                                    LogTime = myDate,
                                    ExternalLogTrunc = myDate.Date,
                                    LogData = fileName,
                                    LogDescription = FlattenException(ex)
                                });
                                unitOfWork.Complete();
                            }

                            isSucceed = false;

                            MoveToBucket(new FileInfo(fileName), _config.MinioErrorBucket);

                        }
                        finally
                        {
                            if (isSucceed)
                            {
                                MoveToBucket(new FileInfo(fileName), _config.MinioProcessedBucket);
                            }
                        }
                    }
                }

                CleaningUpFilesInInitialAndProcessing();

                Logger.Info("Job Ends");
            }
            catch (Exception ex)
            {
                using (var unitOfWork = _container.Resolve<IUnitOfWork>())
                {
                    var myDate = DateTime.Now;
                    unitOfWork.ExternalLogs.Add(new ExternalLog
                    {
                        AuditType = (int) ExternalLogType.InboundFile,
                        CreatorUserId = _config.AppUserId,
                        TenantId = _config.Tenant,
                        CreationTime = myDate,
                        LogTime = myDate,
                        ExternalLogTrunc = myDate.Date,
                        LogData = ex.Message,
                        LogDescription =FlattenException(ex)
                    });
                    unitOfWork.Complete();
                }

                Logger.Error(FlattenException(ex));
                Console.WriteLine(ex);
            }


        }
        public static string FlattenException(Exception exception)
        {
            var stringBuilder = new StringBuilder();

            while (exception != null)
            {
                stringBuilder.AppendLine(exception.Message);
                stringBuilder.AppendLine(exception.StackTrace);

                exception = exception.InnerException;
            }

            return stringBuilder.ToString();
        }
        private void CleaningUpFilesInInitialAndProcessing()
        {

            List<string> excluded = new List<string>() {_config.FilePattern};
            DirectoryInfo dirInfo = new DirectoryInfo(_config.InitialFolder);

            foreach (var file in dirInfo.EnumerateFiles().Where(x => !excluded.Contains(x.Extension)))
            {
                if(File.Exists(file.FullName))
                    File.Delete(file.FullName);
            }


            dirInfo = new DirectoryInfo(_config.ProcessingFolder);

            foreach (var file in dirInfo.EnumerateFiles().Where(x => !excluded.Contains(x.Extension)))
            {
                if(File.Exists(file.FullName))
                    File.Delete(file.FullName);
            }
           
        }

        public ICollection<string> ListFiles(Minio.MinioClient minio,
            string bucketName,
            string prefix = null,
            bool recursive = true)
        {
            try
            {

                List<string> bucketKeys = new List<string>();
                IObservable<Item> observable = minio.ListObjectsAsync(bucketName, prefix, recursive);
                IDisposable subscription = observable.Subscribe
                (
                    item => bucketKeys.Add(item.Key),
                    ex => throw ex
                );
                observable.Wait();

                return bucketKeys;
            }
            
            catch (Exception exception)
            {
                Logger.Error($"Error=true;", exception);

                using (var unitOfWork = _container.Resolve<IUnitOfWork>())
                {
                    var myDate = DateTime.Now;
                    unitOfWork.ExternalLogs.Add(new ExternalLog
                    {

                        AuditType = (int) ExternalLogType.MinioError,
                        CreatorUserId = _config.AppUserId,
                        TenantId = _config.Tenant,
                        CreationTime = myDate,
                        LogTime = myDate,
                        ExternalLogTrunc = myDate.Date,
                        LogData = exception.Message,
                        LogDescription = FlattenException(exception)
                    });
                    unitOfWork.Complete();
                }
            }

            return null;
        }
       

        private List<FileInfo> GetFiles(ICollection<string> allCollStr)
        {
            try
            {
                List<FileInfo> fileInfo = new List<FileInfo>();

                if (allCollStr.Any())
                {
                    foreach (var myCollStr in allCollStr)
                    {
                        string filePath = Path.Combine(_config.InitialFolder, myCollStr);

                        var ms = new FileStream(filePath,FileMode.Create);
                        _minioClient.GetObjectAsync(_config.MinioInitialBucket, myCollStr, stream =>
                        {
                            stream.CopyTo(ms);
                            ms.Position = 0;
                        }).Wait();

                        ms.Close();
                        _minioClient.RemoveObjectAsync(_config.MinioInitialBucket, myCollStr).Wait();
                    }
                    return Directory.EnumerateFiles(_config.InitialFolder, _config.FilePattern)
                        .Select(file => new FileInfo(file))
                        .ToList();
                }

                return fileInfo;

            }
            catch (MinioException exception)
            {
                Logger.Error($"Error=true;", exception);

                using (var unitOfWork = _container.Resolve<IUnitOfWork>())
                {
                    var myDate = DateTime.Now;
                    unitOfWork.ExternalLogs.Add(new ExternalLog
                    {
                        AuditType = (int) ExternalLogType.MinioError,
                        CreatorUserId = _config.AppUserId,
                        TenantId = _config.Tenant,
                        CreationTime = myDate,
                        LogTime = myDate,
                        ExternalLogTrunc = myDate.Date,
                        LogData = exception.Message,
                        LogDescription = FlattenException(exception)
                    });
                    unitOfWork.Complete();
                }
            }

            return null;

        }

     

        private string MoveToInitialFolder(FileInfo fileInfo)
        {
            var initialFolderFile = GetSafeFileName(_config.InitialFolder, fileInfo);
            var processingFolderFile = GetSafeFileName(_config.ProcessingFolder, fileInfo);

            if (File.Exists(initialFolderFile))
                File.Delete(processingFolderFile);
            else
                File.Move(processingFolderFile, initialFolderFile);
            return processingFolderFile;
        }

        private string MoveToProcessingHolder(FileInfo fileInfo)
        {
            var initialFolderFile = GetSafeFileName(_config.InitialFolder, fileInfo);
            var processingFolderFile = GetSafeFileName(_config.ProcessingFolder, fileInfo);

            if (File.Exists(processingFolderFile))
                File.Delete(initialFolderFile);
            else
                File.Move(initialFolderFile, processingFolderFile);
            return processingFolderFile;
        }

        

        private void MoveToBucket(FileInfo fileInfo, string bucketName)
        {
            var processingFile = GetSafeFileName(_config.ProcessingFolder, fileInfo,true);

            var nameWithoutExtenstion = Path.GetFileName(processingFile);
            _minioClient.PutObjectAsync(bucketName, nameWithoutExtenstion, fileInfo.FullName).Wait();

            if (File.Exists(fileInfo.FullName))
                File.Delete(fileInfo.FullName);

        }

        private string GetSafeFileName(string path, FileInfo fi, bool appendDate = false)
        {
            var extension = fi.Extension;
            var nameWithoutExtenstion = Path.GetFileNameWithoutExtension(fi.FullName);

            if (appendDate)
                nameWithoutExtenstion =
                    nameWithoutExtenstion + "_" + DateTime.Now.ToString(DateTimeFileFormat) + extension;
            else
                nameWithoutExtenstion = nameWithoutExtenstion + extension;


            var safePath = path;
            safePath = !path.EndsWith("\\")
                ? $"{safePath}\\{nameWithoutExtenstion}"
                : $"{safePath}{nameWithoutExtenstion}";
            return safePath;
        }
    }
}