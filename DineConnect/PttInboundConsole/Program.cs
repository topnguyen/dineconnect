﻿using System;
using System.CodeDom;
using System.Configuration;
using System.IO;
using Castle.Core.Logging;
using Castle.Facilities.Logging;
using Castle.MicroKernel.Registration;
using Castle.Services.Logging.Log4netIntegration;
using Castle.Windsor;
using PttDatabaseCommon.Data;
using PttDatabaseCommon.Data.Repositories;
using PttInboundConsole.Data;
using PttInboundConsole.Data.Repositories;


namespace PttInboundConsole

{
    public class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                var container = new WindsorContainer();
                container.AddFacility<LoggingFacility>(f => f.LogUsing<Log4netFactory>());

                var logger = container.Resolve<ILogger>();

                var appConfig = GetAppConfig(logger);
                if (args != null && args.Length == 1)
                {
                    appConfig.ConsoleConfig = Convert.ToInt32(args[0]);
                }
                container.Register(
                    Component.For<CoreService>().LifestyleSingleton(),
                    Component.For<AppConfig>().Instance(appConfig).LifestyleSingleton(),
                    Component.For<IWindsorContainer>().Instance(container).LifestyleSingleton(),
                    Component.For<PttInboundDbContext>().LifestyleTransient(),
                    Component.For<IUnitOfWork>().ImplementedBy<UnitOfWork>().LifestyleTransient()
                );

                var uow = container.Resolve<CoreService>();
                uow.Start();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
           
        }

        private static AppConfig GetAppConfig(ILogger logger)
        {
            try
            {
                var appConfig = new AppConfig
                {
                    InitialFolder = ConfigurationManager.AppSettings["InitialFolder"],
                    ProcessingFolder = ConfigurationManager.AppSettings["ProcessingFolder"],
    
                    FilePattern = ConfigurationManager.AppSettings["FilePattern"],
                    MinioServerUrl=ConfigurationManager.AppSettings["MinioServerUrl"],
                    MinioApiKey=ConfigurationManager.AppSettings["MinioApiKey"],
                    MinioApiSecret=ConfigurationManager.AppSettings["MinioApiSecret"],
                    MinioInitialBucket=ConfigurationManager.AppSettings["MinioInitialBucket"],
                    MinioErrorBucket=ConfigurationManager.AppSettings["MinioErrorBucket"],
                    MinioProcessedBucket=ConfigurationManager.AppSettings["MinioProcessedBucket"]
                };

                var tenant = ConfigurationManager.AppSettings["TenantId"];
                appConfig.Tenant = int.TryParse(tenant, out var second) ? second : 1;

                var appUserId = ConfigurationManager.AppSettings["AppUserId"];
                appConfig.AppUserId = int.TryParse(appUserId, out var fourth) ? fourth : 1;

                ValidateFolder(logger, appConfig.InitialFolder, "InitialFolder - Available");
                ValidateFolder(logger, appConfig.ProcessingFolder, "ProcessingFolder - Available");
              

                return appConfig;
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                throw;
            }
        }

        private static void ValidateFolder(ILogger logger, string path, string folderName)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            logger.Info($"{folderName}'s information was checked successfully.");
        }
    }
}