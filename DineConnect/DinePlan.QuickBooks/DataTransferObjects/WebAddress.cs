﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickBooksAPI.DataTransferObjects
{
    public class WebAddress
    {
        public string URI { get; set; }
    }
}
