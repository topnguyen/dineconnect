﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickBooksAPI.DataTransferObjects
{
    public class AuthResponse
    {
        public string access_token { get; set; }
        public string tokent_Type { get; set; }
        public int expires_in { get; set; }
        public string refresh_token { get; set; }
        public int x_refresh_token_expires_in { get; set; }
    }
}
