﻿using System.Collections.Generic;
using QuickBooksAPI.DataTransferObjects;

namespace DinePlan.QuickBooks.DataTransferObjects
{
    public class CompanyInfo
    {
        public string SyncToken { get; set; }
        public string domain { get; set; }
        public Address LegalAddr { get; set; }
        public string SupportedLanguages { get; set; }
        public string CompanyName { get; set; }
        public string Country { get; set; }
        public bool sparse { get; set; }
        public string Id { get; set; }
        public WebAddress WebAddr { get; set; }
        public string FiscalYearStartMonth { get; set; }
        public Address CustomerCommunicationAddr { get; set; }
        public PhoneNumber PrimaryPhone { get; set; }
        public string LegalName { get; set; }
        public string CompanyStartDate { get; set; }
        public EmailAddress Email { get; set; }
        public List<NameValueObj> NameValue { get; set; }
        public MetaDataObj MetaData { get; set; }
    }
}
