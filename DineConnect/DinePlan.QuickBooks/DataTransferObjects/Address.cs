﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickBooksAPI.DataTransferObjects
{
    public class Address
    {
        public string City { get; set; }
        public string Country { get; set; }
        public string Line1 { get; set; }
        public string PostalCode { get; set; }
        public string CountrySubDivisionCode { get; set; }
        public string Id { get; set; }
    }
}
