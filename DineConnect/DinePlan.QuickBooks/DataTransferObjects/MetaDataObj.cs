﻿namespace DinePlan.QuickBooks.DataTransferObjects
{
    public class MetaDataObj
    {
        public string CreateTime { get; set; }
        public string LastUpdatedTime { get; set; }
    }
}
