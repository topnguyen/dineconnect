﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickBooksAPI.DataTransferObjects
{
    public class NameValueObj
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
