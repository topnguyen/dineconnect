﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickBooksAPI.DataTransferObjects
{
    public class PhoneNumber
    {
        public string FreeFormNumber { get; set; }
    }
}
