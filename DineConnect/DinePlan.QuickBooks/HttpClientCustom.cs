﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace QuickBooksAPI
{
    public static class HttpClientCustom
    {
        public static async Task<string> CreateHttpPOST(string requestUri, string authorization, string contentType, string postData)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("POST"), requestUri))
                {
                    request.Headers.TryAddWithoutValidation("Authorization", authorization);

                    request.Content = new StringContent(postData, Encoding.UTF8, contentType);

                    using (var response = await httpClient.SendAsync(request))
                    {
                        return await response.Content.ReadAsStringAsync();
                    }
                }
            }
        }

        public static async Task<string> CreateHttpGET(string requestUri, string authorization, string contentType)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("GET"), requestUri))
                {
                    request.Headers.TryAddWithoutValidation("Authorization", authorization);
                    request.Headers.TryAddWithoutValidation("Accept", contentType);

                    using (var response = await httpClient.SendAsync(request))
                    {
                        return await response.Content.ReadAsStringAsync();
                    }
                }
            }
        }

        //public static string CreateHttpGET_old(string requestUri, string authorization, string contentType)
        //{
        //    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        //    var request = (HttpWebRequest)WebRequest.Create(requestUri);
        //    if (authorization != "")
        //        request.Headers["Authorization"] = authorization;
        //    request.Accept = contentType;
        //    request.Method = "GET";
            
        //    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        //    {
        //        return new StreamReader(response.GetResponseStream()).ReadToEnd();
        //    }
        //}

    }
}
