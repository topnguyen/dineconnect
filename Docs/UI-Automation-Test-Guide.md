Author: Ling

Date: Monday, 4 January 2021

## Preface
This document describes how to apply UI automation testing to the **DineConnect** project guide

## Technical Selection
As the code development and logic of the project are complex, testing is needed to verify the correctness of the project, but manual testing will reduce our patience and efficiency, so we will introduce UI automation testing.

The top 4 web automation frameworks on Github are as follows:

* 1. [Selenium](https://github.com/SeleniumHQ/selenium)
* 2. [RobotFramework](https://github.com/robotframework/robotframework)
* 3. [Webdriverio](https://github.com/webdriverio/webdriverio)
* 4. [Puppeteer](https://github.com/puppeteer/puppeteer)

`Why we choose selenium`

Selenium is basically used to automate the testing across various web browsers. It supports various browsers like Chrome, Mozilla, Firefox, Safari, and IE, and you can very easily automate browser testing across these browsers using Selenium WebDriver.

## Development environment

Selenium provides clients in many languages, such as C#, Java, NodeJS, Rust, Python, but we choose Python here because Python is the preferred language officially recommended by Selenium. It is easier to write, fast, and powerful.

`The development environment required by Selenium Python is as follows`

* [Python3](https://www.python.org/downloads/)
    * [Python Selenium](https://pypi.org/project/selenium) (Selenium python client)
    * [Python Termcolor](https://pypi.org/project/termcolor2) (color output of python print)
* [Chrome](https://www.google.com/intl/en/chrome/) (You can also choose other browsers, Chrome is recommended)
* [ChromeDriver](https://chromedriver.chromium.org/downloads) (Test project has integrated ChromeDriver version 87, can automatic control the Chrome version 87)

## Find Test Project

We have created for DineConnect Python Selenium test project, located in

> DineConnect -> Tests -> DinePlan.DineConnect.Tests.UIAutomation

## Run Test Project

Use the following command to run the test

> python RunTest.py

## Project Specification

**Base** folder should be placed some infrastructure. E.g Create Driver, Login

**Specs** folder should be placed test project, 'Spec' at the file name end

Test method names should begin with 'should', accurately express the content of the need to test. 

E.g 'should_create_company_success'

## Selenium Skills

The commonly used techniques about Selenium

* Waiting for the Input element loaded, the input text

    ``` python
    WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'body > div.page-container > div.page-content-wrapper > div > div > div > div > form > div.portlet.light > div.portlet-body > div.tab-container.tabbable-line.ng-isolate-scope > div > div.tab-pane.ng-scope.active > div:nth-child(1) > div > div > input'))).send_keys(code)
    ```

    Positioning element can be used, the xpath, CSS selector, id..., Through chrome (F12) for debugging

* Waiting for the Input element loaded, the click

    ``` python
    WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'body > div.page-container > div.page-content-wrapper > div > div > div > div > form > div.portlet.light > div.portlet-body > div.tab-container.tabbable-line.ng-isolate-scope > div > div.tab-pane.ng-scope.active > div:nth-child(1) > div > div > input'))).click()
    ```

* Floating menu click

    ``` python
    connectNav = WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'body > div.page-container > div.page-sidebar-wrapper.page-sidebar-menu-closed.ng-scope > div > ul > li:nth-child(2) > a')))
    actionChains = ActionChains(driver)
    actionChains.move_to_element(connectNav).perform()
    ```